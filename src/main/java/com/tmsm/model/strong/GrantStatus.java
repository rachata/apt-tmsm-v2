package com.tmsm.model.strong;

public class GrantStatus {
	private int grantId;
	private String grantName;
	
	public int getGrantId() {
		return grantId;
	}
	public void setGrantId(int grantId) {
		this.grantId = grantId;
	}
	public String getGrantName() {
		return grantName;
	}
	public void setGrantName(String grantName) {
		this.grantName = grantName;
	}
}
