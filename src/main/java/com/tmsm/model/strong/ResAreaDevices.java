package com.tmsm.model.strong;

import java.util.List;

public class ResAreaDevices {
	private int areaId;
	private String areaName;
	private Double areaLat;
	private Double areaLng;
	private List<DetailArea> detailAreas;

	public List<DetailArea> getDetailAreas() {
		return detailAreas;
	}

	public void setDetailAreas(List<DetailArea> detailAreas) {
		this.detailAreas = detailAreas;
	}

	public int getAreaId() {
		return areaId;
	}

	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Double getAreaLat() {
		return areaLat;
	}

	public void setAreaLat(Double areaLat) {
		this.areaLat = areaLat;
	}

	public Double getAreaLng() {
		return areaLng;
	}

	public void setAreaLng(Double areaLng) {
		this.areaLng = areaLng;
	}
	
}
