package com.tmsm.model.strong;

import java.util.ArrayList;
import java.util.List;

import com.tmsm.api.model.Status;

public class ListPriority extends Status {
	List<DevicesPriority> priority = new ArrayList<DevicesPriority>();

	public List<DevicesPriority> getPriority() {
		return priority;
	}

	public void setPriority(List<DevicesPriority> priority) {
		this.priority = priority;
	}
	
}
