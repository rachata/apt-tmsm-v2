package com.tmsm.model.strong;

public class Privilege {
	private int privilegeID;
	private String privilegeName;
	
	public int getPrivilegeID() {
		return privilegeID;
	}
	public void setPrivilegeID(int privilegeID) {
		this.privilegeID = privilegeID;
	}
	public String getPrivilegeName() {
		return privilegeName;
	}
	public void setPrivilegeName(String privilegeName) {
		this.privilegeName = privilegeName;
	}

}
