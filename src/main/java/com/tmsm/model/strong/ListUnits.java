package com.tmsm.model.strong;

import java.util.List;

import com.tmsm.api.model.Status;

public class ListUnits extends Status {
	private List<Unit> unit;

	public List<Unit> getUnit() {
		return unit;
	}

	public void setUnit(List<Unit> unit) {
		this.unit = unit;
	}
}
