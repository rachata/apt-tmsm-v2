package com.tmsm.model.strong;

public class DevicesPriority {
	private Integer priorityID;
	private String priorityName;
	
	public Integer getPriorityID() {
		return priorityID;
	}
	public void setPriorityID(Integer priorityID) {
		this.priorityID = priorityID;
	}
	public String getPriorityName() {
		return priorityName;
	}
	public void setPriorityName(String priorityName) {
		this.priorityName = priorityName;
	}
}
