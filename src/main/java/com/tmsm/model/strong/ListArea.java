package com.tmsm.model.strong;

import java.util.List;

import com.tmsm.api.model.Status;

public class ListArea extends Status {
	private List<ResAreaDevices> areaDevices;

	public List<ResAreaDevices> getAreaDevices() {
		return areaDevices;
	}

	public void setAreaDevices(List<ResAreaDevices> areaDevices) {
		this.areaDevices = areaDevices;
	}
	
}
