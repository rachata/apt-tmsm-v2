package com.tmsm.model.strong;

import java.util.List;

import com.tmsm.api.model.Status;

public class ListGrantStatus extends Status {
	private List<GrantStatus> grantStatus;

	public List<GrantStatus> getGrantStatus() {
		return grantStatus;
	}

	public void setGrantStatus(List<GrantStatus> grantStatus) {
		this.grantStatus = grantStatus;
	}
	
}
