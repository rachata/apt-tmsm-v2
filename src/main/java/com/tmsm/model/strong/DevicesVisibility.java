package com.tmsm.model.strong;

public class DevicesVisibility {
	private int visibilityID;
	private String visibilityName;
	
	public int getVisibilityID() {
		return visibilityID;
	}
	public void setVisibilityID(int visibilityID) {
		this.visibilityID = visibilityID;
	}
	public String getVisibilityName() {
		return visibilityName;
	}
	public void setVisibilityName(String visibilityName) {
		this.visibilityName = visibilityName;
	}	
}
