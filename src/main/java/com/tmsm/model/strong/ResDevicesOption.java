package com.tmsm.model.strong;

import java.util.List;

import com.tmsm.api.model.Status;

public class ResDevicesOption extends Status {
	private List<DevicesPriority> priorities;
	private List<DevicesVisibility> visibilities;
	
	
	public List<DevicesPriority> getPriorities() {
		return priorities;
	}
	public void setPriorities(List<DevicesPriority> priorities) {
		this.priorities = priorities;
	}
	public List<DevicesVisibility> getVisibilities() {
		return visibilities;
	}
	public void setVisibilities(List<DevicesVisibility> visibilities) {
		this.visibilities = visibilities;
	}
	
}
