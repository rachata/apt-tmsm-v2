package com.tmsm.model.strong;

import java.util.List;

import com.tmsm.api.model.Status;

public class ThingsType extends Status {

	private List<ListType> listType = null;

	public List<ListType> getThingType() {
		return listType;
	}

	public void setThingType(List<ListType> thingType) {
		this.listType = thingType;
	}
	
}


