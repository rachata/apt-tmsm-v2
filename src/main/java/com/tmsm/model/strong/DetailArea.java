package com.tmsm.model.strong;

public class DetailArea {
	private int dearId;
	private String dearName;
	private String dearImg;
	private int dearArea;
	
	public int getDearId() {
		return dearId;
	}
	public void setDearId(int dearId) {
		this.dearId = dearId;
	}
	public String getDearName() {
		return dearName;
	}
	public void setDearName(String dearName) {
		this.dearName = dearName;
	}
	public String getDearImg() {
		return dearImg;
	}
	public void setDearImg(String dearImg) {
		this.dearImg = dearImg;
	}
	public int getDearArea() {
		return dearArea;
	}
	public void setDearArea(int dearArea) {
		this.dearArea = dearArea;
	}	
}
