package com.tmsm.model.strong;

import java.util.ArrayList;
import java.util.List;

import com.tmsm.api.model.Status;

public class ListStatus extends Status {
	List<DevicesStatus> status = new ArrayList<DevicesStatus>();

	public List<DevicesStatus> getStatus() {
		return status;
	}

	public void setStatus(List<DevicesStatus> status) {
		this.status = status;
	}
}
