package com.tmsm.api;

import java.util.ArrayList;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		
		return new User(username, "$2a$10$gU2xiOKYetICIXUOHCV2JOYIZIKblz0pc2dKsSLxJAU5aDU1ZAIyq",
				new ArrayList<>());

	}

}