package com.tmsm.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.xml.crypto.Data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mysql.jdbc.Statement;
import com.tmsm.api.model.Status;
import com.tmsm.api.model.mobile.AdminArea;
import com.tmsm.api.model.mobile.ReqAdminArea;
import com.tmsm.api.model.mobile.ResLogin;
import com.tmsm.api.utility.Database;

@RestController
@CrossOrigin
public class JwtAuthenticationController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserDetailsService jwtInMemoryUserDetailsService;

	@CrossOrigin
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {

		String sqlCheckLogin = "select usr_id , privilege from user where usr_user = ? and usr_pass = ? ";

		ResultSet result = null;
		PreparedStatement preparedStmt = null;
		Connection con = Database.connectDatabase();

		ResLogin resLogin = new ResLogin();
		try {

			preparedStmt = con.prepareStatement(sqlCheckLogin);
			preparedStmt.setString(1, authenticationRequest.getUsername());
			preparedStmt.setString(2, authenticationRequest.getPassword());

			result = preparedStmt.executeQuery();

			int count = Database.countResultSet(result);

			if (count == 1) {
		
				
				String sql = "select * from area_administrator where area_admin_user = ?";
				preparedStmt = con.prepareStatement(sql);
				preparedStmt.setInt(1, result.getInt(1));
				
				ResultSet res = preparedStmt.executeQuery();

				
				int c = Database.countResultSet(res);
				
				System.out.println("area_administrator " + c);
				if(c>=1) {
					res.beforeFirst();
					List<Integer> data =  new ArrayList<Integer>();
					
					while(res.next()) {
						data.add(res.getInt("area"));		
					}
					
					resLogin.setAdminArea(data);
				}
				
				
				
				
				int privilege = result.getInt("privilege");

				resLogin.setIdUser(result.getInt(1));
				resLogin.setLevelPrivilege(privilege);
				resLogin.setCodeStatus(200);
				resLogin.setNameStatus("Login Success");

				authenticate(authenticationRequest.getUsername(), "tmsm@2019");

				final UserDetails userDetails = jwtInMemoryUserDetailsService
						.loadUserByUsername(authenticationRequest.getUsername());

				String token = jwtTokenUtil.generateToken(userDetails);

				resLogin.setAccessToken(token);

			} else {

				resLogin.setIdUser(0);
				resLogin.setCodeStatus(400);
				resLogin.setNameStatus("Login Unuccess");
			}
		} catch (SQLException e) {
			// TODO: handle exception

			resLogin.setIdUser(0);
			resLogin.setCodeStatus(500);
			resLogin.setNameStatus(e.getMessage());

		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(resLogin);
		}

	}
	
	
	
	@RequestMapping(value = "/register" , method = RequestMethod.POST , consumes = MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE )
	
	public ResponseEntity<Status> register (@RequestBody Register registers){
		
		
		Status status = new Status();
		Connection con = null;
		PreparedStatement preparedStatement  = null;
		try {
			con = Database.connectDatabase();
			String sql = "select * from user where usr_user = ?";
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setString(1, registers.getUsername());
			ResultSet res = preparedStatement.executeQuery();
			int count = Database.countResultSet(res);
			
			if(count == 0) {
				
				 boolean statusUser = false;
				if(registers.getPrivilege()  == 3 || registers.getPrivilege() == 4) {
					
					statusUser = true;
					
				}
				sql = "insert into  user (usr_user , usr_pass , privilege , status) value (? , ? , ? , ?)";
				preparedStatement = con.prepareStatement(sql , Statement.RETURN_GENERATED_KEYS);
				preparedStatement.setString(1, registers.getUsername());
				preparedStatement.setString(2, registers.getPassword());
				preparedStatement.setInt(3, registers.getPrivilege());
				preparedStatement.setBoolean(4, statusUser);
				
				preparedStatement.executeUpdate();
				ResultSet resID =   preparedStatement.getGeneratedKeys();
			
				resID.next();
				
				int id = resID.getInt(1);
				
				sql = "insert into user_detail (user_detail_id , user_detail_fname , user_detail_lname) value (? , ? , ?)";
				preparedStatement = con.prepareStatement(sql);
				preparedStatement.setInt(1, id);
				preparedStatement.setString(2, registers.getfName());
				preparedStatement.setString(3, registers.getlName());
				
				preparedStatement.execute();

				
				if(registers.getPrivilege() == 2) {
					sql = "insert into area_administrator (area_admin_user , area) value (? , ?)";
					preparedStatement  = con.prepareStatement(sql);
					preparedStatement.setInt(1, id);
					preparedStatement.setInt(2, registers.getArea());
					preparedStatement.execute();
					
				}
				status.setCodeStatus(200);
				status.setNameStatus("Register Success");
				
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Username already exists");
			}
			
			
		} catch (SQLException e) {
			
			status.setCodeStatus(500);
			status.setNameStatus("Register Unsuccess");
			
		}finally {
			
			return ResponseEntity.ok().body(status);
		}
		
		
	}

	
	
	
	@RequestMapping(value = "/req-admin-area" , method = RequestMethod.GET)
	@CrossOrigin
	public ResponseEntity<ReqAdminArea> getReqAdminArea(){
		
		ReqAdminArea req = new ReqAdminArea();
		Connection con = null;
		PreparedStatement preparedStatement = null;
		
		try {
			String sql = "select usr_id , concat(user_detail_fname , ' ' ,  user_detail_lname ) as fullname , dear_name , area from user\r\n" + 
					"inner join user_detail on  usr_id = user_detail_id\r\n" + 
					"inner join area_administrator on  usr_id  = area_admin_user\r\n" + 
					"inner join detail_area on dear_id = area\r\n" + 
					"where privilege = 2 and status = false";
			
			con  = Database.connectDatabase();
			preparedStatement = con.prepareStatement(sql);
			ResultSet rs = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(rs);
			
		
			if(count >= 1) {
				
				List<AdminArea> data = new ArrayList<AdminArea>();
				rs.beforeFirst();
				while(rs.next()) {
					AdminArea adminArea = new AdminArea();
					adminArea.setArea(rs.getString("dear_name"));
					adminArea.setFullName(rs.getString("fullname"));
					adminArea.setIdUser(rs.getInt("usr_id"));
					adminArea.setAreaID(rs.getInt("area"));
					
					data.add(adminArea);
				}
				req.setCodeStatus(200);
				req.setData(data);
				req.setNameStatus("Success");
			}else {
				req.setCodeStatus(204);
				req.setNameStatus("No Data");
			}
			
		} catch (Exception e) {
			req.setCodeStatus(500);
			req.setNameStatus(e.getMessage());
		}finally {
			
			return ResponseEntity.ok().body(req);
		}
	}
	
	
	@RequestMapping(value = "/approve/{idUser}/{idArea}" , method = RequestMethod.GET)
	public ResponseEntity<Status> approveUserArea(@PathVariable("idUser") int idUser , @PathVariable("idArea") int idArea){
		
		Status status = new Status();
		Connection con = null;
		PreparedStatement preparedStatement;
		
		try {
			
			String sql = "select detail_device , detail_name from devices_detail\r\n" + 
					"inner join devices on detail_device = device_id \r\n" + 
					"inner join things on device_esp = things_id\r\n" + 
					"where things_area = ?";
			
			con = Database.connectDatabase();
			preparedStatement =con.prepareStatement(sql);
			preparedStatement.setInt(1, idArea);
			
			ResultSet res = preparedStatement.executeQuery();
			int count = Database.countResultSet(res);
			
			
			if(count >= 1) {
				res.beforeFirst();
				while(res.next()) {
					String nameDevice = res.getString("detail_name");
					int idDevice = res.getInt("detail_device");
					
					sql = "insert into permit_user (permit_user , permit_device , permit_status , \r\n" + 
							"permit_privilage , permit_grant_status , permit_name , enabled) value (? , ? ,1 , '111' , 1 ,? , 1)";
					preparedStatement = con.prepareStatement(sql);
					preparedStatement.setInt(1, idUser);
					preparedStatement.setInt(2, idDevice);
					preparedStatement.setString(3, nameDevice);
					
					preparedStatement.execute();
					
					
							
					sql = "update user set status = true where usr_id = ?";
					preparedStatement = con.prepareStatement(sql);
					preparedStatement.setInt(1, idUser);
					preparedStatement.execute();
				}
				
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess No Device");
			}
			
		} catch (Exception e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			return ResponseEntity.ok().body(status);
		}
		
	}
	private void authenticate(String username, String credentials) {
		Objects.requireNonNull(username);
		Objects.requireNonNull(credentials);

		authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, credentials));

	}
}
