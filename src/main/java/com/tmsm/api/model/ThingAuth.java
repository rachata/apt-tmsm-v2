package com.tmsm.api.model;

public class ThingAuth extends Status{
	
	private String thingMAC;
	private String thingIP;
	private int thingType;
	private String thingName;
	private Integer thingID;
	
	ThingAuth(){}
	
	
	public Integer getThingID() {
		return thingID;
	}
	public void setThingID(Integer thingID) {
		this.thingID = thingID;
	}
	public String getThingMAC() {
		return thingMAC;
	}
	public void setThingMAC(String thingMAC) {
		this.thingMAC = thingMAC;
	}
	public String getThingIP() {
		return thingIP;
	}
	public void setThingIP(String thingIP) {
		this.thingIP = thingIP;
	}
	public int getThingType() {
		return thingType;
	}
	public void setThingType(int thingType) {
		this.thingType = thingType;
	}


	public String getThingName() {
		return thingName;
	}


	public void setThingName(String thingName) {
		this.thingName = thingName;
	}
	
	
	

}
