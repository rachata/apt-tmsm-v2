package com.tmsm.api.model.device;

import java.util.List;

import com.tmsm.api.model.Status;

public class RootTimer extends Status {

	
	private List<Timer> data;

	public List<Timer> getData() {
		return data;
	}

	public void setData(List<Timer> data) {
		this.data = data;
	}
	
	
	
}
