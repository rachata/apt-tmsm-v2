package com.tmsm.api.model.node;

import com.tmsm.api.model.Status;

public class ControlDeviceIO  extends Status{

	
	
	private boolean statusPrivilege;

	public boolean isStatusPrivilege() {
		return statusPrivilege;
	}

	public void setStatusPrivilege(boolean statusPrivilege) {
		this.statusPrivilege = statusPrivilege;
	}
	
	
	
}
