package com.tmsm.api.model.door;

import java.util.List;

import com.tmsm.api.model.Status;

public class ResDoor extends Status {
	List<Door> door;

	public List<Door> getDoor() {
		return door;
	}

	public void setDoor(List<Door> door) {
		this.door = door;
	}
	
}
