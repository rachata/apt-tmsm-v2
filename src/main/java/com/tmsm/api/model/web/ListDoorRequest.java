package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;

public class ListDoorRequest extends Status{
	private List<DoorRequest> requests;

	public List<DoorRequest> getRequests() {
		return requests;
	}

	public void setRequests(List<DoorRequest> requests) {
		this.requests = requests;
	}
	
}
