package com.tmsm.api.model.web;

import java.util.List;

/**
 * @author palm1
 *
 */
public class AreaBillsList {
	private double biils;
	private double units;
	private int things_count;
	private List<ThingsBills> things_list;
	
	public double getBiils() {
		return biils;
	}
	public void setBiils(double biils) {
		this.biils = biils;
	}
	public double getUnits() {
		return units;
	}
	public void setUnits(double units) {
		this.units = units;
	}
	public int getThings_count() {
		return things_count;
	}
	public void setThings_count(int things_count) {
		this.things_count = things_count;
	}
	public List<ThingsBills> getThings_list() {
		return things_list;
	}
	public void setThings_list(List<ThingsBills> things_list) {
		this.things_list = things_list;
	}
}
