package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;

public class ListSettingSensing extends Status{
	private List<SettingSensing> sensings;

	public List<SettingSensing> getSensings() {
		return sensings;
	}

	public void setSensings(List<SettingSensing> sensings) {
		this.sensings = sensings;
	}
}
