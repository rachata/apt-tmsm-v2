package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;
import com.tmsm.model.strong.Position;

public class ResStrongPosition extends Status {
	private List<Position> positions;

	public List<Position> getPositions() {
		return positions;
	}

	public void setPositions(List<Position> positions) {
		this.positions = positions;
	}
	
}
