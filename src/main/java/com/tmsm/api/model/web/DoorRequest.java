package com.tmsm.api.model.web;

public class DoorRequest {
	private int request_id;
	private int request_user;
	private int request_things;
	private boolean request_status;
	private boolean request_enable;
	
	public int getRequest_id() {
		return request_id;
	}
	public void setRequest_id(int request_id) {
		this.request_id = request_id;
	}
	public int getRequest_things() {
		return request_things;
	}
	public void setRequest_things(int request_things) {
		this.request_things = request_things;
	}
	public boolean isRequest_status() {
		return request_status;
	}
	public void setRequest_status(boolean request_status) {
		this.request_status = request_status;
	}
	public boolean isRequest_enable() {
		return request_enable;
	}
	public void setRequest_enable(boolean request_enable) {
		this.request_enable = request_enable;
	}
	public int getRequest_user() {
		return request_user;
	}
	public void setRequest_user(int request_user) {
		this.request_user = request_user;
	}
	
}
