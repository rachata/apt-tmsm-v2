package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;

public class ListAdminRootArea extends Status{
	private List<RootArea> areas;

	public List<RootArea> getAreas() {
		return areas;
	}

	public void setAreas(List<RootArea> areas) {
		this.areas = areas;
	}
	
}
