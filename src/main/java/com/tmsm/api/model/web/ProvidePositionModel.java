package com.tmsm.api.model.web;

public class ProvidePositionModel {
	private int userId;
	private int position;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
}
