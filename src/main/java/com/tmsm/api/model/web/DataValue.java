package com.tmsm.api.model.web;

public class DataValue {
	private int group_count;
	private int things_count;
	private int system_things;
	private int system_area;
	
	public int getGroup_count() {
		return group_count;
	}
	public void setGroup_count(int group_count) {
		this.group_count = group_count;
	}
	public int getThings_count() {
		return things_count;
	}
	public void setThings_count(int things_count) {
		this.things_count = things_count;
	}
	public int getSystem_things() {
		return system_things;
	}
	public void setSystem_things(int system_things) {
		this.system_things = system_things;
	}
	public int getSystem_area() {
		return system_area;
	}
	public void setSystem_area(int system_area) {
		this.system_area = system_area;
	}
	

}
