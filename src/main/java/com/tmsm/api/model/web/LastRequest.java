package com.tmsm.api.model.web;

public class LastRequest {
	private int permit_id;
	private int permit_device;
	private boolean permit_status;
	private String permit_privilage;
	private int permit_grant;
	private boolean permit_enable;
	
	public boolean isPermit_status() {
		return permit_status;
	}
	public void setPermit_status(boolean permit_status) {
		this.permit_status = permit_status;
	}
	public int getPermit_id() {
		return permit_id;
	}
	public void setPermit_id(int permit_id) {
		this.permit_id = permit_id;
	}
	public int getPermit_device() {
		return permit_device;
	}
	public void setPermit_device(int permit_device) {
		this.permit_device = permit_device;
	}
	public String getPermit_privilage() {
		return permit_privilage;
	}
	public void setPermit_privilage(String permit_privilage) {
		this.permit_privilage = permit_privilage;
	}
	public int getPermit_grant() {
		return permit_grant;
	}
	public void setPermit_grant(int permit_grant) {
		this.permit_grant = permit_grant;
	}
	public boolean isPermit_enable() {
		return permit_enable;
	}
	public void setPermit_enable(boolean permit_enable) {
		this.permit_enable = permit_enable;
	}
}
