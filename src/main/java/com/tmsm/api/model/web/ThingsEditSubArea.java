package com.tmsm.api.model.web;

public class ThingsEditSubArea {
	private int things_id;
	private int subarea_id;
	
	public int getThings_id() {
		return things_id;
	}
	public void setThings_id(int things_id) {
		this.things_id = things_id;
	}
	public int getSubarea_id() {
		return subarea_id;
	}
	public void setSubarea_id(int subarea_id) {
		this.subarea_id = subarea_id;
	}
	
}
