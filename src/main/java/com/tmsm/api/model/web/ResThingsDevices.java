package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;

public class ResThingsDevices extends Status{
	private int root_area;
	private int sub_area;
	private List<ThingsDevices> devices;
	
	public int getRoot_area() {
		return root_area;
	}
	public void setRoot_area(int root_area) {
		this.root_area = root_area;
	}
	public int getSub_area() {
		return sub_area;
	}
	public void setSub_area(int sub_area) {
		this.sub_area = sub_area;
	}
	public List<ThingsDevices> getDevices() {
		return devices;
	}
	public void setDevices(List<ThingsDevices> devices) {
		this.devices = devices;
	}
	
}
