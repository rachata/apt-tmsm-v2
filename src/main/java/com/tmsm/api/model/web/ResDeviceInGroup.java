package com.tmsm.api.model.web;

import com.tmsm.api.model.Status;


public class ResDeviceInGroup extends Status{
	private DevicesGroup group;

	public DevicesGroup getGroup() {
		return group;
	}

	public void setGroup(DevicesGroup group) {
		this.group = group;
	}
	
}
