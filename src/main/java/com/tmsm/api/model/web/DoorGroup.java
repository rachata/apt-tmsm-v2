package com.tmsm.api.model.web;

import java.util.List;

public class DoorGroup {
	private int  group_id;
	private List<DoorGroupDetail> details;
	public int getGroup_id() {
		return group_id;
	}
	public void setGroup_id(int group_id) {
		this.group_id = group_id;
	}
	public List<DoorGroupDetail> getDetails() {
		return details;
	}
	public void setDetails(List<DoorGroupDetail> details) {
		this.details = details;
	}
	
}
