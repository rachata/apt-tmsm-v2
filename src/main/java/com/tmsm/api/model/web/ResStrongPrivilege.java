package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;
import com.tmsm.model.strong.Privilege;

public class ResStrongPrivilege extends Status{
	private List<Privilege> privileges;

	public List<Privilege> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(List<Privilege> privileges) {
		this.privileges = privileges;
	}
	
}
