package com.tmsm.api.model.web;

import com.tmsm.api.model.Status;

public class ListDataValue extends Status{
	private DataValue data;

	public DataValue getData() {
		return data;
	}

	public void setData(DataValue data) {
		this.data = data;
	}
	
}
