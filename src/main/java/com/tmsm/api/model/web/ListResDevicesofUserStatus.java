package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;

public class ListResDevicesofUserStatus extends Status{
	List<ResDevicesofUserStatus> datas;

	public List<ResDevicesofUserStatus> getDatas() {
		return datas;
	}

	public void setDatas(List<ResDevicesofUserStatus> datas) {
		this.datas = datas;
	}
}
