package com.tmsm.api.model.web;

import java.util.List;

public class DevicesGroup {
	
	private int  group_id;
	private List<LastStatusDevices> devices;
	

	public int getGroup_id() {
		return group_id;
	}
	public void setGroup_id(int group_id) {
		this.group_id = group_id;
	}
	public List<LastStatusDevices> getDevices() {
		return devices;
	}
	public void setDevices(List<LastStatusDevices> devices) {
		this.devices = devices;
	}
}
