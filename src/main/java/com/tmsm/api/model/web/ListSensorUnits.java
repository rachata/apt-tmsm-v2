package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;

public class ListSensorUnits extends Status{
	 private List<SensorUnit> units;

	public List<SensorUnit> getUnits() {
		return units;
	}

	public void setUnits(List<SensorUnit> units) {
		this.units = units;
	}
}
