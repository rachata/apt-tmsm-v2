package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;

public class ResAccDoor extends Status {
	private List<AccepDoor> data;

	public List<AccepDoor> getData() {
		return data;
	}

	public void setData(List<AccepDoor> data) {
		this.data = data;
	}
	
	
}
