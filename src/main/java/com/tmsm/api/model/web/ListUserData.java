package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;

public class ListUserData extends Status{
	private List<UserData> datas;

	public List<UserData> getDatas() {
		return datas;
	}

	public void setDatas(List<UserData> datas) {
		this.datas = datas;
	}
	
}
