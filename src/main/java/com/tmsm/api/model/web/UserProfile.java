package com.tmsm.api.model.web;

import com.tmsm.api.model.Status;

public class UserProfile extends Status {
	private ResUserProfile profile ;

	public ResUserProfile getProfile() {
		return profile;
	}

	public void setProfile(ResUserProfile profile) {
		this.profile = profile;
	}
	
	
}
