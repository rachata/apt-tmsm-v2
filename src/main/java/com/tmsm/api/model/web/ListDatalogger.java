package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;

public class ListDatalogger extends Status{
	private List<Datalogger> dataloggers;

	public List<Datalogger> getDataloggers() {
		return dataloggers;
	}

	public void setDataloggers(List<Datalogger> dataloggers) {
		this.dataloggers = dataloggers;
	}
}
