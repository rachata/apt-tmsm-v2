package com.tmsm.api.model.web;

public class Datalogger {
	private int log_id;
	private int log_status;
	private String log_name;
	private String log_create;
	private String log_devices;
	private String full_name;
	
	public int getLog_id() {
		return log_id;
	}
	public void setLog_id(int log_id) {
		this.log_id = log_id;
	}
	public int getLog_status() {
		return log_status;
	}
	public void setLog_status(int log_status) {
		this.log_status = log_status;
	}
	public String getLog_name() {
		return log_name;
	}
	public void setLog_name(String log_name) {
		this.log_name = log_name;
	}
	public String getLog_create() {
		return log_create;
	}
	public void setLog_create(String log_create) {
		this.log_create = log_create;
	}
	public String getLog_devices() {
		return log_devices;
	}
	public void setLog_devices(String log_devices) {
		this.log_devices = log_devices;
	}
	public String getFull_name() {
		return full_name;
	}
	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}
}
