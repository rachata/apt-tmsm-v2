package com.tmsm.api.model.web;

public class AdminArea {
	private int area_admin_id;
	private String area_name;
	private boolean area_enable;
	private int area_id;
	private int admin_enable;
	
	public int getArea_admin_id() {
		return area_admin_id;
	}
	public void setArea_admin_id(int area_admin_id) {
		this.area_admin_id = area_admin_id;
	}
	public String getArea_name() {
		return area_name;
	}
	public void setArea_name(String area_name) {
		this.area_name = area_name;
	}
	public boolean isArea_enable() {
		return area_enable;
	}
	public void setArea_enable(boolean area_enable) {
		this.area_enable = area_enable;
	}
	public int getArea_id() {
		return area_id;
	}
	public void setArea_id(int area_id) {
		this.area_id = area_id;
	}
	public int getAdmin_enable() {
		return admin_enable;
	}
	public void setAdmin_enable(int admin_enable) {
		this.admin_enable = admin_enable;
	}
}
