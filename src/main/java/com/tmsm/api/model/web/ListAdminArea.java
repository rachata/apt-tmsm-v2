package com.tmsm.api.model.web;

import java.util.List;



import com.tmsm.api.model.Status;

public class ListAdminArea extends Status{
	private List<AdminArea> admins;

	public List<AdminArea> getAdmins() {
		return admins;
	}

	public void setAdmins(List<AdminArea> admins) {
		this.admins = admins;
	}


}
