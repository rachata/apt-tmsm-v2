package com.tmsm.api.model.web;

public class ResUpdateDevice {
	private int thing;
	private int ch;
	private int priority;
	private int visibility;	
	private String nameDevice;
	private String privilege;
	
	public int getThing() {
		return thing;
	}
	public void setThing(int thing) {
		this.thing = thing;
	}
	public int getCh() {
		return ch;
	}
	public void setCh(int ch) {
		this.ch = ch;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public int getVisibility() {
		return visibility;
	}
	public void setVisibility(int visibility) {
		this.visibility = visibility;
	}
	public String getNameDevice() {
		return nameDevice;
	}
	public void setNameDevice(String nameDevice) {
		this.nameDevice = nameDevice;
	}
	public String getPrivilege() {
		return privilege;
	}
	public void setPrivilege(String privilege) {
		this.privilege = privilege;
	} 
	
}
