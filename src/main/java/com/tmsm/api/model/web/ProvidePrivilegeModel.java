package com.tmsm.api.model.web;

public class ProvidePrivilegeModel {
  private int usrId;
  private int privilege;
public int getUsrId() {
	return usrId;
}
public void setUsrId(int usrId) {
	this.usrId = usrId;
}
public int getPrivilege() {
	return privilege;
}
public void setPrivilege(int privilege) {
	this.privilege = privilege;
}
  
}
