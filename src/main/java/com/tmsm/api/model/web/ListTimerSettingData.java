package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;

public class ListTimerSettingData extends Status{
	private List<TimerSettingData> datas;

	public List<TimerSettingData> getDatas() {
		return datas;
	}

	public void setDatas(List<TimerSettingData> datas) {
		this.datas = datas;
	}
}
