package com.tmsm.api.model.web;

public class DevicesInGroup {
	private int devices_id;
	private int group_id;
	private String group_name;
	private int things_count;
	private boolean is_group;
	
	public int getDevices_id() {
		return devices_id;
	}
	public void setDevices_id(int devices_id) {
		this.devices_id = devices_id;
	}
	public int getGroup_id() {
		return group_id;
	}
	public void setGroup_id(int group_id) {
		this.group_id = group_id;
	}
	public String getGroup_name() {
		return group_name;
	}
	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}
	public int getThings_count() {
		return things_count;
	}
	public void setThings_count(int things_count) {
		this.things_count = things_count;
	}
	public boolean isIs_group() {
		return is_group;
	}
	public void setIs_group(boolean is_group) {
		this.is_group = is_group;
	}
	
}
