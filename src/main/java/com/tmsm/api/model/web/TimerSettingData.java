package com.tmsm.api.model.web;

public class TimerSettingData {
	private int time_id;
	private int user_id;
	private String full_name;
	private String datetime_min;
	private String datetime_max;
	private String time_min;
	private String time_max;
	private boolean select_state;
	private boolean access_status;
	private String create_on;
	private boolean setting_status;
	
	public boolean isAccess_status() {
		return access_status;
	}
	public void setAccess_status(boolean access_status) {
		this.access_status = access_status;
	}	
	public int getTime_id() {
		return time_id;
	}
	public void setTime_id(int time_id) {
		this.time_id = time_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getFull_name() {
		return full_name;
	}
	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}
	public String getDatetime_min() {
		return datetime_min;
	}
	public void setDatetime_min(String datetime_min) {
		this.datetime_min = datetime_min;
	}
	public String getDatetime_max() {
		return datetime_max;
	}
	public void setDatetime_max(String datetime_max) {
		this.datetime_max = datetime_max;
	}
	public String getTime_min() {
		return time_min;
	}
	public void setTime_min(String time_min) {
		this.time_min = time_min;
	}
	public String getTime_max() {
		return time_max;
	}
	public void setTime_max(String time_max) {
		this.time_max = time_max;
	}
	public boolean isSelect_state() {
		return select_state;
	}
	public void setSelect_state(boolean select_state) {
		this.select_state = select_state;
	}
	public String getCreate_on() {
		return create_on;
	}
	public void setCreate_on(String create_on) {
		this.create_on = create_on;
	}
	public boolean isSetting_status() {
		return setting_status;
	}
	public void setSetting_status(boolean setting_status) {
		this.setting_status = setting_status;
	}
}
