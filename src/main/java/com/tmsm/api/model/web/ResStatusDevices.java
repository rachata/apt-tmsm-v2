package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;

public class ResStatusDevices extends Status{	
	private List<LastStatusDevices> statusDevices;

	public List<LastStatusDevices> getStatusDevices() {
		return statusDevices;
	}
	public void setStatusDevices(List<LastStatusDevices> statusDevices) {
		this.statusDevices = statusDevices;
	}
	
}
