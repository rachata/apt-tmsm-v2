package com.tmsm.api.model.web;

public class PermitRes {

	
	private int permitID;
	private int permitUser;
	private int permitStatus;
	private String permitPrivilage;
	private int permitGrant;
    private String  permitTimeMin;
    private String  permitTimeMax;
    private String  fullname;
    private String nameDevice;
    private int device;
    
    private int type;
    
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getDevice() {
		return device;
	}
	public void setDevice(int device) {
		this.device = device;
	}
	
	public String getNameDevice() {
		return nameDevice;
	}
	public void setNameDevice(String nameDevice) {
		this.nameDevice = nameDevice;
	}
	public int getPermitID() {
		return permitID;
	}
	public void setPermitID(int permitID) {
		this.permitID = permitID;
	}
	public int getPermitUser() {
		return permitUser;
	}
	public void setPermitUser(int permitUser) {
		this.permitUser = permitUser;
	}
	public int getPermitStatus() {
		return permitStatus;
	}
	public void setPermitStatus(int permitStatus) {
		this.permitStatus = permitStatus;
	}
	public String getPermitPrivilage() {
		return permitPrivilage;
	}
	public void setPermitPrivilage(String permitPrivilage) {
		this.permitPrivilage = permitPrivilage;
	}
	public int getPermitGrant() {
		return permitGrant;
	}
	public void setPermitGrant(int permitGrant) {
		this.permitGrant = permitGrant;
	}
	public String getPermitTimeMin() {
		return permitTimeMin;
	}
	public void setPermitTimeMin(String permitTimeMin) {
		this.permitTimeMin = permitTimeMin;
	}
	public String getPermitTimeMax() {
		return permitTimeMax;
	}
	public void setPermitTimeMax(String permitTimeMax) {
		this.permitTimeMax = permitTimeMax;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
    
    
    
    
}
