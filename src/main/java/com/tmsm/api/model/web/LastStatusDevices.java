package com.tmsm.api.model.web;

public class LastStatusDevices {
	private int gd_id;
	private int devices_id;
	private String device_name;
	private int log_status;
	private int log_user;
	private boolean log_boolean;
	private String log_timer;
	private String first_name;
	private String last_name;
	private boolean is_faverite = false;
	private int feverite_id;
	public int getDevices_id() {
		return devices_id;
	}
	public void setDevices_id(int devices_id) {
		this.devices_id = devices_id;
	}
	public int getLog_status() {
		return log_status;
	}
	public void setLog_status(int log_status) {
		this.log_status = log_status;
	}
	public int getLog_user() {
		return log_user;
	}
	public void setLog_user(int log_user) {
		this.log_user = log_user;
	}
	public String getLog_timer() {
		return log_timer;
	}
	public void setLog_timer(String log_timer) {
		this.log_timer = log_timer;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public boolean isLog_boolean() {
		return log_boolean;
	}
	public void setLog_boolean(boolean log_boolean) {
		this.log_boolean = log_boolean;
	}
	public boolean isIs_faverite() {
		return is_faverite;
	}
	public void setIs_faverite(boolean is_faverite) {
		this.is_faverite = is_faverite;
	}
	public int getFeverite_id() {
		return feverite_id;
	}
	public void setFeverite_id(int feverite_id) {
		this.feverite_id = feverite_id;
	}
	public String getDevice_name() {
		return device_name;
	}
	public void setDevice_name(String device_name) {
		this.device_name = device_name;
	}
	public int getGd_id() {
		return gd_id;
	}
	public void setGd_id(int gd_id) {
		this.gd_id = gd_id;
	}
	
}
