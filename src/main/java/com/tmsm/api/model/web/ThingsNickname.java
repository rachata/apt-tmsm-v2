package com.tmsm.api.model.web;

public class ThingsNickname {
	private int things_id;
	private String things_name;
	
	public int getThings_id() {
		return things_id;
	}
	public void setThings_id(int things_id) {
		this.things_id = things_id;
	}
	public String getThings_name() {
		return things_name;
	}
	public void setThings_name(String things_name) {
		this.things_name = things_name;
	}
	
	
}
