package com.tmsm.api.model.web;

public class ElectricBill 
{
	private int device_id;
	private String device_name;
	private Double electric_unit = 0.0;
	private Double electric_bill = 0.0;
	
	public int getDevice_id() {
		return device_id;
	}
	public void setDevice_id(int device_id) {
		this.device_id = device_id;
	}
	public String getDevice_name() {
		return device_name;
	}
	public void setDevice_name(String device_name) {
		this.device_name = device_name;
	}
	public Double getElectric_unit() {
		return electric_unit;
	}
	public void setElectric_unit(Double electric_unit) {
		this.electric_unit = electric_unit;
	}
	public Double getElectric_bill() {
		return electric_bill;
	}
	public void setElectric_bill(Double electric_bill) {
		this.electric_bill = electric_bill;
	}
}


