package com.tmsm.api.model.web;

public class GetResetPassword {
	private int otp_id;
	private String token;
	private int user_id;
	
	public int getOtp_id() {
		return otp_id;
	}
	public void setOtp_id(int otp_id) {
		this.otp_id = otp_id;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}	
}
