package com.tmsm.api.model.web;

public class DoorChangeName {
	private int door_id;
	private String door_name;
	
	public int getDoor_id() {
		return door_id;
	}
	public void setDoor_id(int door_id) {
		this.door_id = door_id;
	}
	public String getDoor_name() {
		return door_name;
	}
	public void setDoor_name(String door_name) {
		this.door_name = door_name;
	}
}
