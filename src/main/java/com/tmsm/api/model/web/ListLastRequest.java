package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;

public class ListLastRequest extends Status{
	private List<LastRequest> requests;

	public List<LastRequest> getRequests() {
		return requests;
	}

	public void setRequests(List<LastRequest> requests) {
		this.requests = requests;
	}
	
}
