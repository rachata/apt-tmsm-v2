package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;



public class PermitResRoot extends Status{

	private List<PermitRes> data;

	public List<PermitRes> getData() {
		return data;
	}

	public void setData(List<PermitRes> data) {
		this.data = data;
	}
	
	
}
