package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;

public class ListAdminSubArea extends Status{
	private List<DetailArea> subarea;

	public List<DetailArea> getSubarea() {
		return subarea;
	}

	public void setSubarea(List<DetailArea> subarea) {
		this.subarea = subarea;
	}
	
}
