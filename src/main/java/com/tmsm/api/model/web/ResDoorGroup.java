package com.tmsm.api.model.web;

import com.tmsm.api.model.Status;

public class ResDoorGroup extends Status{
	private DoorGroup group;

	public DoorGroup getGroup() {
		return group;
	}

	public void setGroup(DoorGroup group) {
		this.group = group;
	}
	
}
