package com.tmsm.api.model.web;

public class SettingSensing {
	private int setting_id;
	private int devices_io;
	private int devices_sensor;
	private int setting_user;
	private float setting_min;
	private float setting_max;
	private float setting_datas;
	private Boolean setting_status;
	private Boolean setting_enable;
	private String setting_date;
	private int setting_select;
	private int setting_io;
	private String full_name;
	private String io_name;
	private String sensing_name;
	
	public String getFull_name() {
		return full_name;
	}
	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}
	public String getIo_name() {
		return io_name;
	}
	public void setIo_name(String io_name) {
		this.io_name = io_name;
	}
	public String getSensing_name() {
		return sensing_name;
	}
	public void setSensing_name(String sensing_name) {
		this.sensing_name = sensing_name;
	}
	public int getSetting_id() {
		return setting_id;
	}
	public void setSetting_id(int setting_id) {
		this.setting_id = setting_id;
	}
	public int getDevices_io() {
		return devices_io;
	}
	public void setDevices_io(int devices_io) {
		this.devices_io = devices_io;
	}
	public int getDevices_sensor() {
		return devices_sensor;
	}
	public void setDevices_sensor(int devices_sensor) {
		this.devices_sensor = devices_sensor;
	}
	public int getSetting_user() {
		return setting_user;
	}
	public void setSetting_user(int setting_user) {
		this.setting_user = setting_user;
	}
	public float getSetting_min() {
		return setting_min;
	}
	public void setSetting_min(float setting_min) {
		this.setting_min = setting_min;
	}
	public float getSetting_max() {
		return setting_max;
	}
	public void setSetting_max(float setting_max) {
		this.setting_max = setting_max;
	}
	public float getSetting_datas() {
		return setting_datas;
	}
	public void setSetting_datas(float setting_datas) {
		this.setting_datas = setting_datas;
	}
	public boolean isSetting_status() {
		return setting_status;
	}
	public void setSetting_status(boolean setting_status) {
		this.setting_status = setting_status;
	}
	public boolean isSetting_enable() {
		return setting_enable;
	}
	public void setSetting_enable(boolean setting_enable) {
		this.setting_enable = setting_enable;
	}
	public String getSetting_date() {
		return setting_date;
	}
	public void setSetting_date(String setting_date) {
		this.setting_date = setting_date;
	}
	public int getSetting_select() {
		return setting_select;
	}
	public void setSetting_select(int setting_select) {
		this.setting_select = setting_select;
	}
	public int getSetting_io() {
		return setting_io;
	}
	public void setSetting_io(int setting_io) {
		this.setting_io = setting_io;
	}
}
