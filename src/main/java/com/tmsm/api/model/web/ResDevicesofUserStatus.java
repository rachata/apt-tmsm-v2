package com.tmsm.api.model.web;

public class ResDevicesofUserStatus {
	private int things_id;
	private int things_type;
	private Double things_offsetx;
	private Double things_offsety;
	private int things_area;
	private int devices_id;
	private int devices_chanel;
	private int devices_priority;
	private int devices_visibility;
	private String devices_privilege;
	private int permit_id;
	private String permit_privilege;
	private String type_name;
	private String device_name;
	private int log_status;
	private int log_user;
	private boolean log_boolean;
	private String log_timer;
	private String full_name;
	private boolean is_faverite = false;
	private int feverite_id;
	private int unit_id;
	private String unit_name;
	private float sensor_value;
	private int g_id;
	
	
	
	public int getG_id() {
		return g_id;
	}
	public void setG_id(int g_id) {
		this.g_id = g_id;
	}
	public float getSensor_value() {
		return sensor_value;
	}
	public void setSensor_value(float sensor_value) {
		this.sensor_value = sensor_value;
	}
	public int getUnit_id() {
		return unit_id;
	}
	public void setUnit_id(int unit_id) {
		this.unit_id = unit_id;
	}
	public String getUnit_name() {
		return unit_name;
	}
	public void setUnit_name(String unit_name) {
		this.unit_name = unit_name;
	}
	public int getThings_id() {
		return things_id;
	}
	public void setThings_id(int things_id) {
		this.things_id = things_id;
	}
	public int getThings_type() {
		return things_type;
	}
	public void setThings_type(int things_type) {
		this.things_type = things_type;
	}
	public Double getThings_offsetx() {
		return things_offsetx;
	}
	public void setThings_offsetx(Double things_offsetx) {
		this.things_offsetx = things_offsetx;
	}
	public Double getThings_offsety() {
		return things_offsety;
	}
	public void setThings_offsety(Double things_offsety) {
		this.things_offsety = things_offsety;
	}
	public int getThings_area() {
		return things_area;
	}
	public void setThings_area(int things_area) {
		this.things_area = things_area;
	}
	public int getDevices_id() {
		return devices_id;
	}
	public void setDevices_id(int devices_id) {
		this.devices_id = devices_id;
	}
	public int getDevices_chanel() {
		return devices_chanel;
	}
	public void setDevices_chanel(int devices_chanel) {
		this.devices_chanel = devices_chanel;
	}
	public int getDevices_priority() {
		return devices_priority;
	}
	public void setDevices_priority(int devices_priority) {
		this.devices_priority = devices_priority;
	}
	public int getDevices_visibility() {
		return devices_visibility;
	}
	public void setDevices_visibility(int devices_visibility) {
		this.devices_visibility = devices_visibility;
	}
	public String getDevices_privilege() {
		return devices_privilege;
	}
	public void setDevices_privilege(String devices_privilege) {
		this.devices_privilege = devices_privilege;
	}
	public int getPermit_id() {
		return permit_id;
	}
	public void setPermit_id(int permit_id) {
		this.permit_id = permit_id;
	}
	public String getPermit_privilege() {
		return permit_privilege;
	}
	public void setPermit_privilege(String permit_privilege) {
		this.permit_privilege = permit_privilege;
	}
	public String getType_name() {
		return type_name;
	}
	public void setType_name(String type_name) {
		this.type_name = type_name;
	}
	public String getDevice_name() {
		return device_name;
	}
	public void setDevice_name(String device_name) {
		this.device_name = device_name;
	}
	public int getLog_status() {
		return log_status;
	}
	public void setLog_status(int log_status) {
		this.log_status = log_status;
	}
	public int getLog_user() {
		return log_user;
	}
	public void setLog_user(int log_user) {
		this.log_user = log_user;
	}
	public boolean isLog_boolean() {
		return log_boolean;
	}
	public void setLog_boolean(boolean log_boolean) {
		this.log_boolean = log_boolean;
	}
	public String getLog_timer() {
		return log_timer;
	}
	public void setLog_timer(String log_timer) {
		this.log_timer = log_timer;
	}
	public String getFull_name() {
		return full_name;
	}
	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}
	public boolean isIs_faverite() {
		return is_faverite;
	}
	public void setIs_faverite(boolean is_faverite) {
		this.is_faverite = is_faverite;
	}
	public int getFeverite_id() {
		return feverite_id;
	}
	public void setFeverite_id(int feverite_id) {
		this.feverite_id = feverite_id;
	}
}
