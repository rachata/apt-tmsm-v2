package com.tmsm.api.model.web;

public class DevicesModel {
	private String device_name;
	private String device_privilege;
	private int device_chanel;
	private int device_id;
	private int device_priority;
	private int device_thing;
	private int device_visibility;
	
	public String getDevice_name() {
		return device_name;
	}
	public void setDevice_name(String device_name) {
		this.device_name = device_name;
	}
	public String getDevice_privilege() {
		return device_privilege;
	}
	public void setDevice_privilege(String device_privilege) {
		this.device_privilege = device_privilege;
	}
	public int getDevice_chanel() {
		return device_chanel;
	}
	public void setDevice_chanel(int device_chanel) {
		this.device_chanel = device_chanel;
	}
	public int getDevice_id() {
		return device_id;
	}
	public void setDevice_id(int device_id) {
		this.device_id = device_id;
	}
	public int getDevice_priority() {
		return device_priority;
	}
	public void setDevice_priority(int device_priority) {
		this.device_priority = device_priority;
	}
	public int getDevice_thing() {
		return device_thing;
	}
	public void setDevice_thing(int device_thing) {
		this.device_thing = device_thing;
	}
	public int getDevice_visibility() {
		return device_visibility;
	}
	public void setDevice_visibility(int device_visibility) {
		this.device_visibility = device_visibility;
	}
}
