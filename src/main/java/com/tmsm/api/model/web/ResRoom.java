package com.tmsm.api.model.web;

public class ResRoom {
	private int thingId;
	private int areaId;
	public int getThingId() {
		return thingId;
	}
	public void setThingId(int thingId) {
		this.thingId = thingId;
	}
	public int getAreaId() {
		return areaId;
	}
	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}
}
