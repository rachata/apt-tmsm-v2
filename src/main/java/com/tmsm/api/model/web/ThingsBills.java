package com.tmsm.api.model.web;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ThingsBills {	
	private int things_id;
	private Double things_bills = null;
	private Double things_unit = null;
	private int devices_count;
	private List<ElectricBill> devices_list;
	
	public int getThings_id() {
		return things_id;
	}
	public void setThings_id(int things_id) {
		this.things_id = things_id;
	}
	public List<ElectricBill> getDevices_list() {
		return devices_list;
	}
	public void setDevices_list(List<ElectricBill> devices_list) {
		this.devices_list = devices_list;
	}
	public int getDevices_count() {
		return devices_count;
	}
	public void setDevices_count(int devices_count) {
		this.devices_count = devices_count;
	}
	public Double getThings_bills() {
		return things_bills;
	}
	public void setThings_bills(Double things_bills) {
		this.things_bills = things_bills;
	}
	public Double getThings_unit() {
		return things_unit;
	}
	public void setThings_unit(Double things_unit) {
		this.things_unit = things_unit;
	}	
}
