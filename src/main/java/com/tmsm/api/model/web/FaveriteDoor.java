package com.tmsm.api.model.web;

public class FaveriteDoor {
	private int faveriteId;
	private int thingsId;
	private String thingsName;
	private String thingsDetailName;
	private String detailArea;
	private String areaName;
	private DoorLogStatus logStatus;
	
	public DoorLogStatus getLogStatus() {
		return logStatus;
	}
	public void setLogStatus(DoorLogStatus logStatus) {
		this.logStatus = logStatus;
	}
	public int getFaveriteId() {
		return faveriteId;
	}
	public void setFaveriteId(int faveriteId) {
		this.faveriteId = faveriteId;
	}
	public int getThingsId() {
		return thingsId;
	}
	public void setThingsId(int thingsId) {
		this.thingsId = thingsId;
	}
	public String getThingsName() {
		return thingsName;
	}
	public void setThingsName(String thingsName) {
		this.thingsName = thingsName;
	}
	public String getThingsDetailName() {
		return thingsDetailName;
	}
	public void setThingsDetailName(String thingsDetailName) {
		this.thingsDetailName = thingsDetailName;
	}
	public String getDetailArea() {
		return detailArea;
	}
	public void setDetailArea(String detailArea) {
		this.detailArea = detailArea;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	
}
