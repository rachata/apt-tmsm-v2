package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;

public class ListFaverite extends Status {
	private List<FaveriteDoor> data;

	public List<FaveriteDoor> getData() {
		return data;
	}

	public void setData(List<FaveriteDoor> data) {
		this.data = data;
	}
}
