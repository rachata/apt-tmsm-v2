package com.tmsm.api.model.web;

public class DoorGroupDetail {
	private int gd_id;
	private String thingsName;
	private int thingID;
	private int lastStatus;
	private String statusType = "N/A";
	private String fName = "N/A";
	private String lName = "N/A";
	private boolean is_feverite = false;
	private int feverite_id;
	
	public int getGd_id() {
		return gd_id;
	}
	public void setGd_id(int gd_id) {
		this.gd_id = gd_id;
	}
	public String getThingsName() {
		return thingsName;
	}
	public void setThingsName(String thingsName) {
		this.thingsName = thingsName;
	}
	public int getThingID() {
		return thingID;
	}
	public void setThingID(int thingID) {
		this.thingID = thingID;
	}
	public int getLastStatus() {
		return lastStatus;
	}
	public void setLastStatus(int lastStatus) {
		this.lastStatus = lastStatus;
	}
	public String getStatusType() {
		return statusType;
	}
	public void setStatusType(String statusType) {
		this.statusType = statusType;
	}
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String getlName() {
		return lName;
	}
	public void setlName(String lName) {
		this.lName = lName;
	}
	public boolean isIs_feverite() {
		return is_feverite;
	}
	public void setIs_feverite(boolean is_feverite) {
		this.is_feverite = is_feverite;
	}
	public int getFeverite_id() {
		return feverite_id;
	}
	public void setFeverite_id(int feverite_id) {
		this.feverite_id = feverite_id;
	}
}
