package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;

public class ResAllElectrictBill extends Status {
	private List<ElectricBill> bills;

	public List<ElectricBill> getBills() {
		return bills;
	}

	public void setBills(List<ElectricBill> bills) {
		this.bills = bills;
	}
	
}
