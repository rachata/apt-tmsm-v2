package com.tmsm.api.model.web;

public class DoorLog {
	private int logId;
	private int logStatus;
	private int logThings;
	private int logUsr;
	private String logDt;
	
	public int getLogId() {
		return logId;
	}
	public void setLogId(int logId) {
		this.logId = logId;
	}
	public int getLogStatus() {
		return logStatus;
	}
	public void setLogStatus(int logStatus) {
		this.logStatus = logStatus;
	}
	public int getLogThings() {
		return logThings;
	}
	public void setLogThings(int logThings) {
		this.logThings = logThings;
	}
	public int getLogUsr() {
		return logUsr;
	}
	public void setLogUsr(int logUsr) {
		this.logUsr = logUsr;
	}
	public String getLogDt() {
		return logDt;
	}
	public void setLogDt(String logDt) {
		this.logDt = logDt;
	}
	
}
