package com.tmsm.api.model.web;

public class AcceptSensing {
	private int sensing_id;
	private boolean sensing_status;
	private boolean sensing_enable;
	
	public int getSensing_id() {
		return sensing_id;
	}
	public void setSensing_id(int sensing_id) {
		this.sensing_id = sensing_id;
	}
	public boolean isSensing_status() {
		return sensing_status;
	}
	public void setSensing_status(boolean sensing_status) {
		this.sensing_status = sensing_status;
	}
	public boolean isSensing_enable() {
		return sensing_enable;
	}
	public void setSensing_enable(boolean sensing_enable) {
		this.sensing_enable = sensing_enable;
	}
	
}
