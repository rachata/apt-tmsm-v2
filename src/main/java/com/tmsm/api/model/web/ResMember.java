package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;

public class ResMember extends Status{
	private List<Member> members;

	public List<Member> getMembers() {
		return members;
	}

	public void setMembers(List<Member> members) {
		this.members = members;
	}
	
}
