package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;

public class ListDevicesInGroup extends Status{
	private List<DevicesInGroup> groups;

	public List<DevicesInGroup> getGroups() {
		return groups;
	}

	public void setGroups(List<DevicesInGroup> groups) {
		this.groups = groups;
	}
	
}
