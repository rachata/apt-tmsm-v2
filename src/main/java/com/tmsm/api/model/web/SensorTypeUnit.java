package com.tmsm.api.model.web;

public class SensorTypeUnit {
	private int sensor_unit_id;
	private int sensor_id;
	private int unit_id;
	private String unit_name;
	
	public int getSensor_unit_id() {
		return sensor_unit_id;
	}
	public void setSensor_unit_id(int sensor_unit_id) {
		this.sensor_unit_id = sensor_unit_id;
	}
	public int getSensor_id() {
		return sensor_id;
	}
	public void setSensor_id(int sensor_id) {
		this.sensor_id = sensor_id;
	}
	public int getUnit_id() {
		return unit_id;
	}
	public void setUnit_id(int unit_id) {
		this.unit_id = unit_id;
	}
	public String getUnit_name() {
		return unit_name;
	}
	public void setUnit_name(String unit_name) {
		this.unit_name = unit_name;
	}
}
