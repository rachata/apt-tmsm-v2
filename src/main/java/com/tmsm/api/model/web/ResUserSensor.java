package com.tmsm.api.model.web;

import com.tmsm.api.model.Status;

public class ResUserSensor extends Status{
	private UserSensorUnits units;

	public UserSensorUnits getUnits() {
		return units;
	}

	public void setUnits(UserSensorUnits units) {
		this.units = units;
	}
}
