package com.tmsm.api.model.web;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseCommon {
	private int total_count;
	private List<?> result_list;
	
	public int getTotal_count() {
		return total_count;
	}
	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}
	public List<?> getResult_list() {
		return result_list;
	}
	public void setResult_list(List<?> result_list) {
		this.result_list = result_list;
	}	
}
