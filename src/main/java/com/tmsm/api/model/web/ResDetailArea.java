package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;

public class ResDetailArea extends Status {
	private Integer areaID;
	private String areaName;
	private List<DetailArea> detail ;
	
	
	public Integer getAreaID() {
		return areaID;
	}
	public void setAreaID(Integer areaID) {
		this.areaID = areaID;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public List<DetailArea> getDetail() {
		return detail;
	}
	public void setDetail(List<DetailArea> detail) {
		this.detail = detail;
	}
	
	
}
