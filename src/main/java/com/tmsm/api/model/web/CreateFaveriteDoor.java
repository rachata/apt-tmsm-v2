package com.tmsm.api.model.web;

public class CreateFaveriteDoor {
	private int things_id;
	private int user_id;
	private int devices_id;
	
	public int getThings_id() {
		return things_id;
	}
	public void setThings_id(int things_id) {
		this.things_id = things_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getDevices_id() {
		return devices_id;
	}
	public void setDevices_id(int devices_id) {
		this.devices_id = devices_id;
	}
	
}
