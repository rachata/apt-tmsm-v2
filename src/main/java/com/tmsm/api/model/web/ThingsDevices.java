package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;

public class ThingsDevices{
	private int things_id;
	private double things_offsetx;
	private double things_offsety;
	private String things_nickname;
	private String plan_image;
	private String plan_name;
	private int things_type;
	private List<DevicesModel> devicesModels;

	public int getThings_type() {
		return things_type;
	}
	public void setThings_type(int things_type) {
		this.things_type = things_type;
	}
	public List<DevicesModel> getDevicesModels() {
		return devicesModels;
	}
	public void setDevicesModels(List<DevicesModel> devicesModels) {
		this.devicesModels = devicesModels;
	}
	public int getThings_id() {
		return things_id;
	}
	public void setThings_id(int things_id) {
		this.things_id = things_id;
	}
	public double getThings_offsetx() {
		return things_offsetx;
	}
	public void setThings_offsetx(double things_offsetx) {
		this.things_offsetx = things_offsetx;
	}
	public double getThings_offsety() {
		return things_offsety;
	}
	public void setThings_offsety(double things_offsety) {
		this.things_offsety = things_offsety;
	}
	public String getThings_nickname() {
		return things_nickname;
	}
	public void setThings_nickname(String things_nickname) {
		this.things_nickname = things_nickname;
	}
	public String getPlan_image() {
		return plan_image;
	}
	public void setPlan_image(String plan_image) {
		this.plan_image = plan_image;
	}
	public String getPlan_name() {
		return plan_name;
	}
	public void setPlan_name(String plan_name) {
		this.plan_name = plan_name;
	}
	
}
