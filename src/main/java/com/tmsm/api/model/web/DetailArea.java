package com.tmsm.api.model.web;

public class DetailArea {
	private Integer detailID;
	private String detailName;
	private String detailImage;
	private Integer detailArea;
	
	
	public Integer getDetailID() {
		return detailID;
	}
	public void setDetailID(Integer detailID) {
		this.detailID = detailID;
	}
	public String getDetailName() {
		return detailName;
	}
	public void setDetailName(String detailName) {
		this.detailName = detailName;
	}
	public String getDetailImage() {
		return detailImage;
	}
	public void setDetailImage(String detailImage) {
		this.detailImage = detailImage;
	}
	public Integer getDetailArea() {
		return detailArea;
	}
	public void setDetailArea(Integer detailArea) {
		this.detailArea = detailArea;
	}	
}
