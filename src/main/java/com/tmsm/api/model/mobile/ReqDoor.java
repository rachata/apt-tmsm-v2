package com.tmsm.api.model.mobile;

public class ReqDoor {

	
	private int userID;
	private int thingID;
	private String door_name;
	private int status;
	
	
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public int getThingID() {
		return thingID;
	}
	public void setThingID(int thingID) {
		this.thingID = thingID;
	}
	public String getDoor_name() {
		return door_name;
	}
	public void setDoor_name(String door_name) {
		this.door_name = door_name;
	}
		
}
