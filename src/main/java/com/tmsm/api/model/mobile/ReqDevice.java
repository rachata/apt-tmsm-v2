package com.tmsm.api.model.mobile;

public class ReqDevice {

	private int userID;
	private int deviceID;
	
	
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public int getDeviceID() {
		return deviceID;
	}
	public void setDeviceID(int deviceID) {
		this.deviceID = deviceID;
	}
	
	
	
}
