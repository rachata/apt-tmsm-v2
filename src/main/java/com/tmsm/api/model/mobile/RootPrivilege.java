package com.tmsm.api.model.mobile;

import java.util.List;

import com.tmsm.api.model.Status;

public class RootPrivilege extends Status {

	
	private List<Privilege> privileges;

	public List<Privilege> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(List<Privilege> privileges) {
		this.privileges = privileges;
	}
	
	
	
}
