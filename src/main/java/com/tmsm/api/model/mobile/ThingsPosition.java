package com.tmsm.api.model.mobile;

import java.util.List;

public class ThingsPosition {


private Integer thingsID;
private String thingsName;
private String thingsType;
private Double thingsOffsetX;
private Double thingsOffsetY;
private List<Device> devices = null;

public Integer getThingsID() {
	return thingsID;
}
public void setThingsID(Integer thingsID) {
	this.thingsID = thingsID;
}
public String getThingsName() {
	return thingsName;
}
public void setThingsName(String thingsName) {
	this.thingsName = thingsName;
}
public String getThingsType() {
	return thingsType;
}
public void setThingsType(String thingsType) {
	this.thingsType = thingsType;
}
public Double getThingsOffsetX() {
	return thingsOffsetX;
}
public void setThingsOffsetX(Double thingsOffsetX) {
	this.thingsOffsetX = thingsOffsetX;
}
public Double getThingsOffsetY() {
	return thingsOffsetY;
}
public void setThingsOffsetY(Double thingsOffsetY) {
	this.thingsOffsetY = thingsOffsetY;
}
public List<Device> getDevices() {
	return devices;
}
public void setDevices(List<Device> devices) {
	this.devices = devices;
}


}
