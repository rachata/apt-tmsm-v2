package com.tmsm.api.model.mobile;

import com.tmsm.api.model.Status;

public class ResSelectDevice {
	
	private int selectID;
	private int selectUsr;
	private int selectDevice;
	private int selectStatus;

	private String nameDevice;

	private String fullname;
	
	
	
	
	public String getNameDevice() {
		return nameDevice;
	}
	public void setNameDevice(String nameDevice) {
		this.nameDevice = nameDevice;
	}
	
	
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public int getSelectID() {
		return selectID;
	}
	public void setSelectID(int selectID) {
		this.selectID = selectID;
	}
	public int getSelectUsr() {
		return selectUsr;
	}
	public void setSelectUsr(int selectUsr) {
		this.selectUsr = selectUsr;
	}
	public int getSelectDevice() {
		return selectDevice;
	}
	public void setSelectDevice(int selectDevice) {
		this.selectDevice = selectDevice;
	}
	public int getSelectStatus() {
		return selectStatus;
	}
	public void setSelectStatus(int selectStatus) {
		this.selectStatus = selectStatus;
	}
	
	

}
