package com.tmsm.api.model.mobile;

public class Privilege {

	
	private int permitID;
	private int permitDevice;
	private int permitStatus;
	private String permitPrivilege;
	private int permitGrant;
	private String timeMin;
	private String timeMax;
	public int getPermitID() {
		return permitID;
	}
	public void setPermitID(int permitID) {
		this.permitID = permitID;
	}
	public int getPermitDevice() {
		return permitDevice;
	}
	public void setPermitDevice(int permitDevice) {
		this.permitDevice = permitDevice;
	}
	public int getPermitStatus() {
		return permitStatus;
	}
	public void setPermitStatus(int permitStatus) {
		this.permitStatus = permitStatus;
	}
	public String getPermitPrivilege() {
		return permitPrivilege;
	}
	public void setPermitPrivilege(String permitPrivilege) {
		this.permitPrivilege = permitPrivilege;
	}
	public int getPermitGrant() {
		return permitGrant;
	}
	public void setPermitGrant(int permitGrant) {
		this.permitGrant = permitGrant;
	}
	public String getTimeMin() {
		return timeMin;
	}
	public void setTimeMin(String timeMin) {
		this.timeMin = timeMin;
	}
	public String getTimeMax() {
		return timeMax;
	}
	public void setTimeMax(String timeMax) {
		this.timeMax = timeMax;
	}
	
	
	
	
	
}
