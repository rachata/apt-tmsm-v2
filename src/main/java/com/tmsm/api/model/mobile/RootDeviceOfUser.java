package com.tmsm.api.model.mobile;

import java.util.List;

import com.tmsm.api.model.Status;

public class RootDeviceOfUser extends Status{
	
	private List<DeviceOfUser> devices;

	public List<DeviceOfUser> getDevices() {
		return devices;
	}

	public void setDevices(List<DeviceOfUser> devices) {
		this.devices = devices;
	}
	
	
	

}
