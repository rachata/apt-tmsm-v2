package com.tmsm.api.model.mobile;

import java.util.List;

import com.tmsm.api.model.Status;

public class RootPermitDeviceOfUser extends Status {

	
	private List<PermitDeviceOfUser> data;

	public List<PermitDeviceOfUser> getData() {
		return data;
	}

	public void setData(List<PermitDeviceOfUser> data) {
		this.data = data;
	}
	
	
	
	
}
