package com.tmsm.api.model.mobile;

import java.util.ArrayList;
import java.util.List;

import com.tmsm.api.model.Status;

public class ResArea extends Status {

	List<AddNewArea> area = new ArrayList<AddNewArea>();

	public List<AddNewArea> getArea() {
		return area;
	}

	public void setArea(List<AddNewArea> area) {
		this.area = area;
	}	
	
}
