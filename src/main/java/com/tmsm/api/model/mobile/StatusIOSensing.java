package com.tmsm.api.model.mobile;

public class StatusIOSensing {
	private boolean statusIO;
	private int idThing;
	private int idUser;
	private int ch;
	private int idDevice;
	
	
	public boolean isStatusIO() {
		return statusIO;
	}
	public void setStatusIO(boolean statusIO) {
		this.statusIO = statusIO;
	}
	public int getIdThing() {
		return idThing;
	}
	public void setIdThing(int idThing) {
		this.idThing = idThing;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public int getCh() {
		return ch;
	}
	public void setCh(int ch) {
		this.ch = ch;
	}
	public int getIdDevice() {
		return idDevice;
	}
	public void setIdDevice(int idDevice) {
		this.idDevice = idDevice;
	}
}
