package com.tmsm.api.model.mobile;

import java.util.List;

public class Thing {
	 private Long thingID;

	    private String thingIP;
	    private String thingMAC;
	    private String thingName;
	    private Long thingType;	    
	    private String thingTypeName;	    
	    private String thingArea;
	    private String thingRootArea;
	    private String thing_nickname;
	    private List<Device> devices;
	    private int idArea;
	    private Double offsetx;
	    private Double offsety;
	    private String image;
	      
	    
	    

		public String getThing_nickname() {
			return thing_nickname;
		}

		public void setThing_nickname(String thing_nickname) {
			this.thing_nickname = thing_nickname;
		}

		public int getIdArea() {
			return idArea;
		}

		public void setIdArea(int idArea) {
			this.idArea = idArea;
		}

		public String getThingArea() {
			return thingArea;
		}

		public void setThingArea(String thingArea) {
			this.thingArea = thingArea;
		}
		
		

		public String getThingRootArea() {
			return thingRootArea;
		}

		public void setThingRootArea(String thingRootArea) {
			this.thingRootArea = thingRootArea;
		}

		public List<Device> getDevices() {
			return devices;
		}

		public void setDevices(List<Device> devices) {
			this.devices = devices;
		}

		public String getThingTypeName() {
			return thingTypeName;
		}

		public void setThingTypeName(String thingTypeName) {
			this.thingTypeName = thingTypeName;
		}

		public Long getThingID() {
	        return thingID;
	    }

	    public void setThingID(Long thingID) {
	        this.thingID = thingID;
	    }

	    public String getThingIP() {
	        return thingIP;
	    }

	    public void setThingIP(String thingIP) {
	        this.thingIP = thingIP;
	    }

	    public String getThingMAC() {
	        return thingMAC;
	    }

	    public void setThingMAC(String thingMAC) {
	        this.thingMAC = thingMAC;
	    }

	    public String getThingName() {
	        return thingName;
	    }

	    public void setThingName(String thingName) {
	        this.thingName = thingName;
	    }

	    public Long getThingType() {
	        return thingType;
	    }

	    public void setThingType(Long thingType) {
	        this.thingType = thingType;
	    }

		public Double getOffsetx() {
			return offsetx;
		}

		public void setOffsetx(Double offsetx) {
			this.offsetx = offsetx;
		}

		public Double getOffsety() {
			return offsety;
		}

		public void setOffsety(Double offsety) {
			this.offsety = offsety;
		}

		public String getImage() {
			return image;
		}

		public void setImage(String image) {
			this.image = image;
		}
		
	    
}
