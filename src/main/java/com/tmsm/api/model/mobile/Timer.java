package com.tmsm.api.model.mobile;

public class Timer {

	
	private int tID;
    private int tUser;
    private int tDevice;
    private String tDatetimeMin;
    private String tDatetimeMax;
    private String tTimeMin;
    private String tTimeMax;
    private boolean tSelectST;
    private boolean tStatus; 
    private boolean tEnable;
    private String tSetDatetime;
    private boolean statusDevice;
    
    
    
    
    public boolean isStatusDevice() {
		return statusDevice;
	}
	public void setStatusDevice(boolean statusDevice) {
		this.statusDevice = statusDevice;
	}
	private int privilege;
    
    
	public int getPrivilege() {
		return privilege;
	}
	public void setPrivilege(int privilege) {
		this.privilege = privilege;
	}
	public int gettID() {
		return tID;
	}
	public void settID(int tID) {
		this.tID = tID;
	}
	public int gettUser() {
		return tUser;
	}
	public void settUser(int tUser) {
		this.tUser = tUser;
	}
	public int gettDevice() {
		return tDevice;
	}
	public void settDevice(int tDevice) {
		this.tDevice = tDevice;
	}
	public String gettDatetimeMin() {
		return tDatetimeMin;
	}
	public void settDatetimeMin(String tDatetimeMin) {
		this.tDatetimeMin = tDatetimeMin;
	}
	public String gettDatetimeMax() {
		return tDatetimeMax;
	}
	public void settDatetimeMax(String tDatetimeMax) {
		this.tDatetimeMax = tDatetimeMax;
	}
	public String gettTimeMin() {
		return tTimeMin;
	}
	public void settTimeMin(String tTimeMin) {
		this.tTimeMin = tTimeMin;
	}
	public String gettTimeMax() {
		return tTimeMax;
	}
	public void settTimeMax(String tTimeMax) {
		this.tTimeMax = tTimeMax;
	}
	public boolean istSelectST() {
		return tSelectST;
	}
	public void settSelectST(boolean tSelectST) {
		this.tSelectST = tSelectST;
	}
	public boolean istStatus() {
		return tStatus;
	}
	public void settStatus(boolean tStatus) {
		this.tStatus = tStatus;
	}
	public boolean istEnable() {
		return tEnable;
	}
	public void settEnable(boolean tEnable) {
		this.tEnable = tEnable;
	}
	public String gettSetDatetime() {
		return tSetDatetime;
	}
	public void settSetDatetime(String tSetDatetime) {
		this.tSetDatetime = tSetDatetime;
	}
    
    
    
}
