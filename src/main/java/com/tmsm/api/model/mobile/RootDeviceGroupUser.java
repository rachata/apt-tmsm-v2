package com.tmsm.api.model.mobile;

import java.util.List;

import com.tmsm.api.model.Status;

public class RootDeviceGroupUser  extends Status{

	private List<DeviceGroupUser> data;

	public List<DeviceGroupUser> getData() {
		return data;
	}

	public void setData(List<DeviceGroupUser> data) {
		this.data = data;
	}
	
	
	
}
