package com.tmsm.api.model.mobile;

import java.util.List;

import com.tmsm.api.model.Status;

public class ResThingMap extends Status{

	private List<ThingMap> data;

	public List<ThingMap> getData() {
		return data;
	}

	public void setData(List<ThingMap> data) {
		this.data = data;
	}
	
	
}
