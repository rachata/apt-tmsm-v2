package com.tmsm.api.model.mobile;

public class ThingMapArea {

	private String thingName;
	private int thingID;
	private double offsetX;
	private double offsetY;
	private String typeName;
	
	public ThingMapArea() {}
	public String getThingName() {
		return thingName;
	}
	public void setThingName(String thingName) {
		this.thingName = thingName;
	}
	public int getThingID() {
		return thingID;
	}
	public void setThingID(int thingID) {
		this.thingID = thingID;
	}
	public double getOffsetX() {
		return offsetX;
	}
	public void setOffsetX(double offsetX) {
		this.offsetX = offsetX;
	}
	public double getOffsetY() {
		return offsetY;
	}
	public void setOffsetY(double offsetY) {
		this.offsetY = offsetY;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	
	
}
