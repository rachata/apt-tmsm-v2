package com.tmsm.api.model.mobile;

public class GroupDevice {

		private int idGroup;
		private String nameGroup;
		private String createGroup;
		private int deviceValue;
		
		public int getIdGroup() {
			return idGroup;
		}
		public void setIdGroup(int idGroup) {
			this.idGroup = idGroup;
		}
		public String getNameGroup() {
			return nameGroup;
		}
		public void setNameGroup(String nameGroup) {
			this.nameGroup = nameGroup;
		}
		public String getCreateGroup() {
			return createGroup;
		}
		public void setCreateGroup(String createGroup) {
			this.createGroup = createGroup;
		}
		public int getDeviceValue() {
			return deviceValue;
		}
		public void setDeviceValue(int deviceValue) {
			this.deviceValue = deviceValue;
		}
		
		
}
