package com.tmsm.api.model.mobile;

import java.util.List;

import com.tmsm.api.model.Status;



public class Things  extends Status{


	private List<Thing> things;

	
	public List<Thing> getThings() {
		return things;
	}

	public void setThings(List<Thing> things) {
		this.things = things;
	}
	
}
