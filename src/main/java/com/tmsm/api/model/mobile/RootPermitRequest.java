package com.tmsm.api.model.mobile;

import java.util.List;

import com.tmsm.api.model.Status;

public class RootPermitRequest extends Status {

	
	private List<PermitRequest> data;

	public List<PermitRequest> getData() {
		return data;
	}

	public void setData(List<PermitRequest> data) {
		this.data = data;
	}
	
	
	
}
