package com.tmsm.api.model.mobile;

public class Device {
	
	private Integer deviceID;
	private Integer deviceThing;
	private Integer deviceCH;
	private Integer devicePriority;
	private Integer deviceVisibility;
	private Integer detailID;
	private String detailName;
	private Integer detailDevice;
	private String detailPrivilege;
	private Integer detailArea;
	private String namePriority;
	private String nameVisibility;
	private String nameArea;
	private Integer positionID;
	private String positionName;
	private String image;
	
	private double offsetX;
	private double offsetY;
	
	
	


	
	public double getOffsetX() {
		return offsetX;
	}
	public void setOffsetX(double offsetX) {
		this.offsetX = offsetX;
	}
	public double getOffsetY() {
		return offsetY;
	}
	public void setOffsetY(double offsetY) {
		this.offsetY = offsetY;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public Integer getPositionID() {
		return positionID;
	}
	public void setPositionID(Integer positionID) {
		this.positionID = positionID;
	}
	public String getPositionName() {
		return positionName;
	}
	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}
	public String getNamePriority() {
		return namePriority;
	}
	public void setNamePriority(String namePriority) {
		this.namePriority = namePriority;
	}
	public String getNameVisibility() {
		return nameVisibility;
	}
	public void setNameVisibility(String nameVisibility) {
		this.nameVisibility = nameVisibility;
	}
	public String getNameArea() {
		return nameArea;
	}
	public void setNameArea(String nameArea) {
		this.nameArea = nameArea;
	}
	public Device(){}
	public Integer getDeviceID() {
		return deviceID;
	}
	public void setDeviceID(Integer deviceID) {
		this.deviceID = deviceID;
	}
	public Integer getDeviceThing() {
		return deviceThing;
	}
	public void setDeviceThing(Integer deviceThing) {
		this.deviceThing = deviceThing;
	}
	public Integer getDeviceCH() {
		return deviceCH;
	}
	public void setDeviceCH(Integer deviceCH) {
		this.deviceCH = deviceCH;
	}
	public Integer getDevicePriority() {
		return devicePriority;
	}
	public void setDevicePriority(Integer devicePriority) {
		this.devicePriority = devicePriority;
	}
	public Integer getDeviceVisibility() {
		return deviceVisibility;
	}
	public void setDeviceVisibility(Integer deviceVisibility) {
		this.deviceVisibility = deviceVisibility;
	}
	public Integer getDetailID() {
		return detailID;
	}
	public void setDetailID(Integer detailID) {
		this.detailID = detailID;
	}
	public String getDetailName() {
		return detailName;
	}
	public void setDetailName(String detailName) {
		this.detailName = detailName;
	}
	public Integer getDetailDevice() {
		return detailDevice;
	}
	public void setDetailDevice(Integer detailDevice) {
		this.detailDevice = detailDevice;
	}
	public String getDetailPrivilege() {
		return detailPrivilege;
	}
	public void setDetailPrivilege(String detailPrivilege) {
		this.detailPrivilege = detailPrivilege;
	}
	public Integer getDetailArea() {
		return detailArea;
	}
	public void setDetailArea(Integer detailArea) {
		this.detailArea = detailArea;
	}
	
	
	

}
