package com.tmsm.api.model.mobile;

import java.util.List;

import com.tmsm.api.model.Status;

public class ReqAdminArea extends Status {

	private List<AdminArea> data;

	public List<AdminArea> getData() {
		return data;
	}

	public void setData(List<AdminArea> data) {
		this.data = data;
	}
	
	
	
}
