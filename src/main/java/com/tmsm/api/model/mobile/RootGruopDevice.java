package com.tmsm.api.model.mobile;

import java.util.List;

import com.tmsm.api.model.Status;

public class RootGruopDevice  extends Status{

	
	List<GroupDevice> groups;

	public List<GroupDevice> getGroups() {
		return groups;
	}

	public void setGroups(List<GroupDevice> groups) {
		this.groups = groups;
	}
	
	
}
