package com.tmsm.api.model.mobile;

import java.util.List;

public class ThingMap {

	private String image;
	private String rootArea;
	private String area;
	private double lat;
	private double lng;
	private List<ThingMapArea> things;
	
	
	
	
	public List<ThingMapArea> getThings() {
		return things;
	}
	public void setThings(List<ThingMapArea> things) {
		this.things = things;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getRootArea() {
		return rootArea;
	}
	public void setRootArea(String rootArea) {
		this.rootArea = rootArea;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}
	
	
	
	
}
