package com.tmsm.api.model.mobile;

import java.util.List;

import com.tmsm.api.model.Status;

public class ResThingsPosition extends Status {
	private List<ThingsPosition> datas = null;

	public List<ThingsPosition> getDatas() {
		return datas;
	}

	public void setDatas(List<ThingsPosition> datas) {
		this.datas = datas;
	}
	
}
