package com.tmsm.api.model.mobile;

public class ReqAreaThings {
	private String thingsImage;
	private Double thingsOffsetX;
	private Double thingsOffsetY;
	private Integer thingsID;
	
	public Integer getThingsID() {
		return thingsID;
	}
	public void setThingsID(Integer thingsID) {
		this.thingsID = thingsID;
	}
	public String getThingsImage() {
		return thingsImage;
	}
	public void setThingsImage(String thingsImage) {
		this.thingsImage = thingsImage;
	}
	public Double getThingsOffsetX() {
		return thingsOffsetX;
	}
	public void setThingsOffsetX(Double thingsOffsetX) {
		this.thingsOffsetX = thingsOffsetX;
	}
	public Double getThingsOffsetY() {
		return thingsOffsetY;
	}
	public void setThingsOffsetY(Double thingsOffsetY) {
		this.thingsOffsetY = thingsOffsetY;
	}
	
	
}
