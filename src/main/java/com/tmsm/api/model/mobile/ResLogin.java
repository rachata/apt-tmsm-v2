package com.tmsm.api.model.mobile;

import java.util.List;

import com.tmsm.api.model.Status;

public class ResLogin extends Status {
	
	private int idUser;
	private int levelPrivilege;
	
	private String accessToken;
	
	private List<Integer> adminArea;
	

	public List<Integer> getAdminArea() {
		return adminArea;
	}

	public void setAdminArea(List<Integer> adminArea) {
		this.adminArea = adminArea;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public int getLevelPrivilege() {
		return levelPrivilege;
	}

	public void setLevelPrivilege(int levelPrivilege) {
		this.levelPrivilege = levelPrivilege;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	
	

}

