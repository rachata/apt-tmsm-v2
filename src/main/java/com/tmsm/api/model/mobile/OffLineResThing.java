package com.tmsm.api.model.mobile;

import java.util.List;

import com.tmsm.api.model.Status;

public class OffLineResThing extends Status {
	private int thingID;
	private String thingName;
	private String thingIP;
	private String thingMAC;
	
	
	
	private List<OfflineResDevice> device;
	
	
	
	public String getThingMAC() {
		return thingMAC;
	}
	public void setThingMAC(String thingMAC) {
		this.thingMAC = thingMAC;
	}
	public List<OfflineResDevice> getDevice() {
		return device;
	}
	public void setDevice(List<OfflineResDevice> device) {
		this.device = device;
	}
	public int getThingID() {
		return thingID;
	}
	public void setThingID(int thingID) {
		this.thingID = thingID;
	}
	public String getThingName() {
		return thingName;
	}
	public void setThingName(String thingName) {
		this.thingName = thingName;
	}
	public String getThingIP() {
		return thingIP;
	}
	public void setThingIP(String thingIP) {
		this.thingIP = thingIP;
	}

	
}
