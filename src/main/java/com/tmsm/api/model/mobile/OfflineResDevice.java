package com.tmsm.api.model.mobile;

public class OfflineResDevice {

	private int permitDevice;
	private int permitStatus;
	private String permitPrivilage;
	private int permitGrantStatus;
	private String permitTimeMin;
	private String permitTimeMax;
	private int deviceCH;
	private int devicePriority;
	private int deviceVisibility;
	private String defName;
	public int getPermitDevice() {
		return permitDevice;
	}
	public void setPermitDevice(int permitDevice) {
		this.permitDevice = permitDevice;
	}
	public int getPermitStatus() {
		return permitStatus;
	}
	public void setPermitStatus(int permitStatus) {
		this.permitStatus = permitStatus;
	}
	public String getPermitPrivilage() {
		return permitPrivilage;
	}
	public void setPermitPrivilage(String permitPrivilage) {
		this.permitPrivilage = permitPrivilage;
	}
	public int getPermitGrantStatus() {
		return permitGrantStatus;
	}
	public void setPermitGrantStatus(int permitGrantStatus) {
		this.permitGrantStatus = permitGrantStatus;
	}
	public String getPermitTimeMin() {
		return permitTimeMin;
	}
	public void setPermitTimeMin(String permitTimeMin) {
		this.permitTimeMin = permitTimeMin;
	}
	public String getPermitTimeMax() {
		return permitTimeMax;
	}
	public void setPermitTimeMax(String permitTimeMax) {
		this.permitTimeMax = permitTimeMax;
	}
	public int getDeviceCH() {
		return deviceCH;
	}
	public void setDeviceCH(int deviceCH) {
		this.deviceCH = deviceCH;
	}
	public int getDevicePriority() {
		return devicePriority;
	}
	public void setDevicePriority(int devicePriority) {
		this.devicePriority = devicePriority;
	}
	public int getDeviceVisibility() {
		return deviceVisibility;
	}
	public void setDeviceVisibility(int deviceVisibility) {
		this.deviceVisibility = deviceVisibility;
	}
	public String getDefName() {
		return defName;
	}
	public void setDefName(String defName) {
		this.defName = defName;
	}
	
	
}
