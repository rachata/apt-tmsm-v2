package com.tmsm.api.model.mobile;

public class ReqTimer {

	private int tID;
	private int tUser;
	private int tDevice;
	private String tDatetimeMin;
	private String tDatetimeMax;
	private String tTimeMin;
	private String tTimeMax;
	private boolean tSelect;
	private boolean tStatus;
	private boolean tEnable;
	private String tSetDatetime;
	private boolean statusDevice;
	private String fullName;
	private String deviceName;
	public int gettID() {
		return tID;
	}
	public void settID(int tID) {
		this.tID = tID;
	}
	public int gettUser() {
		return tUser;
	}
	public void settUser(int tUser) {
		this.tUser = tUser;
	}
	public int gettDevice() {
		return tDevice;
	}
	public void settDevice(int tDevice) {
		this.tDevice = tDevice;
	}
	public String gettDatetimeMin() {
		return tDatetimeMin;
	}
	public void settDatetimeMin(String tDatetimeMin) {
		this.tDatetimeMin = tDatetimeMin;
	}
	public String gettDatetimeMax() {
		return tDatetimeMax;
	}
	public void settDatetimeMax(String tDatetimeMax) {
		this.tDatetimeMax = tDatetimeMax;
	}
	public String gettTimeMin() {
		return tTimeMin;
	}
	public void settTimeMin(String tTimeMin) {
		this.tTimeMin = tTimeMin;
	}
	public String gettTimeMax() {
		return tTimeMax;
	}
	public void settTimeMax(String tTimeMax) {
		this.tTimeMax = tTimeMax;
	}
	public boolean istSelect() {
		return tSelect;
	}
	public void settSelect(boolean tSelect) {
		this.tSelect = tSelect;
	}
	public boolean istStatus() {
		return tStatus;
	}
	public void settStatus(boolean tStatus) {
		this.tStatus = tStatus;
	}
	public boolean istEnable() {
		return tEnable;
	}
	public void settEnable(boolean tEnable) {
		this.tEnable = tEnable;
	}
	public String gettSetDatetime() {
		return tSetDatetime;
	}
	public void settSetDatetime(String tSetDatetime) {
		this.tSetDatetime = tSetDatetime;
	}
	public boolean isStatusDevice() {
		return statusDevice;
	}
	public void setStatusDevice(boolean statusDevice) {
		this.statusDevice = statusDevice;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	
	
	
	
}

