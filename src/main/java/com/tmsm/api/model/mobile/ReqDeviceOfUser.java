package com.tmsm.api.model.mobile;

public class ReqDeviceOfUser {

	private int defID;
	private String defName;
	
	public ReqDeviceOfUser() {}

	public int getDefID() {
		return defID;
	}

	public void setDefID(int defID) {
		this.defID = defID;
	}

	public String getDefName() {
		return defName;
	}

	public void setDefName(String defName) {
		this.defName = defName;
	}
	
	
}
