package com.tmsm.api.model.mobile;

import java.util.List;

import com.tmsm.api.model.Status;

public class RootReqTimer extends  Status{

	private List<ReqTimer> data;

	public List<ReqTimer> getData() {
		return data;
	}

	public void setData(List<ReqTimer> data) {
		this.data = data;
	}
	
	
}

