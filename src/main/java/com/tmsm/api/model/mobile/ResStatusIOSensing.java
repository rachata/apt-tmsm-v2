package com.tmsm.api.model.mobile;

import java.util.List;

import com.tmsm.api.model.Status;

public class ResStatusIOSensing extends Status {
	private List<StatusIOSensing> data;

	public List<StatusIOSensing> getData() {
		return data;
	}

	public void setData(List<StatusIOSensing> data) {
		this.data = data;
	}
}
