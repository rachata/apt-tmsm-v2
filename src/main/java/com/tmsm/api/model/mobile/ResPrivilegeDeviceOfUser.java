package com.tmsm.api.model.mobile;

import com.tmsm.api.model.Status;

public class ResPrivilegeDeviceOfUser extends Status {

	private String privilege;
	private String statusPrivilege;
	private int grantID;
	private String grantName;
	private String timeMin;
	private String timeMax;
	public String getPrivilege() {
		return privilege;
	}
	public void setPrivilege(String privilege) {
		this.privilege = privilege;
	}
	public String getStatusPrivilege() {
		return statusPrivilege;
	}
	public void setStatusPrivilege(String statusPrivilege) {
		this.statusPrivilege = statusPrivilege;
	}
	public int getGrantID() {
		return grantID;
	}
	public void setGrantID(int grantID) {
		this.grantID = grantID;
	}
	public String getGrantName() {
		return grantName;
	}
	public void setGrantName(String grantName) {
		this.grantName = grantName;
	}
	public String getTimeMin() {
		return timeMin;
	}
	public void setTimeMin(String timeMin) {
		this.timeMin = timeMin;
	}
	public String getTimeMax() {
		return timeMax;
	}
	public void setTimeMax(String timeMax) {
		this.timeMax = timeMax;
	}
	
	
	
	
	
}
