package com.tmsm.api.model.mobile;

import com.tmsm.api.model.Status;

public class PermitDeviceOfUser  {

	
	private int defID;
	private int defDevice;
	private String defName;
	private int deviceESP;
	private int deviceCH;
	private String detailPrivilege;
	private int permitStatus;
	private String permitPrivilage;
	
	private int type;
	
	
	
	
	 public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getPermitPrivilage() {
		return permitPrivilage;
	}

	public void setPermitPrivilage(String permitPrivilage) {
		this.permitPrivilage = permitPrivilage;
	}
	private int areaID;

	    public int getAreaID() {
	        return areaID;
	    }

	    public void setAreaID(int areaID) {
	        this.areaID = areaID;
	    }
	
	
	public int getPermitStatus() {
		return permitStatus;
	}
	public void setPermitStatus(int permitStatus) {
		this.permitStatus = permitStatus;
	}
	public int getDefID() {
		return defID;
	}
	public void setDefID(int defID) {
		this.defID = defID;
	}
	public int getDefDevice() {
		return defDevice;
	}
	public void setDefDevice(int defDevice) {
		this.defDevice = defDevice;
	}
	public String getDefName() {
		return defName;
	}
	public void setDefName(String defName) {
		this.defName = defName;
	}
	public int getDeviceESP() {
		return deviceESP;
	}
	public void setDeviceESP(int deviceESP) {
		this.deviceESP = deviceESP;
	}
	public int getDeviceCH() {
		return deviceCH;
	}
	public void setDeviceCH(int deviceCH) {
		this.deviceCH = deviceCH;
	}
	public String getDetailPrivilege() {
		return detailPrivilege;
	}
	public void setDetailPrivilege(String detailPrivilege) {
		this.detailPrivilege = detailPrivilege;
	}
	
	
	
	
}
