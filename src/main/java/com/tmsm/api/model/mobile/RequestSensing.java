package com.tmsm.api.model.mobile;

public class RequestSensing {

	private int settingID;
    private int devicesIO ;		
    private int devicesSensor; 
    private int settingUser ;
    private double settingMin; 
    private double settingMax ;
    private boolean settingStatus;
    private boolean settingEnable;
    private  double settingDatas; 
    private String settingDate;
    private boolean settingSelect;
    private boolean settingIOStatus;
	public int getDevicesIO() {
		return devicesIO;
	}
	public void setDevicesIO(int devicesIO) {
		this.devicesIO = devicesIO;
	}
	public int getDevicesSensor() {
		return devicesSensor;
	}
	public void setDevicesSensor(int devicesSensor) {
		this.devicesSensor = devicesSensor;
	}
	public int getSettingUser() {
		return settingUser;
	}
	public void setSettingUser(int settingUser) {
		this.settingUser = settingUser;
	}
	public double getSettingMin() {
		return settingMin;
	}
	public void setSettingMin(double settingMin) {
		this.settingMin = settingMin;
	}
	public double getSettingMax() {
		return settingMax;
	}
	public void setSettingMax(double settingMax) {
		this.settingMax = settingMax;
	}
	public double getSettingDatas() {
		return settingDatas;
	}
	public void setSettingDatas(double settingDatas) {
		this.settingDatas = settingDatas;
	}
	public boolean isSettingSelect() {
		return settingSelect;
	}
	public void setSettingSelect(boolean settingSelect) {
		this.settingSelect = settingSelect;
	}
	public boolean isSettingIOStatus() {
		return settingIOStatus;
	}
	public void setSettingIOStatus(boolean settingIOStatus) {
		this.settingIOStatus = settingIOStatus;
	}
	public int getSettingID() {
		return settingID;
	}
	public void setSettingID(int settingID) {
		this.settingID = settingID;
	}
	public boolean isSettingStatus() {
		return settingStatus;
	}
	public void setSettingStatus(boolean settingStatus) {
		this.settingStatus = settingStatus;
	}
	public boolean isSettingEnable() {
		return settingEnable;
	}
	public void setSettingEnable(boolean settingEnable) {
		this.settingEnable = settingEnable;
	}
	public String getSettingDate() {
		return settingDate;
	}
	public void setSettingDate(String settingDate) {
		this.settingDate = settingDate;
	}
	
	
    
    
}
