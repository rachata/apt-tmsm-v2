package com.tmsm.api.model.mobile;

import java.util.List;

import com.tmsm.api.model.Status;

public class OffLineResRoot extends Status {

	private List<OffLineResThing> data;

	public List<OffLineResThing> getData() {
		return data;
	}

	public void setData(List<OffLineResThing> data) {
		this.data = data;
	}
	
	
	
}
