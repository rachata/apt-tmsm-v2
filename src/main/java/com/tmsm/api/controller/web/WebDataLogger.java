package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.web.Datalogger;
import com.tmsm.api.model.web.ListDatalogger;
import com.tmsm.api.utility.Database;

@CrossOrigin
@RestController
@RequestMapping("web/log")
public class WebDataLogger {
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/door/{door_id}", method = RequestMethod.GET)
	public ResponseEntity<ListDatalogger> getDoorlog(@PathVariable("door_id") int door_id){
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ResultSet set = null;
		ListDatalogger datalogger = new ListDatalogger();
		
		try {
			String sql = "select door_log_id, door_log_status, door_type, door_log_datetime, user_detail_fname, user_detail_lname, things_nickname \r\n" + 
					"					from door_log left join user_detail on user_detail_id = door_log_user\r\n" + 
					"					inner join things on things_id = door_log_things\r\n" + 
					"					inner join door_status_type on door_st = door_log_status where door_log_things = ?";
			
			statement = con.prepareStatement(sql);
			statement.setInt(1, door_id);
			set = statement.executeQuery();
			
			List<Datalogger> list = new ArrayList<Datalogger>();
			while(set.next()) {
				Datalogger logger = new Datalogger();
				logger.setLog_id(set.getInt(1));
				logger.setLog_status(set.getInt(2));
				logger.setLog_name(set.getString(3));
				logger.setLog_create(set.getString(4));
				logger.setFull_name(set.getString(5) + " " + set.getString(6));
				logger.setLog_devices(set.getString(7));
				
				list.add(logger);
			}
			
			datalogger.setCodeStatus(200);
			datalogger.setNameStatus("Success");
			datalogger.setDataloggers(list);
			
		} catch (SQLException e) {
			datalogger.setCodeStatus(500);
			datalogger.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(datalogger);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/door/all", method = RequestMethod.GET)
	public ResponseEntity<ListDatalogger> getDoorlogAll(){
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ResultSet set = null;
		ListDatalogger datalogger = new ListDatalogger();
		
		try {
			String sql = "select door_log_id, door_log_status, door_type, door_log_datetime, user_detail_fname, user_detail_lname, things_nickname \r\n" + 
					"					from door_log left join user_detail on user_detail_id = door_log_user\r\n" + 
					"					inner join things on things_id = door_log_things\r\n" + 
					"					inner join door_status_type on door_st = door_log_status";
			
			statement = con.prepareStatement(sql);
			set = statement.executeQuery();
			
			List<Datalogger> list = new ArrayList<Datalogger>();
			while(set.next()) {
				Datalogger logger = new Datalogger();
				logger.setLog_id(set.getInt(1));
				logger.setLog_status(set.getInt(2));
				logger.setLog_name(set.getString(3));
				logger.setLog_create(set.getString(4));
				logger.setFull_name(set.getString(5) + " " + set.getString(6));
				logger.setLog_devices(set.getString(7));
				
				list.add(logger);
			}
			
			datalogger.setCodeStatus(200);
			datalogger.setNameStatus("Success");
			datalogger.setDataloggers(list);
			
		} catch (SQLException e) {
			datalogger.setCodeStatus(500);
			datalogger.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(datalogger);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/devices/{devices_id}", method = RequestMethod.GET)
	public ResponseEntity<ListDatalogger> getDeviceslog(@PathVariable("devices_id") int devices_id){
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ResultSet set = null;
		ListDatalogger datalogger = new ListDatalogger();
		
		try {
			String sql = "select log_id, log_status, log_datetime, name_status, detail_name, user_detail_fname, user_detail_lname\r\n" + 
					"							from device_log_io left join user_detail on user_detail_id = log_user\r\n" + 
					"							inner join devices_detail on detail_device = log_device\r\n" + 
					"                            inner join status_io on id_status = log_status where detail_device = ?";
			
			statement = con.prepareStatement(sql);
			statement.setInt(1, devices_id);
			set = statement.executeQuery();
			
			List<Datalogger> list = new ArrayList<Datalogger>();
			while(set.next()) {
				Datalogger logger = new Datalogger();
				logger.setLog_id(set.getInt(1));
				logger.setLog_status(set.getInt(2));
				logger.setLog_name(set.getString("name_status"));
				logger.setLog_create(set.getString("log_datetime"));
				logger.setFull_name(set.getString("user_detail_fname") + " " + set.getString("user_detail_lname"));
				logger.setLog_devices(set.getString("detail_name"));
				
				list.add(logger);
			}
			
			datalogger.setCodeStatus(200);
			datalogger.setNameStatus("Success");
			datalogger.setDataloggers(list);
			
		} catch (SQLException e) {
			datalogger.setCodeStatus(500);
			datalogger.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(datalogger);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/devices/all", method = RequestMethod.GET)
	public ResponseEntity<ListDatalogger> getDeviceslogAll(){
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ResultSet set = null;
		ListDatalogger datalogger = new ListDatalogger();
		
		try {
			String sql = "select log_id, log_status, log_datetime, name_status, detail_name, user_detail_fname, user_detail_lname\r\n" + 
					"							from device_log_io left join user_detail on user_detail_id = log_user\r\n" + 
					"							inner join devices_detail on detail_device = log_device\r\n" + 
					"                            inner join status_io on id_status = log_status";
			
			statement = con.prepareStatement(sql);
			set = statement.executeQuery();
			
			List<Datalogger> list = new ArrayList<Datalogger>();
			while(set.next()) {
				Datalogger logger = new Datalogger();
				logger.setLog_id(set.getInt(1));
				logger.setLog_status(set.getInt(2));
				logger.setLog_name(set.getString("name_status"));
				logger.setLog_create(set.getString("log_datetime"));
				logger.setFull_name(set.getString("user_detail_fname") + " " + set.getString("user_detail_lname"));
				logger.setLog_devices(set.getString("detail_name"));
				
				list.add(logger);
			}
			
			datalogger.setCodeStatus(200);
			datalogger.setNameStatus("Success");
			datalogger.setDataloggers(list);
			
		} catch (SQLException e) {
			datalogger.setCodeStatus(500);
			datalogger.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(datalogger);
		}
	}
}
