package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.Status;
import com.tmsm.api.model.web.ProvidePositionModel;
import com.tmsm.api.model.web.ProvidePrivilegeModel;
import com.tmsm.api.utility.Database;
import com.tmsm.model.strong.Privilege;

@RestController
@RequestMapping("/provide")
public class ProvideofMember {

	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/position",method = RequestMethod.PUT)
	public ResponseEntity<Status> providePosotion(@RequestBody ProvidePositionModel position) {
		Status status = new Status();
		Connection con = Database.connectDatabase();
		PreparedStatement statement = null;
		
		try {
			String sql ="update user_detail set user_detail_position = ? where user_detail_id = ?";
			statement = con.prepareStatement(sql);
			statement.setInt(1, position.getPosition());
			statement.setInt(2, position.getUserId());
			
			boolean st = statement.execute();
			
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");
			}
		} catch (SQLException e) {
			   status.setCodeStatus(500);
			   status.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/privilege", method = RequestMethod.PUT)
	public ResponseEntity<Status> providePivilege(@RequestBody ProvidePrivilegeModel privilege ) {
		Status status = new Status();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "update user set privilege = ? where usr_id = ?";
			statement = con.prepareStatement(sql);
			statement.setInt(1, privilege.getPrivilege());
			statement.setInt(2, privilege.getUsrId());
			
			boolean st = statement.execute();
			
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			} else {
				status.setCodeStatus(204);
				status.setNameStatus("Un Success");
			}
			
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
}
