package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.utility.Database;
import com.tmsm.model.strong.GrantStatus;
import com.tmsm.model.strong.ListGrantStatus;

@RestController
@RequestMapping("/grantstatus")
public class ResGrantStatus {

	@CrossOrigin
	@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE,method = RequestMethod.GET)
	public ResponseEntity<ListGrantStatus> getGrantStatus(){
		ListGrantStatus grantStatus = new ListGrantStatus();
		
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ResultSet resultSet = null;
		
		try {
			String sql = "select*from grant_status";
			
			statement = con.prepareStatement(sql);
			resultSet = statement.executeQuery();
			
			List<GrantStatus> listGrant = new ArrayList<GrantStatus>();
			while(resultSet.next()) {
				GrantStatus status = new GrantStatus();
				status.setGrantId(resultSet.getInt(1));
				status.setGrantName(resultSet.getString(2));
				
				listGrant.add(status);
			}
			
			grantStatus.setCodeStatus(200);
			grantStatus.setGrantStatus(listGrant);
			grantStatus.setNameStatus("Success");
			
		} catch (SQLException e) {
			grantStatus.setCodeStatus(500);
			grantStatus.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(grantStatus);
		}
	}
}
