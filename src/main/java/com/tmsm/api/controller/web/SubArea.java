package com.tmsm.api.controller.web;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.Status;
import com.tmsm.api.model.web.DetailArea;
import com.tmsm.api.model.web.ResDetailArea;
import com.tmsm.api.utility.Database;

@RestController
@RequestMapping("/subarea")
public class SubArea {

	@CrossOrigin
	@RequestMapping(value = "/{area_id}", method = RequestMethod.GET)
	public ResponseEntity<ResDetailArea> getSubArea(@PathVariable(value = "area_id") Integer area_id) {

		ResDetailArea resDetail = new ResDetailArea();
		PreparedStatement prepared = null;
		ResultSet result = null;
		Connection con = Database.connectDatabase();
		try {

			String sql = "select * from area_of_devices where area_of_device_id = ?";
			prepared = con.prepareStatement(sql);
			prepared.setInt(1, area_id);
			result = prepared.executeQuery();

			while(result.next()) {

				String resSql = "select * from detail_area where dear_area = ? and dear_status = 1";
				prepared = con.prepareStatement(resSql);
				prepared.setInt(1,result.getInt(1));

				ResultSet res = prepared.executeQuery();

				List<DetailArea> listDetail = new ArrayList<DetailArea>();
				
				while (res.next()) {

					DetailArea detail = new DetailArea();

					detail.setDetailID(res.getInt(1));
					detail.setDetailName(res.getString(2));
					detail.setDetailImage(res.getString(3));
					detail.setDetailArea(res.getInt(4));

					listDetail.add(detail);

				}		

				resDetail.setCodeStatus(200);
				resDetail.setNameStatus("Success");
				resDetail.setAreaID(result.getInt(1));
				resDetail.setAreaName(result.getString(2));
				resDetail.setDetail(listDetail);
			}
			
				Database.closeConnection(con);
				return ResponseEntity.ok().body(resDetail);
			
		} catch (SQLException ex) {

			resDetail.setCodeStatus(400);
			resDetail.setNameStatus(ex.getMessage());

			Database.closeConnection(con);
			return ResponseEntity.ok().body(resDetail);

		}

	}
	

	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Status> updateArea(@RequestBody DetailArea detail){
		
		Status status = new Status();
		PreparedStatement preparedSTM = null;
		Connection con = Database.connectDatabase();
		
		try {
			
			String sql = "update detail_area set dear_name = ?,dear_image = ? where dear_id = ?";
			
			preparedSTM = con.prepareStatement(sql);
			preparedSTM.setString(1, detail.getDetailName());
			preparedSTM.setString(2, detail.getDetailImage());
			preparedSTM.setInt(3, detail.getDetailID());
			
			boolean statusPrepared = preparedSTM.execute();
			
			if(!statusPrepared) {
				status.setCodeStatus(200);
				status.setNameStatus("Update Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Update Unsuccess");
			}
			
		} catch (SQLException ex) {
			status.setCodeStatus(500);
			status.setNameStatus(ex.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}		
	}

	
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
	public ResponseEntity<Status> deleteArea(@PathVariable(value = "id") Integer id){
		
		Status status = new Status();
		PreparedStatement prepared = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "delete from detail_area where dear_id = ?";
			prepared = con.prepareStatement(sql);
			prepared.setInt(1, id);
			
			boolean statusDelete = prepared.execute();
			
			if(!statusDelete) {
				status.setCodeStatus(200);
				status.setNameStatus("Delete Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Delete Unsuccess");
			}
			
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}		
	}
	
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/delete/{area_id}", method = RequestMethod.DELETE)
	public ResponseEntity<Status> deleteSubArea(@PathVariable("area_id") int area_id){
		Status status = new Status();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "update detail_area set dear_status = 0 where dear_id = ?";
			statement = con.prepareStatement(sql);
			statement.setInt(1, area_id);
			
			boolean st = statement.execute();
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Delete Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Delete Unsuccess");
			}
		}catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Status> createArea(@RequestBody DetailArea detail){
		
		Status status = new Status();
		PreparedStatement prepared = null;
		Connection con = Database.connectDatabase();		
		
		try {
			
			String sql = "insert into detail_area (dear_name,dear_image,dear_area) values (?,?,?)";
			
			prepared = con.prepareStatement(sql);
			prepared.setString(1,detail.getDetailName());
			prepared.setString(2, detail.getDetailImage());
			prepared.setInt(3, detail.getDetailArea());
			
			boolean statusCreate = prepared.execute();
			
			if(!statusCreate) {
				status.setCodeStatus(200);
				status.setNameStatus("Create Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Create Unsuccess");
			}
			
		} catch (SQLException ex) {
			 status.setCodeStatus(500);
			 status.setNameStatus(ex.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
		
	}
	
}
