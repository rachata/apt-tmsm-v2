package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.Status;
import com.tmsm.api.model.device.ReqPermitPrivilage;
import com.tmsm.api.model.mobile.PermitRequest;
import com.tmsm.api.model.mobile.RootPermitRequest;
import com.tmsm.api.utility.Database;

@CrossOrigin
@RestController
@RequestMapping("webpermittion")
public class WebPermittion {
	@SuppressWarnings("finally")
	@RequestMapping(value = "/permit/{area_id}" , method = RequestMethod.GET)
	@CrossOrigin
	public ResponseEntity<RootPermitRequest> permitRequset(@PathVariable("area_id") int area_id){	
		Connection  con = Database.connectDatabase();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;		
		RootPermitRequest rootPermitRequest = new RootPermitRequest();
		try {
			
			String sql = "select permit_id , permit_user , permit_status , permit_privilage , \n" + 
					"permit_grant_status , permit_timemin , permit_timemax , user_detail_fname , user_detail_lname , detail_name , permit_device , things_type from permit_user\n" + 
					"inner join user_detail on user_detail_id = permit_user \n" + 
					"inner join devices_detail on permit_device = detail_device\n" + 
					"inner join devices on permit_device = device_id\n" + 
					"inner join things on things_id = device_esp\n" + 
					"inner join detail_area on dear_id = things_area \n" +
					"inner join area_of_devices on area_of_device_id = dear_area \n" +
					"where permit_status = 0  and enabled = 1 and area_of_device_id = ?";		
			
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, area_id);
			resultSet = preparedStatement.executeQuery();
			List<PermitRequest> list = new  ArrayList<PermitRequest>();
			while(resultSet.next()) {
				PermitRequest permitRequest = new PermitRequest();
				
				permitRequest.setPermitID(resultSet.getInt(1));
				permitRequest.setPermitUser(resultSet.getInt(2));
				permitRequest.setPermitStatus(resultSet.getInt(3));
				permitRequest.setPermitPrivilage(resultSet.getString(4));
				permitRequest.setPermitGrantStatus(resultSet.getInt(5));
				permitRequest.setPermitTimeMin(resultSet.getString(6));
				permitRequest.setPermitTimeMax(resultSet.getString(7));
				permitRequest.setUserFName(resultSet.getString(8));
				permitRequest.setUserLName(resultSet.getString(9));
				permitRequest.setDeviceName(resultSet.getString(10));
				permitRequest.setPermitDevice(resultSet.getInt(11));
				permitRequest.setType(resultSet.getInt("things_type"));
				
				list.add(permitRequest);
					
			}
			
			rootPermitRequest.setData(list);			
			rootPermitRequest.setCodeStatus(200);
			rootPermitRequest.setNameStatus("Success");
			
		} catch (SQLException e) {
			
			rootPermitRequest.setCodeStatus(500);
			rootPermitRequest.setNameStatus("unsuccess");
		}finally {
			return ResponseEntity.ok().body(rootPermitRequest);
		}
		
	}
	
	@SuppressWarnings("finally")
	@RequestMapping(value = "/permit" , method = RequestMethod.GET)
	@CrossOrigin
	public ResponseEntity<RootPermitRequest> permitRequsetAdmin(){	
		Connection  con = Database.connectDatabase();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;		
		RootPermitRequest rootPermitRequest = new RootPermitRequest();
		try {
			
			String sql = "select permit_id , permit_user , permit_status , permit_privilage , \n" + 
					"permit_grant_status , permit_timemin , permit_timemax , user_detail_fname , user_detail_lname , detail_name , permit_device , things_type from permit_user\n" + 
					"inner join user_detail on user_detail_id = permit_user \n" + 
					"inner join devices_detail on permit_device = detail_device\n" + 
					"inner join devices on permit_device = device_id\n" + 
					"inner join things on things_id = device_esp\n" + 
					"inner join detail_area on dear_id = things_area \n" +
					"inner join area_of_devices on area_of_device_id = dear_area \n" +
					"where permit_status = 0  and enabled = 1";		
			
			preparedStatement = con.prepareStatement(sql);
			resultSet = preparedStatement.executeQuery();
			List<PermitRequest> list = new  ArrayList<PermitRequest>();
			while(resultSet.next()) {
				PermitRequest permitRequest = new PermitRequest();
				
				permitRequest.setPermitID(resultSet.getInt(1));
				permitRequest.setPermitUser(resultSet.getInt(2));
				permitRequest.setPermitStatus(resultSet.getInt(3));
				permitRequest.setPermitPrivilage(resultSet.getString(4));
				permitRequest.setPermitGrantStatus(resultSet.getInt(5));
				permitRequest.setPermitTimeMin(resultSet.getString(6));
				permitRequest.setPermitTimeMax(resultSet.getString(7));
				permitRequest.setUserFName(resultSet.getString(8));
				permitRequest.setUserLName(resultSet.getString(9));
				permitRequest.setDeviceName(resultSet.getString(10));
				permitRequest.setPermitDevice(resultSet.getInt(11));
				permitRequest.setType(resultSet.getInt("things_type"));
				
				list.add(permitRequest);
					
			}
			
			rootPermitRequest.setData(list);			
			rootPermitRequest.setCodeStatus(200);
			rootPermitRequest.setNameStatus("Success");
			
		} catch (SQLException e) {
			
			rootPermitRequest.setCodeStatus(500);
			rootPermitRequest.setNameStatus("unsuccess");
		}finally {
			return ResponseEntity.ok().body(rootPermitRequest);
		}
		
	}
	
	@SuppressWarnings("finally")
	@RequestMapping(value = "/permit/allow/{area_id}" , method = RequestMethod.GET)
	@CrossOrigin
	public ResponseEntity<RootPermitRequest> permitRequsetAllow(@PathVariable("area_id") int area_id){	
		Connection  con = Database.connectDatabase();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;		
		RootPermitRequest rootPermitRequest = new RootPermitRequest();
		try {
			
			String sql = "select permit_id , permit_user , permit_status , permit_privilage , \n" + 
					"permit_grant_status , permit_timemin , permit_timemax , user_detail_fname , user_detail_lname , detail_name , permit_device , things_type from permit_user\n" + 
					"inner join user_detail on user_detail_id = permit_user \n" + 
					"inner join devices_detail on permit_device = detail_device\n" + 
					"inner join devices on permit_device = device_id\n" + 
					"inner join things on things_id = device_esp\n" + 
					"inner join detail_area on dear_id = things_area \n" +
					"inner join area_of_devices on area_of_device_id = dear_area \n" +
					"where permit_status = 1  and enabled = 1 and area_of_device_id = ?";		
			
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, area_id);
			resultSet = preparedStatement.executeQuery();
			List<PermitRequest> list = new  ArrayList<PermitRequest>();
			while(resultSet.next()) {
				PermitRequest permitRequest = new PermitRequest();
				
				permitRequest.setPermitID(resultSet.getInt(1));
				permitRequest.setPermitUser(resultSet.getInt(2));
				permitRequest.setPermitStatus(resultSet.getInt(3));
				permitRequest.setPermitPrivilage(resultSet.getString(4));
				permitRequest.setPermitGrantStatus(resultSet.getInt(5));
				permitRequest.setPermitTimeMin(resultSet.getString(6));
				permitRequest.setPermitTimeMax(resultSet.getString(7));
				permitRequest.setUserFName(resultSet.getString(8));
				permitRequest.setUserLName(resultSet.getString(9));
				permitRequest.setDeviceName(resultSet.getString(10));
				permitRequest.setPermitDevice(resultSet.getInt(11));
				permitRequest.setType(resultSet.getInt("things_type"));
				
				list.add(permitRequest);
					
			}
			
			rootPermitRequest.setData(list);			
			rootPermitRequest.setCodeStatus(200);
			rootPermitRequest.setNameStatus("Success");
			
		} catch (SQLException e) {
			
			rootPermitRequest.setCodeStatus(500);
			rootPermitRequest.setNameStatus("unsuccess");
		}finally {
			return ResponseEntity.ok().body(rootPermitRequest);
		}
		
	}
	
	@SuppressWarnings("finally")
	@RequestMapping(value = "/permit/allow" , method = RequestMethod.GET)
	@CrossOrigin
	public ResponseEntity<RootPermitRequest> permitRequsetAllowAdmin(){	
		Connection  con = Database.connectDatabase();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;		
		RootPermitRequest rootPermitRequest = new RootPermitRequest();
		try {
			
			String sql = "select permit_id , permit_user , permit_status , permit_privilage , \n" + 
					"permit_grant_status , permit_timemin , permit_timemax , user_detail_fname , user_detail_lname , detail_name , permit_device , things_type from permit_user\n" + 
					"inner join user_detail on user_detail_id = permit_user \n" + 
					"inner join devices_detail on permit_device = detail_device\n" + 
					"inner join devices on permit_device = device_id\n" + 
					"inner join things on things_id = device_esp\n" + 
					"inner join detail_area on dear_id = things_area \n" +
					"inner join area_of_devices on area_of_device_id = dear_area \n" +
					"where permit_status = 1  and enabled = 1";		
			
			preparedStatement = con.prepareStatement(sql);
			resultSet = preparedStatement.executeQuery();
			List<PermitRequest> list = new  ArrayList<PermitRequest>();
			while(resultSet.next()) {
				PermitRequest permitRequest = new PermitRequest();
				
				permitRequest.setPermitID(resultSet.getInt(1));
				permitRequest.setPermitUser(resultSet.getInt(2));
				permitRequest.setPermitStatus(resultSet.getInt(3));
				permitRequest.setPermitPrivilage(resultSet.getString(4));
				permitRequest.setPermitGrantStatus(resultSet.getInt(5));
				permitRequest.setPermitTimeMin(resultSet.getString(6));
				permitRequest.setPermitTimeMax(resultSet.getString(7));
				permitRequest.setUserFName(resultSet.getString(8));
				permitRequest.setUserLName(resultSet.getString(9));
				permitRequest.setDeviceName(resultSet.getString(10));
				permitRequest.setPermitDevice(resultSet.getInt(11));
				permitRequest.setType(resultSet.getInt("things_type"));
				
				list.add(permitRequest);
					
			}
			
			rootPermitRequest.setData(list);			
			rootPermitRequest.setCodeStatus(200);
			rootPermitRequest.setNameStatus("Success");
			
		} catch (SQLException e) {
			
			rootPermitRequest.setCodeStatus(500);
			rootPermitRequest.setNameStatus("unsuccess");
		}finally {
			return ResponseEntity.ok().body(rootPermitRequest);
		}
		
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/permittion", method = RequestMethod.PUT)
	public ResponseEntity<Status> updatePermittion(@RequestBody ReqPermitPrivilage privilage){
		Status status = new Status();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "";
			boolean st = false;
			con.setAutoCommit(false);
			if(privilage.getPermitGrantStatus() == 1) {
				sql = "update permit_user set permit_status = ?, permit_privilage = ?, permit_grant_status = ? where permit_id = ?";
				statement = con.prepareStatement(sql);
				statement.setInt(1, privilage.getPermitStatus());
				statement.setString(2, privilage.getPrivilege());
				statement.setInt(3, privilage.getPermitGrantStatus());
				statement.setInt(4, privilage.getPermitID());
				st = statement.execute();
			}else {
				
				if(privilage.getPermitTimeMin() != null && privilage.getPermitTimeMax() != null) {
					sql = "update permit_user set permit_status = ?, permit_privilage = ?, permit_grant_status = ?, permit_timeMin = ?, permit_timeMax = ? where permit_id = ?";
					statement = con.prepareStatement(sql);
					statement.setInt(1, privilage.getPermitStatus());
					statement.setString(2, privilage.getPrivilege());
					statement.setInt(3, privilage.getPermitGrantStatus());
					statement.setString(4, privilage.getPermitTimeMin());
					statement.setString(5, privilage.getPermitTimeMax());
					statement.setInt(6, privilage.getPermitID());
					st = statement.execute();
				}else {
					sql = "update permit_user set permit_status = ?, permit_privilage = ?, permit_grant_status = ? where permit_id = ?";
					statement = con.prepareStatement(sql);
					statement.setInt(1, privilage.getPermitStatus());
					statement.setString(2, privilage.getPrivilege());
					statement.setInt(3, privilage.getPermitGrantStatus());
					statement.setInt(4, privilage.getPermitID());
					st = statement.execute();
				}
				
			}
			
			con.commit();
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");
			}
			
			
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/unpermit/{permit_id}", method = RequestMethod.DELETE)
	public ResponseEntity<Status> unEnablePermittion(@PathVariable("permit_id") int permit_id){
		Status status = new Status();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "update permit_user set enabled = 0 where permit_id = ?";
			statement = con.prepareStatement(sql);
			statement.setInt(1,permit_id);
			boolean st = statement.execute();
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");
			}
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
}
