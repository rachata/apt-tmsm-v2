package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.Status;
import com.tmsm.api.model.web.ResStrongPosition;
import com.tmsm.api.utility.Database;
import com.tmsm.model.strong.Position;

@RestController
@RequestMapping("/allposition")
public class StrongPosition {
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<ResStrongPosition>getAllPosition() {
		ResStrongPosition position = new ResStrongPosition();
		
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ResultSet set = null;
		
		try {
			String sql = "select * from position";
			
			statement = con.prepareStatement(sql);			
			set = statement.executeQuery();
			
			List<Position> positions = new ArrayList<Position>();
			while(set.next()) {
				Position pos = new Position();
				pos.setPositionId(set.getInt(1));
				pos.setPositionName(set.getString(2));
				
				positions.add(pos);
			}
			
			position.setCodeStatus(200);
			position.setNameStatus("Success");
			position.setPositions(positions);
			
		}catch (SQLException e) {
			position.setCodeStatus(500);
			position.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(position);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Status> createPosition(@RequestBody Position position) {
		PreparedStatement preparedStatement = null;
		Connection connection = Database.connectDatabase();
		Status status = new Status();
		
		try {
			String sql ="insert into position (position_name) value (?)";
			preparedStatement = connection.prepareStatement(sql);
			
			preparedStatement.setString(1, position.getPositionName());
			
			boolean st = preparedStatement.execute();
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}
		}catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(connection);
			return ResponseEntity.ok().body(status);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Status> updatePosition(@RequestBody Position position) {
		PreparedStatement preparedStatement = null;
		Connection connection = Database.connectDatabase();
		Status status = new Status();
		
		try {
			String sql ="update position set position_name = ? where position_id = ?";
			preparedStatement = connection.prepareStatement(sql);
			
			preparedStatement.setString(1, position.getPositionName());
			preparedStatement.setInt(2, position.getPositionId());
			
			boolean st = preparedStatement.execute();
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}
		}catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(connection);
			return ResponseEntity.ok().body(status);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value="/{id}",method = RequestMethod.DELETE)
	public ResponseEntity<Status> deletePosition(@PathVariable("id") int position) {
		PreparedStatement preparedStatement = null;
		Connection connection = Database.connectDatabase();
		Status status = new Status();
		
		try {
			String sql ="delete from position where position_id = ?";
			preparedStatement = connection.prepareStatement(sql);
			
			preparedStatement.setInt(1, position);
			
			boolean st = preparedStatement.execute();
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}
		}catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(connection);
			return ResponseEntity.ok().body(status);
		}
	}
}
