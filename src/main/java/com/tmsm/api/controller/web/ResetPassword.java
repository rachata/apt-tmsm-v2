package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.Status;
import com.tmsm.api.model.web.ExceptionResponse;
import com.tmsm.api.model.web.GetResetPassword;
import com.tmsm.api.utility.Database;

@RestController
@RequestMapping("v1/reset")
public class ResetPassword {
	
	@CrossOrigin
	@RequestMapping(value = "/{token}", method = RequestMethod.GET)
	public ResponseEntity<?> resetPassword(@PathVariable("token") String token){
		ResultSet set = null;
		PreparedStatement statement = null; 
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "select * from otp_password where otp_password = ?";
			statement = con.prepareStatement(sql);
			statement.setString(1, token);
			
			set = statement.executeQuery();
			set.next();
			
			GetResetPassword password = new GetResetPassword();
			password.setOtp_id(set.getInt("otp_id"));
			password.setToken(set.getString("otp_password"));
			password.setUser_id(set.getInt("otp_user"));
			
			return ResponseEntity.ok().body(password);
			
		} catch (Exception e) {
			ExceptionResponse response = new ExceptionResponse();
			response.setMessage(e.getMessage());
			
			return ResponseEntity.badRequest().body(response);
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<?> updatePassword(@RequestBody com.tmsm.api.model.web.ResetPassword reset){
		ExceptionResponse response = new ExceptionResponse();
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "update user set usr_pass = ? where usr_id = ?";
			PreparedStatement statement = con.prepareStatement(sql);
			statement.setString(1, reset.getPassword());
			statement.setInt(2, reset.getUser_id());
			
			boolean st = statement.execute();
			if(!st) {
				response.setMessage("Success");		
			}
			
			return ResponseEntity.ok().body(response);
			
		} catch (Exception e) {
			
			response.setMessage(e.getMessage());
			
			return ResponseEntity.badRequest().body(response);
		}
	}
}
