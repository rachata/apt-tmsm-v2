package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.Status;
import com.tmsm.api.model.mobile.Device;
import com.tmsm.api.model.mobile.Things;
import com.tmsm.api.model.web.ListResDevicesofUserStatus;
import com.tmsm.api.model.web.ResDevicesofUserStatus;
import com.tmsm.api.model.web.ResUserSensor;
import com.tmsm.api.model.web.ThingsEditSubArea;
import com.tmsm.api.model.web.ThingsNickname;
import com.tmsm.api.model.web.UserSensorUnits;
import com.tmsm.api.utility.Database;

@RestController
@CrossOrigin
@RequestMapping("web")
public class WebThings {
	@RequestMapping(value = "/things/{area_id}", method = RequestMethod.GET)
	@CrossOrigin
	public ResponseEntity<?> getThing(@PathVariable("area_id") int area_id) {

		ResultSet result = null;
		PreparedStatement preparedStmt = null;

		Connection con = null;
		String sql = "select things_id , things_name , things_ip , macaddress , things_type , type_name  , dear_name ,  area_of_device_name , dear_image , things_offsetx , things_offsety, things_nickname \n"
				+ ", things_area from things \n" + "inner join things_type on things_type = type_id\n"
				+ "left join detail_area on detail_area.dear_id = things_area\n"
				+ "left join area_of_devices on area_of_device_id = dear_area where area_of_device_id = ? and things_status = 1";
		Things things = new Things();
		try {

			con = Database.connectDatabase();

			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setInt(1, area_id);
			result = preparedStmt.executeQuery();

			java.util.List<com.tmsm.api.model.mobile.Thing> list = new ArrayList<com.tmsm.api.model.mobile.Thing>();

			while (result.next()) {
				String sqlDevice = "select * from devices inner join devices_detail on device_id = detail_device\n"
						+ " inner join devices_priority on priority_id = device_priority\n"
						+ " inner join devices_visibility on visibility_id = device_visibility \n"
						+ " inner join detail_area on dear_id = devices_detail.detail_area\n"
						+ " inner join area_of_devices on area_of_device_id = dear_area  where device_esp = ? and device_visibility = 1";

				preparedStmt = con.prepareStatement(sqlDevice);
				preparedStmt.setLong(1, result.getLong(1));

				ResultSet res = preparedStmt.executeQuery();

				java.util.List<Device> listDevice = new ArrayList<Device>();

				while (res.next()) {

					Device device = new Device();

					device.setDeviceID(res.getInt("device_id"));
					device.setDeviceThing(res.getInt("device_esp"));
					device.setDeviceCH(res.getInt("device_ch"));
					device.setDevicePriority(res.getInt("device_priority"));
					device.setDeviceVisibility(res.getInt("device_visibility"));
					device.setDetailID(res.getInt("detail_id"));
					device.setDetailName(res.getString("detail_name"));
					device.setDetailDevice(res.getInt("detail_device"));
					device.setDetailPrivilege(res.getString("detail_privilege"));
					device.setDetailArea(res.getInt("detail_area"));
					device.setNamePriority(res.getString("priority_name"));
					device.setNameVisibility(res.getString("visibility_name"));
					device.setNameArea(res.getString("dear_name"));
					device.setPositionID(res.getInt("area_of_device_id"));
					device.setPositionName(res.getString("area_of_device_name"));
					device.setImage(result.getString("dear_image"));

					device.setOffsetX(result.getDouble(10));
					device.setOffsetY(result.getDouble(11));

					listDevice.add(device);

					System.out.print(listDevice.toString());
				}

				com.tmsm.api.model.mobile.Thing thing = new com.tmsm.api.model.mobile.Thing();
				thing.setThingID(result.getLong(1));
				thing.setThingIP(result.getString(3));
				thing.setThingMAC(result.getString(4));
				thing.setThingName(result.getString(2));
				thing.setThingType(result.getLong(5));
				thing.setThingTypeName(result.getString(6));
				thing.setThingArea(result.getString(7));
				thing.setThingRootArea(result.getString(8));
				thing.setImage(result.getString(9));
				thing.setOffsetx(result.getDouble(10));
				thing.setOffsety(result.getDouble(11));
				thing.setThing_nickname(result.getString(12));

				thing.setIdArea(result.getInt("things_area"));

				thing.setDevices(listDevice);

				list.add(thing);

			}
			things.setThings(list);
			things.setCodeStatus(200);
			things.setNameStatus("Success");

			System.out.println("Size " + list.size());

			Database.closeConnection(con);
			return ResponseEntity.ok().body(things);
		} catch (Exception ex) {

			things.setCodeStatus(400);

			System.out.print(ex);
			things.setNameStatus(ex.getMessage());
			Database.closeConnection(con);

			return ResponseEntity.ok().body(things);

		}
	}

	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/things/nickname", method = RequestMethod.PUT)
	public ResponseEntity<Status> updateNickname(@RequestBody ThingsNickname nickname) {
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		Status status = new Status();

		try {
			String sql = "update things set things_nickname = ? where things_id = ?";
			statement = con.prepareStatement(sql);
			statement.setString(1, nickname.getThings_name());
			statement.setInt(2, nickname.getThings_id());

			Boolean st = statement.execute();

			if (!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			} else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");
			}

		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}

	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/things/subarea", method = RequestMethod.PUT)
	public ResponseEntity<Status> updateSubAreaofThings(@RequestBody ThingsEditSubArea editSubArea) {
		Status status = new Status();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();

		try {
			String sql = "update things set things_area = ? where things_id = ?";
			statement = con.prepareStatement(sql);
			statement.setInt(1, editSubArea.getSubarea_id());
			statement.setInt(2, editSubArea.getThings_id());

			Boolean st = statement.execute();
			if (!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			} else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");
			}
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}

	@RequestMapping(value = "/things", method = RequestMethod.GET)
	@CrossOrigin
	public ResponseEntity<?> getThingWeb() {
		ResultSet result = null;
		PreparedStatement preparedStmt = null;
		Connection con = null;
		String sql = "select things_id , things_name , things_ip , macaddress , things_type , type_name  , dear_name ,  area_of_device_name , dear_image , things_offsetx , things_offsety, things_nickname \n"
				+ ", things_area from things \n" + "inner join things_type on things_type = type_id\n"
				+ "left join detail_area on detail_area.dear_id = things_area\n"
				+ "left join area_of_devices on area_of_device_id = dear_area where things_status = 1";
		Things things = new Things();
		try {
			con = Database.connectDatabase();
			preparedStmt = con.prepareStatement(sql);
			result = preparedStmt.executeQuery();

			java.util.List<com.tmsm.api.model.mobile.Thing> list = new ArrayList<com.tmsm.api.model.mobile.Thing>();

			while (result.next()) {
				String sqlDevice = "select * from devices inner join devices_detail on device_id = detail_device\n"
						+ " inner join devices_priority on priority_id = device_priority\n"
						+ " inner join devices_visibility on visibility_id = device_visibility \n"
						+ " inner join detail_area on dear_id = devices_detail.detail_area\n"
						+ " inner join area_of_devices on area_of_device_id = dear_area  where device_esp = ? and device_visibility = 1";

				preparedStmt = con.prepareStatement(sqlDevice);
				preparedStmt.setLong(1, result.getLong(1));
				ResultSet res = preparedStmt.executeQuery();
				java.util.List<Device> listDevice = new ArrayList<Device>();

				while (res.next()) {

					Device device = new Device();

					device.setDeviceID(res.getInt(1));
					device.setDeviceThing(res.getInt(2));
					device.setDeviceCH(res.getInt(3));
					device.setDevicePriority(res.getInt(4));
					device.setDeviceVisibility(res.getInt(5));
					device.setDetailID(res.getInt(6));
					device.setDetailName(res.getString(7));
					device.setDetailDevice(res.getInt(8));
					device.setDetailPrivilege(res.getString(9));
					device.setDetailArea(res.getInt(10));
					device.setNamePriority(res.getString(12));
					device.setNameVisibility(res.getString(14));
					device.setNameArea(res.getString(16));
					device.setPositionID(res.getInt(19));
					device.setPositionName(res.getString(20));
					device.setImage(result.getString(9));
					device.setOffsetX(result.getDouble(10));
					device.setOffsetY(result.getDouble(11));

					listDevice.add(device);
				}

				com.tmsm.api.model.mobile.Thing thing = new com.tmsm.api.model.mobile.Thing();
				thing.setThingID(result.getLong(1));
				thing.setThingIP(result.getString(3));
				thing.setThingMAC(result.getString(4));
				thing.setThingName(result.getString(2));
				thing.setThingType(result.getLong(5));
				thing.setThingTypeName(result.getString(6));
				thing.setThingArea(result.getString(7));
				thing.setThingRootArea(result.getString(8));
				thing.setImage(result.getString(9));
				thing.setOffsetx(result.getDouble(10));
				thing.setOffsety(result.getDouble(11));
				thing.setThing_nickname(result.getString(12));
				thing.setIdArea(result.getInt("things_area"));

				thing.setDevices(listDevice);
				list.add(thing);

			}
			things.setThings(list);
			things.setCodeStatus(200);
			things.setNameStatus("Success");

			Database.closeConnection(con);
			return ResponseEntity.ok().body(things);
		} catch (Exception ex) {

			things.setCodeStatus(400);
			things.setNameStatus(ex.getMessage());
			Database.closeConnection(con);

			return ResponseEntity.ok().body(things);

		}
	}
	
	@RequestMapping(value = "/things/sensor", method = RequestMethod.GET)
	@CrossOrigin
	public ResponseEntity<?> getThingSensorWeb() {
		ResultSet result = null;
		PreparedStatement preparedStmt = null;
		Connection con = null;
		String sql = "select things_id , things_name , things_ip , macaddress , things_type , type_name  , dear_name ,  area_of_device_name , dear_image , things_offsetx , things_offsety, things_nickname \n"
				+ ", things_area from things \n" + "inner join things_type on things_type = type_id\n"
				+ "left join detail_area on detail_area.dear_id = things_area\n"
				+ "left join area_of_devices on area_of_device_id = dear_area where things_status = 1 and things_type = 4";
		Things things = new Things();
		try {
			con = Database.connectDatabase();
			preparedStmt = con.prepareStatement(sql);
			result = preparedStmt.executeQuery();

			java.util.List<com.tmsm.api.model.mobile.Thing> list = new ArrayList<com.tmsm.api.model.mobile.Thing>();

			while (result.next()) {
				String sqlDevice = "select * from devices inner join devices_detail on device_id = detail_device\n"
						+ " inner join devices_priority on priority_id = device_priority\n"
						+ " inner join devices_visibility on visibility_id = device_visibility \n"
						+ " inner join detail_area on dear_id = devices_detail.detail_area\n"
						+ " inner join area_of_devices on area_of_device_id = dear_area  where device_esp = ? and device_visibility = 1";

				preparedStmt = con.prepareStatement(sqlDevice);
				preparedStmt.setLong(1, result.getLong(1));
				ResultSet res = preparedStmt.executeQuery();
				java.util.List<Device> listDevice = new ArrayList<Device>();

				while (res.next()) {

					Device device = new Device();

					device.setDeviceID(res.getInt(1));
					device.setDeviceThing(res.getInt(2));
					device.setDeviceCH(res.getInt(3));
					device.setDevicePriority(res.getInt(4));
					device.setDeviceVisibility(res.getInt(5));
					device.setDetailID(res.getInt(6));
					device.setDetailName(res.getString(7));
					device.setDetailDevice(res.getInt(8));
					device.setDetailPrivilege(res.getString(9));
					device.setDetailArea(res.getInt(10));
					device.setNamePriority(res.getString(12));
					device.setNameVisibility(res.getString(14));
					device.setNameArea(res.getString(16));
					device.setPositionID(res.getInt(19));
					device.setPositionName(res.getString(20));
					device.setImage(result.getString(9));
					device.setOffsetX(result.getDouble(10));
					device.setOffsetY(result.getDouble(11));

					listDevice.add(device);
				}

				com.tmsm.api.model.mobile.Thing thing = new com.tmsm.api.model.mobile.Thing();
				thing.setThingID(result.getLong(1));
				thing.setThingIP(result.getString(3));
				thing.setThingMAC(result.getString(4));
				thing.setThingName(result.getString(2));
				thing.setThingType(result.getLong(5));
				thing.setThingTypeName(result.getString(6));
				thing.setThingArea(result.getString(7));
				thing.setThingRootArea(result.getString(8));
				thing.setImage(result.getString(9));
				thing.setOffsetx(result.getDouble(10));
				thing.setOffsety(result.getDouble(11));
				thing.setThing_nickname(result.getString(12));
				thing.setIdArea(result.getInt("things_area"));

				thing.setDevices(listDevice);
				list.add(thing);

			}
			things.setThings(list);
			things.setCodeStatus(200);
			things.setNameStatus("Success");

			Database.closeConnection(con);
			return ResponseEntity.ok().body(things);
		} catch (Exception ex) {

			things.setCodeStatus(400);
			things.setNameStatus(ex.getMessage());
			Database.closeConnection(con);

			return ResponseEntity.ok().body(things);

		}
	}

	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/things/status/{user_id}", method = RequestMethod.GET)
	public ResponseEntity<ListResDevicesofUserStatus> ResponseLastStatusandThingsData(
			@PathVariable("user_id") int user_id) {
		ResultSet set = null;
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ListResDevicesofUserStatus userStatus = new ListResDevicesofUserStatus();

		try {
			String sql = "select things_id, things_type, things_offsetx, things_offsety,things_area,device_id, device_ch, device_priority ,\r\n"
					+ "            device_visibility,detail_privilege,permit_id,permit_privilage,permit_name, type_name, favorite_id, favorite_status\r\n"
					+ "                      from things inner join devices on device_esp = things_id\r\n"
					+ "					     inner join devices_detail on detail_device = device_id\r\n"
					+ "                      inner join permit_user on permit_device = device_id\r\n"
					+ "                      inner join things_type on things_type = type_id\r\n"
					+ "                      left join things_favorite on permit_user.permit_device = things_favorite.favorite_devices\r\n"
					+ "                      where permit_user = ? and things_type = 2 and things_status = 1 \r\n"
					+ "                      and permit_status = 1 and enabled = 1";

			statement = con.prepareStatement(sql);
			statement.setInt(1, user_id);
			set = statement.executeQuery();

			List<ResDevicesofUserStatus> list = new ArrayList<ResDevicesofUserStatus>();
			while (set.next()) {
				if (set.getInt("device_visibility") == 1) {

					String[] devices = set.getString("detail_privilege").split("");

					if (devices[0].equals("1")) {
						String[] permition = set.getString("permit_privilage").split("");

						if (permition[0].equals("1")) {

							String sql2 = "select * from device_log_io left join user_detail on user_detail.user_detail_id = device_log_io.log_user \r\n"
									+ "                            where log_device = ? order by log_id desc limit 1";
							statement = con.prepareStatement(sql2);
							statement.setInt(1, set.getInt("device_id"));
							ResultSet set2 = statement.executeQuery();

							int calculate = Database.countResultSet(set2);
							set2.beforeFirst();

							if (calculate > 0) {
								while (set2.next()) {
									boolean statuslog = true;

									if (set2.getInt(2) == 1 || set2.getInt(2) == 3) {
										statuslog = true;
									} else {
										statuslog = false;
									}

									ResDevicesofUserStatus status = new ResDevicesofUserStatus();
									status.setThings_id(set.getInt(1)); // 1
									status.setThings_type(set.getInt(2)); // 2
									status.setThings_offsetx(set.getDouble(3));// 3
									status.setThings_offsety(set.getDouble(4)); // 4
									status.setThings_area(set.getInt(5)); // 5
									status.setDevices_id(set.getInt(6)); // 6
									status.setDevices_chanel(set.getInt(7)); // 7
									status.setDevices_priority(set.getInt(8)); // 8
									status.setDevices_visibility(set.getInt(9)); // 9
									status.setDevices_privilege(set.getString(10)); // 10
									status.setPermit_id(set.getInt(11)); // 11
									status.setPermit_privilege(set.getString(12)); // 12
									status.setDevice_name(set.getString(13)); // 13
									status.setType_name(set.getString(14)); // 14
									status.setLog_status(set2.getInt(2)); // 15
									status.setLog_boolean(statuslog); // 16
									status.setLog_user(set2.getInt(4)); // 17
									status.setLog_timer(set2.getString(5)); // 18
									status.setFull_name(set2.getString(7) + " " + set2.getString(8)); // 19
									status.setFeverite_id(set.getInt("favorite_id"));
									status.setIs_faverite(set.getBoolean("favorite_status"));

									list.add(status);
								}
							} else {
								ResDevicesofUserStatus status = new ResDevicesofUserStatus();

								status.setThings_id(set.getInt(1)); // 1
								status.setThings_type(set.getInt(2)); // 2
								status.setThings_offsetx(set.getDouble(3));// 3
								status.setThings_offsety(set.getDouble(4)); // 4
								status.setThings_area(set.getInt(5)); // 5
								status.setDevices_id(set.getInt(6)); // 6
								status.setDevices_chanel(set.getInt(7)); // 7
								status.setDevices_priority(set.getInt(8)); // 8
								status.setDevices_visibility(set.getInt(9)); // 9
								status.setDevices_privilege(set.getString(10)); // 10
								status.setPermit_id(set.getInt(11)); // 11
								status.setPermit_privilege(set.getString(12)); // 12
								status.setDevice_name(set.getString(13)); // 13
								status.setType_name(set.getString(14)); // 14
								status.setFeverite_id(set.getInt("favorite_id"));
								status.setIs_faverite(set.getBoolean("favorite_status"));

								list.add(status);
							}
						}
					}
				}

			}

			userStatus.setCodeStatus(200);
			userStatus.setNameStatus("Success");
			userStatus.setDatas(list);

		} catch (SQLException e) {
			userStatus.setCodeStatus(500);
			userStatus.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(userStatus);
		}
	}

	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/door/status/{user_id}", method = RequestMethod.GET)
	public ResponseEntity<ListResDevicesofUserStatus> ResponseLastStatusandDoorData(
			@PathVariable("user_id") int user_id) {
		ResultSet set = null;
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ListResDevicesofUserStatus userStatus = new ListResDevicesofUserStatus();

		try {
			String sql = "select things_id,things_type, things_offsetx, things_offsety,things_area,device_id, device_ch, device_priority ,\r\n"
					+ "            device_visibility,detail_privilege, id, user_name, type_name, favorite_door_id, favorite_door_status\r\n"
					+ "                      from things inner join devices on device_esp = things_id\r\n"
					+ "					     inner join devices_detail on detail_device = device_id\r\n"
					+ "                      inner join door_user on thing = things_id\r\n"
					+ "                      inner join things_type on things_type = type_id\r\n"
					+ "                      left join door_favorite on thing = favorite_door_things\r\n"
					+ "                      where user = ? and things_type = 5 and things_status = 1 \r\n"
					+ "                      and status = 1 and enable = 1";

			statement = con.prepareStatement(sql);
			statement.setInt(1, user_id);
			set = statement.executeQuery();

			List<ResDevicesofUserStatus> list = new ArrayList<ResDevicesofUserStatus>();
			while (set.next()) {
				if (set.getInt("device_visibility") == 1) {

					String[] devices = set.getString("detail_privilege").split("");

					if (devices[0].equals("1")) {

						String sql2 = "select * from door_log left join user_detail on user_detail.user_detail_id = door_log.door_log_user\r\n" + 
								"		inner join door_status_type on door_status_type.door_st = door_log.door_log_status\r\n" + 
								"		where door_log_things = ? order by door_log_id DESC LIMIT  1";
						
						statement = con.prepareStatement(sql2);
						statement.setInt(1, set.getInt("things_id"));
						ResultSet set2 = statement.executeQuery();

						int calculate = Database.countResultSet(set2);
						set2.beforeFirst();

						if (calculate > 0) {
							while (set2.next()) {
								boolean statuslog = true;

								if (set2.getInt(2) == 1 || set2.getInt(2) == 3) {
									statuslog = true;
								} else {
									statuslog = false;
								}

								ResDevicesofUserStatus status = new ResDevicesofUserStatus();
								status.setThings_id(set.getInt(1)); // 1
								status.setThings_type(set.getInt(2)); // 2
								status.setThings_offsetx(set.getDouble(3));// 3
								status.setThings_offsety(set.getDouble(4)); // 4
								status.setThings_area(set.getInt(5)); // 5
								status.setDevices_id(set.getInt(6)); // 6
								status.setDevices_chanel(set.getInt(7)); // 7
								status.setDevices_priority(set.getInt(8)); // 8
								status.setDevices_visibility(set.getInt(9)); // 9
								status.setDevices_privilege(set.getString(10)); // 10
								status.setPermit_id(set.getInt(11)); // 11
//								status.setPermit_privilege(set.getString(12)); 
								status.setDevice_name(set.getString(12)); // 13
								status.setType_name(set.getString(13)); // 14
								status.setLog_status(set2.getInt(2)); // 15
								status.setLog_boolean(statuslog); // 16
								status.setLog_user(set2.getInt(4)); // 17
								status.setLog_timer(set2.getString(5)); // 18
								status.setFull_name(set2.getString(7) + " " + set2.getString(8)); // 19
								status.setFeverite_id(set.getInt("favorite_door_id"));
								status.setIs_faverite(set.getBoolean("favorite_door_status"));

								list.add(status);
							}
						} else {
							ResDevicesofUserStatus status = new ResDevicesofUserStatus();

							status.setThings_id(set.getInt(1)); // 1
							status.setThings_type(set.getInt(2)); // 2
							status.setThings_offsetx(set.getDouble(3));// 3
							status.setThings_offsety(set.getDouble(4)); // 4
							status.setThings_area(set.getInt(5)); // 5
							status.setDevices_id(set.getInt(6)); // 6
							status.setDevices_chanel(set.getInt(7)); // 7
							status.setDevices_priority(set.getInt(8)); // 8
							status.setDevices_visibility(set.getInt(9)); // 9
							status.setDevices_privilege(set.getString(10)); // 10
							status.setPermit_id(set.getInt(11)); // 11
//							status.setPermit_privilege(set.getString(12)); // 12
							status.setDevice_name(set.getString(12)); // 13
							status.setType_name(set.getString(13)); // 14
							status.setFeverite_id(set.getInt("favorite_door_id"));
							status.setIs_faverite(set.getBoolean("favorite_door_status"));

							list.add(status);
						}
					}
				}

			}

			userStatus.setCodeStatus(200);
			userStatus.setNameStatus("Success");
			userStatus.setDatas(list);

		} catch (SQLException e) {
			userStatus.setCodeStatus(500);
			userStatus.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(userStatus);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/sensor/status/{user_id}", method = RequestMethod.GET)
	public ResponseEntity<ListResDevicesofUserStatus> ResponseLastStatusandSensorData(
			@PathVariable("user_id") int user_id) {
		ResultSet set = null;
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ListResDevicesofUserStatus userStatus = new ListResDevicesofUserStatus();

		try {
			String sql = "select things_id, things_type, things_offsetx, things_offsety,things_area,device_id, device_ch, device_priority ,\r\n" + 
					"       device_visibility,detail_privilege,permit_id,permit_privilage,permit_name, type_name, favorite_id, favorite_status,\r\n" + 
					"       sensor_unit,unit_name\r\n" + 
					"                      from things inner join devices on device_esp = things_id\r\n" + 
					"					  inner join devices_detail on detail_device = device_id\r\n" + 
					"                      inner join sensor_unit on sensor_unit_device = device_id\r\n" + 
					"                      inner join unit on unit_id = sensor_unit\r\n" + 
					"                      inner join permit_user on permit_device = device_id\r\n" + 
					"                      inner join things_type on things_type = type_id\r\n" + 
					"                      left join things_favorite on permit_user.permit_device = things_favorite.favorite_devices\r\n" + 
					"                      where permit_user = ? and things_type = 4 and things_status = 1 \r\n" + 
					"                      and permit_status = 1 and enabled = 1";

			statement = con.prepareStatement(sql);
			statement.setInt(1, user_id);
			set = statement.executeQuery();

			List<ResDevicesofUserStatus> list = new ArrayList<ResDevicesofUserStatus>();
			while (set.next()) {
				if (set.getInt("device_visibility") == 1) {

					String[] devices = set.getString("detail_privilege").split("");

					if (devices[0].equals("1")) {
						String[] permition = set.getString("permit_privilage").split("");

						if (permition[0].equals("1")) {

							String sql2 = "select * from sensor_value 	where sensor_device = ? order by sensor_id DESC LIMIT  1";
							statement = con.prepareStatement(sql2);
							statement.setInt(1, set.getInt("device_id"));
							ResultSet set2 = statement.executeQuery();

							int calculate = Database.countResultSet(set2);
							set2.beforeFirst();

							if (calculate > 0) {
								while (set2.next()) {
//								

									ResDevicesofUserStatus status = new ResDevicesofUserStatus();
									status.setThings_id(set.getInt(1)); // 1
									status.setThings_type(set.getInt(2)); // 2
									status.setThings_offsetx(set.getDouble(3));// 3
									status.setThings_offsety(set.getDouble(4)); // 4
									status.setThings_area(set.getInt(5)); // 5
									status.setDevices_id(set.getInt(6)); // 6
									status.setDevices_chanel(set.getInt(7)); // 7
									status.setDevices_priority(set.getInt(8)); // 8
									status.setDevices_visibility(set.getInt(9)); // 9
									status.setDevices_privilege(set.getString(10)); // 10
									status.setPermit_id(set.getInt(11)); // 11
									status.setPermit_privilege(set.getString(12)); // 12
									status.setDevice_name(set.getString(13)); // 13
									status.setType_name(set.getString(14)); // 14
									status.setSensor_value(set2.getFloat(3));
									status.setLog_timer(set2.getString(4)); // 18
									status.setFeverite_id(set.getInt("favorite_id"));
									status.setIs_faverite(set.getBoolean("favorite_status"));
									status.setUnit_id(set.getInt("sensor_unit"));
									status.setUnit_name(set.getString("unit_name"));

									list.add(status);
								}
							} else {
								ResDevicesofUserStatus status = new ResDevicesofUserStatus();

								status.setThings_id(set.getInt(1)); // 1
								status.setThings_type(set.getInt(2)); // 2
								status.setThings_offsetx(set.getDouble(3));// 3
								status.setThings_offsety(set.getDouble(4)); // 4
								status.setThings_area(set.getInt(5)); // 5
								status.setDevices_id(set.getInt(6)); // 6
								status.setDevices_chanel(set.getInt(7)); // 7
								status.setDevices_priority(set.getInt(8)); // 8
								status.setDevices_visibility(set.getInt(9)); // 9
								status.setDevices_privilege(set.getString(10)); // 10
								status.setPermit_id(set.getInt(11)); // 11
								status.setPermit_privilege(set.getString(12)); // 12
								status.setDevice_name(set.getString(13)); // 13
								status.setType_name(set.getString(14)); // 14
								status.setFeverite_id(set.getInt("favorite_id"));
								status.setIs_faverite(set.getBoolean("favorite_status"));
								status.setUnit_id(set.getInt("sensor_unit"));
								status.setUnit_name(set.getString("unit_name"));

								list.add(status);
							}
						}
					}
				}

			}

			userStatus.setCodeStatus(200);
			userStatus.setNameStatus("Success");
			userStatus.setDatas(list);

		} catch (SQLException e) {
			userStatus.setCodeStatus(500);
			userStatus.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(userStatus);
		}
	}
	
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/sensor/unit/{device_id}", method = RequestMethod.GET)
	public ResponseEntity<ResUserSensor> getDevicesSensorStatus(@PathVariable("device_id") int device_id){
		UserSensorUnits unit = new UserSensorUnits();
		ResUserSensor unitres = new ResUserSensor();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ResultSet set = null;
		
		try {
			String sql = "select * from sensor_unit inner join unit on unit_id = sensor_unit where sensor_unit_device = ?";
			statement = con.prepareStatement(sql);
			statement.setInt(1, device_id);
			set = statement.executeQuery();
			
			while(set.next()) {
				unit.setSensor_unit_id(set.getInt(1));
				unit.setUnit_id(set.getInt("unit_id"));
				unit.setUnit_name(set.getString("unit_name"));
			}
			
			unitres.setCodeStatus(200);
			unitres.setNameStatus("Success");
			unitres.setUnits(unit);
		} catch (SQLException e) {
			unitres.setCodeStatus(500);
			unitres.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(unitres);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/sensor/unit/update/{device_id}", method = RequestMethod.POST)
	public ResponseEntity<Status> createUnitSensor(@RequestBody UserSensorUnits units, @PathVariable("device_id") int device_id){
		Status status = new Status();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ResultSet set = null;
		
		try {
			String sql = "select * from sensor_unit where sensor_unit_device = ?";
			statement = con.prepareStatement(sql);
			statement.setInt(1, device_id);
			set = statement.executeQuery();
			
			int count = Database.countResultSet(set);
			set.beforeFirst();
			String sql1= "";
			if(count > 0) {
				
				System.out.print(units.getUnit_id());
				sql1 = "update sensor_unit set sensor_unit = ? where sensor_unit_id = ?";
				statement = con.prepareStatement(sql1);
				statement.setInt(1, units.getUnit_id());
				statement.setInt(2, units.getSensor_unit_id());
			} else {
				sql1 = "insert into sensor_unit (sensor_unit_device, sensor_unit) values (?, ?)";
				statement = con.prepareStatement(sql1);
				statement.setInt(1, device_id);
				statement.setInt(2, units.getUnit_id());
			}
			
			boolean st = statement.execute();
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");
			}
			
			
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
}
