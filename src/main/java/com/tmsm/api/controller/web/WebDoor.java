package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.Status;
import com.tmsm.api.model.mobile.ReqDoor;
import com.tmsm.api.model.web.DoorChangeName;
import com.tmsm.api.utility.Database;

@RestController
@CrossOrigin
@RequestMapping("/doorreq")
public class WebDoor {
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Status> reqDoor(@RequestBody ReqDoor reqDoor) {

		Connection con = null;
		PreparedStatement preparedStatement = null;

		Status status = new Status();

		try {

			con = Database.connectDatabase();
			String sql = "select * from door_user where user = ? and thing = ?";
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, reqDoor.getUserID());
			preparedStatement.setInt(2, reqDoor.getThingID());

			ResultSet res = preparedStatement.executeQuery();
			int count = Database.countResultSet(res);

			if (count == 0) {

				sql = "insert into door_user (user , thing, user_name) value(? , ?, ?) ";
				preparedStatement = con.prepareStatement(sql);
				preparedStatement.setInt(1, reqDoor.getUserID());
				preparedStatement.setInt(2, reqDoor.getThingID());
				preparedStatement.setString(3, reqDoor.getDoor_name());

				boolean statusSQL = preparedStatement.execute();
				if (!statusSQL) {
					status.setCodeStatus(200);
					status.setNameStatus("Success");
				} else {
					status.setCodeStatus(204);
					status.setNameStatus("Unsuccess");
				}

			} else {
				status.setCodeStatus(202);
				status.setNameStatus("Your request already exists.");
			}
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus("Unsuccess :" + e.getErrorCode() + " " + e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}

	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Status> editDoorNameuser(@RequestBody DoorChangeName door){
		Status status = new Status();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql ="update door_user set user_name = ? where id = ?";
			statement = con.prepareStatement(sql);
			statement.setString(1, door.getDoor_name());
			statement.setInt(2, door.getDoor_id());
			
			boolean st = statement.execute();
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");
			}
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
}
