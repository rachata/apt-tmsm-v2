package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.Status;
import com.tmsm.api.model.device.Device;
import com.tmsm.api.model.web.ResRoom;
import com.tmsm.api.utility.Database;

@RestController
@RequestMapping("credevices")
public class CreateDevices {
	
	
	@SuppressWarnings("finally")
	@RequestMapping(value="/updatearea", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin
	public ResponseEntity<Status> updateAreaThings(@RequestBody ResRoom room ){
		Status status = new Status();
		
		PreparedStatement preparedStatement = null;
		Connection con = Database.connectDatabase();
		
		System.out.println(room.getAreaId());
		System.out.println(room.getThingId());
		
		try {
			String sql = "update things set things_area = ? where things_id = ?";
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, room.getAreaId());
			preparedStatement.setInt(2, room.getThingId());
			
			boolean St = preparedStatement.execute();
			if(!St) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");
			}
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	

	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Status> reqDevices(@RequestBody Device device){
		Status status = new Status();
		
		PreparedStatement preparedStatement = null;
		Connection connection = Database.connectDatabase();
		ResultSet resultSet = null;
		
		try {
			String sql = "select * from devices where device_esp = ? and device_ch = ?";
			
			preparedStatement = connection.prepareStatement(sql);
			
			preparedStatement.setInt(1, device.getThing());
			preparedStatement.setInt(2, device.getCh());
			
			resultSet = preparedStatement.executeQuery();
			
			int countResult = Database.countResultSet(resultSet);
			if(countResult ==  0) {
				
				String sqlInsert = "insert into devices (device_esp, device_ch , device_priority , device_visibility) value(? , ? , ? , ?)";
				
				preparedStatement = connection.prepareStatement(sqlInsert , PreparedStatement.RETURN_GENERATED_KEYS);
				
				preparedStatement.setInt(1, device.getThing());
				preparedStatement.setInt(2, device.getCh());
				preparedStatement.setInt(3, device.getPriority());
				preparedStatement.setInt(4, device.getVisibility());
				
				boolean statusCh  = preparedStatement.execute();
				
				if(!statusCh) {
					int keyDevice = 0;
					ResultSet keys = preparedStatement.getGeneratedKeys();
					 
					keys.next();
					keyDevice = (keys.getInt(1)); 
					 
					System.out.println(keyDevice);
					
					String sqlInsertDetail = "insert into devices_detail (detail_name, detail_device , detail_privilege , detail_area) value(? , ? , ? , ?)";
					
					preparedStatement = connection.prepareStatement(sqlInsertDetail);
					
					preparedStatement.setString(1, device.getNameDevice());
					preparedStatement.setInt(2, keyDevice);
					preparedStatement.setString(3, device.getPrivilege());
					preparedStatement.setInt(4, device.getArea());
					
					boolean statusDetail  = preparedStatement.execute();
					
					if(!statusDetail) {	
						
						
						String sqlAdmin = "insert into permit_user (permit_user, permit_device, permit_status, \r\n" + 
										  "permit_privilage, permit_grant_status, permit_name, enabled) value (?,?,?,?,?,?,?)";
						
						preparedStatement = connection.prepareStatement(sqlAdmin);
						preparedStatement.setInt(1, 21);
						preparedStatement.setInt(2, keyDevice);
						preparedStatement.setInt(3, 1);
						preparedStatement.setString(4, "111");
						preparedStatement.setInt(5,  1);
						preparedStatement.setString(6, device.getNameDevice());
						preparedStatement.setInt(7, 1);
						
						boolean stAdmin = preparedStatement.execute();
						
						if(!stAdmin) {
							String Area = "select * from area_administrator where area = ?";
							preparedStatement = connection.prepareStatement(Area);
							preparedStatement.setInt(1, device.getArea());
							
							ResultSet set = preparedStatement.executeQuery();
							
							while(set.next()) {
								String adminArea = "insert into permit_user (permit_user, permit_device, permit_status, \r\n" + 
										  "permit_privilage, permit_grant_status, permit_name, enabled) value (?,?,?,?,?,?,?)";
								
								
								preparedStatement = connection.prepareStatement(adminArea);
								preparedStatement.setInt(1, set.getInt(2));
								preparedStatement.setInt(2, keyDevice);
								preparedStatement.setInt(3, 1);
								preparedStatement.setString(4, "111");
								preparedStatement.setInt(5,  1);
								preparedStatement.setString(6, device.getNameDevice());
								preparedStatement.setInt(7, 1);
								
								boolean st = preparedStatement.execute();
								
								if(!st) {
									status.setCodeStatus(200);
									status.setNameStatus("Success");
								}else {
									status.setCodeStatus(204);
									status.setNameStatus("Unsuccess");
								}	
							}
									
						}
						
					}else {
						status.setCodeStatus(204);
						status.setNameStatus("Unsuccess");
					}					
					
				}
				
			}
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());		
		}finally {
			Database.closeConnection(connection);
			return ResponseEntity.ok().body(status);
		}
	}
}
