package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.Status;
import com.tmsm.api.model.web.AccepDoor;
import com.tmsm.api.model.web.CreateFaveriteDoor;
import com.tmsm.api.model.web.DevicesGroup;
import com.tmsm.api.model.web.DoorGroup;
import com.tmsm.api.model.web.DoorGroupDetail;
import com.tmsm.api.model.web.DoorLogStatus;
import com.tmsm.api.model.web.FaveriteDoor;
import com.tmsm.api.model.web.LastStatusDevices;
import com.tmsm.api.model.web.ListFaverite;
import com.tmsm.api.model.web.ResAccDoor;
import com.tmsm.api.model.web.ResDeviceInGroup;
import com.tmsm.api.model.web.ResDoorGroup;
import com.tmsm.api.utility.Database;

@CrossOrigin
@RestController
@RequestMapping("/faverite")
public class ResFaverite {
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/door/{idUser}" , method = RequestMethod.GET)
	public ResponseEntity<?> getDoorIngroup(@PathVariable("idUser") int idUser){
		 	ResDoorGroup group = new ResDoorGroup();
		    PreparedStatement statement = null;
		    Connection con = Database.connectDatabase();
		    ResultSet set = null;
		    
		    try {
				String sql = "select * from door_favorite inner join door_user on door_user.thing = door_favorite.favorite_door_things \r\n" + 
						"							where user = ? and status = 1 and enable = 1";
				
				statement = con.prepareStatement(sql);
				statement.setInt(1, idUser);
				set = statement.executeQuery();
				
				DoorGroup group2 = new DoorGroup();
				List<DoorGroupDetail> list = new ArrayList<DoorGroupDetail>();
				while(set.next()) {				
					
						String sql2 = "select * from door_log inner join user_detail on user_detail.user_detail_id = door_log.door_log_user\r\n" + 
								"					          inner join door_status_type on door_status_type.door_st = door_log.door_log_status\r\n" + 
								"                             where door_log_things = ? order by door_log_id DESC LIMIT  1";
						
						statement = con.prepareStatement(sql2);
						statement.setInt(1, set.getInt(2));
						ResultSet set2 = statement.executeQuery();
							set2.next();				
							DoorGroupDetail detail = new DoorGroupDetail();
							
							detail.setThingID(set.getInt(2));
							detail.setThingsName(set.getString(12));
							detail.setLastStatus(set2.getInt(2));
							detail.setStatusType(set2.getString(13));
							detail.setfName(set2.getString(7));
							detail.setlName(set2.getString(8));		
							detail.setFeverite_id(set.getInt(1));
							detail.setIs_feverite(set.getBoolean(5));
							
							list.add(detail);														
					
					
					group2.setDetails(list);
				}
				
				group.setCodeStatus(200);
				group.setNameStatus("Success");
				group.setGroup(group2);
				
			} catch (SQLException e) {
				group.setCodeStatus(500);
				group.setNameStatus(e.getMessage());
			}finally {
				Database.closeConnection(con);
				return ResponseEntity.ok().body(group);
			}
	}
	

	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/door", method = RequestMethod.POST)
	public ResponseEntity<Status> createFaveriteDoor(@RequestBody CreateFaveriteDoor door){
		Status status = new Status();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		
		try {
			
			String sql = "select * from door_favorite where favorite_door_user = ?";
			statement = con.prepareStatement(sql);
			statement.setInt(1, door.getUser_id());
			
			ResultSet set = statement.executeQuery();
			int size = Database.countResultSet(set);			
			
			if(size <= 9) {
				String sql2 = "select * from door_favorite where favorite_door_user = ? and favorite_door_things = ?";
				statement = con.prepareStatement(sql2);
				statement.setInt(1, door.getUser_id());
				statement.setInt(2, door.getThings_id());
				
				ResultSet set2 = statement.executeQuery();
				int sizeFav = Database.countResultSet(set2);
				
				if(sizeFav == 0) {
					String sql3 = "insert into door_favorite(favorite_door_things, favorite_door_user) value (?, ?)";
					statement = con.prepareStatement(sql3);
					statement.setInt(1, door.getThings_id());
					statement.setInt(2, door.getUser_id());
					
					boolean st = statement.execute();
					if(!st) {
						status.setCodeStatus(200);
						status.setNameStatus("Success");
					}else {
						status.setCodeStatus(204);
						status.setNameStatus("Unsuccess");
					}
				}else {
					status.setCodeStatus(203);
					status.setNameStatus("You have things faverite.");
				}

			}else {
				status.setCodeStatus(202);
				status.setNameStatus("You have too many things.");
			}

		}catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/door/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Status> updateFaveriteDoor(@PathVariable("id") int id){
		Status status = new Status();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql ="delete from door_favorite where favorite_door_id = ?";			
			statement = con.prepareStatement(sql);
			statement.setInt(1, id);
			
			boolean statusDelete = statement.execute();
			if(!statusDelete) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");
			}
		}catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/devices", method = RequestMethod.POST)
	public ResponseEntity<Status> addFeveriteDevices(@RequestBody CreateFaveriteDoor devices){
		Status status = new Status();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		
		try {
			
			String sql = "select * from things_favorite where favorite_user = ?";
			statement = con.prepareStatement(sql);
			statement.setInt(1, devices.getUser_id());
			
			ResultSet set = statement.executeQuery();
			int size = Database.countResultSet(set);			
			
			if(size <= 9) {
				String sql2 = "select * from things_favorite where favorite_user = ? and favorite_devices = ?";
				statement = con.prepareStatement(sql2);
				statement.setInt(1, devices.getUser_id());
				statement.setInt(2, devices.getDevices_id());
				
				ResultSet set2 = statement.executeQuery();
				int sizeFav = Database.countResultSet(set2);
				
				if(sizeFav == 0) {
					String sql3 = "insert into things_favorite(favorite_user, favorite_things, favorite_devices) value (?, ?, ?)";
					statement = con.prepareStatement(sql3);
					statement.setInt(1, devices.getUser_id());
					statement.setInt(2, devices.getThings_id());
					statement.setInt(3, devices.getDevices_id());
					
					boolean st = statement.execute();
					if(!st) {
						status.setCodeStatus(200);
						status.setNameStatus("Success");
					}else {
						status.setCodeStatus(204);
						status.setNameStatus("Unsuccess");
					}
				}else {
					status.setCodeStatus(203);
					status.setNameStatus("You have things faverite.");
				}

			}else {
				status.setCodeStatus(202);
				status.setNameStatus("You have too many things.");
			}

		}catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/devices/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Status> deleteFaveriteDevices(@PathVariable("id") int  id){
		Status status = new Status();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql ="delete from things_favorite where favorite_id = ?";			
			statement = con.prepareStatement(sql);
			statement.setInt(1, id);
			
			boolean statusDelete = statement.execute();
			if(!statusDelete) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");
			}
		}catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
	@SuppressWarnings("finally")
	@RequestMapping(value = "/device/{idUser}" , method = RequestMethod.GET)
	@CrossOrigin
	public ResponseEntity<ResDeviceInGroup> getDeviceOfGroupUser(@PathVariable("idUser") int idUser ){
		    ResDeviceInGroup group = new ResDeviceInGroup();
		    PreparedStatement statement = null;
		    Connection con = Database.connectDatabase();
		    ResultSet set = null;
		    
		    try {
				String sql = "select * from things_favorite inner join permit_user on  permit_user.permit_device = things_favorite.favorite_devices \r\n" + 
						"							   where permit_user = ? and enabled = true and permit_status = true;";
				
				statement = con.prepareStatement(sql);
				statement.setInt(1, idUser);
				set = statement.executeQuery();
				
				DevicesGroup group2 = new DevicesGroup();
				List<LastStatusDevices> list = new ArrayList<LastStatusDevices>();
				while(set.next()) {
					
					String sql2 = "select * from device_log_io inner join user_detail on user_detail.user_detail_id = device_log_io.log_user \r\n" + 
							"                    where log_device = ? order by log_id desc limit 1";
					
					statement = con.prepareStatement(sql2);
					statement.setInt(1, set.getInt(3));
					ResultSet set2 = statement.executeQuery();
					
					
					while(set2.next()) {
						
						boolean status = true;
						
						if(set2.getInt(2) == 1 || set2.getInt(2) == 3) {
							status = true;
						}else {
							status = false;
						}
						
						LastStatusDevices lastStatusDevices = new LastStatusDevices();						
						lastStatusDevices.setDevice_name(set.getString(15));
						lastStatusDevices.setLog_status(set2.getInt(2));
						lastStatusDevices.setLog_boolean(status);
						lastStatusDevices.setDevices_id(set.getInt(3));
						lastStatusDevices.setLog_user(set2.getInt(4));
						lastStatusDevices.setLog_timer(set2.getString(5));
						lastStatusDevices.setFirst_name(set2.getString(7));
						lastStatusDevices.setLast_name(set2.getString(8));
						lastStatusDevices.setFeverite_id(set.getInt(1));
						lastStatusDevices.setIs_faverite(set.getBoolean(6));
						
						list.add(lastStatusDevices);
					}
					group2.setGroup_id(set.getInt(2));
					group2.setDevices(list);
				}
				
				group.setCodeStatus(200);
				group.setNameStatus("Success");
				group.setGroup(group2);
				
			} catch (SQLException e) {
				group.setCodeStatus(500);
				group.setNameStatus(e.getMessage());
			}finally {
				Database.closeConnection(con);
				return ResponseEntity.ok().body(group);
			}
	}
	
} 
