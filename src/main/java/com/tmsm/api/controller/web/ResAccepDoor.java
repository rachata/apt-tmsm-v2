package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.web.AccepDoor;
import com.tmsm.api.model.web.ResAccDoor;
import com.tmsm.api.utility.Database;

@CrossOrigin
@RestController
@RequestMapping("resdooraccep")
public class ResAccepDoor {

	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/{usr}",method = RequestMethod.GET)
	public ResponseEntity<ResAccDoor> resAccDoor(@PathVariable(name = "usr") int usrID){
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Connection con = Database.connectDatabase();
		ResAccDoor accDoor = new ResAccDoor();
		
		try {
			
			String sql = "select things_id, things_name, dear_name, area_of_device_name, door_log_status, door_type, user_detail_fname, user_detail_lname from door_user inner join things on door_user.thing = things.things_id\r\n" + 
					"						inner join detail_area on detail_area.dear_id = things.things_area\r\n" + 
					"                       inner join area_of_devices on  detail_area.dear_area = area_of_devices.area_of_device_id\r\n" +
					"						inner join door_log on door_log.door_log_things = things.things_id\r\n" +
					"						inner join door_status_type on door_status_type.door_st = door_log.door_log_status\r\n" +
					"						inner join user_detail on user_detail.user_detail_id = door_log.door_log_user\r\n" +
					"                       where user = ? and status = 1 ORDER  BY door_log_id DESC LIMIT  1";
			
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, usrID);
			resultSet = preparedStatement.executeQuery();
			int count = Database.countResultSet(resultSet);
			resultSet.beforeFirst();
			
			List<AccepDoor> list = new ArrayList<AccepDoor>();
			
			if(count != 0) {			
			
				while(resultSet.next()) {
					AccepDoor door = new AccepDoor();
					door.setThingID(resultSet.getInt(1));
					door.setThingsName(resultSet.getString(2));
					door.setSubArea(resultSet.getString(3));
					door.setRootArea(resultSet.getString(4));
					door.setLastStatus(resultSet.getInt(5));
					door.setStatusType(resultSet.getString(6));
					door.setfName(resultSet.getString(7));
					door.setlName(resultSet.getString(8));
					
					list.add(door);
				}
			}
			
			accDoor.setCodeStatus(200);
			accDoor.setNameStatus("Success");
			accDoor.setData(list);
			
		} catch (SQLException e) {
			accDoor.setCodeStatus(400);
			accDoor.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(accDoor);
		}
	}

}
