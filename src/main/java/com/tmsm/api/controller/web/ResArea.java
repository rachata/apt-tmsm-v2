package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.utility.Database;
import com.tmsm.model.strong.DetailArea;
import com.tmsm.model.strong.ListArea;
import com.tmsm.model.strong.ResAreaDevices;

@RestController
@CrossOrigin
@RequestMapping("/allarea")
public class ResArea {
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<ListArea> getArea(){
		ListArea area = new ListArea();
		
		PreparedStatement preparedStatement = null;
		Connection con = Database.connectDatabase(); 
		ResultSet re = null;
		
		try {
			String sql = "select * from area_of_devices where area_enable = 1";
			preparedStatement = con.prepareStatement(sql);
			re = preparedStatement.executeQuery();
			
			List<ResAreaDevices> list = new ArrayList<ResAreaDevices>();
			
			while(re.next()) {
				String sql2 = "select * from detail_area where dear_area = ? and dear_status = 1";
				preparedStatement = con.prepareStatement(sql2);
				preparedStatement.setInt(1, re.getInt(1));
				ResultSet resultSet = preparedStatement.executeQuery();
				
				List<DetailArea> detailAreas = new ArrayList<DetailArea>();
				
				while(resultSet.next()) {
					DetailArea detailArea = new DetailArea();
					
					detailArea.setDearId(resultSet.getInt(1));
					detailArea.setDearName(resultSet.getString(2));
					detailArea.setDearImg(resultSet.getString(3));
					detailArea.setDearArea(resultSet.getInt(4));
					
					detailAreas.add(detailArea);
				}
				
				ResAreaDevices devices = new ResAreaDevices();
				
				devices.setAreaId(re.getInt(1));
				devices.setAreaName(re.getString(2));
				devices.setAreaLat(re.getDouble(3));
				devices.setAreaLng(re.getDouble(4));
				devices.setDetailAreas(detailAreas);
				
				list.add(devices);
			}
			
			area.setAreaDevices(list);
			area.setCodeStatus(200);
			area.setNameStatus("Success");
			
		} catch (SQLException e) {
			area.setCodeStatus(500);
			area.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(area);
		}
	}

}
