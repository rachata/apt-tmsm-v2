package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.Status;
import com.tmsm.api.model.door.ResDoor;
import com.tmsm.api.model.mobile.ReqDoor;
import com.tmsm.api.utility.Database;

@RestController
@RequestMapping("/webdoor")
public class ResUserRequestDoor {
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/accep",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResDoor> ResponseDoor() {
		ResDoor door = new ResDoor();
		PreparedStatement preparedStatement = null;
		Connection con = Database.connectDatabase();
		ResultSet resultSet = null;
		
		try {
			String sql ="select * from door_user inner join user_detail on user_detail_id = user\r\n" + 
					"						inner join things on thing = things_id where status = 1 ";
			preparedStatement = con.prepareStatement(sql);
			resultSet = preparedStatement.executeQuery();
			
			List<com.tmsm.api.model.door.Door> listDoor = new ArrayList<com.tmsm.api.model.door.Door>();
			
			while (resultSet.next()) {
				com.tmsm.api.model.door.Door mdoor = new com.tmsm.api.model.door.Door();
				mdoor.setId(resultSet.getInt(1));
				mdoor.setUser(resultSet.getInt(2));
				mdoor.setThing(resultSet.getInt(3));
				mdoor.setStatus(resultSet.getInt(4));
				mdoor.setDt(resultSet.getString(5));				
				mdoor.setFname(resultSet.getString(9));
				mdoor.setLname(resultSet.getString(10));
				mdoor.setFull_name(resultSet.getString(9) + " " + resultSet.getString(10));
				mdoor.setThing_name(resultSet.getString("things_nickname"));
				
				listDoor.add(mdoor);
			}
			
			door.setCodeStatus(200);
			door.setNameStatus("Success");
			door.setDoor(listDoor);
			
		} catch (SQLException e) {
			door.setCodeStatus(500);
			door.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(door);
		}
	}
	
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/accep/{area_id}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResDoor> ResponseDoorofArea(@PathVariable("area_id") int area_id) {
		ResDoor door = new ResDoor();
		PreparedStatement preparedStatement = null;
		Connection con = Database.connectDatabase();
		ResultSet resultSet = null;
		
		try {
			String sql ="select id, user, thing, status, dt, user_detail_fname, user_detail_lname, things_nickname from area_of_devices inner join detail_area on area_of_device_id = dear_area\r\n" + 
					"							  inner join things on things_area = dear_id\r\n" + 
					"                              inner join door_user on things_id = thing\r\n" + 
					"                              inner join user_detail on user_detail_id = user\r\n" + 
					"                              where area_of_device_id = ? and status = 1";
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, area_id);
			resultSet = preparedStatement.executeQuery();
			
			List<com.tmsm.api.model.door.Door> listDoor = new ArrayList<com.tmsm.api.model.door.Door>();
			
			while (resultSet.next()) {
				com.tmsm.api.model.door.Door mdoor = new com.tmsm.api.model.door.Door();
				mdoor.setId(resultSet.getInt(1));
				mdoor.setUser(resultSet.getInt(2));
				mdoor.setThing(resultSet.getInt(3));
				mdoor.setStatus(resultSet.getInt(4));
				mdoor.setDt(resultSet.getString(5));				
				mdoor.setFname(resultSet.getString(6));
				mdoor.setLname(resultSet.getString(7));
				mdoor.setFull_name(resultSet.getString(6) + " " + resultSet.getString(7));
				mdoor.setThing_name(resultSet.getString(8));
				
				listDoor.add(mdoor);
			}
			
			door.setCodeStatus(200);
			door.setNameStatus("Success");
			door.setDoor(listDoor);
			
		} catch (SQLException e) {
			door.setCodeStatus(500);
			door.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(door);
		}
	}
	
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/unaccep",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResDoor> ResponseUnaccepDoor() {
		ResDoor door = new ResDoor();
		PreparedStatement preparedStatement = null;
		Connection con = Database.connectDatabase();
		ResultSet resultSet = null;
		
		try {
			String sql ="select * from door_user inner join user_detail on user_detail_id = user\r\n" + 
					"						inner join things on thing = things_id where status = 0 ";
			preparedStatement = con.prepareStatement(sql);
			resultSet = preparedStatement.executeQuery();
			
			List<com.tmsm.api.model.door.Door> listDoor = new ArrayList<com.tmsm.api.model.door.Door>();
			
			while (resultSet.next()) {
				com.tmsm.api.model.door.Door mdoor = new com.tmsm.api.model.door.Door();
				mdoor.setId(resultSet.getInt(1));
				mdoor.setUser(resultSet.getInt(2));
				mdoor.setThing(resultSet.getInt(3));
				mdoor.setStatus(resultSet.getInt(4));
				mdoor.setDt(resultSet.getString(5));				
				mdoor.setFname(resultSet.getString(9));
				mdoor.setLname(resultSet.getString(10));
				mdoor.setFull_name(resultSet.getString(9) + " " + resultSet.getString(10));				
				mdoor.setThing_name(resultSet.getString("things_nickname"));
				
				listDoor.add(mdoor);
			}
			
			door.setCodeStatus(200);
			door.setNameStatus("Success");
			door.setDoor(listDoor);
			
		} catch (SQLException e) {
			door.setCodeStatus(500);
			door.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(door);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/unaccep/{area_id}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResDoor> ResponseUnaccepDoorofArea(@PathVariable("area_id") int area_id) {
		ResDoor door = new ResDoor();
		PreparedStatement preparedStatement = null;
		Connection con = Database.connectDatabase();
		ResultSet resultSet = null;
		
		try {
			String sql ="select id, user, thing, status, dt, user_detail_fname, user_detail_lname, things_nickname from area_of_devices inner join detail_area on area_of_device_id = dear_area\r\n" + 
					"							  inner join things on things_area = dear_id\r\n" + 
					"                              inner join door_user on things_id = thing\r\n" + 
					"                              inner join user_detail on user_detail_id = user\r\n" + 
					"                              where area_of_device_id = ? and status = 0";
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, area_id);
			resultSet = preparedStatement.executeQuery();
			
			List<com.tmsm.api.model.door.Door> listDoor = new ArrayList<com.tmsm.api.model.door.Door>();
			
			while (resultSet.next()) {
				com.tmsm.api.model.door.Door mdoor = new com.tmsm.api.model.door.Door();
				mdoor.setId(resultSet.getInt(1));
				mdoor.setUser(resultSet.getInt(2));
				mdoor.setThing(resultSet.getInt(3));
				mdoor.setStatus(resultSet.getInt(4));
				mdoor.setDt(resultSet.getString(5));				
				mdoor.setFname(resultSet.getString(6));
				mdoor.setLname(resultSet.getString(7));
				mdoor.setFull_name(resultSet.getString(6) + " " + resultSet.getString(7));
				mdoor.setThing_name(resultSet.getString(8));				
				
				listDoor.add(mdoor);
			}
			
			door.setCodeStatus(200);
			door.setNameStatus("Success");
			door.setDoor(listDoor);
			
		} catch (SQLException e) {
			door.setCodeStatus(500);
			door.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(door);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Status> updateStatus(@RequestBody ReqDoor reqDoor) {

		Connection con = null;
		PreparedStatement preparedStatement = null;

		Status status = new Status();
		try {
			String sql = "update door_user set status = ? where user = ? and thing = ?";
			con = Database.connectDatabase();
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, reqDoor.getStatus());
			preparedStatement.setInt(2, reqDoor.getUserID());
			preparedStatement.setInt(3, reqDoor.getThingID());

			boolean statusSQL = preparedStatement.execute();

			if (!statusSQL) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");

			} else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");

			}
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus("Unsuccess : " + e.getErrorCode() + " " + e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
}
