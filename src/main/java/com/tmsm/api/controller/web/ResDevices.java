package com.tmsm.api.controller.web;

import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.web.AccepDoor;
import com.tmsm.api.model.web.DevicesModel;
import com.tmsm.api.model.web.DoorRequest;
import com.tmsm.api.model.web.LastRequest;
import com.tmsm.api.model.web.LastStatusDevices;
import com.tmsm.api.model.web.ListDoorRequest;
import com.tmsm.api.model.web.ListLastRequest;
import com.tmsm.api.model.web.ResAccDoor;
import com.tmsm.api.model.web.ResStatusDevices;
import com.tmsm.api.model.web.ResThingsDevices;
import com.tmsm.api.model.web.ThingsDevices;
import com.tmsm.api.utility.Database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@CrossOrigin
@RestController
@RequestMapping("/webdevices")
public class ResDevices {
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/{root}/{sub}",method = RequestMethod.GET)
	public ResponseEntity<?> resDevicesModel(@PathVariable("root") int root, @PathVariable("sub") int sub){
		ResThingsDevices devices = new ResThingsDevices();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ResultSet set = null;
		
		try {
			String sql = "select things_id, things_offsetx, things_offsety, things_type, things_nickname, dear_image, dear_name from things inner join detail_area on detail_area.dear_id = things.things_area where things_area = ?";
			statement = con.prepareStatement(sql);
			statement.setInt(1, sub);
			set = statement.executeQuery();
			List<ThingsDevices> list = new ArrayList<ThingsDevices>();			
			while(set.next()) {
				String sql2 = "select * from devices inner join devices_detail on devices.device_id = devices_detail.detail_device\r\n" + 
							  " where device_esp = ? and device_visibility = 1";
				statement = con.prepareStatement(sql2);
				statement.setInt(1, set.getInt(1));
				ResultSet set2 = statement.executeQuery();
				
				List<DevicesModel> models = new ArrayList<DevicesModel>();
				while(set2.next()) {
					DevicesModel model = new DevicesModel();
					model.setDevice_id(set2.getInt(1));
					model.setDevice_thing(set2.getInt(2));
					model.setDevice_chanel(set2.getInt(3));
					model.setDevice_priority(set2.getInt(4));
					model.setDevice_visibility(set2.getInt(5));
					model.setDevice_name(set2.getString(7));
					model.setDevice_privilege(set2.getString(9));
					
					models.add(model);
				}
				
				ThingsDevices things = new ThingsDevices();
				things.setThings_id(set.getInt(1));
				things.setThings_offsetx(set.getDouble(2));
				things.setThings_offsety(set.getDouble(3));
				things.setThings_type(set.getInt(4));
				things.setThings_nickname(set.getString(5));
				things.setPlan_image(set.getString(6));
				things.setPlan_name(set.getString(7));
				things.setDevicesModels(models);
				
				list.add(things);
			}
			
			devices.setCodeStatus(200);
			devices.setNameStatus("Success");
			devices.setRoot_area(root);
			devices.setSub_area(sub);
			devices.setDevices(list);
			
		}catch (SQLException e) {
			devices.setCodeStatus(500);
			devices.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(devices);
		}
	}
	
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "requests/{user}/{things}", method = RequestMethod.GET)
	public ResponseEntity<ListLastRequest> getLastRequest(@PathVariable("user") int user_id, @PathVariable("things") int things_id) {
		ListLastRequest lastRequest = new ListLastRequest();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ResultSet set = null;
		
		try {
			String sql = "select device_id from things inner join devices on things.things_id = device_esp where things_id = ?";
			statement = con.prepareStatement(sql);
			statement.setInt(1, things_id);
			set = statement.executeQuery();
			List<LastRequest> list = new ArrayList<LastRequest>();
			while(set.next()) {
				String sql2 = "select permit_id, permit_device, permit_status, permit_privilage, permit_grant_status, enabled from permit_user where permit_device = ? and permit_user = ? order by permit_id desc limit 1";
				statement = con.prepareStatement(sql2);
				statement.setInt(1, set.getInt(1));
				statement.setInt(2, user_id);
				ResultSet set2 = statement.executeQuery();		
				
				while(set2.next()) {
					LastRequest request = new LastRequest();
					request.setPermit_id(set2.getInt(1));
					request.setPermit_device(set2.getInt(2));
					request.setPermit_status(set2.getBoolean(3));
					request.setPermit_privilage(set2.getString(4));
					request.setPermit_grant(set2.getInt(5));
					request.setPermit_enable(set2.getBoolean(6));
					
					list.add(request);
				}				
				
			}
			
			lastRequest.setCodeStatus(200);
			lastRequest.setNameStatus("Success");
			lastRequest.setRequests(list);
			
		}catch (SQLException e) {
			lastRequest.setCodeStatus(500);
			lastRequest.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(lastRequest);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "requests/door/{user}/{things}", method = RequestMethod.GET)
	public ResponseEntity<ListDoorRequest> getDoorLastRequest(@PathVariable("user") int user_id, @PathVariable("things") int things_id) {
		ListDoorRequest list = new ListDoorRequest();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ResultSet set = null;
		
		try {
			String sql = "select * from door_user where user = ? and thing = ?";
			statement = con.prepareStatement(sql);
			statement.setInt(1, user_id);
			statement.setInt(2, things_id);
			set = statement.executeQuery();
			
			List<DoorRequest> requests = new ArrayList<DoorRequest>();
			while(set.next()) {
				DoorRequest request = new DoorRequest();
				request.setRequest_id(set.getInt(1));
				request.setRequest_user(set.getInt(2));
				request.setRequest_things(set.getInt(3));
				request.setRequest_status(set.getBoolean(4));
				request.setRequest_enable(set.getBoolean(6));
				
				requests.add(request);
			}
			
			list.setCodeStatus(200);
			list.setNameStatus("Success");
			list.setRequests(requests);
			
		}catch (SQLException e) {
			list.setCodeStatus(500);
			list.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(list);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/status/{user}")
	public ResponseEntity<ResStatusDevices> getLastStatusDevices(@PathVariable("user") int user_id){
		ResStatusDevices devices = new ResStatusDevices();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ResultSet set = null;
		
		try {
			String sql = "select * from permit_user where permit_user = ? and permit_status = 1 and enabled = 1";
			statement = con.prepareStatement(sql);
			statement.setInt(1, user_id);
			set = statement.executeQuery();
			List<LastStatusDevices> list = new ArrayList<LastStatusDevices>();
			while(set.next()) {				
				String sql2 = "select * from device_log_io inner join user_detail on user_detail.user_detail_id = device_log_io.log_user \r\n" + 
						"					 left join things_favorite on things_favorite.favorite_devices = device_log_io.log_device  \r\n" + 
						"                    where log_device = ? order by log_id desc limit 1";
				statement = con.prepareStatement(sql2);
				statement.setInt(1, set.getInt(3));
				ResultSet set2 = statement.executeQuery();
				
				while(set2.next()) {
					
					boolean status = true;
					
					if(set2.getInt(2) == 1 || set2.getInt(2) == 3) {
						status = true;
					}else {
						status = false;
					}
					
					LastStatusDevices lastStatusDevices = new LastStatusDevices();
					lastStatusDevices.setLog_status(set2.getInt(2));
					lastStatusDevices.setLog_boolean(status);
					lastStatusDevices.setDevices_id(set2.getInt(3));
					lastStatusDevices.setLog_user(set2.getInt(4));
					lastStatusDevices.setLog_timer(set2.getString(5));
					lastStatusDevices.setFirst_name(set2.getString(7));
					lastStatusDevices.setLast_name(set2.getString(8));
					lastStatusDevices.setFeverite_id(set2.getInt(12));
					lastStatusDevices.setIs_faverite(set2.getBoolean(17));
					
					list.add(lastStatusDevices);
				}
				
			}
			
			devices.setCodeStatus(200);
			devices.setNameStatus("Success");
			devices.setStatusDevices(list);
			
		} catch (SQLException e) {
			devices.setCodeStatus(500);
			devices.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(devices);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/status/door/{user_id}", method = RequestMethod.GET)
	public ResponseEntity<ResAccDoor> getDoorStatus(@PathVariable("user_id") int user_id){
		ResAccDoor door = new ResAccDoor();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ResultSet set = null;
		
		try {
			String sql ="select * from door_user left join door_favorite on door_favorite.favorite_door_things = door_user.thing where status = 1 and enable = 1 and user = ?";
			statement = con.prepareStatement(sql);
			statement.setInt(1, user_id);
			set = statement.executeQuery();
			
			List<AccepDoor> doors = new ArrayList<AccepDoor>();
			while(set.next()) {
				AccepDoor accepDoor = new AccepDoor();
				String sql2 = "select * from door_log inner join user_detail on user_detail.user_detail_id = door_log.door_log_user\r\n" + 
						"					          inner join door_status_type on door_status_type.door_st = door_log.door_log_status\r\n" + 
						"                             where door_log_things = ? order by door_log_id DESC LIMIT  1";
				statement = con.prepareStatement(sql2);
				statement.setInt(1, set.getInt(3));
				ResultSet set2 = statement.executeQuery();
				set2.next();
				
				accepDoor.setThingID(set.getInt(3));
				accepDoor.setThingsName(set.getString(7));
				accepDoor.setLastStatus(set2.getInt(2));
				accepDoor.setStatusType(set2.getString(13));
				accepDoor.setfName(set2.getString(7));
				accepDoor.setlName(set2.getString(8));
				accepDoor.setFeverite_id(set.getInt(8));
				accepDoor.setIs_feverite(set.getBoolean(12));
				
				doors.add(accepDoor);
			}
			
			door.setCodeStatus(200);
			door.setNameStatus("Success");
			door.setData(doors);
		} catch (SQLException e) {
			door.setCodeStatus(500);
			door.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(door);
		}
	}

}
