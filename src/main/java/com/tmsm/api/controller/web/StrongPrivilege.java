package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.Status;
import com.tmsm.api.model.web.ResStrongPrivilege;
import com.tmsm.api.utility.Database;
import com.tmsm.model.strong.Privilege;

@RestController
@RequestMapping("/allprivilege")
public class StrongPrivilege {

	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<ResStrongPrivilege> getAllPrivilege() {
		ResStrongPrivilege privilege = new ResStrongPrivilege();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ResultSet set = null;
		
		try {
			String sql = "select *from privilege";
			statement = con.prepareStatement(sql);
			set = statement.executeQuery();
			
			List<Privilege> privileges = new ArrayList<Privilege>();
			while(set.next()) {
				Privilege pri = new Privilege();
				pri.setPrivilegeID(set.getInt(1));
				pri.setPrivilegeName(set.getString(2));
				
				privileges.add(pri);
			}
			
			privilege.setCodeStatus(200);
			privilege.setNameStatus("Success");
			privilege.setPrivileges(privileges);
		}catch (SQLException e) {
			privilege.setCodeStatus(500);
			privilege.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(privilege);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Status> createPrivilege(@RequestBody Privilege privilege) {
		PreparedStatement preparedStatement = null;
		Connection connection = Database.connectDatabase();
		Status status = new Status();
		
		try {
			String sql ="insert into privilege (privilege_name) value (?)";
			preparedStatement = connection.prepareStatement(sql);
			
			preparedStatement.setString(1, privilege.getPrivilegeName());
			
			boolean st = preparedStatement.execute();
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}
		}catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(connection);
			return ResponseEntity.ok().body(status);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Status> updatePrivilege(@RequestBody Privilege privilege) {
		PreparedStatement preparedStatement = null;
		Connection connection = Database.connectDatabase();
		Status status = new Status();
		
		try {
			String sql ="update privilege set privilege_name = ? where privilege_id = ?";
			preparedStatement = connection.prepareStatement(sql);
			
			preparedStatement.setString(1, privilege.getPrivilegeName());
			preparedStatement.setInt(2, privilege.getPrivilegeID());
			
			boolean st = preparedStatement.execute();
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}
		}catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(connection);
			return ResponseEntity.ok().body(status);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Status> deletePrivilege(@PathVariable("id") int privilege) {
		PreparedStatement preparedStatement = null;
		Connection connection = Database.connectDatabase();
		Status status = new Status();
		
		try {
			String sql ="delete from privilege where privilege_id = ?";
			preparedStatement = connection.prepareStatement(sql);
			
			preparedStatement.setInt(1, privilege);
			
			boolean st = preparedStatement.execute();
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}
		}catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(connection);
			return ResponseEntity.ok().body(status);
		}
	}
}
