package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.web.DataValue;
import com.tmsm.api.model.web.ListDataValue;
import com.tmsm.api.utility.Database;

@CrossOrigin
@RestController
@RequestMapping("/valueof")
public class ResNumberData {

	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/{id}",method = RequestMethod.GET)
	public ResponseEntity<ListDataValue> getMyValue(@PathVariable("id") int id){
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ListDataValue value = new ListDataValue();
	
		try {
			String sql = "select count(device_id) from devices";
			statement = con.prepareStatement(sql);
			ResultSet set = statement.executeQuery();
			DataValue dataValue = new DataValue();			
			set.next();
			
				String sql2 = "select count(area_of_device_id) from area_of_devices";
				statement = con.prepareStatement(sql2);
				ResultSet set2 = statement.executeQuery();				
				set2.next();
				
				String sql3 = "select count(g_id) from group_device_of_user  where g_usr = ?";
				statement = con.prepareStatement(sql3);
				statement.setInt(1, id);
				ResultSet set3 = statement.executeQuery();
				set3.next();
				
				String sql4 = "select count(permit_id) from permit_user where permit_user = ? and enabled = 1";
				statement = con.prepareStatement(sql4);
				statement.setInt(1, id);
				ResultSet set4 = statement.executeQuery();
				set4.next();
				
				String sql5 = "select count(id) from door_user where user = ? and enable = 1";
				statement = con.prepareStatement(sql5);
				statement.setInt(1, id);
				ResultSet set5 = statement.executeQuery();
				set5.next();
			
				dataValue.setSystem_things(set.getInt(1));
				dataValue.setSystem_area(set2.getInt(1));
				dataValue.setGroup_count(set3.getInt(1));
				dataValue.setThings_count(set4.getInt(1) + set5.getInt(1));
				
				value.setCodeStatus(200);
				value.setNameStatus("Success");
				value.setData(dataValue);
		} catch (SQLException e) {
			value.setCodeStatus(500);
			value.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(value);
		}
	}
}
