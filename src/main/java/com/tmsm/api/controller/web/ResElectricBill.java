package com.tmsm.api.controller.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.web.AreaBillsList;
import com.tmsm.api.model.web.ElectricBill;
import com.tmsm.api.model.web.ExceptionResponse;
import com.tmsm.api.model.web.ResAllElectrictBill;
import com.tmsm.api.model.web.ResponseCommon;
import com.tmsm.api.model.web.ThingsBills;
import com.tmsm.api.utility.Database;
import java.text.DecimalFormat;

@RestController
@RequestMapping("v1/bill")
public class ResElectricBill {

//	@SuppressWarnings("finally")
//	@CrossOrigin
//	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
//	public ResponseEntity<ResAllElectrictBill> electricday(@PathVariable("id") int area) throws ParseException {
//
//		ResAllElectrictBill electrictBill = new ResAllElectrictBill();
//		Date date = new Date(System.currentTimeMillis());
//		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//		df.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
//		String datetime = df.format(date);
//
//		Connection connection = Database.connectDatabase();
//		PreparedStatement statement = null;
//		ResultSet set = null;
//
//		try {
//
//			String sql = "select things_id, device_id, detail_name, detail_power from things inner join  devices on things_id = device_esp \r\n"
//					+ "inner join devices_detail on detail_device = device_id where  things_id = ?";
//
//			statement = connection.prepareStatement(sql);
//			statement.setInt(1, area);
//			set = statement.executeQuery();
//
//			String timeBegin = datetime + " 00:00:00";
//			String timeEnd = datetime + " 01:00:00";
//			Date date1 = null;
//			
//			List<ElectricBill> bills =  new ArrayList<ElectricBill>();
//
//			while (set.next()) {
//				
//				String sql2 = "select * from device_log_io where log_datetime >= ?  AND  log_datetime >= ? AND log_device = ?";
//				statement = connection.prepareStatement(sql2);
//				statement.setString(1, timeBegin);
//				statement.setString(2, timeEnd);
//				statement.setInt(3, set.getInt(2));
//				
//				ResultSet set2 = statement.executeQuery();
//
//				SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
//				set2.last();
//				int size = set2.getRow();
//				set2.beforeFirst();
//				float allTime = 0;
//				
//				float powerWatt = set.getInt(4);
//				ElectricBill bill = new ElectricBill();
//
//				while (set2.next()) {	
//					
//					if (set2.getInt(2) == 1 || set2.getInt(2) == 3) {
//						String[] begin = set2.getString(5).split(" ");
//						date1 = format.parse(begin[1]);
//					}
//					
//					if(set2.getInt(2) == 2 || set2.getInt(2) == 4) {
//						String[] end = set2.getString(5).split(" ");						
//						Date date2 = format.parse(end[1]);
//						long differance = date2.getTime() - date1.getTime();						
//						allTime += differance;
//					}
//
//					size = size - 1;
//					
//					if (size == 0) {
//						double price = 0;
//						float unit = 0;
//						float timeFinal = (allTime / (60 * 1000) % 60) / 60;
//						
//						float rawUnit = (powerWatt / 1000) * timeFinal;
//						String formattedString = String.format("%.02f", rawUnit);
//						
//						if(Float.valueOf(formattedString) > 0 && Float.valueOf(formattedString) <= 35) {
//							price = 85.21;							
//							unit = Float.valueOf(formattedString) - 35;
//						}
//						
//						if(unit > 0 && unit <= 115 ) {
//							price = price + (unit * 1.1236);
//							unit = unit - 115;
//						}
//						
//						if(unit > 0 && unit <= 250 ) {
//							price = price + (unit * 1.1236);
//							unit = unit - 115;
//						}
//						
//						if( unit > 400 ) {
//							price = price + (unit * 2.4226);
//						} else if (unit > 0 && unit < 400) {
//							price = price + (unit * 1.1236);
//						}
//						
//						bill.setElectricBill(price);
//						bill.setElectricUnit(Double.valueOf(formattedString));	
//					}		
//					
//				}
//				
//				bill.setDeviceId(set.getInt(2));
//				bill.setDeviceName(set.getString(3));
//				
//				bills.add(bill);
//
//			}
//			
//			electrictBill.setCodeStatus(200);
//			electrictBill.setNameStatus("Success");
//			electrictBill.setBills(bills);
//
//		} catch (SQLException e) {
//			electrictBill.setCodeStatus(500);
//			electrictBill.setNameStatus(e.getMessage());
//		}finally {
//			Database.closeConnection(connection);
//			return ResponseEntity.ok().body(electrictBill);
//		}
//		
//	}

	@CrossOrigin
	@RequestMapping(value = "daily/{id}/things")
	public ResponseEntity<?> getDailyBillThings(@PathVariable("id") int thing_id) {
		List<ElectricBill> devicesBills = new ArrayList<ElectricBill>();
		List<ThingsBills> listTings = new ArrayList<ThingsBills>();
		ExceptionResponse exeption = new ExceptionResponse();
		ResponseCommon response = new ResponseCommon();
		ThingsBills thingsBills = new ThingsBills();

		Connection con = Database.connectDatabase();
//		Date date = new Date(System.currentTimeMillis());
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));

//		String datetime = df.format(date);			
		String beginDateTime = "2020-02-23 00:00:00";
		String endDateTime = "2020-02-23 23:59:59";
		Double sumUnit = 0.0;

		try {
			String sqlGetDevices = "select things_id, device_id, detail_name, detail_power, detail_number_electric from things inner join  devices on things_id = device_esp \r\n"
					+ "inner join devices_detail on detail_device = device_id where  things_id = ?";

			PreparedStatement statementDevices = con.prepareStatement(sqlGetDevices);
			statementDevices.setInt(1, thing_id);
			ResultSet setDevices = statementDevices.executeQuery();

			while (setDevices.next()) {
				String sql = "select * from device_log_io where log_datetime >= ?  AND  log_datetime <= ? AND log_device = ? order by log_id ASC";

				PreparedStatement statement = con.prepareStatement(sql);
				statement.setString(1, beginDateTime);
				statement.setString(2, endDateTime);
				statement.setInt(3, setDevices.getInt("device_id"));

				ResultSet set = statement.executeQuery();

				long anyTime = 0;
				Date beginTime = null;
				Date endTime = null;
				SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");

				if (set.first()) {
					String[] begin = set.getString("log_datetime").split(" ");

					if (begin[1] != "00:00:00.0" && set.getInt("log_status") == 2 || set.getInt("log_status") == 4) {
						beginTime = format.parse("00:00:00.0");
						endTime = format.parse(begin[1]);

						long differance = endTime.getTime() - beginTime.getTime();
						anyTime = differance / (60 * 60 * 1000);
					}
				}

				ElectricBill bill = new ElectricBill();

				while (set.next()) {

					if (set.getInt("log_status") == 1 || set.getInt("log_status") == 3) {
						String[] onLight = set.getString("log_datetime").split(" ");

						beginTime = format.parse(onLight[1]);
					}

					if (set.getInt("log_status") == 2 || set.getInt("log_status") == 4) {
						String[] closeLight = set.getString("log_datetime").split(" ");

						endTime = format.parse(closeLight[1]);

						long differance = endTime.getTime() - beginTime.getTime();
						anyTime = anyTime + differance / (60 * 60 * 1000);
					}
				}

				Double rawUnit = (double) ((setDevices.getFloat("detail_power")
						* setDevices.getFloat("detail_number_electric")) / 1000) * anyTime;
				Double price = countElectricityBill(rawUnit);
				sumUnit = sumUnit + rawUnit;

				bill.setDevice_id(setDevices.getInt("device_id"));
				bill.setDevice_name(setDevices.getString("detail_name"));
				bill.setElectric_unit(rawUnit);
				bill.setElectric_bill(price);

				devicesBills.add(bill);
			}

			thingsBills.setThings_id(thing_id);
			thingsBills.setThings_unit(sumUnit);
			thingsBills.setThings_bills(countElectricityBill(sumUnit));
			thingsBills.setDevices_count(devicesBills.size());
			thingsBills.setDevices_list(devicesBills);

			listTings.add(thingsBills);

			response.setResult_list(listTings);
			response.setTotal_count(listTings.size());
			Database.closeConnection(con);

			return ResponseEntity.ok().body(response);
		} catch (SQLException e) {
			Database.closeConnection(con);
			exeption.setMessage(e.getMessage());

			return ResponseEntity.badRequest().body(exeption);
		} catch (ParseException e) {

			Database.closeConnection(con);
			exeption.setMessage(e.getMessage());

			return ResponseEntity.badRequest().body(exeption);
		}
	}

	@CrossOrigin
	@RequestMapping(value = "daily/{id}/subareas")
	public ResponseEntity<?> getDailyBillSubArea(@PathVariable("id") int subarea_id) {
		Connection con = Database.connectDatabase();
//		Date date = new Date(System.currentTimeMillis());
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));

//		String datetime = df.format(date);			
		String beginDateTime = "2020-02-23 00:00:00";
		String endDateTime = "2020-02-23 23:59:59";
		int thingsId = 0;
		Double sumUnit = 0.0;

		try {
			String sqlThing = "select things_id, things_nickname, device_id, detail_name, detail_power, detail_number_electric from things \r\n"
					+ "inner join  devices on things_id = device_esp \r\n"
					+ "inner join devices_detail on detail_device = device_id \r\n" + "where  things_area = ?";

			PreparedStatement devicesState = con.prepareStatement(sqlThing);
			devicesState.setInt(1, subarea_id);
			ResultSet devicesSet = devicesState.executeQuery();

			List<ThingsBills> thingsBills = new ArrayList<ThingsBills>();

			while (devicesSet.next()) {

				String sql = "select * from device_log_io where log_datetime >= ?  AND  log_datetime <= ? AND log_device = ? order by log_id ASC";

				PreparedStatement statement = con.prepareStatement(sql);
				statement.setString(1, beginDateTime);
				statement.setString(2, endDateTime);
				statement.setInt(3, devicesSet.getInt("device_id"));

				ResultSet set = statement.executeQuery();

				long anyTime = 0;
				Date beginTime = null;
				Date endTime = null;
				SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");

				if (set.first()) {
					String[] begin = set.getString("log_datetime").split(" ");

					if (begin[1] != "00:00:00.0" && set.getInt("log_status") == 2 || set.getInt("log_status") == 4) {
						beginTime = format.parse("00:00:00.0");
						endTime = format.parse(begin[1]);

						long differance = endTime.getTime() - beginTime.getTime();
						anyTime = differance / (60 * 60 * 1000);
					}
				}
				
				while (set.next()) {

					if (set.getInt("log_status") == 1 || set.getInt("log_status") == 3) {
						String[] onLight = set.getString("log_datetime").split(" ");

						beginTime = format.parse(onLight[1]);
					}

					if (set.getInt("log_status") == 2 || set.getInt("log_status") == 4) {
						String[] closeLight = set.getString("log_datetime").split(" ");

						endTime = format.parse(closeLight[1]);

						long differance = endTime.getTime() - beginTime.getTime();
						anyTime = anyTime + differance / (60 * 60 * 1000);
					}
				}
				
				Double rawUnit = (double) ((devicesSet.getFloat("detail_power")	* devicesSet.getFloat("detail_number_electric")) / 1000) * anyTime;
				Double price = countElectricityBill(rawUnit);
				sumUnit = sumUnit + rawUnit;

				if (devicesSet.getInt("things_id") == thingsId) {
					for (int i = 0; i < thingsBills.size(); i++) {

						ElectricBill bill = new ElectricBill();
						List<ElectricBill> list = new ArrayList<ElectricBill>();

						if (thingsBills.get(i).getThings_id() == thingsId) {
							list = thingsBills.get(i).getDevices_list();
							
							bill.setDevice_id(devicesSet.getInt("device_id"));
							bill.setDevice_name(devicesSet.getString("detail_name"));
							bill.setElectric_unit(rawUnit);
							bill.setElectric_bill(price);
							
							list.add(bill);

							thingsBills.get(i).setThings_unit(thingsBills.get(i).getThings_unit() + sumUnit);
							thingsBills.get(i).setThings_bills(countElectricityBill(sumUnit));
							thingsBills.get(i).setDevices_count(list.size());
							thingsBills.get(i).setDevices_list(list);
							sumUnit = 0.0;
						}
					}

				} else {
					thingsId = devicesSet.getInt("things_id");
					ElectricBill bill = new ElectricBill();
					List<ElectricBill> list = new ArrayList<ElectricBill>();
					ThingsBills bills = new ThingsBills();

					bill.setDevice_id(devicesSet.getInt("device_id"));
					bill.setDevice_name(devicesSet.getString("detail_name"));
					bill.setElectric_unit(rawUnit);
					bill.setElectric_bill(price);
					list.add(bill);

					bills.setThings_id(thingsId);
					bills.setThings_unit(sumUnit);
					bills.setThings_bills(countElectricityBill(sumUnit));
					bills.setDevices_list(list);
					bills.setDevices_count(list.size());

					thingsBills.add(bills);
					sumUnit = 0.0;
				}

			}
			
			ResponseCommon response = new ResponseCommon();
			response.setResult_list(thingsBills);
			response.setTotal_count(thingsBills.size());

			return ResponseEntity.ok().body(response);
		} catch (Exception e) {
			ExceptionResponse exeption = new ExceptionResponse();
			Database.closeConnection(con);
			exeption.setMessage(e.getMessage());

			return ResponseEntity.badRequest().body(exeption);
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "daily/{id}/areas")
	public ResponseEntity<?> getDailyBillArea(@PathVariable("id") int area_id) {
		Connection con = Database.connectDatabase();
//		Date date = new Date(System.currentTimeMillis());
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));

//		String datetime = df.format(date);			
		String beginDateTime = "2020-02-23 00:00:00";
		String endDateTime = "2020-02-23 23:59:59";
		int thingsId = 0;
		Double sumUnit = 0.0;
		Double countBills = 0.0;
		Double countUnit = 0.0;

		try {
			String sqlThing = "select things_id, things_nickname, device_id, detail_name, detail_power, detail_number_electric from things \r\n" + 
					"inner join detail_area on things_area = dear_id \r\n" + 
					"inner join area_of_devices on area_of_device_id = dear_area \r\n" + 
					"inner join  devices on things_id = device_esp \r\n" + 
					"inner join devices_detail on detail_device = device_id\r\n" + 
					"where area_of_device_id = ?";

			PreparedStatement devicesState = con.prepareStatement(sqlThing);
			devicesState.setInt(1, area_id);
			ResultSet devicesSet = devicesState.executeQuery();

			List<ThingsBills> thingsBills = new ArrayList<ThingsBills>();
			List<AreaBillsList> areaList = new ArrayList<AreaBillsList>();
			AreaBillsList areaBillsList = new AreaBillsList();

			while (devicesSet.next()) {

				String sql = "select * from device_log_io where log_datetime >= ?  AND  log_datetime <= ? AND log_device = ? order by log_id ASC";

				PreparedStatement statement = con.prepareStatement(sql);
				statement.setString(1, beginDateTime);
				statement.setString(2, endDateTime);
				statement.setInt(3, devicesSet.getInt("device_id"));

				ResultSet set = statement.executeQuery();

				long anyTime = 0;
				Date beginTime = null;
				Date endTime = null;
				SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");

				if (set.first()) {
					String[] begin = set.getString("log_datetime").split(" ");

					if (begin[1] != "00:00:00.0" && set.getInt("log_status") == 2 || set.getInt("log_status") == 4) {
						beginTime = format.parse("00:00:00.0");
						endTime = format.parse(begin[1]);

						long differance = endTime.getTime() - beginTime.getTime();
						anyTime = differance / (60 * 60 * 1000);
					}
				}
				
				while (set.next()) {

					if (set.getInt("log_status") == 1 || set.getInt("log_status") == 3) {
						String[] onLight = set.getString("log_datetime").split(" ");

						beginTime = format.parse(onLight[1]);
					}

					if (set.getInt("log_status") == 2 || set.getInt("log_status") == 4) {
						String[] closeLight = set.getString("log_datetime").split(" ");

						endTime = format.parse(closeLight[1]);

						long differance = endTime.getTime() - beginTime.getTime();
						anyTime = anyTime + differance / (60 * 60 * 1000);
					}
				}
				
				Double rawUnit = (double) ((devicesSet.getFloat("detail_power")	* devicesSet.getFloat("detail_number_electric")) / 1000) * anyTime;
				Double price = countElectricityBill(rawUnit);
				sumUnit = sumUnit + rawUnit;
				countBills = countBills + price;
				countUnit = countBills + rawUnit;

				if (devicesSet.getInt("things_id") == thingsId) {
					for (int i = 0; i < thingsBills.size(); i++) {

						ElectricBill bill = new ElectricBill();
						List<ElectricBill> list = new ArrayList<ElectricBill>();

						if (thingsBills.get(i).getThings_id() == thingsId) {
							list = thingsBills.get(i).getDevices_list();
							
							bill.setDevice_id(devicesSet.getInt("device_id"));
							bill.setDevice_name(devicesSet.getString("detail_name"));
							bill.setElectric_unit(rawUnit);
							bill.setElectric_bill(price);
							
							list.add(bill);

							thingsBills.get(i).setThings_unit(thingsBills.get(i).getThings_unit() + sumUnit);
							thingsBills.get(i).setThings_bills(countElectricityBill(sumUnit));
							thingsBills.get(i).setDevices_count(list.size());
							thingsBills.get(i).setDevices_list(list);
							sumUnit = 0.0;
						}
					}

				} else {
					thingsId = devicesSet.getInt("things_id");
					ElectricBill bill = new ElectricBill();
					List<ElectricBill> list = new ArrayList<ElectricBill>();
					ThingsBills bills = new ThingsBills();

					bill.setDevice_id(devicesSet.getInt("device_id"));
					bill.setDevice_name(devicesSet.getString("detail_name"));
					bill.setElectric_unit(rawUnit);
					bill.setElectric_bill(price);
					list.add(bill);

					bills.setThings_id(thingsId);
					bills.setThings_unit(sumUnit);
					bills.setThings_bills(countElectricityBill(sumUnit));
					bills.setDevices_list(list);
					bills.setDevices_count(list.size());

					thingsBills.add(bills);
					sumUnit = 0.0;
				}

			}
			
			areaBillsList.setUnits(countUnit);
			areaBillsList.setBiils(countElectricityBill(countUnit));
			areaBillsList.setThings_list(thingsBills);
			areaBillsList.setThings_count(thingsBills.size());
			
			areaList.add(areaBillsList);
			
			ResponseCommon response = new ResponseCommon();
			response.setResult_list(areaList);
			response.setTotal_count(areaList.size());

			return ResponseEntity.ok().body(response);
		} catch (Exception e) {
			ExceptionResponse exeption = new ExceptionResponse();
			Database.closeConnection(con);
			exeption.setMessage(e.getMessage());

			return ResponseEntity.badRequest().body(exeption);
		}
	}
	
//	@CrossOrigin
//	@RequestMapping(value = "monthly/{id}/areas")
//	public ResponseEntity<?> getmonthlyBillArea(@PathVariable("id") int area_id) {
//		Connection con = Database.connectDatabase();
////		Date date = new Date(System.currentTimeMillis());
//		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//		df.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
//
////		String datetime = df.format(date);			
//		String beginDateTime = "2020-02-23 00:00:00";
//		String endDateTime = "2020-02-23 23:59:59";
//		int thingsId = 0;
//		Double sumUnit = 0.0;
//		Double countBills = 0.0;
//		Double countUnit = 0.0;
//
//		try {
//			String sqlThing = "select things_id, things_nickname, device_id, detail_name, detail_power, detail_number_electric from things \r\n" + 
//					"inner join detail_area on things_area = dear_id \r\n" + 
//					"inner join area_of_devices on area_of_device_id = dear_area \r\n" + 
//					"inner join  devices on things_id = device_esp \r\n" + 
//					"inner join devices_detail on detail_device = device_id\r\n" + 
//					"where area_of_device_id = ?";
//
//			PreparedStatement devicesState = con.prepareStatement(sqlThing);
//			devicesState.setInt(1, area_id);
//			ResultSet devicesSet = devicesState.executeQuery();
//
//			List<ThingsBills> thingsBills = new ArrayList<ThingsBills>();
//			List<AreaBillsList> areaList = new ArrayList<AreaBillsList>();
//			AreaBillsList areaBillsList = new AreaBillsList();
//
//			while (devicesSet.next()) {
//
//				String sql = "select * from device_log_io where log_datetime >= ?  AND  log_datetime <= ? AND log_device = ? order by log_id ASC";
//
//				PreparedStatement statement = con.prepareStatement(sql);
//				statement.setString(1, beginDateTime);
//				statement.setString(2, endDateTime);
//				statement.setInt(3, devicesSet.getInt("device_id"));
//
//				ResultSet set = statement.executeQuery();
//
//				long anyTime = 0;
//				Date beginTime = null;
//				Date endTime = null;
//				SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
//
//				if (set.first()) {
//					String[] begin = set.getString("log_datetime").split(" ");
//
//					if (begin[1] != "00:00:00.0" && set.getInt("log_status") == 2 || set.getInt("log_status") == 4) {
//						beginTime = format.parse("00:00:00.0");
//						endTime = format.parse(begin[1]);
//
//						long differance = endTime.getTime() - beginTime.getTime();
//						anyTime = differance / (60 * 60 * 1000);
//					}
//				}
//				
//				while (set.next()) {
//
//					if (set.getInt("log_status") == 1 || set.getInt("log_status") == 3) {
//						String[] onLight = set.getString("log_datetime").split(" ");
//
//						beginTime = format.parse(onLight[1]);
//					}
//
//					if (set.getInt("log_status") == 2 || set.getInt("log_status") == 4) {
//						String[] closeLight = set.getString("log_datetime").split(" ");
//
//						endTime = format.parse(closeLight[1]);
//
//						long differance = endTime.getTime() - beginTime.getTime();
//						anyTime = anyTime + differance / (60 * 60 * 1000);
//					}
//				}
//				
//				Double rawUnit = (double) ((devicesSet.getFloat("detail_power")	* devicesSet.getFloat("detail_number_electric")) / 1000) * anyTime;
//				Double price = countElectricityBill(rawUnit);
//				sumUnit = sumUnit + rawUnit;
//				countBills = countBills + price;
//				countUnit = countBills + rawUnit;
//
//				if (devicesSet.getInt("things_id") == thingsId) {
//					for (int i = 0; i < thingsBills.size(); i++) {
//
//						ElectricBill bill = new ElectricBill();
//						List<ElectricBill> list = new ArrayList<ElectricBill>();
//
//						if (thingsBills.get(i).getThings_id() == thingsId) {
//							list = thingsBills.get(i).getDevices_list();
//							
//							bill.setDevice_id(devicesSet.getInt("device_id"));
//							bill.setDevice_name(devicesSet.getString("detail_name"));
//							bill.setElectric_unit(rawUnit);
//							bill.setElectric_bill(price);
//							
//							list.add(bill);
//
//							thingsBills.get(i).setThings_unit(thingsBills.get(i).getThings_unit() + sumUnit);
//							thingsBills.get(i).setThings_bills(countElectricityBill(sumUnit));
//							thingsBills.get(i).setDevices_count(list.size());
//							thingsBills.get(i).setDevices_list(list);
//							sumUnit = 0.0;
//						}
//					}
//
//				} else {
//					thingsId = devicesSet.getInt("things_id");
//					ElectricBill bill = new ElectricBill();
//					List<ElectricBill> list = new ArrayList<ElectricBill>();
//					ThingsBills bills = new ThingsBills();
//
//					bill.setDevice_id(devicesSet.getInt("device_id"));
//					bill.setDevice_name(devicesSet.getString("detail_name"));
//					bill.setElectric_unit(rawUnit);
//					bill.setElectric_bill(price);
//					list.add(bill);
//
//					bills.setThings_id(thingsId);
//					bills.setThings_unit(sumUnit);
//					bills.setThings_bills(countElectricityBill(sumUnit));
//					bills.setDevices_list(list);
//					bills.setDevices_count(list.size());
//
//					thingsBills.add(bills);
//					sumUnit = 0.0;
//				}
//
//			}
//			
//			areaBillsList.setUnits(countUnit);
//			areaBillsList.setBiils(countElectricityBill(countUnit));
//			areaBillsList.setThings_list(thingsBills);
//			areaBillsList.setThings_count(thingsBills.size());
//			
//			areaList.add(areaBillsList);
//			
//			ResponseCommon response = new ResponseCommon();
//			response.setResult_list(areaList);
//			response.setTotal_count(areaList.size());
//
//			return ResponseEntity.ok().body(response);
//		} catch (Exception e) {
//			ExceptionResponse exeption = new ExceptionResponse();
//			Database.closeConnection(con);
//			exeption.setMessage(e.getMessage());
//
//			return ResponseEntity.badRequest().body(exeption);
//		}
//	}

	private Double countElectricityBill(Double unit) {
		Double price = null;

		if (unit == 0.0) {

			return 0.0;
		}

		price = 85.21;

		if (unit > 35.0) {
			unit -= 35;

			if (unit <= 115.0 && unit > 0.0) {
				price += unit * 1.1236;
				unit -= 115;

				if (unit <= 250.0 && unit > 0.0) {
					price += unit * 2.1329;
					unit -= 250;

					if (unit >= 400.0) {
						price += unit * 2.4226;
					} else if (unit > 0.0) {
						price += unit * 2.1329;
						unit -= unit;
					}
				}
			}
		}

		return price;
	}

}
