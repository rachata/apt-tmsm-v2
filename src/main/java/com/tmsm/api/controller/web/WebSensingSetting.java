package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.Status;
import com.tmsm.api.model.web.AcceptSensing;
import com.tmsm.api.model.web.ListSettingSensing;
import com.tmsm.api.model.web.SettingSensing;
import com.tmsm.api.utility.Database;

@RestController
@CrossOrigin
@RequestMapping("web/sensing")
public class WebSensingSetting {
	
	@CrossOrigin
	@SuppressWarnings("finally")
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Status> acceptSensing(@RequestBody AcceptSensing sensing){
		Status status = new Status();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "update setting_remote_sensing set setting_status = ?, setting_enable = ? where setting_id = ?";
			statement = con.prepareStatement(sql);
			statement.setBoolean(1, sensing.isSensing_status());
			statement.setBoolean(2, sensing.isSensing_enable());
			statement.setInt(3, sensing.getSensing_id());
			
			boolean st = statement.execute();
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");
			}
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/accept/{area_id}/{privilege_id}/{user_id}", method = RequestMethod.GET)
	public ResponseEntity<ListSettingSensing> getAcceptSetting(@PathVariable("area_id") int area_id, @PathVariable("privilege_id") int privilege, @PathVariable("user_id") int user_id){
		ListSettingSensing sensing = new ListSettingSensing();
		PreparedStatement statement = null;
		ResultSet set = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "select things_id, things_type, things_nickname, dear_area, device_id from things inner join detail_area on dear_id = things_area \r\n" + 
					"					 inner join devices on things_id = device_esp\r\n" + 
					"					 where dear_area = ? and things_status = 1";
			statement = con.prepareStatement(sql);
			statement.setInt(1, area_id);
			set = statement.executeQuery();
			
			List<SettingSensing> lists = new ArrayList<SettingSensing>();
			while(set.next()) {
				
				String sql2 = "select setting_id,devices_io,devices_sensor,setting_user,setting_min,setting_max,setting_status,setting_datas,\r\n" + 
						"	   setting_enable,setting_date,setting_select,setting_io_status,user_detail_fname,user_detail_fname,things_nickname from \r\n" + 
						"		setting_remote_sensing inner join user_detail on setting_user = user_detail_id\r\n" + 
						"		inner join devices on devices_sensor = device_id\r\n" + 
						"		inner join things on things_id = device_esp\r\n" + 
						"       where devices_io = ? and setting_status = 1 and setting_enable = 1";
				statement = con.prepareStatement(sql2);
				statement.setInt(1, set.getInt("device_id"));
				
				ResultSet set2 = statement.executeQuery();
				
				while(set2.next()) {
					SettingSensing sensing2 = new SettingSensing();
					sensing2.setSetting_id(set2.getInt(1));
					sensing2.setDevices_io(set2.getInt(2));
					sensing2.setDevices_sensor(set2.getInt(3));
					sensing2.setSetting_user(set2.getInt(4));
					sensing2.setSetting_min(set2.getInt(5));
					sensing2.setSetting_max(set2.getInt(6));
					sensing2.setSetting_status(set2.getBoolean(7));
					sensing2.setSetting_datas(set2.getFloat(8));
					sensing2.setSetting_enable(set2.getBoolean(9));
					sensing2.setSetting_date(set2.getString(10));
					sensing2.setSetting_select(set2.getInt(11));
					sensing2.setSetting_io(set2.getInt(12));
					sensing2.setFull_name(set2.getString(13) + " " + set2.getString(14));
					sensing2.setIo_name(set.getString("things_nickname"));
					sensing2.setSensing_name(set2.getString("things_nickname"));
					lists.add(sensing2);
				}
				
			}
			
			sensing.setCodeStatus(200);
			sensing.setNameStatus("Success");
			sensing.setSensings(lists);
		} catch (SQLException e) {
			sensing.setCodeStatus(200);
			sensing.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(sensing);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/unaccept/{area_id}/{privilege_id}/{user_id}", method = RequestMethod.GET)
	public ResponseEntity<ListSettingSensing> getUnacceptSetting(@PathVariable("area_id") int area_id, @PathVariable("privilege_id") int privilege, @PathVariable("user_id") int user_id){
		ListSettingSensing sensing = new ListSettingSensing();
		PreparedStatement statement = null;
		ResultSet set = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "select things_id, things_type, things_nickname, dear_area, device_id from things inner join detail_area on dear_id = things_area \r\n" + 
					"					 inner join devices on things_id = device_esp\r\n" + 
					"					 where dear_area = ? and things_status = 1";
			statement = con.prepareStatement(sql);
			statement.setInt(1, area_id);
			set = statement.executeQuery();
			
			List<SettingSensing> lists = new ArrayList<SettingSensing>();
			while(set.next()) {
				
				String sql2 = "select setting_id,devices_io,devices_sensor,setting_user,setting_min,setting_max,setting_status,setting_datas,\r\n" + 
						"	   setting_enable,setting_date,setting_select,setting_io_status,user_detail_fname,user_detail_lname,things_nickname from \r\n" + 
						"		setting_remote_sensing inner join user_detail on setting_user = user_detail_id\r\n" + 
						"		inner join devices on devices_sensor = device_id\r\n" + 
						"		inner join things on things_id = device_esp\r\n" + 
						"       where devices_io = ? and setting_status = 0 and setting_enable = 1";
				statement = con.prepareStatement(sql2);
				statement.setInt(1, set.getInt("device_id"));
				
				ResultSet set2 = statement.executeQuery();
				
				while(set2.next()) {
					SettingSensing sensing2 = new SettingSensing();
					sensing2.setSetting_id(set2.getInt(1));
					sensing2.setDevices_io(set2.getInt(2));
					sensing2.setDevices_sensor(set2.getInt(3));
					sensing2.setSetting_user(set2.getInt(4));
					sensing2.setSetting_min(set2.getInt(5));
					sensing2.setSetting_max(set2.getInt(6));
					sensing2.setSetting_status(set2.getBoolean(7));
					sensing2.setSetting_datas(set2.getFloat(8));
					sensing2.setSetting_enable(set2.getBoolean(9));
					sensing2.setSetting_date(set2.getString(10));
					sensing2.setSetting_select(set2.getInt(11));
					sensing2.setSetting_io(set2.getInt(12));
					sensing2.setFull_name(set2.getString(13) + " " + set2.getString(14));
					sensing2.setIo_name(set.getString("things_nickname"));
					sensing2.setSensing_name(set2.getString("things_nickname"));
					lists.add(sensing2);
				}
				
			}
			
			sensing.setCodeStatus(200);
			sensing.setNameStatus("Success");
			sensing.setSensings(lists);
		} catch (SQLException e) {
			sensing.setCodeStatus(200);
			sensing.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(sensing);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/{device_id}", method = RequestMethod.GET)
	public ResponseEntity<ListSettingSensing> getDevicesSetting(@PathVariable("device_id") int device_id){
		ListSettingSensing sensing = new ListSettingSensing();
		PreparedStatement statement = null;
		ResultSet set = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "select things_id, things_type, things_nickname, dear_area, device_id from things inner join detail_area on dear_id = things_area \r\n" + 
					"					 inner join devices on things_id = device_esp\r\n" + 
					"					 where device_id = ? and things_status = 1";
			statement = con.prepareStatement(sql);
			statement.setInt(1, device_id);
			set = statement.executeQuery();
			
			List<SettingSensing> lists = new ArrayList<SettingSensing>();
			while(set.next()) {
				
				String sql2 = "select setting_id,devices_io,devices_sensor,setting_user,setting_min,setting_max,setting_status,setting_datas,\r\n" + 
						"	   setting_enable,setting_date,setting_select,setting_io_status,user_detail_fname,user_detail_lname,things_nickname from \r\n" + 
						"		setting_remote_sensing inner join user_detail on setting_user = user_detail_id\r\n" + 
						"		inner join devices on devices_sensor = device_id\r\n" + 
						"		inner join things on things_id = device_esp\r\n" + 
						"       where devices_io = ? and setting_status = 1 and setting_enable = 1";
				statement = con.prepareStatement(sql2);
				statement.setInt(1, set.getInt("device_id"));
				
				ResultSet set2 = statement.executeQuery();
				
				while(set2.next()) {
					SettingSensing sensing2 = new SettingSensing();
					sensing2.setSetting_id(set2.getInt(1));
					sensing2.setDevices_io(set2.getInt(2));
					sensing2.setDevices_sensor(set2.getInt(3));
					sensing2.setSetting_user(set2.getInt(4));
					sensing2.setSetting_min(set2.getInt(5));
					sensing2.setSetting_max(set2.getInt(6));
					sensing2.setSetting_status(set2.getBoolean(7));
					sensing2.setSetting_datas(set2.getFloat(8));
					sensing2.setSetting_enable(set2.getBoolean(9));
					sensing2.setSetting_date(set2.getString(10));
					sensing2.setSetting_select(set2.getInt(11));
					sensing2.setSetting_io(set2.getInt(12));
					sensing2.setFull_name(set2.getString(13) + " " + set2.getString(14));
					sensing2.setIo_name(set.getString("things_nickname"));
					sensing2.setSensing_name(set2.getString("things_nickname"));
					lists.add(sensing2);
				}
				
			}
			
			sensing.setCodeStatus(200);
			sensing.setNameStatus("Success");
			sensing.setSensings(lists);
		} catch (SQLException e) {
			sensing.setCodeStatus(200);
			sensing.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(sensing);
		}
	}
	
}
