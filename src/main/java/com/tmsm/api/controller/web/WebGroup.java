package com.tmsm.api.controller.web;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.web.DevicesGroup;
import com.tmsm.api.model.web.DoorGroup;
import com.tmsm.api.model.web.DoorGroupDetail;
import com.tmsm.api.model.web.LastStatusDevices;
import com.tmsm.api.model.web.ListResDevicesofUserStatus;
import com.tmsm.api.model.web.ResDeviceInGroup;
import com.tmsm.api.model.web.ResDevicesofUserStatus;
import com.tmsm.api.model.web.ResDoorGroup;
import com.tmsm.api.utility.Database;

@CrossOrigin
@RestController
@RequestMapping("/getgroup")
public class WebGroup {
	

	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/things/status/{user_id}/{group_id}", method = RequestMethod.GET)
	public ResponseEntity<ListResDevicesofUserStatus> ResponseLastStatusandThingsData(
			@PathVariable("user_id") int user_id, @PathVariable("group_id") int group_id) {
		ResultSet set = null;
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ListResDevicesofUserStatus userStatus = new ListResDevicesofUserStatus();

		try {
			String sql = "select things_id, things_type, things_offsetx, things_offsety,things_area,device_id, device_ch, device_priority ,\r\n"
					+ "            device_visibility,detail_privilege,permit_id,permit_privilage,permit_name, type_name, favorite_id, favorite_status, gd_id , gd_group\r\n"
					+ "                      from things inner join devices on device_esp = things_id\r\n"
					+ "					     inner join devices_detail on detail_device = device_id\r\n"
					+ "                      inner join permit_user on permit_device = device_id\r\n"
					+ "                      inner join things_type on things_type = type_id\r\n"
					+ "                      inner join group_device on permit_device = gd_device\r\n"
					+ "                      left join things_favorite on permit_user.permit_device = things_favorite.favorite_devices\r\n"
					+ "                      where permit_user = ? and things_type = 2 and things_status = 1 \r\n"
					+ "                      and permit_status = 1 and enabled = 1 and gd_group = ?";

			statement = con.prepareStatement(sql);
			statement.setInt(1, user_id);
			statement.setInt(2, group_id);
			set = statement.executeQuery();

			List<ResDevicesofUserStatus> list = new ArrayList<ResDevicesofUserStatus>();
			while (set.next()) {
				if (set.getInt("device_visibility") == 1) {

					String[] devices = set.getString("detail_privilege").split("");

					if (devices[0].equals("1")) {
						String[] permition = set.getString("permit_privilage").split("");

						if (permition[0].equals("1")) {

							String sql2 = "select * from device_log_io left join user_detail on user_detail.user_detail_id = device_log_io.log_user \r\n"
									+ "                            where log_device = ? order by log_id desc limit 1";
							statement = con.prepareStatement(sql2);
							statement.setInt(1, set.getInt("device_id"));
							ResultSet set2 = statement.executeQuery();

							int calculate = Database.countResultSet(set2);
							set2.beforeFirst();

							if (calculate > 0) {
								while (set2.next()) {
									boolean statuslog = true;

									if (set2.getInt(2) == 1 || set2.getInt(2) == 3) {
										statuslog = true;
									} else {
										statuslog = false;
									}

									ResDevicesofUserStatus status = new ResDevicesofUserStatus();
									status.setThings_id(set.getInt(1)); // 1
									status.setThings_type(set.getInt(2)); // 2
									status.setThings_offsetx(set.getDouble(3));// 3
									status.setThings_offsety(set.getDouble(4)); // 4
									status.setThings_area(set.getInt(5)); // 5
									status.setDevices_id(set.getInt(6)); // 6
									status.setDevices_chanel(set.getInt(7)); // 7
									status.setDevices_priority(set.getInt(8)); // 8
									status.setDevices_visibility(set.getInt(9)); // 9
									status.setDevices_privilege(set.getString(10)); // 10
									status.setPermit_id(set.getInt(11)); // 11
									status.setPermit_privilege(set.getString(12)); // 12
									status.setDevice_name(set.getString(13)); // 13
									status.setType_name(set.getString(14)); // 14
									status.setLog_status(set2.getInt(2)); // 15
									status.setLog_boolean(statuslog); // 16
									status.setLog_user(set2.getInt(4)); // 17
									status.setLog_timer(set2.getString(5)); // 18
									status.setFull_name(set2.getString(7) + " " + set2.getString(8)); // 19
									status.setFeverite_id(set.getInt("favorite_id"));
									status.setIs_faverite(set.getBoolean("favorite_status"));
									status.setG_id(set.getInt("gd_id"));
									
									list.add(status);
								}
							} else {
								ResDevicesofUserStatus status = new ResDevicesofUserStatus();

								status.setThings_id(set.getInt(1)); // 1
								status.setThings_type(set.getInt(2)); // 2
								status.setThings_offsetx(set.getDouble(3));// 3
								status.setThings_offsety(set.getDouble(4)); // 4
								status.setThings_area(set.getInt(5)); // 5
								status.setDevices_id(set.getInt(6)); // 6
								status.setDevices_chanel(set.getInt(7)); // 7
								status.setDevices_priority(set.getInt(8)); // 8
								status.setDevices_visibility(set.getInt(9)); // 9
								status.setDevices_privilege(set.getString(10)); // 10
								status.setPermit_id(set.getInt(11)); // 11
								status.setPermit_privilege(set.getString(12)); // 12
								status.setDevice_name(set.getString(13)); // 13
								status.setType_name(set.getString(14)); // 14
								status.setFeverite_id(set.getInt("favorite_id"));
								status.setIs_faverite(set.getBoolean("favorite_status"));
								status.setG_id(set.getInt("gd_id"));
								
								list.add(status);
							}
						}
					}
				}

			}

			userStatus.setCodeStatus(200);
			userStatus.setNameStatus("Success");
			userStatus.setDatas(list);

		} catch (SQLException e) {
			userStatus.setCodeStatus(500);
			userStatus.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(userStatus);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/device/{idUser}/{idGroup}" , method = RequestMethod.GET)
	@CrossOrigin
	public ResponseEntity<ResDeviceInGroup> getDeviceOfGroupUser(@PathVariable("idUser") int idUser , @PathVariable("idGroup") int idGroup ){
		    ResDeviceInGroup group = new ResDeviceInGroup();
		    PreparedStatement statement = null;
		    Connection con = Database.connectDatabase();
		    ResultSet set = null;
		    
		    try {
				String sql = "select gd_id , gd_group , permit_name, gd_device from group_device \r\n" + 
						"	inner join devices_detail on detail_device = gd_device\r\n" + 
						"	inner join permit_user on permit_device = gd_device\r\n" + 
						"	where permit_user = ? and permit_status = true and enabled = true and gd_group = ?";
				
				statement = con.prepareStatement(sql);
				statement.setInt(1, idUser);
				statement.setInt(2, idGroup);
				set = statement.executeQuery();
				
				DevicesGroup group2 = new DevicesGroup();
				List<LastStatusDevices> list = new ArrayList<LastStatusDevices>();
				while(set.next()) {
					
					String sql2 = "select * from device_log_io inner join user_detail on user_detail.user_detail_id = device_log_io.log_user \r\n" + 
							"					 left join things_favorite on things_favorite.favorite_devices = device_log_io.log_device  \r\n" + 
							"                    where log_device = ? order by log_id desc limit 1";
					
					statement = con.prepareStatement(sql2);
					statement.setInt(1, set.getInt(4));
					ResultSet set2 = statement.executeQuery();
					
					
					while(set2.next()) {
						
						boolean status = true;
						
						if(set2.getInt(2) == 1 || set2.getInt(2) == 3) {
							status = true;
						}else {
							status = false;
						}
						
						LastStatusDevices lastStatusDevices = new LastStatusDevices();
						lastStatusDevices.setGd_id(set.getInt(1));
						lastStatusDevices.setDevice_name(set.getString(3));
						lastStatusDevices.setLog_status(set2.getInt(2));
						lastStatusDevices.setLog_boolean(status);
						lastStatusDevices.setDevices_id(set2.getInt(3));
						lastStatusDevices.setLog_user(set2.getInt(4));
						lastStatusDevices.setLog_timer(set2.getString(5));
						lastStatusDevices.setFirst_name(set2.getString(7));
						lastStatusDevices.setLast_name(set2.getString(8));
						lastStatusDevices.setFeverite_id(set2.getInt(12));
						lastStatusDevices.setIs_faverite(set2.getBoolean(17));
						
						list.add(lastStatusDevices);
					}
					group2.setGroup_id(set.getInt(2));
					group2.setDevices(list);
				}
				
				group.setCodeStatus(200);
				group.setNameStatus("Success");
				group.setGroup(group2);
				
			} catch (SQLException e) {
				group.setCodeStatus(500);
				group.setNameStatus(e.getMessage());
			}finally {
				Database.closeConnection(con);
				return ResponseEntity.ok().body(group);
			}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/door/status/{user_id}/{group_id}", method = RequestMethod.GET)
	public ResponseEntity<ListResDevicesofUserStatus> ResponseLastStatusandDoorData(
			@PathVariable("user_id") int user_id, @PathVariable("group_id") int group_id) {
		ResultSet set = null;
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ListResDevicesofUserStatus userStatus = new ListResDevicesofUserStatus();

		try {
			String sql = "select things_id,things_type, things_offsetx, things_offsety,things_area,device_id, device_ch, device_priority ,\r\n"
					+ "            device_visibility,detail_privilege, id, user_name, type_name, favorite_door_id, favorite_door_status, gd_id\r\n"
					+ "                      from things inner join devices on device_esp = things_id\r\n"
					+ "					     inner join devices_detail on detail_device = device_id\r\n"
					+ "                      inner join door_user on thing = things_id\r\n"
					+ "                      inner join things_type on things_type = type_id\r\n"
					+ "                      inner join group_device on thing = gd_things\r\n"
					+ "                      left join door_favorite on thing = favorite_door_things\r\n"
					+ "                      where user = ? and things_type = 5 and things_status = 1 \r\n"
					+ "                      and status = 1 and enable = 1 and gd_group = ?";

			statement = con.prepareStatement(sql);
			statement.setInt(1, user_id);
			statement.setInt(2, group_id);
			set = statement.executeQuery();

			List<ResDevicesofUserStatus> list = new ArrayList<ResDevicesofUserStatus>();
			while (set.next()) {
				if (set.getInt("device_visibility") == 1) {

					String[] devices = set.getString("detail_privilege").split("");

					if (devices[0].equals("1")) {

						String sql2 = "select * from door_log inner join user_detail on user_detail.user_detail_id = door_log.door_log_user\r\n" + 
								"		inner join door_status_type on door_status_type.door_st = door_log.door_log_status\r\n" + 
								"		where door_log_things = ? order by door_log_id DESC LIMIT  1";
						
						statement = con.prepareStatement(sql2);
						statement.setInt(1, set.getInt("things_id"));
						ResultSet set2 = statement.executeQuery();

						int calculate = Database.countResultSet(set2);
						set2.beforeFirst();

						if (calculate > 0) {
							while (set2.next()) {
								boolean statuslog = true;

								if (set2.getInt(2) == 1 || set2.getInt(2) == 3) {
									statuslog = true;
								} else {
									statuslog = false;
								}

								ResDevicesofUserStatus status = new ResDevicesofUserStatus();
								status.setThings_id(set.getInt(1)); // 1
								status.setThings_type(set.getInt(2)); // 2
								status.setThings_offsetx(set.getDouble(3));// 3
								status.setThings_offsety(set.getDouble(4)); // 4
								status.setThings_area(set.getInt(5)); // 5
								status.setDevices_id(set.getInt(6)); // 6
								status.setDevices_chanel(set.getInt(7)); // 7
								status.setDevices_priority(set.getInt(8)); // 8
								status.setDevices_visibility(set.getInt(9)); // 9
								status.setDevices_privilege(set.getString(10)); // 10
								status.setPermit_id(set.getInt(11)); // 11
//								status.setPermit_privilege(set.getString(12)); 
								status.setDevice_name(set.getString(12)); // 13
								status.setType_name(set.getString(13)); // 14
								status.setLog_status(set2.getInt(2)); // 15
								status.setLog_boolean(statuslog); // 16
								status.setLog_user(set2.getInt(4)); // 17
								status.setLog_timer(set2.getString(5)); // 18
								status.setFull_name(set2.getString(7) + " " + set2.getString(8)); // 19
								status.setFeverite_id(set.getInt("favorite_door_id"));
								status.setIs_faverite(set.getBoolean("favorite_door_status"));
								status.setG_id(set.getInt("gd_id"));
								
								list.add(status);
							}
						} else {
							ResDevicesofUserStatus status = new ResDevicesofUserStatus();

							status.setThings_id(set.getInt(1)); // 1
							status.setThings_type(set.getInt(2)); // 2
							status.setThings_offsetx(set.getDouble(3));// 3
							status.setThings_offsety(set.getDouble(4)); // 4
							status.setThings_area(set.getInt(5)); // 5
							status.setDevices_id(set.getInt(6)); // 6
							status.setDevices_chanel(set.getInt(7)); // 7
							status.setDevices_priority(set.getInt(8)); // 8
							status.setDevices_visibility(set.getInt(9)); // 9
							status.setDevices_privilege(set.getString(10)); // 10
							status.setPermit_id(set.getInt(11)); // 11
//							status.setPermit_privilege(set.getString(12)); // 12
							status.setDevice_name(set.getString(12)); // 13
							status.setType_name(set.getString(13)); // 14
							status.setFeverite_id(set.getInt("favorite_door_id"));
							status.setIs_faverite(set.getBoolean("favorite_door_status"));
							status.setG_id(set.getInt("gd_id"));
							
							list.add(status);
						}
					}
				}

			}

			userStatus.setCodeStatus(200);
			userStatus.setNameStatus("Success");
			userStatus.setDatas(list);

		} catch (SQLException e) {
			userStatus.setCodeStatus(500);
			userStatus.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(userStatus);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/door/{idUser}/{idGroup}" , method = RequestMethod.GET)
	public ResponseEntity<?> getDoorIngroup(@PathVariable("idUser") int idUser , @PathVariable("idGroup") int idGroup){
		 	ResDoorGroup group = new ResDoorGroup();
		    PreparedStatement statement = null;
		    Connection con = Database.connectDatabase();
		    ResultSet set = null;
		    
		    try {
				String sql = "select gd_id, gd_group, gd_things, user_name, favorite_door_id, favorite_door_status from group_device 	\r\n" + 
						"	inner join door_user on door_user.thing = group_device.gd_things\r\n" + 
						"	left join door_favorite on door_favorite.favorite_door_things = door_user.thing\r\n" + 
						"	where user = ? and status = true and enable = true and gd_group = ?";
				
				statement = con.prepareStatement(sql);
				statement.setInt(1, idUser);
				statement.setInt(2, idGroup);
				set = statement.executeQuery();
				
				DoorGroup group2 = new DoorGroup();
				List<DoorGroupDetail> list = new ArrayList<DoorGroupDetail>();
				while(set.next()) {				
					
						String sql2 = "select * from door_log inner join user_detail on user_detail.user_detail_id = door_log.door_log_user\r\n" + 
								"					          inner join door_status_type on door_status_type.door_st = door_log.door_log_status\r\n" + 
								"                             where door_log_things = ? order by door_log_id DESC LIMIT  1";
						
						statement = con.prepareStatement(sql2);
						statement.setInt(1, set.getInt(3));
						ResultSet set2 = statement.executeQuery();
							set2.next();				
							DoorGroupDetail detail = new DoorGroupDetail();
							
							detail.setGd_id(set.getInt(1));
							detail.setThingID(set.getInt(3));
							detail.setThingsName(set.getString(4));
							detail.setLastStatus(set2.getInt(2));
							detail.setStatusType(set2.getString(13));
							detail.setfName(set2.getString(7));
							detail.setlName(set2.getString(8));		
							detail.setFeverite_id(set.getInt(5));
							detail.setIs_feverite(set.getBoolean(6));
							
							list.add(detail);
						
									
					
					group2.setGroup_id(idGroup);
					group2.setDetails(list);
				}
				
				group.setCodeStatus(200);
				group.setNameStatus("Success");
				group.setGroup(group2);
				
			} catch (SQLException e) {
				group.setCodeStatus(500);
				group.setNameStatus(e.getMessage());
			}finally {
				Database.closeConnection(con);
				return ResponseEntity.ok().body(group);
			}
	}
	

	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/sensor/status/{user_id}/{group_id}", method = RequestMethod.GET)
	public ResponseEntity<ListResDevicesofUserStatus> ResponseLastStatusandSensorData(
			@PathVariable("user_id") int user_id, @PathVariable("group_id") int group_id) {
		ResultSet set = null;
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ListResDevicesofUserStatus userStatus = new ListResDevicesofUserStatus();

		try {
			String sql = "select things_id, things_type, things_offsetx, things_offsety,things_area,device_id, device_ch, device_priority ,\r\n"
					+ "            device_visibility,detail_privilege,permit_id,permit_privilage,permit_name, type_name, favorite_id, favorite_status, gd_id , gd_group,sensor_unit,unit_name\r\n"
					+ "                      from things inner join devices on device_esp = things_id\r\n"
					+ "					     inner join devices_detail on detail_device = device_id\r\n"
					+ "                      inner join sensor_unit on sensor_unit_device = device_id\r\n" 
					+ "                      inner join unit on unit_id = sensor_unit\r\n" 
					+ "                      inner join permit_user on permit_device = device_id\r\n"
					+ "                      inner join things_type on things_type = type_id\r\n"
					+ "                      inner join group_device on permit_device = gd_device\r\n"
					+ "                      left join things_favorite on permit_user.permit_device = things_favorite.favorite_devices\r\n"
					+ "                      where permit_user = ? and things_type = 4 and things_status = 1 \r\n"
					+ "                      and permit_status = 1 and enabled = 1 and gd_group = ?";

			statement = con.prepareStatement(sql);
			statement.setInt(1, user_id);
			statement.setInt(2, group_id);
			set = statement.executeQuery();

			List<ResDevicesofUserStatus> list = new ArrayList<ResDevicesofUserStatus>();
			while (set.next()) {
				if (set.getInt("device_visibility") == 1) {

					String[] devices = set.getString("detail_privilege").split("");

					if (devices[0].equals("1")) {
						String[] permition = set.getString("permit_privilage").split("");

						if (permition[0].equals("1")) {

							String sql2 = "select * from sensor_value 	where sensor_device = ? order by sensor_id DESC LIMIT  1";
							statement = con.prepareStatement(sql2);
							statement.setInt(1, set.getInt("device_id"));
							ResultSet set2 = statement.executeQuery();

							int calculate = Database.countResultSet(set2);
							set2.beforeFirst();

							if (calculate > 0) {
								while (set2.next()) {
									
									ResDevicesofUserStatus status = new ResDevicesofUserStatus();
									status.setThings_id(set.getInt(1)); // 1
									status.setThings_type(set.getInt(2)); // 2
									status.setThings_offsetx(set.getDouble(3));// 3
									status.setThings_offsety(set.getDouble(4)); // 4
									status.setThings_area(set.getInt(5)); // 5
									status.setDevices_id(set.getInt(6)); // 6
									status.setDevices_chanel(set.getInt(7)); // 7
									status.setDevices_priority(set.getInt(8)); // 8
									status.setDevices_visibility(set.getInt(9)); // 9
									status.setDevices_privilege(set.getString(10)); // 10
									status.setPermit_id(set.getInt(11)); // 11
									status.setPermit_privilege(set.getString(12)); // 12
									status.setDevice_name(set.getString(13)); // 13
									status.setType_name(set.getString(14)); // 14
									status.setSensor_value(set2.getFloat(3));
									status.setLog_timer(set2.getString(4)); // 18
									status.setFeverite_id(set.getInt("favorite_id"));
									status.setIs_faverite(set.getBoolean("favorite_status"));
									status.setUnit_id(set.getInt("sensor_unit"));
									status.setUnit_name(set.getString("unit_name"));
									status.setG_id(set.getInt("gd_id"));
									
									list.add(status);
								}
							} else {
								ResDevicesofUserStatus status = new ResDevicesofUserStatus();

								status.setThings_id(set.getInt(1)); // 1
								status.setThings_type(set.getInt(2)); // 2
								status.setThings_offsetx(set.getDouble(3));// 3
								status.setThings_offsety(set.getDouble(4)); // 4
								status.setThings_area(set.getInt(5)); // 5
								status.setDevices_id(set.getInt(6)); // 6
								status.setDevices_chanel(set.getInt(7)); // 7
								status.setDevices_priority(set.getInt(8)); // 8
								status.setDevices_visibility(set.getInt(9)); // 9
								status.setDevices_privilege(set.getString(10)); // 10
								status.setPermit_id(set.getInt(11)); // 11
								status.setPermit_privilege(set.getString(12)); // 12
								status.setDevice_name(set.getString(13)); // 13
								status.setType_name(set.getString(14)); // 14
								status.setFeverite_id(set.getInt("favorite_id"));
								status.setIs_faverite(set.getBoolean("favorite_status"));
								status.setUnit_id(set.getInt("sensor_unit"));
								status.setUnit_name(set.getString("unit_name"));
								status.setG_id(set.getInt("gd_id"));
								
								list.add(status);
							}
						}
					}
				}

			}

			userStatus.setCodeStatus(200);
			userStatus.setNameStatus("Success");
			userStatus.setDatas(list);

		} catch (SQLException e) {
			userStatus.setCodeStatus(500);
			userStatus.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(userStatus);
		}
	}
	
}
