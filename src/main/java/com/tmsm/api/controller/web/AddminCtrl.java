package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.tmsm.api.model.web.DeviceStatusControll;
import com.tmsm.api.model.web.ResAllDeviceCtrl;
import com.tmsm.api.model.web.ResDeviceCtrlStatus;
import com.tmsm.api.utility.Database;

@RestController
@RequestMapping("/webctrl")
public class AddminCtrl {

	@CrossOrigin
	@RequestMapping(value = "/{id}",method = RequestMethod.GET)
	public ResponseEntity<ResAllDeviceCtrl> getAllDevices(@PathVariable("id") int area){
		
		ResultSet set = null;
		PreparedStatement statement = null;
		Connection con = null;
		
		ResAllDeviceCtrl allDevices = new ResAllDeviceCtrl();
		
		try {
//			String sql = "select * from devices inner join devices_detail on detail_device = device_id order by device_id";
			String sql = "select device_id, device_esp, device_ch, device_priority, device_visibility, \r\n" + 
					"       detail_id, detail_name, detail_device, detail_privilege, detail_area \r\n" + 
					"       from things inner join devices on device_esp = things_id\r\n" + 
					"       inner join devices_detail on detail_device = device_id where things_area = ?";
			con = Database.connectDatabase();
			statement = con.prepareStatement(sql);
			statement.setInt(1, area);
			set = statement.executeQuery();
			
			List<ResDeviceCtrlStatus> devices = new ArrayList<ResDeviceCtrlStatus>();
			
			while(set.next()) {
				String sql2 = "select * from device_log_io  where log_device = ? order by log_id DESC limit 1";
				
				statement = con.prepareStatement(sql2);
				statement.setInt(1, set.getInt(1));
				ResultSet res = statement.executeQuery();
				
				List<DeviceStatusControll> CtrlList = new ArrayList<DeviceStatusControll>();
				while(res.next()) {
					DeviceStatusControll controll = new DeviceStatusControll();
					controll.setStatus(res.getInt(2));					
					CtrlList.add(controll);
				}
				
				ResDeviceCtrlStatus ctrl = new ResDeviceCtrlStatus();
				ctrl.setDeviceID(set.getInt(1));
				ctrl.setDeviceThing(set.getInt(2));
				ctrl.setDeviceCH(set.getInt(3));
				ctrl.setDevicePriority(set.getInt(4));
				ctrl.setDeviceVisibility(set.getInt(5));
				ctrl.setDeviceName(set.getString(7));
				ctrl.setDevicePrivilege(set.getString(9));
				ctrl.setStatus(CtrlList);
				
				devices.add(ctrl);
				
			}
			
			allDevices.setCodeStatus(200);
			allDevices.setNameStatus("Success");
			allDevices.setDevices(devices);
			
		} catch (SQLException e) {
			allDevices.setCodeStatus(500);
			allDevices.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(allDevices);
		}
	}
	
	
	
}
