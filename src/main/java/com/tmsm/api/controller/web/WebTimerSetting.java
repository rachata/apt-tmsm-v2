package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.web.ListTimerSettingData;
import com.tmsm.api.model.web.TimerSettingData;
import com.tmsm.api.utility.Database;

@CrossOrigin
@RestController
@RequestMapping("web/timer")
public class WebTimerSetting {
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/{device_id}", method = RequestMethod.GET)
	public ResponseEntity<ListTimerSettingData> getTimerSetting(@PathVariable("device_id") int device_id){
		ListTimerSettingData data = new ListTimerSettingData();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ResultSet set = null;
		
		try {
			String sql = "select * from timer_setting inner join user_detail on user_detail_id = t_user where t_device = ? and t_enable = 1";
			statement = con.prepareStatement(sql);
			statement.setInt(1, device_id);
			set = statement.executeQuery();
			
			List<TimerSettingData> list = new ArrayList<TimerSettingData>();
			while(set.next()) {
				TimerSettingData settingData = new TimerSettingData();
				settingData.setTime_id(set.getInt("tid"));
				settingData.setUser_id(set.getInt("user_detail_id"));
				settingData.setFull_name(set.getString("user_detail_fname") + " " + set.getString("user_detail_lname"));
				settingData.setDatetime_max(set.getString("t_datetime_max"));
				settingData.setDatetime_min(set.getString("t_datetime_min"));
				settingData.setTime_min(set.getString("t_time_min"));
				settingData.setTime_max(set.getString("t_time_max"));
				settingData.setSelect_state(set.getBoolean("t_select_st"));
				settingData.setAccess_status(set.getBoolean("t_status"));
				settingData.setCreate_on(set.getString("t_set_datetime"));
				settingData.setSetting_status(set.getBoolean("status_device"));
				
				list.add(settingData);
			}
			
			data.setCodeStatus(200);
			data.setNameStatus("Success");
			data.setDatas(list);
		} catch (SQLException e) {
			data.setCodeStatus(500);
			data.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(data);
		}
	}
}
