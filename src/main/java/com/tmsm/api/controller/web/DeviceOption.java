package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.Status;
import com.tmsm.api.model.web.ResUpdateDevice;
import com.tmsm.api.utility.Database;
import com.tmsm.model.strong.DevicesPriority;
import com.tmsm.model.strong.DevicesVisibility;
import com.tmsm.model.strong.ResDevicesOption;

@RestController
@RequestMapping("optdevice")
public class DeviceOption {
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResDevicesOption> getOption() {
		ResDevicesOption option = new ResDevicesOption();
		
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ResultSet result = null;
		
		try {
			String sql = "select*from devices_priority";
			String sql2 = "select * from devices_visibility";
			statement = con.prepareStatement(sql);
			result = statement.executeQuery();
			
			List<DevicesPriority> priority = new ArrayList<DevicesPriority>();
			while(result.next()) {
				DevicesPriority devicesPriority = new DevicesPriority();
				devicesPriority.setPriorityID(result.getInt(1));
				devicesPriority.setPriorityName(result.getString(2));
				
				priority.add(devicesPriority);
			}	
			
			PreparedStatement statement2 = con.prepareStatement(sql2);
			ResultSet resultSet = statement2.executeQuery();
			
			List<DevicesVisibility> visibilities = new ArrayList<DevicesVisibility>();
			
			while (resultSet.next()) {
				DevicesVisibility visibility = new DevicesVisibility();
				visibility.setVisibilityID(resultSet.getInt(1));
				visibility.setVisibilityName(resultSet.getString(2));
				
				visibilities.add(visibility);
			}
			
			option.setCodeStatus(200);
			option.setNameStatus("Success");
			option.setPriorities(priority);
			option.setVisibilities(visibilities);
			
		} catch (SQLException e) {
			option.setCodeStatus(500);
			option.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(option);
		}		
	}
	
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Status> updateSubDevice(@RequestBody ResUpdateDevice device){
		Status status = new Status();
		
		ResultSet result = null;
		PreparedStatement preparedStmt = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "update devices set device_priority = ?, device_visibility = ? where device_esp = ? and device_ch = ?";
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setInt(1, device.getPriority());
			preparedStmt.setInt(2, device.getVisibility());
			preparedStmt.setInt(3, device.getThing());
			preparedStmt.setInt(4, device.getCh());
			
			int st = preparedStmt.executeUpdate();
			
			if(st == 1) {
				String sqlId  = "select device_id from devices where device_esp = ? and device_ch = ?";
				
				preparedStmt = con.prepareStatement(sqlId);
				
				preparedStmt.setInt(1, device.getThing());
				preparedStmt.setInt(2, device.getCh());
				
				result = preparedStmt.executeQuery();
				result.next();
				
						
				String sqlUpdateDeviceDetail = "update devices_detail set detail_name = ? , detail_privilege = ?   where detail_device = ?";
				
				preparedStmt = con.prepareStatement(sqlUpdateDeviceDetail);
				
				preparedStmt.setString(1, device.getNameDevice());
				preparedStmt.setString(2, device.getPrivilege());				
				preparedStmt.setInt(3, result.getInt(1));
				
				int statusUpdateDeviceDetail  = preparedStmt.executeUpdate();
				
				if(statusUpdateDeviceDetail == 1) {
					status.setCodeStatus(200);
					status.setNameStatus("Success");
				}else {
					status.setCodeStatus(204);
					status.setNameStatus("Unsuccess");
				}
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");
			}
			
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
}
