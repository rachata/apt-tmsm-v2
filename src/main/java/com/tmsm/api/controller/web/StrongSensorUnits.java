package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.Status;
import com.tmsm.api.model.web.ListSensorUnits;
import com.tmsm.api.model.web.SensorUnit;
import com.tmsm.api.utility.Database;

@CrossOrigin
@RestController
@RequestMapping("strong/unit")
public class StrongSensorUnits {
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<ListSensorUnits> resAllSensorUnits(){
		ListSensorUnits units = new ListSensorUnits();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ResultSet set = null;
		
		try {
			String sql = "select * from unit";
			statement = con.prepareStatement(sql);
			set = statement.executeQuery();
			
			List<SensorUnit> list = new ArrayList<SensorUnit>();
			while(set.next()) {
				SensorUnit unit = new SensorUnit();
				unit.setUnit_id(set.getInt(1));
				unit.setUnit_name(set.getString(2));
				
				list.add(unit);
			}
			
			units.setCodeStatus(200);
			units.setNameStatus("Success");
			units.setUnits(list);
		} catch (SQLException e) {
			units.setCodeStatus(500);
			units.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(units);
		}
	}
	
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Status> createUnits(@RequestBody SensorUnit unit){
		Status status = new Status();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "insert into unit (unit_name) values (?)";
			statement = con.prepareStatement(sql);
			statement.setString(1, unit.getUnit_name());
			boolean st = statement.execute();
			
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");
			}
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Status> updateUnits(@RequestBody SensorUnit unit){
		Status status = new Status();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "update unit set unit_name = ? where unit_id = ?";
			statement = con.prepareStatement(sql);
			statement.setInt(1, unit.getUnit_id());
			statement.setString(2, unit.getUnit_name());
			boolean st = statement.execute();
			
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");
			}
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/{unit_id}",method = RequestMethod.DELETE)
	public ResponseEntity<Status> updateUnits(@PathVariable("unit_id") int unit_id){
		Status status = new Status();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "delete from unit where unit_id = ?";
			statement = con.prepareStatement(sql);
			statement.setInt(1, unit_id);
			boolean st = statement.execute();
			
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");
			}
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
}
