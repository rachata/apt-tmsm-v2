package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.Status;
import com.tmsm.api.model.web.AdminArea;
import com.tmsm.api.model.web.CreateAdminArea;
import com.tmsm.api.model.web.DetailArea;
import com.tmsm.api.model.web.ListAdminArea;
import com.tmsm.api.model.web.ListAdminRootArea;
import com.tmsm.api.model.web.ListAdminSubArea;
import com.tmsm.api.model.web.ListResDevicesofUserStatus;
import com.tmsm.api.model.web.ResDevicesofUserStatus;
import com.tmsm.api.model.web.RootArea;
import com.tmsm.api.utility.Database;

@CrossOrigin
@RestController
@RequestMapping("adminarea")
public class WebAdminArea {
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/area/root/{user_id}")
	public ResponseEntity<?> responseAreaofAdmin(@PathVariable("user_id") int user_id){
		ResultSet set = null;
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ListAdminRootArea rootArea = new ListAdminRootArea();
		
		try {
			String sql = "select area,area_of_device_name, area_latitude, area_longitudes from area_administrator inner join area_of_devices on area_of_devices.area_of_device_id = area_administrator.area \r\n" + 
					"			  where area_admin_user = ? and area_enable = true";
			statement = con.prepareStatement(sql);
			statement.setInt(1, user_id);
			set = statement.executeQuery();
			
			List<RootArea> areas = new ArrayList<RootArea>();
			while(set.next()) {
				RootArea area = new RootArea();
				area.setArea_id(set.getInt(1));
				area.setArea_name(set.getString(2));
				area.setLatitude(set.getDouble(3));
				area.setLongtitude(set.getDouble(4));
				
				areas.add(area);
			}
			
			rootArea.setAreas(areas);
			rootArea.setCodeStatus(200);
			rootArea.setNameStatus("Success");
		}catch (SQLException e) {
			rootArea.setCodeStatus(500);
			rootArea.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(rootArea);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/area/sub/{area_id}")
	public ResponseEntity<?> responseSubAreaofAdmin(@PathVariable("area_id") int area_id){
		ResultSet set = null;
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ListAdminSubArea rootArea = new ListAdminSubArea();
		
		try {
			String sql = "select * from detail_area where dear_area = ?";
			statement = con.prepareStatement(sql);
			statement.setInt(1, area_id);
			set = statement.executeQuery();
			
			List<DetailArea> areas = new ArrayList<DetailArea>();
			while(set.next()) {
				DetailArea area = new DetailArea();
				area.setDetailID(set.getInt(1));
				area.setDetailName(set.getString(2));
				area.setDetailImage(set.getString(3));
				area.setDetailArea(set.getInt(4));
				
				areas.add(area);
			}
			
			rootArea.setSubarea(areas);
			rootArea.setCodeStatus(200);
			rootArea.setNameStatus("Success");
		}catch (SQLException e) {
			rootArea.setCodeStatus(500);
			rootArea.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(rootArea);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/edit", method = RequestMethod.PUT)
	public ResponseEntity<Status> editNameArea(@RequestBody RootArea area){
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		Status status = new Status();
		
		try {
			String sql = "update area_of_devices set area_of_device_name = ? where area_of_device_id = ?";
			statement = con.prepareStatement(sql);
			statement.setString(1, area.getArea_name());
			statement.setInt(2, area.getArea_id());
			
			boolean st = statement.execute();
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Delete Unsuccess");
			}
		}catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/edit/location", method = RequestMethod.PUT)
	public ResponseEntity<Status> editLocationArea(@RequestBody RootArea area){
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		Status status = new Status();
		
		try {
			String sql = "update area_of_devices set area_latitude = ?, area_longitudes = ? where area_of_device_id = ?";
			statement = con.prepareStatement(sql);
			statement.setDouble(1, area.getLatitude());
			statement.setDouble(2, area.getLongtitude());
			statement.setInt(3, area.getArea_id());
			
			boolean st = statement.execute();
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Delete Unsuccess");
			}
		}catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/delete/{area_id}", method = RequestMethod.DELETE)
	public ResponseEntity<Status> editDeleteArea(@PathVariable("area_id") int area_id){
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		Status status = new Status();
		
		try {
			String sql = "update area_of_devices set area_enable = 0 where area_of_device_id = ?";
			statement = con.prepareStatement(sql);
			statement.setDouble(1, area_id);
			
			
			boolean st = statement.execute();
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Delete Unsuccess");
			}
		}catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<Status> createAdminArea(@RequestBody CreateAdminArea create){
		Status status = new Status();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "insert into area_administrator(area_admin_user, area) values (?, ?)";
			statement = con.prepareStatement(sql);
			statement.setInt(1, create.getUser_id());
			statement.setInt(2, create.getArea_id());
			
			boolean st = statement.execute();
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");
			}
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/delete/adminarea/{admin_id}", method = RequestMethod.DELETE)
	public ResponseEntity<Status> deleteAdminArea(@PathVariable("admin_id") int admin_id){
		Status status = new Status();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "update area_administrator set admin_enable = 0 where area_admin_id = ?";
			statement = con.prepareStatement(sql);
			statement.setInt(1, admin_id);
			
			boolean st = statement.execute();
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");
			}
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/admins/{user_id}", method = RequestMethod.GET)
	public ResponseEntity<ListAdminArea> getAdminArea(@PathVariable("user_id") int user_id){
		ResultSet set = null;
		ListAdminArea  area = new ListAdminArea();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "select * from area_administrator inner join area_of_devices on area_of_device_id = area where area_admin_user = ?";
			statement = con.prepareStatement(sql);
			statement.setInt(1, user_id);
			set = statement.executeQuery();
			
			List<AdminArea> areas = new ArrayList<AdminArea>();
			while(set.next()) {
				AdminArea area2 = new AdminArea();
				area2.setArea_admin_id(set.getInt(1));
				area2.setArea_id(set.getInt(3));
				area2.setAdmin_enable(set.getInt(4));
				area2.setArea_name(set.getString(6));
				area2.setArea_enable(set.getBoolean(9));
				
				areas.add(area2);
			}
			
			area.setAdmins(areas);
			area.setCodeStatus(200);
			area.setNameStatus("Success");
		} catch (SQLException e) {
			area.setCodeStatus(500);
			area.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(area);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/things/status/{area_id}", method = RequestMethod.GET)
	public ResponseEntity<ListResDevicesofUserStatus> ResponseLastStatusandThingsData(
			@PathVariable("area_id") int area_id) {
		ResultSet set = null;
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ListResDevicesofUserStatus userStatus = new ListResDevicesofUserStatus();

		try {
			String sql = "select things_id, things_type, things_offsetx, things_offsety,things_area,device_id, device_ch, device_priority ,\r\n"
					+ "            device_visibility,detail_privilege, type_name , things_nickname\r\n"
					+ "                      from things inner join devices on device_esp = things_id\r\n"
					+ "					     inner join devices_detail on detail_device = device_id\r\n"
					+ "                      inner join things_type on things_type = type_id\r\n"
					+ "                      left join things_favorite on device_id = things_favorite.favorite_devices\r\n"
					+ "                      where things_area = ? and things_type = 2 and things_status = 1 ";
					                      

			statement = con.prepareStatement(sql);
			statement.setInt(1, area_id);
			set = statement.executeQuery();

			List<ResDevicesofUserStatus> list = new ArrayList<ResDevicesofUserStatus>();
			while (set.next()) {
				
							String sql2 = "select * from device_log_io left join user_detail on user_detail.user_detail_id = device_log_io.log_user \r\n"
									+ "                            where log_device = ? order by log_id desc limit 1";
							statement = con.prepareStatement(sql2);
							statement.setInt(1, set.getInt("device_id"));
							ResultSet set2 = statement.executeQuery();

							int calculate = Database.countResultSet(set2);
							set2.beforeFirst();

							if (calculate > 0) {
								while (set2.next()) {
									boolean statuslog = true;

									if (set2.getInt(2) == 1 || set2.getInt(2) == 3) {
										statuslog = true;
									} else {
										statuslog = false;
									}

									ResDevicesofUserStatus status = new ResDevicesofUserStatus();
									status.setThings_id(set.getInt(1)); // 1
									status.setThings_type(set.getInt(2)); // 2
									status.setThings_offsetx(set.getDouble("things_offsetx"));// 3
									status.setThings_offsety(set.getDouble("things_offsety")); // 4
									status.setThings_area(set.getInt("things_area")); // 5
									status.setDevices_id(set.getInt("device_id")); // 6
									status.setDevices_chanel(set.getInt("device_ch")); // 7
									status.setDevices_priority(set.getInt("device_priority")); // 8
									status.setDevices_visibility(set.getInt("device_visibility")); // 9
									status.setDevices_privilege(set.getString("detail_privilege")); // 10
									status.setDevice_name(set.getString("things_nickname")); // 13
									status.setType_name(set.getString("type_name")); // 14
									status.setLog_status(set2.getInt(2)); // 15
									status.setLog_boolean(statuslog); // 16
									status.setLog_user(set2.getInt(4)); // 17
									status.setLog_timer(set2.getString(5)); // 18
									status.setFull_name(set2.getString(7) + " " + set2.getString(8)); // 19
									
									list.add(status);
								}
							} else {
								ResDevicesofUserStatus status = new ResDevicesofUserStatus();

								status.setThings_id(set.getInt(1)); // 1
								status.setThings_type(set.getInt(2)); // 2
								status.setThings_offsetx(set.getDouble("things_offsetx"));// 3
								status.setThings_offsety(set.getDouble("things_offsety")); // 4
								status.setThings_area(set.getInt("things_area")); // 5
								status.setDevices_id(set.getInt("device_id")); // 6
								status.setDevices_chanel(set.getInt("device_ch")); // 7
								status.setDevices_priority(set.getInt("device_priority")); // 8
								status.setDevices_visibility(set.getInt("device_visibility")); // 9
								status.setDevices_privilege(set.getString("detail_privilege")); // 10
								status.setDevice_name(set.getString("things_nickname")); // 13
								status.setType_name(set.getString("type_name")); // 14

								list.add(status);
							}
					
				

			}

			userStatus.setCodeStatus(200);
			userStatus.setNameStatus("Success");
			userStatus.setDatas(list);

		} catch (SQLException e) {
			userStatus.setCodeStatus(500);
			userStatus.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(userStatus);
		}
	}

	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/door/status/{area_id}", method = RequestMethod.GET)
	public ResponseEntity<ListResDevicesofUserStatus> ResponseLastStatusandDoorData(
			@PathVariable("area_id") int area_id) {
		ResultSet set = null;
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ListResDevicesofUserStatus userStatus = new ListResDevicesofUserStatus();

		try {
			String sql = "select things_id,things_type, things_offsetx, things_offsety,things_area,device_id, device_ch, device_priority ,\r\n"
					+ "            device_visibility,detail_privilege,type_name, things_nickname\r\n"
					+ "                      from things inner join devices on device_esp = things_id\r\n"
					+ "					     inner join devices_detail on detail_device = device_id\r\n"
					+ "                      inner join things_type on things_type = type_id\r\n"
					+ "                      where things_area = ? and things_type = 5 and things_status = 1";

			statement = con.prepareStatement(sql);
			statement.setInt(1, area_id);
			set = statement.executeQuery();

			List<ResDevicesofUserStatus> list = new ArrayList<ResDevicesofUserStatus>();
			while (set.next()) {
				if (set.getInt("device_visibility") == 1) {

					String[] devices = set.getString("detail_privilege").split("");

					if (devices[0].equals("1")) {

						String sql2 = "select * from door_log inner join user_detail on user_detail.user_detail_id = door_log.door_log_user\r\n" + 
								"		inner join door_status_type on door_status_type.door_st = door_log.door_log_status\r\n" + 
								"		where door_log_things = ? order by door_log_id DESC LIMIT  1";
						
						statement = con.prepareStatement(sql2);
						statement.setInt(1, set.getInt("things_id"));
						ResultSet set2 = statement.executeQuery();

						int calculate = Database.countResultSet(set2);
						set2.beforeFirst();

						if (calculate > 0) {
							while (set2.next()) {
								boolean statuslog = true;

								if (set2.getInt(2) == 1 || set2.getInt(2) == 3) {
									statuslog = true;
								} else {
									statuslog = false;
								}

								ResDevicesofUserStatus status = new ResDevicesofUserStatus();
								status.setThings_id(set.getInt(1)); // 1
								status.setThings_type(set.getInt(2)); // 2
								status.setThings_offsetx(set.getDouble("things_offsetx"));// 3
								status.setThings_offsety(set.getDouble("things_offsety")); // 4
								status.setThings_area(set.getInt("things_area")); // 5
								status.setDevices_id(set.getInt("device_id")); // 6
								status.setDevices_chanel(set.getInt("device_ch")); // 7
								status.setDevices_priority(set.getInt("device_priority")); // 8
								status.setDevices_visibility(set.getInt("device_visibility")); // 9
								status.setDevices_privilege(set.getString("detail_privilege")); // 10
								status.setDevice_name(set.getString("things_nickname")); // 13
								status.setType_name(set.getString("type_name")); // 14
								status.setLog_status(set2.getInt(2)); // 15
								status.setLog_boolean(statuslog); // 16
								status.setLog_user(set2.getInt(4)); // 17
								status.setLog_timer(set2.getString(5)); // 18
								status.setFull_name(set2.getString(7) + " " + set2.getString(8)); // 19
								

								list.add(status);
							}
						} else {
							ResDevicesofUserStatus status = new ResDevicesofUserStatus();

							status.setThings_id(set.getInt(1)); // 1
							status.setThings_type(set.getInt(2)); // 2
							status.setThings_offsetx(set.getDouble("things_offsetx"));// 3
							status.setThings_offsety(set.getDouble("things_offsety")); // 4
							status.setThings_area(set.getInt("things_area")); // 5
							status.setDevices_id(set.getInt("device_id")); // 6
							status.setDevices_chanel(set.getInt("device_ch")); // 7
							status.setDevices_priority(set.getInt("device_priority")); // 8
							status.setDevices_visibility(set.getInt("device_visibility")); // 9
							status.setDevices_privilege(set.getString("detail_privilege")); // 10
							status.setDevice_name(set.getString("things_nickname")); // 13
							status.setType_name(set.getString("type_name")); // 14

							list.add(status);
						}
					}
				}

			}

			userStatus.setCodeStatus(200);
			userStatus.setNameStatus("Success");
			userStatus.setDatas(list);

		} catch (SQLException e) {
			userStatus.setCodeStatus(500);
			userStatus.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(userStatus);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/sensor/status/{area_id}", method = RequestMethod.GET)
	public ResponseEntity<ListResDevicesofUserStatus> ResponseLastStatusandSensorData(
			@PathVariable("area_id") int area_id) {
		ResultSet set = null;
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ListResDevicesofUserStatus userStatus = new ListResDevicesofUserStatus();

		try {
			String sql = "select things_id, things_type, things_offsetx, things_offsety,things_area,device_id, device_ch, device_priority ,\r\n" + 
					"       device_visibility, detail_privilege, type_name, sensor_unit, unit_name, things_nickname\r\n" + 
					"                      from things inner join devices on device_esp = things_id\r\n" + 
					"					   inner join devices_detail on detail_device = device_id\r\n" + 
					"                      inner join sensor_unit on sensor_unit_device = device_id\r\n" + 
					"                      inner join unit on unit_id = sensor_unit\r\n" + 
					"                      inner join things_type on things_type = type_id\r\n" + 
					"                      where things_area = ? and things_type = 4 and things_status = 1 ";

			statement = con.prepareStatement(sql);
			statement.setInt(1, area_id);
			set = statement.executeQuery();

			List<ResDevicesofUserStatus> list = new ArrayList<ResDevicesofUserStatus>();
			while (set.next()) {
				
							String sql2 = "select * from sensor_value 	where sensor_device = ? order by sensor_id DESC LIMIT  1";
							statement = con.prepareStatement(sql2);
							statement.setInt(1, set.getInt("device_id"));
							ResultSet set2 = statement.executeQuery();

							int calculate = Database.countResultSet(set2);
							set2.beforeFirst();

							if (calculate > 0) {
								while (set2.next()) {
//								

									ResDevicesofUserStatus status = new ResDevicesofUserStatus();
									status.setThings_id(set.getInt(1)); // 1
									status.setThings_type(set.getInt(2)); // 2
									status.setThings_offsetx(set.getDouble("things_offsetx"));// 3
									status.setThings_offsety(set.getDouble("things_offsety")); // 4
									status.setThings_area(set.getInt("things_area")); // 5
									status.setDevices_id(set.getInt("device_id")); // 6
									status.setDevices_chanel(set.getInt("device_ch")); // 7
									status.setDevices_priority(set.getInt("device_priority")); // 8
									status.setDevices_visibility(set.getInt("device_visibility")); // 9
									status.setDevices_privilege(set.getString("detail_privilege")); // 10
									status.setDevice_name(set.getString("things_nickname")); // 13
									status.setType_name(set.getString("type_name")); // 14
									status.setSensor_value(set2.getFloat(3));
									status.setLog_timer(set2.getString(4)); // 18
									status.setUnit_id(set.getInt("sensor_unit"));
									status.setUnit_name(set.getString("unit_name"));

									list.add(status);
								}
							} else {
								ResDevicesofUserStatus status = new ResDevicesofUserStatus();

								status.setThings_id(set.getInt(1)); // 1
								status.setThings_type(set.getInt(2)); // 2
								status.setThings_offsetx(set.getDouble("things_offsetx"));// 3
								status.setThings_offsety(set.getDouble("things_offsety")); // 4
								status.setThings_area(set.getInt("things_area")); // 5
								status.setDevices_id(set.getInt("device_id")); // 6
								status.setDevices_chanel(set.getInt("device_ch")); // 7
								status.setDevices_priority(set.getInt("device_priority")); // 8
								status.setDevices_visibility(set.getInt("device_visibility")); // 9
								status.setDevices_privilege(set.getString("detail_privilege")); // 10
								status.setDevice_name(set.getString("things_nickname")); // 13
								status.setType_name(set.getString("type_name")); // 14
								status.setUnit_id(set.getInt("sensor_unit"));
								status.setUnit_name(set.getString("unit_name"));

								list.add(status);
							}
						}
					

			userStatus.setCodeStatus(200);
			userStatus.setNameStatus("Success");
			userStatus.setDatas(list);

		} catch (SQLException e) {
			userStatus.setCodeStatus(500);
			userStatus.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(userStatus);
		}
	}
}
