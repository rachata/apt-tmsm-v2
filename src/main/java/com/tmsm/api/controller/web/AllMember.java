package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.web.Member;
import com.tmsm.api.model.web.ResMember;
import com.tmsm.api.utility.Database;

@RestController
@CrossOrigin
@RequestMapping("/member")
public class AllMember {
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<ResMember> getAllMember() {
		ResMember member = new ResMember();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ResultSet set = null;
		
		try {
			String sql = "select user_detail_id, user_detail_fname, user_detail_lname, user_detail_position, privilege, position_name, privilege_name  \r\n" + 
					"	   from user inner join user_detail on user_detail_id = usr_id\r\n" + 
					"       inner join privilege on privilege_id = privilege\r\n" + 
					"       inner join position on user_detail_position = position_id order by user_detail_id asc";
			
			statement = con.prepareStatement(sql);
			set = statement.executeQuery();
			List<Member> members = new ArrayList<Member>();
			
			while(set.next()) {
				Member member2 = new Member();
				member2.setUsrId(set.getInt(1));
				member2.setfName(set.getString(2));
				member2.setlName(set.getString(3));
				member2.setPosition(set.getInt(4));
				member2.setPrivilege(set.getInt(5));
				member2.setPositionName(set.getString(6));
				member2.setPrivilegeName(set.getString(7));
				
				members.add(member2);
			}
			
			member.setCodeStatus(200);
			member.setNameStatus("Success");
			member.setMembers(members);
		} catch (SQLException e) {
			member.setCodeStatus(500);
			member.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(member);
		}
	}
}
