package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.web.ListUserData;
import com.tmsm.api.model.web.ResUserProfile;
import com.tmsm.api.model.web.UserData;
import com.tmsm.api.model.web.UserProfile;
import com.tmsm.api.utility.Database;

@CrossOrigin
@RestController
@RequestMapping("/profile")
public class WebProfileUser {
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/{id}" , method = RequestMethod.GET)
	private ResponseEntity<UserProfile> getUserProfile(@PathVariable(value = "id") Integer id){
		UserProfile user = new UserProfile();
		ResultSet set = null;
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "select * from user_detail where user_detail_id = ?";
			statement = con.prepareStatement(sql);
			statement.setInt(1, id);
			set = statement.executeQuery();
			ResUserProfile profile = new ResUserProfile();
			set.next();
			
			profile.setFirstName(set.getString(2));
			profile.setLastName(set.getString(3));
			profile.setEmail(set.getString(5));
			profile.setPhoneNumber(set.getString(6));
		
				
			user.setCodeStatus(200);
			user.setNameStatus("Success");
			user.setProfile(profile);
			
		}catch (SQLException e) {
			user.setCodeStatus(400);
			user.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(user);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET)
	private ResponseEntity<ListUserData> getAllUserProfile(){
		ListUserData user = new ListUserData();
		ResultSet set = null;
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "select * from user_detail";
			statement = con.prepareStatement(sql);
			set = statement.executeQuery();
			
			List<UserData> datas = new ArrayList<UserData>();
			while(set.next()) {			
				UserData data = new UserData();
				data.setUser_id(set.getInt(1));
				data.setFirst_name(set.getString(2));
				data.setLast_name(set.getString(3));
				data.setFull_name(set.getString(2) + " " + set.getString(3));
				data.setEmail(set.getString(5));
				data.setPhone_number(set.getString(6));
				
				datas.add(data);
			}
				
			user.setCodeStatus(200);
			user.setNameStatus("Success");
			user.setDatas(datas);
			
		}catch (SQLException e) {
			user.setCodeStatus(400);
			user.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(user);
		}
	}
}
