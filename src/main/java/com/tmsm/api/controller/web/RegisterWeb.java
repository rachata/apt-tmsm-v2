package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.Register;
import com.tmsm.api.model.Status;
import com.tmsm.api.utility.Database;

@CrossOrigin
@RestController
@RequestMapping(value = "/webregis")
public class RegisterWeb {
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Status> WebRegis(@RequestBody Register register){
		Status status  = new Status();
		PreparedStatement statement = null;
		Connection connection = Database.connectDatabase();
		
		try {
			String sql = "select * from user where usr_user = ?";
			
			statement = connection.prepareStatement(sql);
			statement.setString(1, register.getUsername());
			
			ResultSet resultSet = statement.executeQuery();
			int countResultSet = Database.countResultSet(resultSet);
			if(countResultSet == 0) {
				String sqlUser = "insert into user (usr_user, usr_pass, privilege, status) values (?,?,?,?)";
				statement = connection.prepareStatement(sqlUser, PreparedStatement.RETURN_GENERATED_KEYS);
				
				statement.setString(1, register.getUsername());
				statement.setString(2, register.getPassword());
				statement.setInt(3, 5);
				statement.setInt(4, 1);
				
				boolean statusInsert = statement.execute();
				if(!statusInsert) {
					int keyUser = 0;
					ResultSet keys = statement.getGeneratedKeys();
					keys.next();
					keyUser = (keys.getInt(1));
					
					String sqlDetail = "insert into user_detail (user_detail_id, user_detail_fname, user_detail_lname, user_detail_position, user_detail_email, user_detail_phone) value (?, ?, ?, ?, ?, ?)";
					
					statement = connection.prepareStatement(sqlDetail);
					statement.setInt(1, keyUser);
					statement.setString(2, register.getfName());
					statement.setString(3, register.getlName());
					statement.setInt(4, 2);
					statement.setString(5, register.getEmail());
					statement.setString(6, register.getPhonenumber());
					
					boolean statusDetail = statement.execute();
					
					if(!statusDetail) {
						status.setCodeStatus(200);
						status.setNameStatus("Register Success");
					}else {
						status.setCodeStatus(204);
						status.setNameStatus("Register Unsuccess");
					}
				}
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Register Unsuccess");
			}
			
		}catch (SQLException e) {
			status.setCodeStatus(400);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(connection);
			return ResponseEntity.ok().body(status);
		}
	}
}
