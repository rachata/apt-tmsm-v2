package com.tmsm.api.controller.device;

import java.awt.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.Status;
import com.tmsm.api.model.device.RootTimer;
import com.tmsm.api.model.device.Timer;
import com.tmsm.api.utility.Database;

@RestController
@RequestMapping("/device")
public class Device {
	
	@RequestMapping(method = RequestMethod.POST)
	@CrossOrigin
	public ResponseEntity<Status> createDevice(@RequestBody com.tmsm.api.model.device.Device device){
		
		 
		 	ResultSet result = null;
			PreparedStatement preparedStmt = null;
			Connection con = Database.connectDatabase();
			
			Status statusHttp = new Status();

			try {
				  
				
				String sql = "select * from devices where device_esp = ? and device_ch = ?";
				
				preparedStmt = con.prepareStatement(sql);
				
				preparedStmt.setInt(1, device.getThing());
				preparedStmt.setInt(2, device.getCh());
				
				result = preparedStmt.executeQuery();
				
				int countResult = Database.countResultSet(result);
				
				if(countResult ==  0) {
					
					String sqlInsert = "insert into devices (device_esp, device_ch , device_priority , device_visibility) value(? , ? , ? , ?)";
					
					preparedStmt = con.prepareStatement(sqlInsert , PreparedStatement.RETURN_GENERATED_KEYS);
					
					preparedStmt.setInt(1, device.getThing());
					preparedStmt.setInt(2, device.getCh());
					preparedStmt.setInt(3, device.getPriority());
					preparedStmt.setInt(4, device.getVisibility());
					
					boolean status  = preparedStmt.execute();
					
					if(!status) {
						int keyDevice = 0;
						ResultSet keys = preparedStmt.getGeneratedKeys();
						 
						keys.next();
						keyDevice = (keys.getInt(1)); 
						 
						
						String sqlInsertDetail = "insert into devices_detail (detail_name, detail_device , detail_privilege , detail_area) value(? , ? , ? , ?)";
						
						preparedStmt = con.prepareStatement(sqlInsertDetail);
						
						preparedStmt.setString(1, device.getNameDevice());
						preparedStmt.setInt(2, keyDevice);
						preparedStmt.setString(3, device.getPrivilege());
						preparedStmt.setInt(4, device.getArea());
						
						boolean statusDetail  = preparedStmt.execute();
						
						if(!statusDetail) {
							statusHttp.setCodeStatus(200);
							statusHttp.setNameStatus("Success");
							
							
							
						}else {
							
							statusHttp.setCodeStatus(500);
							statusHttp.setNameStatus("ERROR INsert Device");
							
							
						}
						
						
					}
					
				}else {
					
					String sqlUpdateDevice = "update devices set device_ch = ? , device_priority = ? , device_visibility = ? where device_esp = ? and device_ch = ?";
					
					preparedStmt = con.prepareStatement(sqlUpdateDevice);
					
					preparedStmt.setInt(1, device.getCh());
					preparedStmt.setInt(2, device.getPriority());
					preparedStmt.setInt(3, device.getVisibility());
					preparedStmt.setInt(4, device.getThing());
					preparedStmt.setInt(5, device.getCh());
					
					int statusUpdateDevice  = preparedStmt.executeUpdate();
					
					if(statusUpdateDevice == 1) {
						
						
						String sqlId  = "select device_id from devices where device_esp = ? and device_ch = ?";
						
						preparedStmt = con.prepareStatement(sqlId);
						
						preparedStmt.setInt(1, device.getThing());
						preparedStmt.setInt(2, device.getCh());
						
						result = preparedStmt.executeQuery();
						result.next();
						
								
						String sqlUpdateDeviceDetail = "update devices_detail set detail_name = ? , detail_privilege = ?  , detail_area = ? where detail_device = ?";
						
						preparedStmt = con.prepareStatement(sqlUpdateDeviceDetail);
						
						preparedStmt.setString(1, device.getNameDevice());
						preparedStmt.setString(2, device.getPrivilege());
						preparedStmt.setInt(3, device.getArea());
						preparedStmt.setInt(4, result.getInt(1));
						
						int statusUpdateDeviceDetail  = preparedStmt.executeUpdate();
						
						if(statusUpdateDeviceDetail == 1) {
							System.out.println("Success Detail");
							
							statusHttp.setCodeStatus(200);
							statusHttp.setNameStatus("Success");
							
							
							
						}else {
							
							System.out.println("ERROR Update detail");
							
							statusHttp.setCodeStatus(500);
							statusHttp.setNameStatus("ERROR Update detail");
							
							
						}
						
						
					}else {
						statusHttp.setCodeStatus(500);
						statusHttp.setNameStatus("ERROR Update device");
						
						
					}
					
		
				}
			}catch (SQLException e) {
				
				
				System.out.println("ERROR SQLException");
				statusHttp.setCodeStatus(500);
				statusHttp.setNameStatus("ERROR " + e.getMessage());
		
			}finally {
				Database.closeConnection(con);
				return ResponseEntity.ok().body(statusHttp);
			}
	
	} 




	@RequestMapping(value = "/timer" , method = RequestMethod.GET , produces =MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RootTimer> getTimer(){
		
		
		
		RootTimer rootTimer = new RootTimer();
		Connection con = null;
		PreparedStatement preparedStatement = null;
		
		try {
				
			con = Database.connectDatabase();
			
			String  sql = "select * from timer_setting\n" + 
					"where t_status = true and t_enable = true ";
			
			preparedStatement = con.prepareStatement(sql);
			
			
			ResultSet res = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(res);
			System.out.println(count);
			if(count >= 1) {
				
			
				res.beforeFirst();
				
				System.out.println(res.isBeforeFirst());
				java.util.List<Timer> list = new ArrayList<Timer>();
				while(res.next()) {
					
					
					Timer timer = new Timer();
					System.out.print("ddd");
					boolean select = res.getBoolean("t_select_st");
					
					if(select) {
						int device = res.getInt("t_device");
						boolean status = res.getBoolean("status_device");
						SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
				        Date dateMin = sdf.parse(res.getString("t_time_min"));
				        Date dateMax = sdf.parse(res.getString("t_time_max"));
		
				        
				        Date check = sdf.parse(sdf.format(new Date()));
				        
				       
		
				        if (check.compareTo(dateMin) >= 0 && check.compareTo(dateMax) <= 0) {
				        	System.out.println("YES");
				        	
				        	String sqlCH  = "select device_ch , device_esp from devices\n" + 
				        			"where device_id = ?";
				        	
				        	String sqlStatus =  "SELECT * FROM device_log_io \n" + 
				        			"where log_device = ?\n" + 
				        			"ORDER BY log_id DESC LIMIT 1";
				        	
				        	
				        	preparedStatement = con.prepareStatement(sqlCH);
				        	preparedStatement.setInt(1, device);
				        	
				        	ResultSet ress  = preparedStatement.executeQuery();
				        	ress.next();
				        	
				        	int ch = ress.getInt("device_ch");
				        	int thing = ress.getInt("device_esp");
				        	
				        	
				        	preparedStatement = con.prepareStatement(sqlStatus);
				        	preparedStatement.setInt(1, device);
				        	
				        	ress = preparedStatement.executeQuery();
				        	ress.next();
				        	
				        	int statusDevice = ress.getInt("log_status");
				        	
				        	if(statusDevice == 1 || statusDevice == 3) {
				        		
				        		System.out.println("last on");
				        		if(status  == false) {
				        			
				        			
				        			timer.setCh(ch);
				        			timer.setStatus(status);
				        			timer.setThing(thing);
				        			timer.setDevice(device);
				        			System.out.println("thing " + thing + " device " + device + " ch " + ch + " status " + status) ;
				        			list.add(timer);
				        		}
				        		
				        		
				        	}else if(statusDevice == 2 || statusDevice == 4) {
				        		System.out.println("last off");
				        		if(status == true) {
				        			timer.setCh(ch);
				        			timer.setStatus(status);
				        			timer.setThing(thing);
				        			timer.setDevice(device);
				        			System.out.println("thing " + thing + " device " + device + " ch " + ch + " status " + status) ;
				        			list.add(timer);
				        		}
				        	}
				        	
				        	
				        }else {
				        	System.out.println("NO");
				        }
						
				       
					}else {
						
						int device = res.getInt("t_device");
						boolean status = res.getBoolean("status_device");
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");
				        Date dateMin = sdf.parse(res.getString("t_datetime_min"));
				        Date dateMax = sdf.parse(res.getString("t_datetime_max"));
		
				        
				        Date check = sdf.parse(sdf.format(new Date()));
				        
				        
		
				        if (check.compareTo(dateMin) >= 0 && check.compareTo(dateMax) <= 0) {
				        	System.out.println("YES");
				        	
				        	String sqlCH  = "select device_ch , device_esp from devices\n" + 
				        			"where device_id = ?";
				        	
				        	String sqlStatus =  "SELECT * FROM device_log_io \n" + 
				        			"where log_device = ?\n" + 
				        			"ORDER BY log_id DESC LIMIT 1";
				        	
				        	
				        	preparedStatement = con.prepareStatement(sqlCH);
				        	preparedStatement.setInt(1, device);
				        	
				        	ResultSet ress  = preparedStatement.executeQuery();
				        	ress.next();
				        	
				        	int ch = ress.getInt("device_ch");
				        	int thing = ress.getInt("device_esp");
				        	
				        	
				        	preparedStatement = con.prepareStatement(sqlStatus);
				        	preparedStatement.setInt(1, device);
				        	
				        	ress = preparedStatement.executeQuery();
				        	ress.next();
				        	
				        	int statusDevice = ress.getInt("log_status");
				        	
				        	if(statusDevice == 1 || statusDevice == 3) {
				        		
				        		System.out.println("last on");
				        		if(status  == false) {
				        			timer.setCh(ch);
				        			timer.setStatus(status);
				        			timer.setThing(thing);
				        			timer.setDevice(device);
				        			System.out.println("thing " + thing + " device " + device + " ch " + ch + " status " + status) ;
				        			list.add(timer);
				        		}
				        		
				        		
				        	}else if(statusDevice == 2 || statusDevice == 4) {
				        		System.out.println("last off");
				        		if(status == true) {
				        			timer.setCh(ch);
				        			timer.setStatus(status);
				        			timer.setThing(thing);
				        			timer.setDevice(device);
				        			System.out.println("thing " + thing + " device " + device + " ch " + ch + " status " + status) ;
				        			list.add(timer);
				        		}
				        	}
				        	
				        	
				        	
				        	
				        }else {
				        	System.out.println("NO");
				        }
				        
					}
					
					rootTimer.setCodeStatus(200);
					rootTimer.setNameStatus("Success");
					rootTimer.setData(list);
					
				}
			}else {
				
				rootTimer.setCodeStatus(204);
				rootTimer.setNameStatus("Unsuccess");
			}
			
			
		} catch (SQLException e) {
			rootTimer.setCodeStatus(500);
			rootTimer.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(rootTimer);	
		}
		 
		
		
	}


	@RequestMapping(value = "/getname/{idDevice}" , method = RequestMethod.GET)
	@CrossOrigin
	public ResponseEntity<com.tmsm.api.model.device.Device> getName(@PathVariable("idDevice") int idDevice){
		
		 
		 	ResultSet result = null;
			PreparedStatement preparedStmt = null;
			Connection con = Database.connectDatabase();
			
			 com.tmsm.api.model.device.Device device = new com.tmsm.api.model.device.Device();

			try {
				 String sql = "select detail_name from devices_detail where detail_device = ?";
				 preparedStmt = con.prepareStatement(sql);
				 preparedStmt.setInt(1, idDevice);
				 
				 result = preparedStmt.executeQuery();		 
				 result.next();	 
				 device.setNameDevice(result.getString("detail_name"));
				 
				 return ResponseEntity.ok().body(device);

			}catch (SQLException e) {
				
				return ResponseEntity.badRequest().body(device);
			
			}
			
	} 
}
