package com.tmsm.api.controller.thing;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.Status;
import com.tmsm.api.model.ThingAuth;
import com.tmsm.api.utility.Database;



@RestController
@RequestMapping("/thing")
public class Things {

	

	@RequestMapping(value = "/auth", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ThingAuth> thingAuth(@RequestBody ThingAuth thingAuth) {

		ResultSet result = null;
		PreparedStatement preparedStmt = null;
		Connection con = Database.connectDatabase();

		String sql = "select * from things where macaddress = ?";

		try {

			preparedStmt = con.prepareStatement(sql);

			preparedStmt.setString(1, thingAuth.getThingMAC());

			result = preparedStmt.executeQuery();

			int countResult = Database.countResultSet(result);

			if (countResult == 0) {

				Timestamp timestamp = new Timestamp(System.currentTimeMillis());

				String sqlInsert = "insert into things (things_name , macaddress , things_ip , things_type) value (? , ? , ? , ?)";

				String name = "TMSM" + timestamp.getTime();

				preparedStmt = con.prepareStatement(sqlInsert);

				preparedStmt.setString(1, name);
				preparedStmt.setString(2, thingAuth.getThingMAC());
				preparedStmt.setString(3, thingAuth.getThingIP());
				preparedStmt.setInt(4, thingAuth.getThingType());

				boolean status = preparedStmt.execute();

				System.out.println("Status " + status);

				if (!status) {
					String getThings = "select * from things where macaddress = ?";

					preparedStmt = con.prepareStatement(getThings);
					preparedStmt.setString(1, thingAuth.getThingMAC());

					ResultSet res = null;

					res = preparedStmt.executeQuery();

					if (res.next()) {
						thingAuth.setThingName(name);
						thingAuth.setThingID(res.getInt(1));
						thingAuth.setCodeStatus(200);
						thingAuth.setNameStatus("Success : New Things");
					}
				}

				Database.closeConnection(con);
				return ResponseEntity.ok().body(thingAuth);

			} else {

				String sqlUpdare = "update things set things_ip= ? where macaddress = ?";

				preparedStmt = con.prepareStatement(sqlUpdare);

				preparedStmt.setString(1, thingAuth.getThingIP());
				preparedStmt.setString(2, thingAuth.getThingMAC());

				boolean status = preparedStmt.execute();

				String sqlSelect = "select things_id,things_name from things where macaddress= ? ";

				preparedStmt = con.prepareStatement(sqlSelect);

				preparedStmt.setString(1, thingAuth.getThingMAC());

				result = preparedStmt.executeQuery();

				result.next();

				thingAuth.setThingID(result.getInt(1));
				thingAuth.setThingName(result.getString(2));
				thingAuth.setCodeStatus(200);
				thingAuth.setNameStatus("Success : Old Things");

				Database.closeConnection(con);
				return ResponseEntity.ok().body(thingAuth);

			}

		} catch (SQLException e) {

			e.printStackTrace();

			thingAuth.setCodeStatus(400);
			thingAuth.setNameStatus("ERROR ;" + e.getMessage().toString());

			Database.closeConnection(con);
			return ResponseEntity.ok().body(thingAuth);
		}

	}

	@CrossOrigin
	@RequestMapping(value = "/sensor",method = RequestMethod.POST)
	public ResponseEntity<ThingAuth> authSensor(@RequestBody ThingAuth thingAuth){
		
		ResultSet result = null;
		PreparedStatement preparedStm = null;
		Connection con = Database.connectDatabase();
		Status status = new Status();
		
		String sql = "select * from things where macaddress = ?";
		
		try {
			
            preparedStm = con.prepareStatement(sql);			
			preparedStm.setString(1, thingAuth.getThingMAC());			
			result = preparedStm.executeQuery();			
			int countResult = Database.countResultSet(result);
			
			if(countResult ==  0) {
				try {		
					
					Timestamp timestamp = new Timestamp(System.currentTimeMillis());
					String insertSensor = "insert into things (things_name,things_type,things_ip,macaddress)values (?,?,?,?)";
					String name = "TMSM"+timestamp.getTime();
					
					preparedStm = con.prepareStatement(insertSensor, preparedStm.RETURN_GENERATED_KEYS);			
					
					preparedStm.setString(1,name);
					preparedStm.setInt(2, thingAuth.getThingType());
					preparedStm.setString(3, thingAuth.getThingIP());
					preparedStm.setString(4, thingAuth.getThingMAC());			
				
					preparedStm.executeUpdate();
					result = preparedStm.getGeneratedKeys();
					
					result.next();
					
					int Id = result.getInt(1);
					
					try {
						
						String insertDevice = "insert into devices (device_esp,device_ch,device_priority,device_visibility)values(?,?,?,?) ";
						
						preparedStm = con.prepareStatement(insertDevice,preparedStm.RETURN_GENERATED_KEYS);
						
						preparedStm.setInt(1, Id);
						preparedStm.setInt(2, 1);
						preparedStm.setInt(3, 1);
						preparedStm.setInt(4, 1);
						
						preparedStm.executeUpdate();
						ResultSet resDevice = preparedStm.getGeneratedKeys();
						resDevice.next();
						
						int idDevice = resDevice.getInt(1);
						
						try {
							
							String insertDetail = "insert into devices_detail (detail_name,detail_device,detail_privilege)values(?,?,?)";
							
							preparedStm = con.prepareStatement(insertDetail);
							
							preparedStm.setString(1, name);
							preparedStm.setInt(2, idDevice);
							preparedStm.setString(3, "100");
							
							boolean statusDetail =  preparedStm.execute();
							
							if(!statusDetail) {
								thingAuth.setCodeStatus(200);
								thingAuth.setNameStatus("Create Success");
								thingAuth.setThingID(Id);
								thingAuth.setThingName(name);
								
								Database.closeConnection(con);
								return ResponseEntity.ok().body(thingAuth);
							}
							
							
						} catch (SQLException e) {
							thingAuth.setCodeStatus(400);
							thingAuth.setNameStatus(e.getMessage());
							
							Database.closeConnection(con);
							return ResponseEntity.ok().body(thingAuth);
						}
						
					} catch (SQLException e) {
						thingAuth.setCodeStatus(400);
						thingAuth.setNameStatus(e.getMessage());
						
						Database.closeConnection(con);
						return ResponseEntity.ok().body(thingAuth);
					}
				   
					
				} catch (SQLException e) {
					thingAuth.setCodeStatus(400);
					thingAuth.setNameStatus(e.getMessage());
					Database.closeConnection(con);
					return ResponseEntity.ok().body(thingAuth);
				}
			}else {
				
				String sqlUpdare = "update things set things_ip = ? where macaddress = ?";

				preparedStm = con.prepareStatement(sqlUpdare);

				preparedStm.setString(1, thingAuth.getThingIP());
				preparedStm.setString(2, thingAuth.getThingMAC());

				preparedStm.execute();

				String sqlSelect = "select things_id,things_name from things where macaddress= ? ";
				
				preparedStm = con.prepareStatement(sqlSelect);
				preparedStm.setString(1, thingAuth.getThingMAC());
				result = preparedStm.executeQuery();
				result.next();
				
				thingAuth.setThingID(result.getInt(1));
				thingAuth.setThingName(result.getString(2));
				thingAuth.setCodeStatus(200);
				thingAuth.setNameStatus("Success : Old Things Sensor");
				
				
			}			
			
		} catch (SQLException e) {
			thingAuth.setCodeStatus(400);
			thingAuth.setNameStatus(e.getMessage());
			
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(thingAuth);
		}
	
	}

	
	
	@RequestMapping(value = "/log-mobile/{idUser}/{idStatus}/{idDevice}" , method = RequestMethod.POST)
	@CrossOrigin
	public ResponseEntity<Status> logIOMobile(@PathVariable("idUser") int idUser , 
												@PathVariable("idStatus") int idStatus,
												@PathVariable("idDevice") int idDevice){
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		
		Status status = new Status();
		try {
			con = Database.connectDatabase();		
			String sql = "insert into device_log_io (log_status , log_device ,  log_user) value (? ,? , ?)";
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, idStatus);
			preparedStatement.setInt(2, idDevice);
			preparedStatement.setInt(3, idUser);
			
			boolean statusSQL = preparedStatement.execute();
			if(!statusSQL) {
				status.setCodeStatus(200);
				status.setNameStatus("success");
			}else {
				status.setCodeStatus(500);
				status.setNameStatus("Unsuccess");
			}
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus("Unsuccess : " + e.getErrorCode() + " " + e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
	
	@RequestMapping(value = "/log-manual/{idStatus}/{idDevice}" , method = RequestMethod.POST)
	@CrossOrigin
	public ResponseEntity<Status> logIOManual(@PathVariable("idStatus") int idStatus,
											@PathVariable("idDevice") int idDevice){
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		
		Status status = new Status();
		try {
			con = Database.connectDatabase();		
			String sql = "insert into device_log_io (log_status , log_device) value (? ,?)";
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, idStatus);
			preparedStatement.setInt(2, idDevice);
			
			boolean statusSQL = preparedStatement.execute();
			if(!statusSQL) {
				status.setCodeStatus(200);
				status.setNameStatus("success");
			}else {
				status.setCodeStatus(500);
				status.setNameStatus("Unsuccess");
			}
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus("Unsuccess : " + e.getErrorCode() + " " + e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/door-mobile/{idUsr}/{idStatus}/{idThings}",method = RequestMethod.POST)
	public ResponseEntity<Status> logDoorMobile(@PathVariable("idUsr") int idUsr, @PathVariable("idStatus") int idStatus, @PathVariable("idThings") int idThings){
		Status status = new Status();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "insert into door_log(door_log_status, door_log_things, door_log_user) value (?, ?, ?)";
			statement = con.prepareStatement(sql);
			statement.setInt(1, idStatus);
			statement.setInt(2, idThings);
			statement.setInt(3, idUsr);
			
			boolean st = statement.execute();
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Un Success");
			}
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/door-manual/{idStatus}/{idThings}", method = RequestMethod.POST )
	public ResponseEntity<Status> logDoorManual(@PathVariable("idStatus") int idStatus, @PathVariable("idThings") int idThings) {
		Status status = new Status();
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "insert into door_log(door_log_status, door_log_things) value (?, ?)";
			statement = con.prepareStatement(sql);
			statement.setInt(1, idStatus);
			statement.setInt(2, idThings);
			
			
			boolean st = statement.execute();
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Un Success");
			}
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/log-sensor/{idThings}/{idCh}/{value}", method = RequestMethod.POST)
	public ResponseEntity<Status> logSensor(@PathVariable("idThings") int idThings, @PathVariable("idCh") int Ch, @PathVariable("value") double value) {
		Status status = new Status();
		
		PreparedStatement statement = null;
		Connection con = Database.connectDatabase();
		ResultSet set = null;
		
		try {
			String sql = "select * from devices where device_esp = ? and device_ch = ?";
			statement = con.prepareStatement(sql);
			statement.setInt(1, idThings);
			statement.setInt(2, Ch);
			
			set = statement.executeQuery();
			
			set.next();
			
			String sql2 = "insert into sensor_value(sensor_device, sensor_values) value (?, ?)";
			statement = con.prepareStatement(sql2);
			statement.setInt(1, set.getInt(1));
			statement.setDouble(2, value);
			
			boolean st = statement.execute();
			if(!st) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");
			}
			
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
}
