package com.tmsm.api.controller.mobile;

import java.awt.PageAttributes.MediaType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.Status;
import com.tmsm.api.model.mobile.AddNewArea;
import com.tmsm.api.model.mobile.Device;
import com.tmsm.api.model.mobile.ReqAreaThings;
import com.tmsm.api.model.mobile.ResArea;
import com.tmsm.api.model.mobile.ResThingsPosition;
import com.tmsm.api.model.mobile.ThingsPosition;
import com.tmsm.api.utility.Database;

@RestController
@RequestMapping("/area")
public class AreaThings {

	@RequestMapping(method = RequestMethod.PUT)
	@CrossOrigin
	public ResponseEntity<Status> setArea(@RequestBody ReqAreaThings resReqAreaThings) {

		Status status = new Status();
		PreparedStatement preparedStatement = null;
		Connection con = Database.connectDatabase();

		try {
			String sql = "update things set  things_offsetx = ? , things_offsety = ? where things_id = ?";

			preparedStatement = con.prepareStatement(sql);
			
			preparedStatement.setDouble(1, resReqAreaThings.getThingsOffsetX());
			preparedStatement.setDouble(2, resReqAreaThings.getThingsOffsetY());
			preparedStatement.setInt(3, resReqAreaThings.getThingsID());

			boolean statusUpdate = preparedStatement.execute();

			if (!statusUpdate) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");

			} else {
				status.setCodeStatus(204);
				status.setNameStatus("UnSuccess");

			}

		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus("Erorr " + e.getMessage());

		} finally {
			return ResponseEntity.ok().body(status);
		}

	}

	@RequestMapping(value = "/position", method = RequestMethod.GET)
	@CrossOrigin
	public ResponseEntity<ResThingsPosition> getThingsPosition() {

		ResThingsPosition things = new ResThingsPosition();

		Connection con = Database.connectDatabase();
		ResultSet result = null;
		PreparedStatement preparedStmt = null;

		String sql = "select things_id, things_name,things_offsetx,things_offsety,type_name from things inner join  things_type on things_type.type_id = things.things_type";

		try {

			preparedStmt = con.prepareStatement(sql);
			result = preparedStmt.executeQuery();

			List<ThingsPosition> listPosition = new ArrayList<ThingsPosition>();

			while (result.next()) {

				String sqlDevice = "select * from devices inner join devices_detail on device_id = detail_device\n"
						+ " inner join devices_priority on priority_id = device_priority\n"
						+ " inner join devices_visibility on visibility_id = device_visibility \n"
						+ " inner join detail_area on dear_id = devices_detail.detail_area\n"
						+ " inner join area_of_devices on area_of_device_id = dear_area  where device_esp = ?";

				preparedStmt = con.prepareStatement(sqlDevice);
				preparedStmt.setLong(1, result.getInt(1));

				ResultSet res = preparedStmt.executeQuery();

				List<Device> listDevice = new ArrayList<Device>();

				while (res.next()) {

					Device device = new Device();

					device.setDeviceID(res.getInt(1));
					device.setDeviceThing(res.getInt(2));
					device.setDeviceCH(res.getInt(3));
					device.setDevicePriority(res.getInt(4));
					device.setDeviceVisibility(res.getInt(5));
					device.setDetailID(res.getInt(6));
					device.setDetailName(res.getString(7));
					device.setDetailDevice(res.getInt(8));
					device.setDetailPrivilege(res.getString(9));
					device.setDetailArea(res.getInt(10));					
					device.setNamePriority(res.getString(12));
					device.setNameVisibility(res.getString(14));
					device.setNameArea(res.getString(16));
					device.setImage(res.getString(17));
					device.setPositionID(res.getInt(19));
					device.setPositionName(res.getString(20));

					listDevice.add(device);

				}

				ThingsPosition position = new ThingsPosition();

				position.setThingsID(result.getInt(1));
				position.setThingsName(result.getString(2));
				position.setThingsOffsetX(result.getDouble(3));
				position.setThingsOffsetY(result.getDouble(4));
				position.setThingsType(result.getString(5));
				position.setDevices(listDevice);

				listPosition.add(position);
			}

			things.setCodeStatus(200);
			things.setNameStatus("Success");
			things.setDatas(listPosition);

			Database.closeConnection(con);
			return ResponseEntity.ok().body(things);

		} catch (SQLException ex) {
			things.setCodeStatus(400);
			things.setNameStatus(ex.getMessage());

			Database.closeConnection(con);
			return ResponseEntity.ok().body(things);
		}
	}
	
	
	@CrossOrigin
	@RequestMapping(value = "/addarea",method = RequestMethod.POST)
	public ResponseEntity<Status> AddArea(@RequestBody AddNewArea newArea){
		
		Status status = new Status();
		PreparedStatement preparedStatement = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "insert into area_of_devices (area_of_device_name,area_latitude,area_longitudes) values (?,?,?) ";
			
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setString(1, newArea.getNameArea());
			preparedStatement.setDouble(2, newArea.getLatitude());
			preparedStatement.setDouble(3, newArea.getLongitude());
			
			boolean statusUpdate = preparedStatement.execute();

			if (!statusUpdate) {
				status.setCodeStatus(200);
				status.setNameStatus("Create Success");

			} else {
				status.setCodeStatus(204);
				status.setNameStatus("Create UnSuccess");

			}			
			
		} catch (SQLException ex) {
			status.setCodeStatus(500);
			status.setNameStatus(ex.getMessage());
		}finally {
			return ResponseEntity.ok().body(status);
		}		
	}
	
	
	@CrossOrigin
	@RequestMapping(value = "/root",method = RequestMethod.GET)
	public ResponseEntity<ResArea> getArea(){
		
		ResArea area = new ResArea();
		
		ResultSet result = null;
		PreparedStatement preparedstatement = null;
		Connection con = Database.connectDatabase();
		
		String sql = "select * from area_of_devices";
		
		try {
			
			preparedstatement = con.prepareStatement(sql);
			result = preparedstatement.executeQuery();
			List<AddNewArea> listArea = new ArrayList<AddNewArea>();
			
			while(result.next()) {
				AddNewArea addArea = new AddNewArea();
				
				addArea.setAreaID(result.getInt(1));
				addArea.setNameArea(result.getString(2));
				addArea.setLatitude(result.getDouble(3));
				addArea.setLongitude(result.getDouble(4));
				
				listArea.add(addArea);
			}
			
			area.setCodeStatus(200);
			area.setNameStatus("Success");
			area.setArea(listArea);			
			
			Database.closeConnection(con);
			return ResponseEntity.ok().body(area);
		} catch (SQLException ex) {
			
			area.setCodeStatus(500);
			area.setNameStatus(ex.getMessage());
			
			Database.closeConnection(con);
			return ResponseEntity.ok().body(area);
		}	
	}
	
	
}
