package com.tmsm.api.controller.mobile;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.mobile.ReqLogin;
import com.tmsm.api.model.mobile.ResLogin;
import com.tmsm.api.utility.Database;


@RestController
@RequestMapping("/user")
public class User {

	@RequestMapping(value = "/login" , 
					method =  RequestMethod.POST,
					consumes = MediaType.APPLICATION_JSON_VALUE,
					produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin
	public ResponseEntity<ResLogin> login(@RequestBody ReqLogin reqLogin){
		
		
		String sqlCheckLogin = "select usr_id , privilege from user where usr_user = ? and usr_pass = ? ";
		
		ResultSet result = null;
		PreparedStatement preparedStmt = null;
		Connection con = Database.connectDatabase();
		
		
		ResLogin resLogin = new ResLogin();
		try {
			
			preparedStmt = con.prepareStatement(sqlCheckLogin);
			preparedStmt.setString(1, reqLogin.getUser());
			preparedStmt.setString(2 , reqLogin.getPass());
			
			result = preparedStmt.executeQuery();
			
			int count = Database.countResultSet(result);
			
			if(count == 1) {
				
				
				
				int privilege = result.getInt("privilege");
				
				
				resLogin.setIdUser(result.getInt(1));
				resLogin.setLevelPrivilege(privilege);
				resLogin.setCodeStatus(200);
				resLogin.setNameStatus("Login Success");
			}else {
				
				resLogin.setIdUser(0);
				resLogin.setCodeStatus(400);
				resLogin.setNameStatus("Login Unuccess");
			}
		} catch (SQLException e) {
			// TODO: handle exception
			
			resLogin.setIdUser(0);
			resLogin.setCodeStatus(500);
			resLogin.setNameStatus(e.getMessage());
			
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(resLogin); 
		}
		

	}



	@RequestMapping(value = "/name" , method = RequestMethod.GET)
	public String name() {
		
		return "rachata";
	}
}
