package com.tmsm.api.controller.mobile;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.Status;
import com.tmsm.api.model.mobile.RequestSensing;
import com.tmsm.api.model.mobile.ResStatusIOSensing;
import com.tmsm.api.model.mobile.StatusIOSensing;
import com.tmsm.api.model.web.ListSettingSensing;
import com.tmsm.api.model.web.SettingSensing;
import com.tmsm.api.utility.Database;

@RestController
@RequestMapping("/sensing")
public class Sensing {

	@CrossOrigin
	@RequestMapping(value = "/setting", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Status> setting(@RequestBody RequestSensing requestSensing) {

		Connection con = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		Status status = new Status();
		try {

			con = Database.connectDatabase();

			if (requestSensing.isSettingSelect() == true) {

				String sql = "insert into setting_remote_sensing (devices_io , devices_sensor , setting_user , \n"
						+ "									setting_datas , setting_enable , setting_select , setting_io_status)\n"
						+ "                                    values (? , ? , ? , ? , ? , ? , ?)";

				preparedStatement = con.prepareStatement(sql);
				preparedStatement.setInt(1, requestSensing.getDevicesIO());
				preparedStatement.setInt(2, requestSensing.getDevicesSensor());
				preparedStatement.setInt(3, requestSensing.getSettingUser());
				preparedStatement.setDouble(4, requestSensing.getSettingDatas());

				preparedStatement.setBoolean(5, true);
				preparedStatement.setBoolean(6, true);
				preparedStatement.setBoolean(7, requestSensing.isSettingIOStatus());

				boolean statusAPI = preparedStatement.execute();

				if (statusAPI == false) {

					status.setCodeStatus(200);
					status.setNameStatus("Success");

				} else {

					status.setCodeStatus(204);
					status.setNameStatus("Unsuccess");

				}
			} else {

				String sql = "insert into setting_remote_sensing (devices_io , devices_sensor , setting_user , \n"
						+ "									setting_min , setting_max , setting_enable , setting_select , setting_io_status)\n"
						+ "                                    values (? , ? , ? , ? , ? , ? , ? , ? )";

				preparedStatement = con.prepareStatement(sql);
				preparedStatement.setInt(1, requestSensing.getDevicesIO());
				preparedStatement.setInt(2, requestSensing.getDevicesSensor());
				preparedStatement.setInt(3, requestSensing.getSettingUser());
				preparedStatement.setDouble(4, requestSensing.getSettingMin());
				preparedStatement.setDouble(5, requestSensing.getSettingMax());

				preparedStatement.setBoolean(6, true);

				preparedStatement.setBoolean(7, false);
				preparedStatement.setBoolean(8, requestSensing.isSettingIOStatus());

				boolean statusAPI = preparedStatement.execute();

				if (statusAPI == false) {

					status.setCodeStatus(200);
					status.setNameStatus("Success");

				} else {

					status.setCodeStatus(204);
					status.setNameStatus("Unsuccess");

				}
			}

		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}

//	@SuppressWarnings("finally")
//	@CrossOrigin
//	@RequestMapping(value = "/accept/{area_id}/{privilege_id}/{user_id}", method = RequestMethod.GET)
//	public ResponseEntity<ListSettingSensing> getAcceptSetting(@PathVariable("area_id") int area_id, @PathVariable("privilege_id") int privilege, @PathVariable("user_id") int user_id){
//		ListSettingSensing sensing = new ListSettingSensing();
//		PreparedStatement statement = null;
//		ResultSet set = null;
//		Connection con = Database.connectDatabase();
//		
//		try {
//			String sql = "select things_id, things_type, things_nickname, dear_area, device_id from things inner join detail_area on dear_id = things_area \r\n" + 
//					"					 inner join devices on things_id = device_esp\r\n" + 
//					"					 where dear_area = ? and things_status = 1";
//			statement = con.prepareStatement(sql);
//			statement.setInt(1, area_id);
//			set = statement.executeQuery();
//			
//			List<SettingSensing> lists = new ArrayList<SettingSensing>();
//			while(set.next()) {
//				
//				String sql2 = "select setting_id,devices_io,devices_sensor,setting_user,setting_min,setting_max,setting_status,setting_datas,\r\n" + 
//						"	   setting_enable,setting_date,setting_select,setting_io_status,user_detail_fname,user_detail_fname,things_nickname from \r\n" + 
//						"		setting_remote_sensing inner join user_detail on setting_user = user_detail_id\r\n" + 
//						"		inner join devices on devices_sensor = device_id\r\n" + 
//						"		inner join things on things_id = device_esp\r\n" + 
//						"       where devices_io";
//				statement = con.prepareStatement(sql2);
//				statement.setInt(1, set.getInt("device_id"));
//				
//				ResultSet set2 = statement.executeQuery();
//				
//				while(set2.next()) {
//					SettingSensing sensing2 = new SettingSensing();
//					sensing2.setSetting_id(set2.getInt(1));
//					sensing2.setDevices_io(set2.getInt(2));
//					sensing2.setDevices_sensor(set2.getInt(3));
//					sensing2.setSetting_user(set2.getInt(4));
//					sensing2.setSetting_min(set2.getInt(5));
//					sensing2.setSetting_max(set2.getInt(6));
//					sensing2.setSetting_status(set2.getBoolean(7));
//					sensing2.setSetting_datas(set2.getFloat(8));
//					sensing2.setSetting_enable(set2.getBoolean(9));
//					sensing2.setSetting_date(set2.getString(10));
//					sensing2.setSetting_select(set2.getInt(11));
//					sensing2.setSetting_io(set2.getInt(12));
//					sensing2.setFull_name(set2.getString(13) + " " + set2.getString(14));
//					sensing2.setIo_name(set.getString("things_nickname"));
//					sensing2.setSensing_name(set2.getString("things_nickname"));
//					lists.add(sensing2);
//				}
//				
//			}
//			
//			sensing.setCodeStatus(200);
//			sensing.setNameStatus("Success");
//			sensing.setSensings(lists);
//		} catch (SQLException e) {
//			sensing.setCodeStatus(200);
//			sensing.setNameStatus(e.getMessage());
//		}finally {
//			Database.closeConnection(con);
//			return ResponseEntity.ok().body(sensing);
//		}
//	}
//	
	@CrossOrigin
	@RequestMapping(value = "/status/{sensorID}/{sensorValue}", method = RequestMethod.GET)
	public ResponseEntity<ResStatusIOSensing> getStatusIO(@PathVariable("sensorID") int sensorID,
			@PathVariable("sensorValue") int sensorValue) {
		
		ResStatusIOSensing body = new ResStatusIOSensing();
		
		List<StatusIOSensing> ioSensings = new ArrayList<StatusIOSensing>();
		
		Connection con = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		
		
		try {
			con = Database.connectDatabase();
			
			String sql = "select * from setting_remote_sensing \n" + 
					"where setting_enable = true and setting_status = true and devices_sensor = ? ";
			
			preparedStatement = con.prepareStatement(sql);	
			preparedStatement.setInt(1, sensorID);
			
			resultSet = preparedStatement.executeQuery();
			if(Database.countResultSet(resultSet) > 0) {
				
				resultSet.beforeFirst();
				
				while(resultSet.next()) {
					
					int idDevice = resultSet.getInt("devices_io");
					
					sql = "select * from devices where device_id = ?";
					
					preparedStatement = con.prepareStatement(sql);
					preparedStatement.setInt(1, idDevice);
					
					ResultSet re = preparedStatement.executeQuery();
					
					re.next();
					
					int ch = re.getInt("device_ch");
					int deviceID = re.getInt("device_id");
					int idThing =  re.getInt("device_esp");
					
					StatusIOSensing  StatusIOSensing  = new  StatusIOSensing();
					StatusIOSensing.setCh(ch);
					StatusIOSensing.setIdDevice(deviceID);
					StatusIOSensing.setIdThing(idThing);
					StatusIOSensing.setIdUser(resultSet.getInt("setting_user"));
					
					sql = "SELECT *  FROM device_log_io \n" + 
							"where log_device = ? \n" + 
							" ORDER BY log_id DESC LIMIT 1";
					
					preparedStatement = con.prepareStatement(sql);
					preparedStatement.setInt(1, deviceID);
					ResultSet res = preparedStatement.executeQuery();
					
					res.next();
					
					int status  = res.getInt("log_status");
	
					boolean statusSensing = resultSet.getBoolean("setting_io_status");
					
					
					if((status == 1 || status == 3 ) && statusSensing == false) {
						StatusIOSensing.setStatusIO(false);
					}
					
					if((status == 2 || status == 4 ) && statusSensing == true) {
						StatusIOSensing.setStatusIO(true);
					}
					
					ioSensings.add(StatusIOSensing);
				}
				
				
				
				body.setCodeStatus(200);
				body.setNameStatus("Success");
				body.setData(ioSensings);
			}else {
				body.setCodeStatus(204);
				body.setNameStatus("No Data");
			}
			
		} catch (SQLException e) {

			body.setCodeStatus(500);
			body.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(body);
			
		}
		

	}

}
