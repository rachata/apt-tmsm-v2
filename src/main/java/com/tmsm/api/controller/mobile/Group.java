package com.tmsm.api.controller.mobile;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.spi.DirStateFactory.Result;
import javax.xml.crypto.Data;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.Status;
import com.tmsm.api.model.mobile.DeviceGroupUser;
import com.tmsm.api.model.mobile.GroupDevice;
import com.tmsm.api.model.mobile.ReqCreateGroup;
import com.tmsm.api.model.mobile.RootDeviceGroupUser;
import com.tmsm.api.model.mobile.RootGruopDevice;
import com.tmsm.api.model.web.DevicesInGroup;
import com.tmsm.api.model.web.ListDevicesInGroup;
import com.tmsm.api.utility.Database;

@RestController
@RequestMapping("/group")
public class Group {

	
	@SuppressWarnings("finally")
	@RequestMapping(method = RequestMethod.POST)
	@CrossOrigin
	public ResponseEntity<?> createGroup(@RequestBody ReqCreateGroup reqCreateGroup){
		
		Connection con = Database.connectDatabase();
		PreparedStatement preparedStatement = null;
		
		Status status  = new Status();
		try {
			String sql = "insert into  group_device_of_user (g_name , g_usr ) value(? , ?)";
			preparedStatement = con.prepareStatement(sql);
			
			preparedStatement.setString(1, reqCreateGroup.getNameGroup());
			preparedStatement.setInt(2, reqCreateGroup.getUserID());
			
			boolean statusQuery = preparedStatement.execute();
			
			if(!statusQuery) {
				status.setCodeStatus(200);
				status.setNameStatus("Create Group Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Create Group Unsuccess");
			}
			
			
			
			
			
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus("ERROR " + e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
		
	}

	
	
	@SuppressWarnings("finally")
	@RequestMapping(value = "/{id}" , method = RequestMethod.GET)
	@CrossOrigin
	
	public ResponseEntity<?> getGroupOfUser(@PathVariable("id") int idUser){
		
		
		Connection con = Database.connectDatabase();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		
		RootGruopDevice rootGruopDevice = new RootGruopDevice();
		
		try {
			
			String sql = "select * from group_device_of_user where g_usr = ?";
			
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1 , idUser);
			resultSet = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(resultSet);
			
			if(count >= 1) {
				rootGruopDevice.setCodeStatus(200);
				rootGruopDevice.setNameStatus("Success");
					
				List<GroupDevice> list = new ArrayList<GroupDevice>();
				resultSet.beforeFirst();
				while(resultSet.next()) {
					
					String sql2 = "select count(gd_id) from group_device where gd_group = ?";					
					preparedStatement = con.prepareStatement(sql2);
					preparedStatement.setInt(1, resultSet.getInt(1));
					ResultSet set = preparedStatement.executeQuery();
					set.next();
					
					GroupDevice groupDevice = new GroupDevice();
					groupDevice.setIdGroup(resultSet.getInt(1));
					groupDevice.setNameGroup(resultSet.getString(2));
					groupDevice.setCreateGroup(resultSet.getString(4));
					groupDevice.setDeviceValue(set.getInt(1));
					
					list.add(groupDevice);
					
				}
				
				rootGruopDevice.setGroups(list);
				
			}else {
				rootGruopDevice.setCodeStatus(203);
				rootGruopDevice.setNameStatus("No information for your group");
			}
			
		} catch (SQLException e) {
			rootGruopDevice.setCodeStatus(500);
			rootGruopDevice.setNameStatus("Error " + e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(rootGruopDevice);
		}
	}
	
	@SuppressWarnings("finally")
	@RequestMapping(value = "/{idGroup}/{idDevice}" , method = RequestMethod.POST)
	@CrossOrigin
	
	public ResponseEntity<Status> groupDevice(@PathVariable("idGroup") int idGroup , @PathVariable("idDevice") int idDevice){
		
		
		Status status = new Status();
		Connection con = Database.connectDatabase();
		PreparedStatement preparedStatement = null;
		try {
			String sql = "select * from group_device where gd_group = ? and gd_device = ?";
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, idGroup);
			preparedStatement.setInt(2, idDevice);
			
			ResultSet rs = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(rs);
			
			if(count >= 1) {
				status.setCodeStatus(203);
				status.setNameStatus("Has already been set up");
			}else {
				String sqlInsert = "insert into group_device (gd_group ,gd_device ) value(? ,?)";
				preparedStatement = con.prepareStatement(sqlInsert);
				preparedStatement.setInt(1, idGroup);
				preparedStatement.setInt(2, idDevice);
				boolean statusInsert  = preparedStatement.execute();
				
				if(!statusInsert) {
					status.setCodeStatus(200);
					status.setNameStatus("Add Success");
				
				}else {
					status.setCodeStatus(204);
					status.setNameStatus("Add Unsuccess");
				}
				
			}
			
			
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus("ERROR " +e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
		
	
	
	@SuppressWarnings("finally")
	@RequestMapping(value = "/{idGroup}" , method = RequestMethod.DELETE)
	@CrossOrigin
	
	public ResponseEntity<Status> deleteGroup(@PathVariable("idGroup") int idGroup){
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		
		Status status = new  Status();
		try {
			
			con = Database.connectDatabase();
			String sql ="delete from group_device_of_user where g_id = ?";
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1  , idGroup);
			
			boolean statusSQL = preparedStatement.execute();
			
			if(!statusSQL) {
				status.setCodeStatus(200);
				status.setNameStatus("Delete Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Delete Unsuccess");
			}
		} catch (SQLException e) {
			status.setCodeStatus(204);
			status.setNameStatus("ERROR : " + e.getErrorCode() + " " + e.getMessage());
			
		}finally {
			
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}

	
	
	@SuppressWarnings("finally")
	@RequestMapping(method = RequestMethod.PUT)
	@CrossOrigin
	
	public ResponseEntity<Status> editGroup(@RequestBody GroupDevice groupDevice ){
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		
		Status status = new  Status();
		try {
			
			con = Database.connectDatabase();
			String sql ="update  group_device_of_user  set g_name = ? where g_id = ?";
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setString(1  , groupDevice.getNameGroup());
			preparedStatement.setInt(2, groupDevice.getIdGroup());
			
			boolean statusSQL = preparedStatement.execute();
			
			if(!statusSQL) {
				status.setCodeStatus(200);
				status.setNameStatus("Update Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Update Unsuccess");
			}
		} catch (SQLException e) {
			status.setCodeStatus(204);
			status.setNameStatus("ERROR : " + e.getErrorCode() + " " + e.getMessage());
			
		}finally {
			
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
	
	
	@SuppressWarnings("finally")
	@RequestMapping(value = "/device/{idUser}/{idGroup}" , method = RequestMethod.GET)
	@CrossOrigin
	public ResponseEntity<RootDeviceGroupUser> getDeviceOfGroupUser(@PathVariable("idUser") int idUser , @PathVariable("idGroup") int idGroup ){
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		
		
		RootDeviceGroupUser deviceGroupUser = new RootDeviceGroupUser();
		try {
			
			String sql ="select gd_id , gd_group , permit_name from group_device\n" + 
					"inner join devices_detail on detail_device = gd_device\n" + 
					"inner join permit_user on permit_device = gd_device\n" + 
					"where permit_user = ? and permit_status = true and enabled = true\n" + 
					"and gd_group = ?";
			
			con = Database.connectDatabase();
			
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, idUser);
			preparedStatement.setInt(2, idGroup);
			
			ResultSet res = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(res);
			if(count >= 1) {
				
				deviceGroupUser.setCodeStatus(200);
				deviceGroupUser.setNameStatus("Success");
				
				
				res.beforeFirst();
				
				List<DeviceGroupUser> list = new ArrayList<DeviceGroupUser>();
				while(res.next()) {
					
					DeviceGroupUser deviceGroupUser2= new DeviceGroupUser();
					
					deviceGroupUser2.setGdID(res.getInt("gd_id"));
					deviceGroupUser2.setGroupID(res.getInt("gd_group"));
					deviceGroupUser2.setDeviceName(res.getString("permit_name"));
					
					list.add(deviceGroupUser2);
				}
				deviceGroupUser.setData(list);
			}else {
				deviceGroupUser.setCodeStatus(204);
				deviceGroupUser.setNameStatus("No Data");
			}
		} catch (SQLException e) {
			// TODO: handle exception
			deviceGroupUser.setCodeStatus(500);
			deviceGroupUser.setNameStatus(e.getMessage());
		}finally {
			
			Database.closeConnection(con);
			return ResponseEntity.ok().body(deviceGroupUser);
		}
	}
	
	
	@SuppressWarnings("finally")
	@RequestMapping(value = "/remove/{gid}" , method = RequestMethod.DELETE)
	@CrossOrigin
	public ResponseEntity<Status> getDeviceOfGroupUser(@PathVariable("gid") int gid){
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		
		
		Status status = new Status();
		try {
			
			String sql ="delete from group_device where gd_id = ?";
			
			con = Database.connectDatabase();
			
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, gid);
	
			boolean statusSQL = preparedStatement.execute();
			
			if(!statusSQL) {
				status.setCodeStatus(200);
				status.setNameStatus("Delete Success");
			}else {
				
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");
			}
		} catch (SQLException e) {
			// TODO: handle exception
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/isgroup/{user_id}/{device_id}", method = RequestMethod.GET)
	public ResponseEntity<ListDevicesInGroup> isCheckDevicesIngroup(@PathVariable("user_id") int user_id, @PathVariable("device_id") int device_id){
		ListDevicesInGroup group = new ListDevicesInGroup();
		PreparedStatement statement = null;
		ResultSet set = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "select * from group_device_of_user where g_usr = ?";
			statement = con.prepareStatement(sql);
			statement.setInt(1, user_id);
			set = statement.executeQuery();
			
			List<DevicesInGroup> groups = new ArrayList<DevicesInGroup>();
			while(set.next()) {
				DevicesInGroup inGroup = new DevicesInGroup();
				String sql2 = "select * from group_device where gd_group = ? and gd_device = ?";
				statement = con.prepareStatement(sql2);
				statement.setInt(1, set.getInt(1));
				statement.setInt(2, device_id);
				
				ResultSet set2 = statement.executeQuery();
				int count = Database.countResultSet(set2);
				boolean is_group = false;
				if(count > 0) {
					is_group = true;
				}else {
					is_group = false;
				}
				
				String sql3 = "select * from group_device where gd_group = ?";					
				statement = con.prepareStatement(sql3);
				statement.setInt(1, set.getInt(1));
				ResultSet set3 = statement.executeQuery();
				int countResult = Database.countResultSet(set3);
				
				inGroup.setDevices_id(device_id);
				inGroup.setIs_group(is_group);
				inGroup.setThings_count(countResult);
				inGroup.setGroup_id(set.getInt(1));
				inGroup.setGroup_name(set.getString(2));
				
				groups.add(inGroup);
			}
			
			group.setCodeStatus(200);
			group.setNameStatus("Success");
			group.setGroups(groups);
			
		} catch (SQLException e) {
			group.setCodeStatus(500);
			group.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(group);
		}
	}
	
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(value = "/isgroup/door/{user_id}/{things_id}", method = RequestMethod.GET)
	public ResponseEntity<ListDevicesInGroup> isCheckDoorIngroup(@PathVariable("user_id") int user_id, @PathVariable("things_id") int things_id){
		ListDevicesInGroup group = new ListDevicesInGroup();
		PreparedStatement statement = null;
		ResultSet set = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "select * from group_device_of_user where g_usr = ?";
			statement = con.prepareStatement(sql);
			statement.setInt(1, user_id);
			set = statement.executeQuery();
			
			List<DevicesInGroup> groups = new ArrayList<DevicesInGroup>();
			while(set.next()) {
				DevicesInGroup inGroup = new DevicesInGroup();
				String sql2 = "select * from group_device where gd_group = ? and gd_things = ?";
				statement = con.prepareStatement(sql2);
				statement.setInt(1, set.getInt(1));
				statement.setInt(2, things_id);
				
				ResultSet set2 = statement.executeQuery();
				int count = Database.countResultSet(set2);
				boolean is_group = false;
				if(count > 0) {
					is_group = true;
				}else {
					is_group = false;
				}
				
				String sql3 = "select * from group_device where gd_group = ?";					
				statement = con.prepareStatement(sql3);
				statement.setInt(1, set.getInt(1));
				ResultSet set3 = statement.executeQuery();
				int countResult = Database.countResultSet(set3);
				
				inGroup.setDevices_id(things_id);
				inGroup.setIs_group(is_group);
				inGroup.setThings_count(countResult);
				inGroup.setGroup_id(set.getInt(1));
				inGroup.setGroup_name(set.getString(2));
				
				groups.add(inGroup);
			}
			
			group.setCodeStatus(200);
			group.setNameStatus("Success");
			group.setGroups(groups);
			
		} catch (SQLException e) {
			group.setCodeStatus(500);
			group.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(group);
		}
	}
	
	@SuppressWarnings("finally")
	@RequestMapping(value = "/door/{group_id}/{things_id}" , method = RequestMethod.POST)
	@CrossOrigin	
	public ResponseEntity<Status> addDoorTogroup(@PathVariable("group_id") int group_id , @PathVariable("things_id") int things_id){		
		Status status = new Status();
		Connection con = Database.connectDatabase();
		PreparedStatement preparedStatement = null;
		try {
			String sql = "select * from group_device where gd_group = ? and gd_things = ?";
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, group_id);
			preparedStatement.setInt(2, things_id);
			
			ResultSet rs = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(rs);
			
			if(count >= 1) {
				status.setCodeStatus(203);
				status.setNameStatus("Has already been set up");
			}else {
				String sqlInsert = "insert into group_device (gd_group ,gd_things ) value(? ,?)";
				preparedStatement = con.prepareStatement(sqlInsert);
				preparedStatement.setInt(1, group_id);
				preparedStatement.setInt(2, things_id);
				boolean statusInsert  = preparedStatement.execute();
				
				if(!statusInsert) {
					status.setCodeStatus(200);
					status.setNameStatus("Add Success");
				
				}else {
					status.setCodeStatus(204);
					status.setNameStatus("Add Unsuccess");
				}				
			}	
			
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus("ERROR " +e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
		
}
