package com.tmsm.api.controller.mobile;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.mobile.OffLineResRoot;
import com.tmsm.api.model.mobile.OffLineResThing;
import com.tmsm.api.model.mobile.OfflineResDevice;
import com.tmsm.api.utility.Database;

@RequestMapping("/sync")
@RestController
public class OffLine {

	
	@RequestMapping(value = "/{idUser}/{privilege}" , method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin
	public ResponseEntity<OffLineResRoot> getDeviceIOOffline(@PathVariable("idUser") int idUser , @PathVariable("privilege") int privilege){
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		
		OffLineResRoot lineResRoot  = new OffLineResRoot();
		
		try {
			
			con = Database.connectDatabase();
			String sql = "";
			if(privilege != 1) {
				sql = "select things_id , things_name , things_ip , macaddress from  permit_user\n" + 
						"					inner join devices on permit_device = device_id\n" + 
						"					inner join things on device_esp = things_id\n" + 
						"					where permit_user = ? and permit_status = 1 and enabled = 1\n" + 
						"                    group by things_id";
				preparedStatement = con.prepareStatement(sql);
				preparedStatement.setInt(1, idUser);
			}else if(privilege == 1) {
				sql = "select things_id , things_name , things_ip , macaddress from  things";
				preparedStatement = con.prepareStatement(sql);
			}
			
			
			ResultSet res = preparedStatement.executeQuery();
			int count = Database.countResultSet(res);
			if(count>=1) {
				System.out.println("things_id " + count);
				res.beforeFirst();
				
				List<OffLineResThing> data = new ArrayList<OffLineResThing>();
				while(res.next()) {
					
					OffLineResThing lineResThing = new OffLineResThing();
					lineResThing.setThingID(res.getInt("things_id"));
					lineResThing.setThingIP(res.getString("things_ip"));
					lineResThing.setThingName(res.getString("things_name"));
					lineResThing.setThingMAC(res.getString("macaddress"));
					
					
					if(privilege != 1) {
						
						sql = "select permit_device , permit_status , permit_privilage , permit_grant_status , permit_timeMin , permit_timeMax , \n" + 
								"device_ch , device_priority , device_visibility  , permit_name from permit_user\n" + 
								"inner join devices on permit_device = device_id\n" + 
								"where device_priority = 1 and permit_user = ? and device_esp = ? and permit_status = 1 and enabled = 1";
						
						preparedStatement = con.prepareStatement(sql);
						preparedStatement.setInt(1, idUser);
						preparedStatement.setInt(2, lineResThing.getThingID());
						
						ResultSet rs = preparedStatement.executeQuery();
						
						int countRS  = Database.countResultSet(rs);
						if(countRS >= 1) {
							System.out.println("count " + countRS);
							rs.beforeFirst();
							
							List<OfflineResDevice> listDevice = new ArrayList<OfflineResDevice>();
							while(rs.next()) {
								OfflineResDevice device = new OfflineResDevice();
								device.setPermitDevice(rs.getInt("permit_device"));
								device.setPermitStatus(rs.getInt("permit_status"));
								device.setPermitPrivilage(rs.getString("permit_privilage"));
								device.setPermitGrantStatus(rs.getInt("permit_grant_status"));
								device.setPermitTimeMin(rs.getString("permit_timeMin"));
								device.setPermitTimeMax(rs.getString("permit_timeMax"));
								device.setDeviceCH(rs.getInt("device_ch"));
								device.setDevicePriority(rs.getInt("device_priority"));
								device.setDeviceVisibility(rs.getInt("device_visibility"));
								device.setDefName(rs.getString("permit_name"));
								listDevice.add(device);
							}
							
							lineResThing.setDevice(listDevice);
							lineResThing.setCodeStatus(200);
							lineResThing.setNameStatus("Success");
							
						}else {
							lineResThing.setCodeStatus(204);
							lineResThing.setNameStatus("Unsuccess : No Data (Device)");
						}
						data.add(lineResThing);
						
					}else if(privilege == 1) {
						
						
						sql = "select device_id, device_esp , device_ch , device_priority , device_visibility  , detail_name from devices\n" + 
								"inner join devices_detail on device_id  = detail_device\n" + 
								"where device_priority = 1 and  device_esp = ?";
						
						preparedStatement = con.prepareStatement(sql);
						preparedStatement.setInt(1,  lineResThing.getThingID());
						ResultSet rs = preparedStatement.executeQuery();
						
						int countRS  = Database.countResultSet(rs);
						if(countRS >= 1) {
							System.out.println("count " + countRS);
							rs.beforeFirst();
							
							List<OfflineResDevice> listDevice = new ArrayList<OfflineResDevice>();
							while(rs.next()) {
								OfflineResDevice device = new OfflineResDevice();
								device.setPermitDevice(rs.getInt("device_id"));
								device.setDeviceCH(rs.getInt("device_ch"));
								device.setDevicePriority(rs.getInt("device_priority"));
								device.setDeviceVisibility(rs.getInt("device_visibility"));
								device.setDefName(rs.getString("detail_name"));
								listDevice.add(device);
							}
							
							lineResThing.setDevice(listDevice);
							lineResThing.setCodeStatus(200);
							lineResThing.setNameStatus("Success");
							
						}else {
							lineResThing.setCodeStatus(204);
							lineResThing.setNameStatus("Unsuccess : No Data (Device)");
						}
						data.add(lineResThing);
						
						
					}
					
					
					
				}
				
				lineResRoot.setCodeStatus(200);
				lineResRoot.setNameStatus("Success");
				lineResRoot.setData(data);
			
			}else {
				lineResRoot.setCodeStatus(204);
				lineResRoot.setNameStatus("Unsuccess : No Data (Thing)");
			}
			
		} catch (SQLException e) {
			lineResRoot.setCodeStatus(500);
			lineResRoot.setNameStatus("Unsuccess : " + e.getErrorCode() + " " + e.getMessage());
		}finally {
			
			Database.closeConnection(con);
			return ResponseEntity.ok().body(lineResRoot);
		}
	}
}
