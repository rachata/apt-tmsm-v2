package com.tmsm.api.controller.mobile;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.utility.Database;

@CrossOrigin
@RestController
@RequestMapping("/area-device")
public class Devices {

	
	@CrossOrigin
	@RequestMapping( value = "/{idArea}" , method = RequestMethod.GET , consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Map<String, Integer>>> getDeviceCHWithArea(@PathVariable("idArea" ) int idArea){
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		
		
		List<Map<String  , Integer>> list = new ArrayList<Map<String,Integer>>();
		try {
			
			
			String sql = "select things_id , device_ch  from devices\n" + 
					"inner join things on device_esp = things_id\n" + 
					"inner join detail_area on things_area = dear_id\n" + 
					"inner join area_of_devices on area_of_device_id = dear_area\n" + 
					"where area_of_device_id = ?";
			
			con  = Database.connectDatabase();
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, idArea);
			
			ResultSet res = preparedStatement.executeQuery();
			
			
			while(res.next()) {
				
				Map<String , Integer> map = new TreeMap<String, Integer>();
				
				map.put("thing", res.getInt("things_id"));
				map.put("ch", res.getInt("device_ch"));
				
				
				list.add(map);
			}
		} catch (SQLException e) {
			
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(list);
		}
		
	}
}

