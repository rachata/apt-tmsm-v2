package com.tmsm.api.controller.mobile;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.tmsm.api.model.Status;
import com.tmsm.api.model.mobile.ReqTimer;
import com.tmsm.api.model.mobile.RootReqTimer;
import com.tmsm.api.utility.Database;

@RestController
@RequestMapping("/timerset")
public class Timer  {

	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST , 
					consumes = MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Status> createReq( @RequestBody com.tmsm.api.model.mobile.Timer timer){
		
		
		Status status = new Status();
		Connection con = null;
		PreparedStatement preparedStatement;
		try {
			
			con  = Database.connectDatabase();
			
			String sql;
			sql = "select detail_privilege from devices_detail where detail_device = ?";
			
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, timer.gettDevice());
			
			ResultSet res = preparedStatement.executeQuery();
			res.next();
			
			String privilegeDevice = res.getString("detail_privilege");
			char priDArr[] = privilegeDevice.toCharArray();
			
			
			if(priDArr[1] == '1' && timer.getPrivilege() == 1) {
				
				sql = "insert into timer_setting (t_user , t_device , t_datetime_min , t_datetime_max , t_time_min , "
						+ "t_time_max , t_select_st , status_device , t_status , t_enable)\n" + 
						"value (? , ? , ? , ? , ? , ? , ? , ? , ? , ?)";
				
				
				boolean selectSet = false;
				if(timer.gettDatetimeMin() == null && timer.gettDatetimeMin() == null) {
					
					selectSet = true;
				}else if(timer.gettTimeMin() == null && timer.gettTimeMax() == null){
					
					selectSet= false;
				}
				preparedStatement = con.prepareStatement(sql);
				preparedStatement.setInt(1, timer.gettUser());
				preparedStatement.setInt(2, timer.gettDevice());
				preparedStatement.setString(3, timer.gettDatetimeMin());
				preparedStatement.setString(4, timer.gettDatetimeMax());
				preparedStatement.setString(5, timer.gettTimeMin());
				preparedStatement.setString(6, timer.gettTimeMax());
				preparedStatement.setBoolean(7, selectSet);
				preparedStatement.setBoolean(8, timer.isStatusDevice());
				
				preparedStatement.setInt( 9 ,1);
				preparedStatement.setInt( 10 ,1);
				
				boolean statusSQL = preparedStatement.execute();
				
				if(!statusSQL) {
					status.setCodeStatus(200);
					status.setNameStatus("Success");
				}else {
					status.setCodeStatus(204);
					status.setNameStatus("Unsuccess");
				}
				
				
			}else {
			sql = "select permit_privilage from permit_user where permit_user = ? and permit_device = ?  and permit_status = 1 and enabled = 1";
			
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, timer.gettUser());
			preparedStatement.setInt(2, timer.gettDevice());
			
			res = preparedStatement.executeQuery();
			res.next();
			
			String privilegeUser = res.getString("permit_privilage");
			char priUArr[] = privilegeUser.toCharArray();
			
			
			if(priDArr[1] == '1') {
				
				if(priUArr[1] == '1') {
					sql = "insert into timer_setting (t_user , t_device , t_datetime_min , t_datetime_max , t_time_min , "
							+ "t_time_max , t_select_st , status_device , t_status , t_enable)\n" + 
							"value (? , ? , ? , ? , ? , ? , ? , ? , ? , ?)";
					
					
					boolean selectSet = false;
					if(timer.gettDatetimeMin() == null && timer.gettDatetimeMin() == null) {
						
						selectSet = true;
					}else if(timer.gettTimeMin() == null && timer.gettTimeMax() == null){
						
						selectSet= false;
					}
					preparedStatement = con.prepareStatement(sql);
					preparedStatement.setInt(1, timer.gettUser());
					preparedStatement.setInt(2, timer.gettDevice());
					preparedStatement.setString(3, timer.gettDatetimeMin());
					preparedStatement.setString(4, timer.gettDatetimeMax());
					preparedStatement.setString(5, timer.gettTimeMin());
					preparedStatement.setString(6, timer.gettTimeMax());
					preparedStatement.setBoolean(7, selectSet);
					preparedStatement.setBoolean(8, timer.isStatusDevice());
					
					if(timer.getPrivilege() == 2) {
						preparedStatement.setInt( 9 ,1);
						preparedStatement.setInt( 10 ,1);
					}else {
						
						preparedStatement.setInt( 9 ,0);
						preparedStatement.setInt( 10 ,1);
					}
					boolean statusSQL = preparedStatement.execute();
					
					if(!statusSQL) {
						status.setCodeStatus(200);
						status.setNameStatus("Success");
					}else {
						status.setCodeStatus(204);
						status.setNameStatus("Unsuccess");
					}
					
					
					
				}else {
					status.setCodeStatus(204);
					status.setNameStatus("You do not have permission to schedule the device.");
				}
				
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("This device cannot set the time.");
			}
			}
			
			
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus("Unsuccess : " + e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}

	}

	
	
	@CrossOrigin
	@RequestMapping(value = "/{idUser}/{privilege}"  , method = RequestMethod.GET ,  produces = MediaType.APPLICATION_JSON_VALUE)
	
	public ResponseEntity<RootReqTimer> getReq(@PathVariable("idUser") int idUser , @PathVariable("privilege") int privilege){
		
		RootReqTimer reqTimer = new RootReqTimer();
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		try {
			
			con =  Database.connectDatabase();
			
			String sql = "";
			if(privilege == 1) {
				sql = "select tid , t_user , t_device , t_datetime_min , t_datetime_max , t_time_min , t_time_max\n" + 
						",t_select_st , t_status , t_enable , t_set_datetime , status_device , concat(user_detail_fname , ' ' , user_detail_lname) as fullname , detail_name from timer_setting\n" + 
						"inner join user_detail on user_detail_id = t_user \n" + 
						"inner join devices_detail on t_device = detail_device\n" + 
						"inner join devices on t_device = device_id\n" + 
						"where t_status = 0  and t_enable = 1";
				
				preparedStatement = con.prepareStatement(sql);
			}else {
				
				sql = "select tid , t_user , t_device , t_datetime_min , t_datetime_max , t_time_min , t_time_max\n" + 
						",t_select_st , t_status , t_enable , t_set_datetime , status_device , concat(user_detail_fname , ' ' , user_detail_lname) as fullname , detail_name from timer_setting\n" + 
						"inner join user_detail on user_detail_id = t_user \n" + 
						"inner join devices_detail on t_device = detail_device\n" + 
						"inner join devices on t_device = device_id\n" + 
						"inner join things on things_id = device_esp\n" + 
						"inner join detail_area on dear_id = things_area\n" + 
						"inner join area_administrator on area = dear_area\n" + 
						"where t_status = 0  and t_enable = 1 and area_admin_user = ?";
				preparedStatement = con.prepareStatement(sql);
				preparedStatement.setInt(1, idUser);
			}
			
			ResultSet resultSet = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(resultSet);
			if(count >= 1) {
				resultSet.beforeFirst();
				
				
				List<ReqTimer> list = new ArrayList<ReqTimer>();
				while(resultSet.next()) {
					ReqTimer req = new ReqTimer();
					
					req.settID(resultSet.getInt("tid"));
					req.settUser(resultSet.getInt("t_user"));
					req.settDevice(resultSet.getInt("t_device"));
					req.settDatetimeMin(resultSet.getString("t_datetime_min"));
					req.settDatetimeMax(resultSet.getString("t_datetime_max"));
					req.settTimeMin(resultSet.getString("t_time_min"));
					req.settTimeMax(resultSet.getString("t_time_max"));
					req.settSelect(resultSet.getBoolean("t_select_st"));
					req.settStatus(resultSet.getBoolean("t_status"));
					req.settEnable(resultSet.getBoolean("t_enable"));
					req.settSetDatetime(resultSet.getString("t_set_datetime"));
					req.setStatusDevice(resultSet.getBoolean("status_device"));
					req.setFullName(resultSet.getString("fullname"));
					req.setDeviceName(resultSet.getString("detail_name"));
					
					list.add(req);
				}
				
				reqTimer.setCodeStatus(200);
				reqTimer.setNameStatus("Success");
				reqTimer.setData(list);
			}else {
				reqTimer.setCodeStatus(204);
				reqTimer.setNameStatus("No Data");
			}
					
			
		} catch (SQLException e) {
			reqTimer.setCodeStatus(500);
			reqTimer.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(reqTimer);
		}
	}

	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.PUT , consumes = MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<Status> updateStatusEnable(@RequestBody ReqTimer reqTimer){
		
		Status status  = new Status();
		Connection con =  null;
		PreparedStatement preparedStatement;
		try {
			
			con = Database.connectDatabase();
			String sql = "update timer_setting set t_status = ? , t_enable = ? where tid = ?";
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setBoolean(1, reqTimer.istStatus());
			preparedStatement.setBoolean(2, reqTimer.istEnable());
			preparedStatement.setInt(3, reqTimer.gettID());
			
			boolean statusSQL = preparedStatement.execute();
			
			if(!statusSQL) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");
			}
			
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
	
	@CrossOrigin
	@RequestMapping(value = "/{idUser}/device/{idDevice}"  , method = RequestMethod.GET ,  produces = MediaType.APPLICATION_JSON_VALUE)
	
	public ResponseEntity<RootReqTimer> getReqUser(@PathVariable("idUser") int idUser  , @PathVariable("idDevice") int idDevice){
		
		RootReqTimer reqTimer = new RootReqTimer();
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		try {
			
			con =  Database.connectDatabase();
			
			String sql = "select tid , t_user , t_device , t_datetime_min , t_datetime_max , t_time_min , t_time_max ,\n" + 
					"t_select_st , t_status , t_enable , t_set_datetime , status_device  , permit_name as 'detail_name'  from timer_setting\n" + 
					"inner join user_detail on user_detail_id = t_user\n" + 
					"inner join devices_detail on t_device = detail_device\n" + 
					"inner join permit_user on permit_device = t_device\n" + 
					"where t_status = true and t_enable = true and t_user = ? and permit_user = ? and t_device = ?";
	
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, idUser);
			preparedStatement.setInt(2, idUser);
			preparedStatement.setInt(3, idDevice);
			
			
			ResultSet resultSet = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(resultSet);
			if(count >= 1) {
				resultSet.beforeFirst();
				
				
				List<ReqTimer> list = new ArrayList<ReqTimer>();
				while(resultSet.next()) {
					ReqTimer req = new ReqTimer();
					
					req.settID(resultSet.getInt("tid"));
					req.settUser(resultSet.getInt("t_user"));
					req.settDevice(resultSet.getInt("t_device"));
					req.settDatetimeMin(resultSet.getString("t_datetime_min"));
					req.settDatetimeMax(resultSet.getString("t_datetime_max"));
					req.settTimeMin(resultSet.getString("t_time_min"));
					req.settTimeMax(resultSet.getString("t_time_max"));
					req.settSelect(resultSet.getBoolean("t_select_st"));
					req.settStatus(resultSet.getBoolean("t_status"));
					req.settEnable(resultSet.getBoolean("t_enable"));
					req.settSetDatetime(resultSet.getString("t_set_datetime"));
					req.setStatusDevice(resultSet.getBoolean("status_device"));
					req.setDeviceName(resultSet.getString("detail_name"));
					
					list.add(req);
				}
				
				reqTimer.setCodeStatus(200);
				reqTimer.setNameStatus("Success");
				reqTimer.setData(list);
			}else {
				reqTimer.setCodeStatus(204);
				reqTimer.setNameStatus("No Data");
			}
					
			
		} catch (SQLException e) {
			reqTimer.setCodeStatus(500);
			reqTimer.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(reqTimer);
		}
	}

	
	@CrossOrigin
	@RequestMapping(value =  "/{idTimer}" ,  method = RequestMethod.DELETE ,  produces = MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<Status> deleteTimer(@PathVariable("idTimer") int idTimer){
		
		Status status  = new Status();
		Connection con =  null;
		PreparedStatement preparedStatement;
		try {
			
			con = Database.connectDatabase();
			String sql = "update timer_setting  set t_enable = false where tid = ?";
			preparedStatement = con.prepareStatement(sql);
			
			preparedStatement.setInt(1, idTimer);

			
			boolean statusSQL = preparedStatement.execute();
			
			if(!statusSQL) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");
			}
			
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/permit/{idUser}/{privilege}"  , method = RequestMethod.GET ,  produces = MediaType.APPLICATION_JSON_VALUE)
	
	public ResponseEntity<RootReqTimer> getTimerEnableStatusPermit(@PathVariable("idUser") int idUser , @PathVariable("privilege") int privilege){
		
		RootReqTimer reqTimer = new RootReqTimer();
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		try {
			
			con =  Database.connectDatabase();
			
			String sql = "";
			if(privilege == 1) {
				sql = "select tid , t_user , t_device , t_datetime_min , t_datetime_max , t_time_min , t_time_max\n" + 
						",t_select_st , t_status , t_enable , t_set_datetime , status_device , concat(user_detail_fname , ' ' , user_detail_lname) as fullname , detail_name from timer_setting\n" + 
						"inner join user_detail on user_detail_id = t_user \n" + 
						"inner join devices_detail on t_device = detail_device\n" + 
						"inner join devices on t_device = device_id\n" + 
						"where t_status = 1  and t_enable = 1";
				
				preparedStatement = con.prepareStatement(sql);
			}else {
				
				sql = "select tid , t_user , t_device , t_datetime_min , t_datetime_max , t_time_min , t_time_max\n" + 
						",t_select_st , t_status , t_enable , t_set_datetime , status_device , concat(user_detail_fname , ' ' , user_detail_lname) as fullname , detail_name from timer_setting\n" + 
						"inner join user_detail on user_detail_id = t_user \n" + 
						"inner join devices_detail on t_device = detail_device\n" + 
						"inner join devices on t_device = device_id\n" + 
						"inner join things on things_id = device_esp\n" + 
						"inner join detail_area on dear_id = things_area\n" + 
						"inner join area_administrator on area = dear_area\n" + 
						"where t_status = 1  and t_enable = 1 and area_admin_user = ?";
				preparedStatement = con.prepareStatement(sql);
				preparedStatement.setInt(1, idUser);
			}
			
			ResultSet resultSet = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(resultSet);
			if(count >= 1) {
				resultSet.beforeFirst();
				
				
				List<ReqTimer> list = new ArrayList<ReqTimer>();
				while(resultSet.next()) {
					ReqTimer req = new ReqTimer();
					
					req.settID(resultSet.getInt("tid"));
					req.settUser(resultSet.getInt("t_user"));
					req.settDevice(resultSet.getInt("t_device"));
					req.settDatetimeMin(resultSet.getString("t_datetime_min"));
					req.settDatetimeMax(resultSet.getString("t_datetime_max"));
					req.settTimeMin(resultSet.getString("t_time_min"));
					req.settTimeMax(resultSet.getString("t_time_max"));
					req.settSelect(resultSet.getBoolean("t_select_st"));
					req.settStatus(resultSet.getBoolean("t_status"));
					req.settEnable(resultSet.getBoolean("t_enable"));
					req.settSetDatetime(resultSet.getString("t_set_datetime"));
					req.setStatusDevice(resultSet.getBoolean("status_device"));
					req.setFullName(resultSet.getString("fullname"));
					req.setDeviceName(resultSet.getString("detail_name"));
					
					list.add(req);
				}
				
				reqTimer.setCodeStatus(200);
				reqTimer.setNameStatus("Success");
				reqTimer.setData(list);
			}else {
				reqTimer.setCodeStatus(204);
				reqTimer.setNameStatus("No Data");
			}
					
			
		} catch (SQLException e) {
			reqTimer.setCodeStatus(500);
			reqTimer.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(reqTimer);
		}
	}

	
	
}
