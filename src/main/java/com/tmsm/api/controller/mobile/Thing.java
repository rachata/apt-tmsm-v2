package com.tmsm.api.controller.mobile;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.mobile.Device;
import com.tmsm.api.model.mobile.ResThingMap;
import com.tmsm.api.model.mobile.ThingMap;
import com.tmsm.api.model.mobile.Things;
import com.tmsm.api.utility.Database;



@RestController
@RequestMapping("/mobile")
public class Thing {

	@RequestMapping(value = "/things", method = RequestMethod.GET)
	@CrossOrigin
	public ResponseEntity<?> getThing() {

		ResultSet result = null;
		PreparedStatement preparedStmt = null;
		
		Connection con = null;
		
		
		

		String sql = "select things_id , things_name , things_ip , macaddress , things_type , type_name  , dear_name ,  area_of_device_name , dear_image , things_offsetx , things_offsety \n" + 
				", things_area from things \n" + 
				"inner join things_type on things_type = type_id\n" + 
				"left join detail_area on detail_area.dear_id = things_area\n" + 
				"left join area_of_devices on area_of_device_id = dear_area";
		Things things = new Things();
		try {

			con = Database.connectDatabase();
			
			System.out.print("con " + con.toString());
			preparedStmt = con.prepareStatement(sql);
			result = preparedStmt.executeQuery();

			java.util.List<com.tmsm.api.model.mobile.Thing> list = new ArrayList<com.tmsm.api.model.mobile.Thing>();
			
			
			while (result.next()) {
				String sqlDevice = "select * from devices inner join devices_detail on device_id = detail_device\n" + 
														" inner join devices_priority on priority_id = device_priority\n" + 
														" inner join devices_visibility on visibility_id = device_visibility \n" + 
														" inner join detail_area on dear_id = devices_detail.detail_area\n" + 
														" inner join area_of_devices on area_of_device_id = dear_area  where device_esp = ? and device_visibility = 1";
				

				preparedStmt = con.prepareStatement(sqlDevice);
				preparedStmt.setLong(1, result.getLong(1));
			
				ResultSet res = preparedStmt.executeQuery();				

				java.util.List<Device> listDevice = new ArrayList<Device>();
				
				while(res.next()) {
					
					Device device = new Device();
					
					device.setDeviceID(res.getInt(1));
					device.setDeviceThing(res.getInt(2));
					device.setDeviceCH(res.getInt(3));
					device.setDevicePriority(res.getInt(4));
					device.setDeviceVisibility(res.getInt(5));
					device.setDetailID(res.getInt(6));
					device.setDetailName(res.getString(7));
					device.setDetailDevice(res.getInt(8));
					device.setDetailPrivilege(res.getString(9));
					device.setDetailArea(res.getInt(10));					
					device.setNamePriority(res.getString(12));
					device.setNameVisibility(res.getString(14));
					device.setNameArea(res.getString(16));
					device.setPositionID(res.getInt(19));
					device.setPositionName(res.getString(20));
					device.setImage(result.getString(9));
					

					device.setOffsetX(result.getDouble(10));
					device.setOffsetY(result.getDouble(11));
					
					listDevice.add(device);
					
					System.out.print(listDevice.toString());
				}
				
				
				com.tmsm.api.model.mobile.Thing thing = new com.tmsm.api.model.mobile.Thing();
				thing.setThingID(result.getLong(1));
				thing.setThingIP(result.getString(3));
				thing.setThingMAC(result.getString(4));
				thing.setThingName(result.getString(2));
				thing.setThingType(result.getLong(5));
				thing.setThingTypeName(result.getString(6));
				thing.setThingArea(result.getString(7));
				thing.setThingRootArea(result.getString(8));
				
				thing.setIdArea(result.getInt("things_area"));

				
				thing.setDevices(listDevice);
				
				list.add(thing);				

			}
			things.setThings(list);
			things.setCodeStatus(200);
			things.setNameStatus("Success");
			
			System.out.println("Size " + list.size());

			Database.closeConnection(con);
			return ResponseEntity.ok().body(things);
		} catch (Exception ex) {
			
			things.setCodeStatus(400);
			
			System.out.print(ex);
			things.setNameStatus(ex.getMessage());
			Database.closeConnection(con);
			
			return ResponseEntity.ok().body(things);
			
			
		}


	}

	
	@SuppressWarnings("finally")
	@RequestMapping(value = "/things/{idUser}", method = RequestMethod.GET)
	@CrossOrigin
	public ResponseEntity<ResThingMap> getThingMap(@PathVariable("idUser") int  idUser){
		
		Connection con = null;
		ResultSet resultSet;
		PreparedStatement preparedStatement;
		
		ResThingMap resThingMap  = new ResThingMap();
		try {
			
			con = Database.connectDatabase();
			String sql = "select  things_id ,  things_name , things_offsetx , things_offsety ,  dear_image  , type_name , area_of_device_name , dear_name , area_latitude , area_longitudes , detail_area\n" + 
					" from (things \n" + 
					"inner join devices  on device_esp = things_id\n" + 
					"inner join things_type on  things_type = type_id\n" + 
					"inner join user_select_devices on device_id = select_device\n" + 
					"inner join devices_detail on device_id = detail_device\n" + 
					"inner join detail_area on dear_id = devices_detail.detail_area\n" + 
					"inner join area_of_devices on area_of_device_id = dear_area )\n" + 
					"where select_usr = ?";
			
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, idUser);
			resultSet = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(resultSet);
			if(count >= 1) {
				resultSet.beforeFirst();
				
				java.util.List<Integer> temp = new ArrayList<Integer>();
				
				java.util.List<ThingMap> data = new  ArrayList<ThingMap>();
				while(resultSet.next()) {
					ThingMap map = new ThingMap();
					if(!temp.contains(resultSet.getInt("things_id"))) {
						temp.add(resultSet.getInt("things_id"));
					}else {
						continue;
					}
					
					
					
					map.setImage(resultSet.getString("dear_image"));
					
					map.setRootArea(resultSet.getString("area_of_device_name"));
					map.setArea(resultSet.getString("dear_name"));
					map.setLat(resultSet.getDouble("area_latitude"));
					map.setLng(resultSet.getDouble("area_longitudes"));
					
					data.add(map);
				}
				resThingMap.setCodeStatus(200);
				resThingMap.setNameStatus("Success");
				resThingMap.setData(data);
			}else {
				
				resThingMap.setCodeStatus(204);
				resThingMap.setNameStatus("Unsuccess : No Data");
			}
		} catch (SQLException e) {
			resThingMap.setCodeStatus(500);
			resThingMap.setNameStatus("Unsuccess : " + e.getErrorCode() + " " + e.getMessage());
		}finally {
			
			Database.closeConnection(con);
			return ResponseEntity.ok().body(resThingMap);
		}
	}
}
