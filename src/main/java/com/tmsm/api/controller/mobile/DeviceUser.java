package com.tmsm.api.controller.mobile;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.naming.spi.DirStateFactory.Result;
import javax.xml.crypto.Data;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.Status;
import com.tmsm.api.model.mobile.DeviceOfUser;
import com.tmsm.api.model.mobile.Privilege;
import com.tmsm.api.model.mobile.ReqDeviceOfUser;
import com.tmsm.api.model.mobile.ResPrivilegeDeviceOfUser;
import com.tmsm.api.model.mobile.RootDeviceOfUser;
import com.tmsm.api.model.mobile.RootPrivilege;
import com.tmsm.api.utility.Database;

@CrossOrigin
@RestController
@RequestMapping("/mobile")
public class DeviceUser {
	
	@CrossOrigin
	@RequestMapping(value = "/device/io/{id}" , method = RequestMethod.GET)
	public ResponseEntity<RootDeviceOfUser> getDevicesIOUser(@PathVariable("id") int userID){
		
		
		
		Connection con = Database.connectDatabase();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		
		RootDeviceOfUser  rootDeviceOfUser = new RootDeviceOfUser();
		
		try {
			
			String sql = "select     things_id ,   device_id , device_ch , permit_name , permit_status , \n" + 
					"permit_grant_status  ,permit_timeMin , \n" + 
					"permit_timeMax , permit_privilage   , detail_privilege   \n" + 
					"from permit_user\n" + 
					"inner join devices on permit_device = device_id\n" + 
					"inner join devices_detail on device_id= detail_device\n" + 
					"inner join things on device_esp = things_id\n" + 
					"where ( things_type = 1 or things_type = 2 or things_type = 3 ) and permit_user = ? and permit_status = 1 and enabled = 1";
			
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1 , userID);
			
			resultSet = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(resultSet);
			
			resultSet.beforeFirst();
			if(count >= 1) {
				
				List<DeviceOfUser> data = new ArrayList<DeviceOfUser>();
				while(resultSet.next()) {
					DeviceOfUser deviceOfUser = new DeviceOfUser();
					
					String userPrivilage = resultSet.getString("permit_privilage");
					String devicePrivilage = resultSet.getString("detail_privilege");
					
					int statusPermit = resultSet.getInt("permit_status");
					int grantStatus = resultSet.getInt("permit_grant_status");
					
					String dateMin = resultSet.getString("permit_timemin");
					String dateMax = resultSet.getString("permit_timemax");
					
					int statusDevice = 0;
					if(statusPermit == 1) {
						// อนุญาติ
						if(grantStatus == 1) {
							// ถาวร
							
							char[] arrUser = userPrivilage.toCharArray();
							char[] arrDevice = devicePrivilage.toCharArray();
							
							if(arrDevice[0] == '1') {
								// อ่านได้
								if(arrUser[0] == '1') {
									
									if(arrDevice[2] == '1') {
										
										if(arrUser[2] == '1') {
											// control 
											statusDevice = 2;
										}else {
											statusDevice = 1;
											// user read only
										}
									}else {
										statusDevice = 1;
										// device read only
										
									}
									
									
								}else {
									//user อ่านไม่ได้
									
									 continue;
									
								}
							}else {
								// อ่านไม่ได้
								
								continue;
							}
							
						}else {
							// ชั่วคราว
							
				
							
							SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
							String dateString = format.format( new Date() );
							
						
						    Date date1=new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(dateMin);  
						    Date date2=new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(dateMax);  
						    Date l = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(dateString);  
						    
							if(date2.after(l) && date1.before(l)) {
							    // In between
								System.out.println("In between");
								char[] arrUser = userPrivilage.toCharArray();
								char[] arrDevice = devicePrivilage.toCharArray();
								
								if(arrDevice[0] == '1') {
									// อ่านได้
									if(arrUser[0] == '1') {
										
										if(arrDevice[2] == '1') {
											
											if(arrUser[2] == '1') {
												// control 
												statusDevice = 2;
											}else {
												statusDevice = 1;
												// user read only
											}
										}else {
											statusDevice = 1;
											// device read only
											
										}
										
										
									}else {
										//user อ่านไม่ได้
										
										 continue;
										
									}
								}else {
									// อ่านไม่ได้
									
									continue;
								}
								
								
								
							}else {
								continue;
							}
							
						}
						
					}else {
						
						continue;
							// ไม่อนุญาติ	
					}
					deviceOfUser.setDefDevice(resultSet.getInt("device_id"));
					deviceOfUser.setDeviceCH(resultSet.getInt("device_ch"));
					deviceOfUser.setDefName(resultSet.getString("permit_name"));
					deviceOfUser.setPermitStatus(resultSet.getInt("permit_status"));
					deviceOfUser.setPermitGrantStatus(resultSet.getInt("permit_grant_status"));
					deviceOfUser.setPermitTimemin(resultSet.getString("permit_timemin"));
					deviceOfUser.setPermitTimemax(resultSet.getString("permit_timemax"));
					deviceOfUser.setPermitPrivilage(resultSet.getString("permit_privilage"));
					deviceOfUser.setDetailPrivilege(resultSet.getString("detail_privilege"));
					deviceOfUser.setThingsID(resultSet.getInt("things_id"));
					deviceOfUser.setStatusIO(statusDevice);
					
					data.add(deviceOfUser);
					
				}
				
				rootDeviceOfUser.setCodeStatus(200);
				rootDeviceOfUser.setNameStatus("Success");
				rootDeviceOfUser.setDevices(data);
				
				
			}else {
				rootDeviceOfUser.setCodeStatus(204);
				rootDeviceOfUser.setNameStatus("No device");
				
			}
			
		} catch (SQLException e) {
			rootDeviceOfUser.setCodeStatus(500);
			rootDeviceOfUser.setNameStatus("ERROR " + e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(rootDeviceOfUser);
		}
	}
	
	
	@CrossOrigin
	@RequestMapping(value = "/device/io/{id}/{group}" , method = RequestMethod.GET)
	public ResponseEntity<RootDeviceOfUser> getDevicesIOUserGroup(@PathVariable("id") int userID , @PathVariable("group") int group){
		
		
		
		Connection con = Database.connectDatabase();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		
		RootDeviceOfUser  rootDeviceOfUser = new RootDeviceOfUser();
		
		try {
			
			String sql = "select     things_id ,   device_id , device_ch , permit_name , permit_status ,\n" + 
					"					permit_grant_status  ,permit_timeMin ,\n" + 
					"					permit_timeMax , permit_privilage   , detail_privilege  \n" + 
					"					from permit_user\n" + 
					"					inner join devices on permit_device = device_id\n" + 
					"					inner join devices_detail on device_id= detail_device\n" + 
					"					inner join things on device_esp = things_id\n" + 
					"                    inner join group_device on gd_device = permit_device\n" + 
					"					where ( things_type = 1 or things_type = 2 or things_type = 3 ) and permit_user = ? and permit_status = 1 and enabled = 1\n" + 
					"                    and gd_group = ?";
			
			preparedStatement = con.prepareStatement(sql);

			preparedStatement.setInt(1 , userID);
			preparedStatement.setInt(2 , group);
			
			resultSet = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(resultSet);
			
			resultSet.beforeFirst();
			if(count >= 1) {
				
				List<DeviceOfUser> data = new ArrayList<DeviceOfUser>();
				while(resultSet.next()) {
					DeviceOfUser deviceOfUser = new DeviceOfUser();
					
					String userPrivilage = resultSet.getString("permit_privilage");
					String devicePrivilage = resultSet.getString("detail_privilege");
					
					int statusPermit = resultSet.getInt("permit_status");
					int grantStatus = resultSet.getInt("permit_grant_status");
					
					String dateMin = resultSet.getString("permit_timemin");
					String dateMax = resultSet.getString("permit_timemax");
					
					int statusDevice = 0;
					if(statusPermit == 1) {
						// อนุญาติ
						if(grantStatus == 1) {
							// ถาวร
							
							char[] arrUser = userPrivilage.toCharArray();
							char[] arrDevice = devicePrivilage.toCharArray();
							
							if(arrDevice[0] == '1') {
								// อ่านได้
								if(arrUser[0] == '1') {
									
									if(arrDevice[2] == '1') {
										
										if(arrUser[2] == '1') {
											// control 
											statusDevice = 2;
										}else {
											statusDevice = 1;
											// user read only
										}
									}else {
										statusDevice = 1;
										// device read only
										
									}
									
									
								}else {
									//user อ่านไม่ได้
									
									 continue;
									
								}
							}else {
								// อ่านไม่ได้
								
								continue;
							}
							
						}else {
							// ชั่วคราว
							
				
							
							SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
							String dateString = format.format( new Date() );
							
						
						    Date date1=new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(dateMin);  
						    Date date2=new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(dateMax);  
						    Date l = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(dateString);  
						    
							if(date2.after(l) && date1.before(l)) {
							    // In between
								System.out.println("In between");
								char[] arrUser = userPrivilage.toCharArray();
								char[] arrDevice = devicePrivilage.toCharArray();
								
								if(arrDevice[0] == '1') {
									// อ่านได้
									if(arrUser[0] == '1') {
										
										if(arrDevice[2] == '1') {
											
											if(arrUser[2] == '1') {
												// control 
												statusDevice = 2;
											}else {
												statusDevice = 1;
												// user read only
											}
										}else {
											statusDevice = 1;
											// device read only
											
										}
										
										
									}else {
										//user อ่านไม่ได้
										
										 continue;
										
									}
								}else {
									// อ่านไม่ได้
									
									continue;
								}
								
								
								
							}else {
								continue;
							}
							
						}
						
					}else {
						
						continue;
							// ไม่อนุญาติ	
					}
					deviceOfUser.setDefDevice(resultSet.getInt("device_id"));
					deviceOfUser.setDeviceCH(resultSet.getInt("device_ch"));
					deviceOfUser.setDefName(resultSet.getString("permit_name"));
					deviceOfUser.setPermitStatus(resultSet.getInt("permit_status"));
					deviceOfUser.setPermitGrantStatus(resultSet.getInt("permit_grant_status"));
					deviceOfUser.setPermitTimemin(resultSet.getString("permit_timemin"));
					deviceOfUser.setPermitTimemax(resultSet.getString("permit_timemax"));
					deviceOfUser.setPermitPrivilage(resultSet.getString("permit_privilage"));
					deviceOfUser.setDetailPrivilege(resultSet.getString("detail_privilege"));
					deviceOfUser.setThingsID(resultSet.getInt("things_id"));
					deviceOfUser.setStatusIO(statusDevice);
					
					data.add(deviceOfUser);
					
				}
				
				rootDeviceOfUser.setCodeStatus(200);
				rootDeviceOfUser.setNameStatus("Success");
				rootDeviceOfUser.setDevices(data);
				
				
			}else {
				rootDeviceOfUser.setCodeStatus(204);
				rootDeviceOfUser.setNameStatus("No device");
				
			}
			
		} catch (SQLException e) {
			rootDeviceOfUser.setCodeStatus(500);
			rootDeviceOfUser.setNameStatus("ERROR " + e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(rootDeviceOfUser);
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/privilege/{id}" , method = RequestMethod.GET )
	public ResponseEntity<RootPrivilege> getPrivilege(@PathVariable("id") int userID){
		
		
		RootPrivilege rootPrivilege = new RootPrivilege();
		
		Connection con = Database.connectDatabase();
		PreparedStatement  preparedStatement = null;
		
		try {
			ResultSet rs = null;
			String sql ="select * from permit_user where permit_user = ?";
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1 ,userID );
			rs = preparedStatement.executeQuery();
			
			int count =Database.countResultSet(rs);
			
			if(count >= 1) {
				
				List<Privilege> list = new ArrayList<Privilege>();
				rs.beforeFirst();
				while(rs.next()) {
					
					Privilege privilege = new Privilege();
					
					privilege.setPermitID(rs.getInt(1));
					privilege.setPermitDevice(rs.getInt(3));
					privilege.setPermitStatus(rs.getInt(4));
					privilege.setPermitPrivilege(rs.getString(5));
					privilege.setPermitGrant(rs.getInt(6));
					privilege.setTimeMin(rs.getString(7));
					privilege.setTimeMax(rs.getString(8));
					
					list.add(privilege);
					
				}
				
				rootPrivilege.setCodeStatus(200);
				rootPrivilege.setNameStatus("Success");
				rootPrivilege.setPrivileges(list);
				
			}else {
				rootPrivilege.setCodeStatus(204);
				rootPrivilege.setNameStatus("No Data");
				
			}
			
		} catch (SQLException e) {
			rootPrivilege.setCodeStatus(500);
			rootPrivilege.setNameStatus("ERROR " + e.getMessage());
			
			
		}finally {
			
			Database.closeConnection(con);
			return ResponseEntity.ok().body(rootPrivilege);
		}
	}
	
	
	
	@RequestMapping(value = "/device" , method = RequestMethod.PUT , consumes = MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE )
	@CrossOrigin
	public ResponseEntity<Status> renameDeviceName(@RequestBody ReqDeviceOfUser deviceOfUser){
		
		
		Connection con = null;
		PreparedStatement preparedStatement= null;
		
		Status status = new Status();
		try {
			con = Database.connectDatabase();
			String sql ="update permit_user set permit_name = ? where permit_id = ?";
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setString(1, deviceOfUser.getDefName());
			preparedStatement.setInt(2, deviceOfUser.getDefID());
			
			 boolean statusSQL = preparedStatement.execute();
			 
			 if(!statusSQL ) {
					status.setCodeStatus(200);
					status.setNameStatus("Success");
			 }else {
					status.setCodeStatus(204);
					status.setNameStatus("Unsuccess");
			 }
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus("Unsuccess : " + e.getErrorCode() + " " + e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}



	@SuppressWarnings("finally")
	@RequestMapping(value = "/device/{idUser}/{idDevice}/{status}/{id}" , 
					method = RequestMethod.DELETE , 
					produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin
	public ResponseEntity<Status> deleteDeviceOfUser(@PathVariable("idUser") int idUser , 
														@PathVariable("idDevice") int idDevice,
														@PathVariable("status") int statuss,
														@PathVariable("id") int id){
		
		Status status = new Status();
		Connection con = null;
		PreparedStatement preparedStatement; 
		
		try {
			
			con = Database.connectDatabase();
			if(statuss == 0) {
				String  delete = "delete from permit_user where permit_id = ?";
				preparedStatement = con.prepareStatement(delete);
				preparedStatement.setInt(1, id);
				preparedStatement.execute();
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
			
			
			String sqlDeletePermitUser = "update  permit_user  set enabled = 0 where  permit_user = ? and permit_device = ?";
	
			
			
	
			
			
			
			preparedStatement = con.prepareStatement(sqlDeletePermitUser);
			preparedStatement.setInt(1, idUser);
			preparedStatement.setInt(2, idDevice);
			preparedStatement.execute();
			
			

			String sql = "select gd_id from group_device\n" + 
					"			inner join group_device_of_user on gd_group = g_id\n" + 
					"				where g_usr = ? and gd_device = ?";
			
			
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, idUser);
			preparedStatement.setInt(2, idDevice);
			
			
			ResultSet res = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(res);
			
			if(count >= 1) {
				res.beforeFirst();
				
				
				while(res.next()) {
					sql = "delete from group_device where gd_id = ?";
					preparedStatement = con.prepareStatement(sql);
					
					preparedStatement.setInt(1, res.getInt("gd_id"));
					
					preparedStatement.execute();
				}
				
				
			}
			status.setCodeStatus(200);
			status.setNameStatus("Success");
			}
	
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus("Unsuccess : " + e.getErrorCode() + " " +e.getMessage());
			con.rollback();
		}finally {
			
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}

	@CrossOrigin
	@RequestMapping(value = "/device/sensor/{id}/{group}" , method = RequestMethod.GET)
	public ResponseEntity<RootDeviceOfUser> getSensor(@PathVariable("id") int userID, @PathVariable("group") int group) {
		

		
		Connection con = Database.connectDatabase();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		
		RootDeviceOfUser  rootDeviceOfUser = new RootDeviceOfUser();
		
		try {
			
			String sql = "select     things_id ,   device_id , device_ch , permit_name , permit_status ,\n" + 
					"permit_grant_status  ,permit_timeMin ,\n" + 
					"permit_timeMax , permit_privilage   , detail_privilege  , unit_id , unit_name \n" + 
					"from permit_user\n" + 
					"inner join devices on permit_device = device_id\n" + 
					"inner join devices_detail on device_id= detail_device\n" + 
					"inner join things on device_esp = things_id\n" + 
					"inner join group_device on gd_device = permit_device\n" + 
					"inner join sensor_unit on sensor_unit_device = device_id\n" + 
					"inner join unit on sensor_unit  = unit_id\n" + 
					"where  things_type = 4 and permit_user = ? and permit_status = 1 and enabled = 1 and gd_group = ?";
			
			preparedStatement = con.prepareStatement(sql);

			preparedStatement.setInt(1 , userID);
			preparedStatement.setInt(2 , group);
			
			resultSet = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(resultSet);
			
			resultSet.beforeFirst();
			if(count >= 1) {
				
				List<DeviceOfUser> data = new ArrayList<DeviceOfUser>();
				while(resultSet.next()) {
					DeviceOfUser deviceOfUser = new DeviceOfUser();
					
					String userPrivilage = resultSet.getString("permit_privilage");
					String devicePrivilage = resultSet.getString("detail_privilege");
					
					int statusPermit = resultSet.getInt("permit_status");
					int grantStatus = resultSet.getInt("permit_grant_status");
					
					String dateMin = resultSet.getString("permit_timemin");
					String dateMax = resultSet.getString("permit_timemax");
					
					int statusDevice = 0;
					if(statusPermit == 1) {
						// อนุญาติ
						if(grantStatus == 1) {
							// ถาวร
							
							char[] arrUser = userPrivilage.toCharArray();
							char[] arrDevice = devicePrivilage.toCharArray();
							
							if(arrDevice[0] == '1') {
								// อ่านได้
								if(arrUser[0] == '1') {
									

									deviceOfUser.setDefDevice(resultSet.getInt("device_id"));
									deviceOfUser.setDeviceCH(resultSet.getInt("device_ch"));
									deviceOfUser.setDefName(resultSet.getString("permit_name"));
									deviceOfUser.setPermitStatus(resultSet.getInt("permit_status"));
									deviceOfUser.setPermitGrantStatus(resultSet.getInt("permit_grant_status"));
									deviceOfUser.setPermitTimemin(resultSet.getString("permit_timemin"));
									deviceOfUser.setPermitTimemax(resultSet.getString("permit_timemax"));
									deviceOfUser.setPermitPrivilage(resultSet.getString("permit_privilage"));
									deviceOfUser.setDetailPrivilege(resultSet.getString("detail_privilege"));
									deviceOfUser.setThingsID(resultSet.getInt("things_id"));
									deviceOfUser.setStatusIO(statusDevice);
									deviceOfUser.setUnit(resultSet.getInt("unit_id"));
									deviceOfUser.setUnitName(resultSet.getString("unit_name"));
									
								}else {
									//user อ่านไม่ได้
									
									 continue;
									
								}
							}else {
								// อ่านไม่ได้
								
								continue;
							}
							
						}
						
					}else {
						
						continue;
							// ไม่อนุญาติ	
					}
					
					
					data.add(deviceOfUser);
					
				}
				
				rootDeviceOfUser.setCodeStatus(200);
				rootDeviceOfUser.setNameStatus("Success");
				rootDeviceOfUser.setDevices(data);
				
				
			}else {
				rootDeviceOfUser.setCodeStatus(204);
				rootDeviceOfUser.setNameStatus("No device");
				
			}
			
		} catch (SQLException e) {
			rootDeviceOfUser.setCodeStatus(500);
			rootDeviceOfUser.setNameStatus("ERROR " + e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(rootDeviceOfUser);
		}
		
	}


	@CrossOrigin
	@RequestMapping(value = "/device/sensor/{id}" , method = RequestMethod.GET)
	public ResponseEntity<RootDeviceOfUser> getSensorAll(@PathVariable("id") int userID) {
		

		
		Connection con = Database.connectDatabase();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		
		RootDeviceOfUser  rootDeviceOfUser = new RootDeviceOfUser();
		
		try {
			
			String sql = "select     things_id ,   device_id , device_ch , permit_name , permit_status ,\n" + 
					"permit_grant_status  ,permit_timeMin ,\n" + 
					"permit_timeMax , permit_privilage   , detail_privilege  , unit_id , unit_name \n" + 
					"from permit_user\n" + 
					"inner join devices on permit_device = device_id\n" + 
					"inner join devices_detail on device_id= detail_device\n" + 
					"inner join things on device_esp = things_id\n" + 
					"inner join sensor_unit on sensor_unit_device = device_id\n" + 
					"inner join unit on sensor_unit  = unit_id\n" + 
					"where  things_type = 4 and permit_user = ? and permit_status = 1 and enabled = 1";
			
			preparedStatement = con.prepareStatement(sql);

			preparedStatement.setInt(1 , userID);

			
			resultSet = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(resultSet);
			
			resultSet.beforeFirst();
			if(count >= 1) {
				
				List<DeviceOfUser> data = new ArrayList<DeviceOfUser>();
				while(resultSet.next()) {
					DeviceOfUser deviceOfUser = new DeviceOfUser();
					
					String userPrivilage = resultSet.getString("permit_privilage");
					String devicePrivilage = resultSet.getString("detail_privilege");
					
					int statusPermit = resultSet.getInt("permit_status");
					int grantStatus = resultSet.getInt("permit_grant_status");
					
					String dateMin = resultSet.getString("permit_timemin");
					String dateMax = resultSet.getString("permit_timemax");
					
					int statusDevice = 0;
					if(statusPermit == 1) {
						// อนุญาติ
						if(grantStatus == 1) {
							// ถาวร
							
							char[] arrUser = userPrivilage.toCharArray();
							char[] arrDevice = devicePrivilage.toCharArray();
							
							if(arrDevice[0] == '1') {
								// อ่านได้
								if(arrUser[0] == '1') {
									

									deviceOfUser.setDefDevice(resultSet.getInt("device_id"));
									deviceOfUser.setDeviceCH(resultSet.getInt("device_ch"));
									deviceOfUser.setDefName(resultSet.getString("permit_name"));
									deviceOfUser.setPermitStatus(resultSet.getInt("permit_status"));
									deviceOfUser.setPermitGrantStatus(resultSet.getInt("permit_grant_status"));
									deviceOfUser.setPermitTimemin(resultSet.getString("permit_timemin"));
									deviceOfUser.setPermitTimemax(resultSet.getString("permit_timemax"));
									deviceOfUser.setPermitPrivilage(resultSet.getString("permit_privilage"));
									deviceOfUser.setDetailPrivilege(resultSet.getString("detail_privilege"));
									deviceOfUser.setThingsID(resultSet.getInt("things_id"));
									deviceOfUser.setStatusIO(statusDevice);
									deviceOfUser.setUnit(resultSet.getInt("unit_id"));
									deviceOfUser.setUnitName(resultSet.getString("unit_name"));
									
								}else {
									//user อ่านไม่ได้
									
									 continue;
									
								}
							}else {
								// อ่านไม่ได้
								
								continue;
							}
							
						}
						
					}else {
						
						continue;
							// ไม่อนุญาติ	
					}
					
					
					data.add(deviceOfUser);
					
				}
				
				rootDeviceOfUser.setCodeStatus(200);
				rootDeviceOfUser.setNameStatus("Success");
				rootDeviceOfUser.setDevices(data);
				
				
			}else {
				rootDeviceOfUser.setCodeStatus(204);
				rootDeviceOfUser.setNameStatus("No device");
				
			}
			
		} catch (SQLException e) {
			rootDeviceOfUser.setCodeStatus(500);
			rootDeviceOfUser.setNameStatus("ERROR " + e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(rootDeviceOfUser);
		}
		
	}

}
