package com.tmsm.api.controller.mobile;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.tmsm.api.model.Status;
import com.tmsm.api.model.device.ReqPermitPrivilage;
import com.tmsm.api.model.mobile.PermitDevice;
import com.tmsm.api.model.mobile.PermitDeviceOfUser;
import com.tmsm.api.model.mobile.PermitRequest;
import com.tmsm.api.model.mobile.ReqDevice;
import com.tmsm.api.model.mobile.ReqPrivilege;

import com.tmsm.api.model.mobile.ResDevice;
import com.tmsm.api.model.mobile.ResPrivilegeDeviceOfUser;
import com.tmsm.api.model.mobile.ResSelectDevice;
import com.tmsm.api.model.mobile.RootPermitDeviceOfUser;
import com.tmsm.api.model.mobile.RootPermitRequest;
import com.tmsm.api.model.web.PermitRes;
import com.tmsm.api.model.web.PermitResRoot;
import com.tmsm.api.utility.Database;

@RestController
@RequestMapping("/reqdevices")
public class RequestDevice {

	
	// **
	@SuppressWarnings("finally")
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST,
					consumes = MediaType.APPLICATION_JSON_VALUE,
					produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Status> req(@RequestBody ReqPrivilege reqPrivilege){
		
		Status status = new Status();
		String sql = "insert into permit_user (permit_user , permit_device , permit_status , permit_privilage , permit_grant_status , permit_name) \n" + 
				"value(? , ? , ? , ? , ? , ?) ";
		Connection con = Database.connectDatabase();
		PreparedStatement preparedStatement;
		
		try {
			
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, reqPrivilege.getUserID());
			preparedStatement.setInt(2, reqPrivilege.getDeviceID());
			preparedStatement.setInt(3, 0);
			preparedStatement.setString(4, reqPrivilege.getPrivilege());
			preparedStatement.setInt(5 ,2);
			preparedStatement.setString(6,reqPrivilege.getNameDevice());
			
			boolean statusSQL = preparedStatement.execute();
			
			if(!statusSQL) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");
				
			}
			
		} catch (SQLException e) {			
			status.setCodeStatus(500);
			status.setNameStatus("ERROR " + e.getMessage());			
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}

	}




	@RequestMapping(value = "/permitofuser/{id}" , method = RequestMethod.GET)
	@CrossOrigin

	public ResponseEntity<RootPermitDeviceOfUser> permitOfUser(@PathVariable(name = "id") int reqWithUser){
		
		ResultSet result = null;
		PreparedStatement preparedStatement = null;
		Connection con = Database.connectDatabase();
		
		
		
		RootPermitDeviceOfUser rootPermitDeviceOfUser = new RootPermitDeviceOfUser();
		
		try {
			String sql = "select permit_id , permit_device , permit_name , device_esp , device_ch  , detail_privilege , permit_status  , detail_area , permit_privilage , things_type from permit_user\n" + 
					"inner join devices on device_id = permit_device\n" + 
					"inner join devices_detail on detail_device = permit_device inner join things on device_esp = things_id \n"+
					"where   permit_user = ? and enabled = 1";
			
			preparedStatement = con.prepareStatement(sql);
			

			preparedStatement.setInt(1, reqWithUser);
			
			result = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(result);
			result.beforeFirst();
			
			List<PermitDeviceOfUser> list = new ArrayList<PermitDeviceOfUser>();
			if(count != 0) {
				// data on
				while(result.next()) {
					PermitDeviceOfUser permitDeviceOfUser = new PermitDeviceOfUser();
					permitDeviceOfUser.setDefID(result.getInt("permit_id"));
					permitDeviceOfUser.setDefDevice(result.getInt("permit_device"));
					permitDeviceOfUser.setDefName(result.getString("permit_name"));
					permitDeviceOfUser.setDeviceESP(result.getInt("device_esp"));
					permitDeviceOfUser.setDeviceCH(result.getInt("device_ch"));
					permitDeviceOfUser.setDetailPrivilege(result.getString("detail_privilege"));
					permitDeviceOfUser.setPermitStatus(result.getInt("permit_status"));
					permitDeviceOfUser.setAreaID(result.getInt("detail_area"));
					permitDeviceOfUser.setPermitPrivilage(result.getString("permit_privilage"));
					permitDeviceOfUser.setType(result.getInt("things_type"));
					
					list.add(permitDeviceOfUser);
					
				}
				rootPermitDeviceOfUser.setData(list);
				rootPermitDeviceOfUser.setCodeStatus(200);
				rootPermitDeviceOfUser.setNameStatus("Scuuess");
				
				
			}else {
				// no data
				
				rootPermitDeviceOfUser.setCodeStatus(204);
				rootPermitDeviceOfUser.setNameStatus("No Data");
				
			}
			
		} catch (SQLException e) {
			// TODO: handle exception
			
			rootPermitDeviceOfUser.setCodeStatus(500);
			rootPermitDeviceOfUser.setNameStatus("Error " + e.getMessage());
			
		}finally {
			return ResponseEntity.ok().body(rootPermitDeviceOfUser);
		}
				
	}


	@RequestMapping(value = "/reqpermit" , 
					method= RequestMethod.POST)
	@CrossOrigin
	public ResponseEntity<Status> reqPrivilege(@RequestBody ReqPrivilege reqPrivilege){
		
		
		
		ResultSet result = null;
		PreparedStatement preparedStatement = null;
		Connection con = Database.connectDatabase();
		
		Status status = new Status();
		try {
			
			
			
			String sqlCheck ="select * from permit_user\n" + 
					"where permit_status = 0 and enabled = 1 and permit_user = ? and permit_device = ?";
			
		
			preparedStatement = con.prepareStatement(sqlCheck);
			preparedStatement.setInt(1, reqPrivilege.getUserID());
			preparedStatement.setInt(2, reqPrivilege.getDeviceID());
			
			ResultSet res = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(res);
			if(count == 0) {
				String sql = "insert into permit_user (permit_user , permit_device ,permit_status , permit_privilage , permit_grant_status , permit_name) value (? , ? , 0 , ? , 2 , ?)";
				
				preparedStatement = con.prepareStatement(sql);
				preparedStatement.setInt(1, reqPrivilege.getUserID());
				preparedStatement.setInt(2, reqPrivilege.getDeviceID());
				preparedStatement.setString(3, reqPrivilege.getPrivilege());
				preparedStatement.setString(4, reqPrivilege.getNameDevice());
				
				boolean statusQuery = preparedStatement.execute();
				
				if(!statusQuery) {
					status.setCodeStatus(200);
					status.setNameStatus("Success");
				}else {
					status.setCodeStatus(204);
					status.setNameStatus("Unsuccess");
					
				}
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess : Your request already exists.");
			}
			
				

			
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus("ERROR " +  e.getMessage());
			
		}finally {
			return ResponseEntity.ok().body(status);
		}
	}

	
	
	@RequestMapping(value = "/permition" , method = RequestMethod.POST , consumes = MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin
	public ResponseEntity<Status> updateStatus(@RequestBody ReqPermitPrivilage reqPermitPrivilage){
		 
		Status status = new Status();	
		PreparedStatement preparedStatement = null;
		Connection con = Database.connectDatabase();
		
		
		try {
			
			
			con.setAutoCommit(false);
			String sql = "";
			if(reqPermitPrivilage.getPrivilege() != null) {
				
				
				sql = "update permit_user set enabled = 0 where permit_user = ? and permit_device = ? and permit_id != ?";
				preparedStatement = con.prepareStatement(sql);
				preparedStatement.setInt(1, reqPermitPrivilage.getUser());
				preparedStatement.setInt(2, reqPermitPrivilage.getDevice());
				preparedStatement.setInt(3,reqPermitPrivilage.getPermitID());
				
				preparedStatement.execute();
				
				
				
				sql = "update permit_user set permit_status = ? , permit_grant_status = ? , permit_timemin = ? , permit_timemax = ? , permit_privilage = ?  , enabled = ?  where permit_id = ?";
				preparedStatement = con.prepareStatement(sql);
				preparedStatement.setInt(1, reqPermitPrivilage.getPermitStatus());
				preparedStatement.setInt(2, reqPermitPrivilage.getPermitGrantStatus());
				preparedStatement.setString(3 , reqPermitPrivilage.getPermitTimeMin());
				preparedStatement.setString(4, reqPermitPrivilage.getPermitTimeMax());
				preparedStatement.setString(5, reqPermitPrivilage.getPrivilege());
				preparedStatement.setBoolean(6, reqPermitPrivilage.isEnable());
				preparedStatement.setInt(7, reqPermitPrivilage.getPermitID());
				
				preparedStatement.execute();
			}else {
				
				
				sql = "update permit_user set enabled = 0 where permit_user = ? and permit_device = ? and permit_id != ?";
				preparedStatement = con.prepareStatement(sql);
				preparedStatement.setInt(1, reqPermitPrivilage.getUser());
				preparedStatement.setInt(2, reqPermitPrivilage.getDevice());
				preparedStatement.setInt(3,reqPermitPrivilage.getPermitID());
				preparedStatement.execute();
				
				
				
				sql = "update permit_user set permit_status = ? , permit_grant_status = ? , permit_timemin = ? , permit_timemax = ?   where permit_id = ?";
				
				preparedStatement = con.prepareStatement(sql);
				preparedStatement.setInt(1, reqPermitPrivilage.getPermitStatus());
				preparedStatement.setInt(2, reqPermitPrivilage.getPermitGrantStatus());
				preparedStatement.setString(3 , reqPermitPrivilage.getPermitTimeMin());
				preparedStatement.setString(4, reqPermitPrivilage.getPermitTimeMax());
				preparedStatement.setInt(5, reqPermitPrivilage.getPermitID());
				preparedStatement.execute();
				
			}
					
			
			con.commit();
				status.setCodeStatus(200);
				status.setNameStatus("Success");
					
		} catch (SQLException e) {
			
		con.rollback();
			status.setCodeStatus(500);
			status.setNameStatus("Erorr " + e.getMessage());
		
		}finally {
			return ResponseEntity.ok().body(status);
		}
		
	}
	


	@RequestMapping(value = "/permit" , method = RequestMethod.GET)
	@CrossOrigin
	public ResponseEntity<RootPermitRequest> permitRequset(){
		
		
		
		Connection  con = Database.connectDatabase();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		RootPermitRequest rootPermitRequest = new RootPermitRequest();
		try {
			
			String sql = "select permit_id , permit_user , permit_status , permit_privilage , \n" + 
					"permit_grant_status , permit_timemin , permit_timemax , user_detail_fname , user_detail_lname , detail_name , permit_device , things_type from permit_user\n" + 
					"inner join user_detail on user_detail_id = permit_user \n" + 
					"inner join devices_detail on permit_device = detail_device\n" + 
					"inner join devices on permit_device = device_id\n" + 
					"inner join things on things_id = device_esp\n" + 
					"where permit_status = 0  and enabled = 1";
			
			
			preparedStatement = con.prepareStatement(sql);
			resultSet = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(resultSet);
			
			
			resultSet.beforeFirst();
			
			
			List<PermitRequest> list = new  ArrayList<PermitRequest>();
			
			
			while(resultSet.next()) {
				PermitRequest permitRequest = new PermitRequest();
				
				permitRequest.setPermitID(resultSet.getInt(1));
				permitRequest.setPermitUser(resultSet.getInt(2));
				permitRequest.setPermitStatus(resultSet.getInt(3));
				permitRequest.setPermitPrivilage(resultSet.getString(4));
				permitRequest.setPermitGrantStatus(resultSet.getInt(5));
				permitRequest.setPermitTimeMin(resultSet.getString(6));
				permitRequest.setPermitTimeMax(resultSet.getString(7));
				permitRequest.setUserFName(resultSet.getString(8));
				permitRequest.setUserLName(resultSet.getString(9));
				permitRequest.setDeviceName(resultSet.getString(10));
				permitRequest.setPermitDevice(resultSet.getInt(11));
				permitRequest.setType(resultSet.getInt("things_type"));
				
				list.add(permitRequest);
					
			}
			
			rootPermitRequest.setData(list);
			
			rootPermitRequest.setCodeStatus(200);
			rootPermitRequest.setNameStatus("Success");
			System.out.println(count + "  ewf " );
			
			
			
			
		} catch (SQLException e) {
			// TODO: handle exception
			
			rootPermitRequest.setCodeStatus(500);
			rootPermitRequest.setNameStatus("unsuccess");
		}finally {
			return ResponseEntity.ok().body(rootPermitRequest);
		}
		
	}


	
	@RequestMapping(value = "/permit/{idUser}" , method = RequestMethod.GET)
	@CrossOrigin
	public ResponseEntity<RootPermitRequest> permitRequsetOfAdminArea(@PathVariable("idUser") int idUser){
		
		
		
		Connection  con = Database.connectDatabase();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		RootPermitRequest rootPermitRequest = new RootPermitRequest();
		try {
			
			String sql = "select permit_id , permit_user , permit_status , permit_privilage , \n" + 

					"permit_grant_status , permit_timemin , permit_timemax , user_detail_fname , user_detail_lname from permit_user\n" + 
					"inner join user_detail on user_detail_id = permit_user where permit_status = 0 \n" + 

					"permit_grant_status , permit_timemin , permit_timemax , user_detail_fname , user_detail_lname , detail_name , permit_device  , things_type from permit_user\n" + 
					"inner join user_detail on user_detail_id = permit_user \n" + 
					"inner join devices_detail on permit_device = detail_device\n" + 
					"inner join devices on permit_device = device_id\n" + 
					"inner join things on things_id = device_esp\n" + 
					"inner join detail_area on dear_id = things_area\n" + 
					"inner join area_administrator on area = dear_area\n" + 
					"where   permit_status = 0  and enabled = 1 and area_admin_user = ? and permit_user != ? \n" + 

					"";

			
			
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, idUser);
			preparedStatement.setInt(2, idUser);
			resultSet = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(resultSet);
			
			
			resultSet.beforeFirst();
			
			
			List<PermitRequest> list = new  ArrayList<PermitRequest>();
			
			
			while(resultSet.next()) {
				PermitRequest permitRequest = new PermitRequest();
				
				permitRequest.setPermitID(resultSet.getInt(1));
				permitRequest.setPermitUser(resultSet.getInt(2));
				permitRequest.setPermitStatus(resultSet.getInt(3));
				permitRequest.setPermitPrivilage(resultSet.getString(4));
				permitRequest.setPermitGrantStatus(resultSet.getInt(5));
				permitRequest.setPermitTimeMin(resultSet.getString(6));
				permitRequest.setPermitTimeMax(resultSet.getString(7));
				permitRequest.setUserFName(resultSet.getString(8));
				permitRequest.setUserLName(resultSet.getString(9));
				permitRequest.setDeviceName(resultSet.getString(10));
				permitRequest.setPermitDevice(resultSet.getInt(11));
				permitRequest.setType(resultSet.getInt("things_type"));
				
				list.add(permitRequest);
					
			}
			
			rootPermitRequest.setData(list);
			
			rootPermitRequest.setCodeStatus(200);
			rootPermitRequest.setNameStatus("Success");
			System.out.println(count + "  ewf " );
			
			
			
			
		} catch (SQLException e) {
			// TODO: handle exception
			
			rootPermitRequest.setCodeStatus(500);
			rootPermitRequest.setNameStatus("unsuccess");
		}finally {
			return ResponseEntity.ok().body(rootPermitRequest);
		}
		
	}
	
	@SuppressWarnings("finally")
	@RequestMapping(value = "/permit/{idUser}/{idDevice}" , method = RequestMethod.GET)
	@CrossOrigin
	public ResponseEntity<ResPrivilegeDeviceOfUser> getDevicePermit(@PathVariable("idUser") int idUser , @PathVariable("idDevice") int idDevice){
			
		Connection  con = Database.connectDatabase();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		ResPrivilegeDeviceOfUser deviceOfUser = new ResPrivilegeDeviceOfUser() ;
		try {
			
			String sql = "select permit_privilage , permit_status , gs_id , gs_na , permit_timemin , permit_timemax from permit_user\n" + 
					"inner join  grant_status on gs_id = permit_grant_status\n" + 
					"where permit_user = ?  and permit_device = ? and enabled = 1";
			
			
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, idUser);
			preparedStatement.setInt(2, idDevice);
			resultSet = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(resultSet);
			
			if(count >= 1) {
				resultSet.beforeFirst();
				resultSet.next();
				
				
				deviceOfUser.setPrivilege(resultSet.getString("permit_privilage"));
				
				if(resultSet.getInt("permit_status") == 1) {
					deviceOfUser.setStatusPrivilege("Allow");
				}else {
					deviceOfUser.setStatusPrivilege("Does not Allow");
				}
				deviceOfUser.setGrantID(resultSet.getInt("gs_id"));
				deviceOfUser.setGrantName(resultSet.getString("gs_na"));
				deviceOfUser.setTimeMax(resultSet.getString("permit_timemax"));
				deviceOfUser.setTimeMin(resultSet.getString("permit_timemin"));
				deviceOfUser.setCodeStatus(200);
				deviceOfUser.setNameStatus("Success");
				
			}else {
				deviceOfUser.setCodeStatus(204);
				deviceOfUser.setNameStatus("Unsuccess : No Data");
			}
			
					
		} catch (SQLException e) {

			
			deviceOfUser.setCodeStatus(500);
			deviceOfUser.setNameStatus("Unsuccess : " + e.getErrorCode() + " " + e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(deviceOfUser);
		}
		
	}



	@RequestMapping(value = "/permit/allow" , method = RequestMethod.GET)
	@CrossOrigin
	public ResponseEntity<PermitResRoot> getPermitOk(){
		
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		
		Connection con = Database.connectDatabase();
			
		
		
		PermitResRoot resPermit = new PermitResRoot();
		try {
			String sql = "select permit_device , detail_name , permit_id , permit_user , permit_status , \n" + 
					"permit_privilage , permit_grant_status , permit_timemin ,  permit_timemax , CONCAT (user_detail_fname ,\" \", user_detail_lname ) as fullname , \n" + 
					"things_type from permit_user\n" + 
					"inner join user_detail on user_detail_id  =  permit_user\n" + 
					"inner join devices_detail on permit_device = detail_device\n" + 
					"inner join devices on permit_device = device_id\n" + 
					"inner join things on things_id = device_esp\n" + 
					"where permit_status = 1 and enabled = 1";
			
			preparedStatement = con.prepareStatement(sql);
			resultSet = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(resultSet);
			

			if(count == 0) {
				
				
				
				resPermit.setCodeStatus(204);
				resPermit.setNameStatus("No Data");
				
				System.out.println("Count 0 ");
			}else {
				
				
				
				List<PermitRes> list = new ArrayList<PermitRes>();
				resultSet.beforeFirst();
				while(resultSet.next()) {
					PermitRes permitRes = new  PermitRes();
					permitRes.setNameDevice(resultSet.getString("detail_name"));
					permitRes.setPermitID(resultSet.getInt("permit_id"));
					permitRes.setPermitUser(resultSet.getInt("permit_user"));
					permitRes.setPermitStatus(resultSet.getInt("permit_status"));
					permitRes.setPermitPrivilage(resultSet.getString("permit_privilage"));
					permitRes.setPermitGrant(resultSet.getInt("permit_grant_status"));
					permitRes.setPermitTimeMin(resultSet.getString("permit_timemin"));
					permitRes.setPermitTimeMax(resultSet.getString("permit_timemax"));
					permitRes.setFullname(resultSet.getString("fullname"));
					permitRes.setDevice(resultSet.getInt("permit_device"));
					permitRes.setType(resultSet.getInt("things_type"));
					
				
					list.add(permitRes);
					
					
				}
				resPermit.setCodeStatus(200);
				resPermit.setNameStatus("Success");
				resPermit.setData(list);
				
				
				
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			
			resPermit.setCodeStatus(500);
			resPermit.setNameStatus("ERROR " + e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(resPermit);
		}
		
	}

	
	
	@RequestMapping(value = "/permit/allow/{idUser}" , method = RequestMethod.GET)
	@CrossOrigin
	public ResponseEntity<PermitResRoot> getPermitOkWithUserArea(@PathVariable("idUser") int idUser){
		
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		
		Connection con = Database.connectDatabase();
			
		
		
		PermitResRoot resPermit = new PermitResRoot();
		try {
			String sql = "select permit_device , detail_name , permit_id , permit_user , permit_status , \n" + 
					"permit_privilage , permit_grant_status , permit_timemin ,  permit_timemax , CONCAT (user_detail_fname ,\" \", user_detail_lname ) as fullname , \n" + 
					"things_type from permit_user\n" + 
					"inner join user_detail on user_detail_id  =  permit_user\n" + 
					"inner join devices_detail on permit_device = detail_device\n" + 
					"inner join devices on permit_device = device_id\n" + 
					"inner join things on things_id = device_esp\n" + 
					"inner join detail_area on dear_id = things_area\n" + 
					"inner join area_administrator on area = dear_area\n" + 
					"where permit_status = 1 and enabled = 1 and area_admin_user = ? and permit_user != ?";
			
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, idUser);
			preparedStatement.setInt(2, idUser);
			resultSet = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(resultSet);
			

			if(count == 0) {
				
				
				
				resPermit.setCodeStatus(204);
				resPermit.setNameStatus("No Data");
				
				System.out.println("Count 0 ");
			}else {
				
				
				
				List<PermitRes> list = new ArrayList<PermitRes>();
				resultSet.beforeFirst();
				while(resultSet.next()) {
					PermitRes permitRes = new  PermitRes();
					permitRes.setNameDevice(resultSet.getString("detail_name"));
					permitRes.setPermitID(resultSet.getInt("permit_id"));
					permitRes.setPermitUser(resultSet.getInt("permit_user"));
					permitRes.setPermitStatus(resultSet.getInt("permit_status"));
					permitRes.setPermitPrivilage(resultSet.getString("permit_privilage"));
					permitRes.setPermitGrant(resultSet.getInt("permit_grant_status"));
					permitRes.setPermitTimeMin(resultSet.getString("permit_timemin"));
					permitRes.setPermitTimeMax(resultSet.getString("permit_timemax"));
					permitRes.setFullname(resultSet.getString("fullname"));
					permitRes.setDevice(resultSet.getInt("permit_device"));
					permitRes.setType(resultSet.getInt("things_type"));
					
				
					list.add(permitRes);
					
					
				}
				resPermit.setCodeStatus(200);
				resPermit.setNameStatus("Success");
				resPermit.setData(list);
				
				
				
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			
			resPermit.setCodeStatus(500);
			resPermit.setNameStatus("ERROR " + e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(resPermit);
		}
		
	}
	

	
	// **

	@RequestMapping(value = "/check/{idUser}/{idDevice}" , method = RequestMethod.GET  , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Status> checkPermitReq(@PathVariable("idUser") int idUser , @PathVariable("idDevice") int idDevice){
		
		Connection con =  null;
		PreparedStatement preparedStatement = null;
		
		Status status = new Status();
		try {
			
			String sql ="select permit_id from permit_user\n" + 
					"where permit_user = ? and permit_device  = ? and enabled != 0";
			
			con = Database.connectDatabase();
			preparedStatement =con.prepareStatement(sql);
			preparedStatement.setInt(1, idUser);
			preparedStatement.setInt(2, idDevice);
			
			ResultSet res = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(res);
			
			System.out.println("count " + count);
			if(count == 0) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");
			}
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus("Unsuccess : " + e.getErrorCode() + " " + e.getMessage());
		}finally {
			
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}
}

