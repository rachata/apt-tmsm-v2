package com.tmsm.api.controller.node;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.node.ControlDeviceIO;
import com.tmsm.api.utility.Database;

@RestController
@RequestMapping("/iodevice")
public class IODevice {
	
	
	@RequestMapping(value = "/control/{idUser}/{idDevice}" , method = RequestMethod.GET)
	@CrossOrigin
	
	public ResponseEntity<ControlDeviceIO> checkPrivilege(@PathVariable("idUser") int idUser , @PathVariable("idDevice") int idDevice){
		
		
		
		ControlDeviceIO controlDeviceIO = new ControlDeviceIO();
		
		ResultSet rs = null;
		PreparedStatement preparedStatement = null;
		Connection con = Database.connectDatabase();
		
		try {
			
			String privilegeDevice;
			String device = "select detail_privilege from devices_detail where detail_device = ?";
			
			preparedStatement  = con.prepareStatement(device);
			preparedStatement.setInt(1, idDevice);
			
			rs = preparedStatement.executeQuery();
			rs.next();
			
			privilegeDevice = rs.getString(1);
			
			System.out.println("privilegeDevice " +privilegeDevice );
			
			
			String user = "select * from permit_user where permit_device = ? and permit_user = ?";
			
			preparedStatement  = con.prepareStatement(user);
			preparedStatement.setInt(1, idDevice);
			preparedStatement.setInt(2, idUser);
			
			rs = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(rs);
			
			
			rs.beforeFirst();
			if(count >= 1) {
				rs.next();
				
				char userPrivilege = rs.getString("permit_privilage").charAt(2);
				char devicePrivilege = privilegeDevice.charAt(2);
				
				
				if(userPrivilege == '1' && devicePrivilege == '1') {
					controlDeviceIO.setCodeStatus(200);
					controlDeviceIO.setNameStatus("Success");
					controlDeviceIO.setStatusPrivilege(true);
					
				}else if(userPrivilege == '0' && devicePrivilege == '1') {
					
					controlDeviceIO.setCodeStatus(201);
					controlDeviceIO.setNameStatus("No Privilege (User)");
					controlDeviceIO.setStatusPrivilege(false);
					
					// 	ไม่มีสิทธิ์ ร้องขอสิทธิ์
					
				}else if(userPrivilege == '1' && devicePrivilege == '0') {
					
					
					controlDeviceIO.setCodeStatus(201);
					controlDeviceIO.setNameStatus("No Privilege (Device)");
					controlDeviceIO.setStatusPrivilege(false);
					// 	อุปกรณ์ไม่มีสิทธิ์ควบคุมให้ user
					
				}else if(userPrivilege == '0' && devicePrivilege == '0') {
					
					controlDeviceIO.setCodeStatus(201);
					controlDeviceIO.setNameStatus("No Privilege (User)");
					controlDeviceIO.setStatusPrivilege(false);
					// 	ไม่มีสิทธิ์ 
					
				}
				
				System.out.println("User " + userPrivilege + " Device "  + devicePrivilege);
				
			}else {
				
				controlDeviceIO.setCodeStatus(204);
				controlDeviceIO.setNameStatus("No Privilege Record");
				controlDeviceIO.setStatusPrivilege(false);
				System.out.println("privilegeDevice  no record" );
				
				
			}
			
			
		} catch (SQLException e) {
			
			controlDeviceIO.setCodeStatus(500);
			controlDeviceIO.setNameStatus("ERROR " + e.getMessage());
			
		}finally {
			return ResponseEntity.ok().body(controlDeviceIO);
		}
		
		
		
		
	}

}
