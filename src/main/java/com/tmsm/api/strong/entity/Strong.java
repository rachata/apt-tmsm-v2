package com.tmsm.api.strong.entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.utility.Database;
import com.tmsm.model.strong.DevicesPriority;
import com.tmsm.model.strong.DevicesStatus;
import com.tmsm.model.strong.ListPriority;
import com.tmsm.model.strong.ListStatus;
import com.tmsm.model.strong.ListType;
import com.tmsm.model.strong.ThingsType;

@RestController
@RequestMapping("/strong")
public class Strong {

	@RequestMapping(value = "/thingtype",method = RequestMethod.GET)
	public ResponseEntity<ThingsType> Types() {		
		
		ThingsType type = new ThingsType();
		
		HttpHeaders responseHeaders = new HttpHeaders();
	    responseHeaders.set("Access-Control-Allow-Origin", "*");
	    
		ResultSet result = null;
		PreparedStatement preparedStmt = null;
		Connection con = Database.connectDatabase();
		
		String sql = "SELECT * FROM things_type";
		
		try {
			
			preparedStmt = con.prepareStatement(sql);
			result = preparedStmt.executeQuery();			
			
			List<ListType> list = new ArrayList<ListType>();
			
			while(result.next()) {
				
				ListType listType = new ListType();
				
				listType.setTypeID(result.getInt(1));
				listType.setTypeName(result.getString(2));
				
				list.add(listType);
				
			}			
		
			type.setThingType(list);
			type.setCodeStatus(200);
			type.setNameStatus("Success");
			
			Database.closeConnection(con);
			return ResponseEntity.ok().headers(responseHeaders).body(type);
			
		}catch (SQLException ex) {
			type.setCodeStatus(200);
			type.setNameStatus(ex.getMessage());
			
			Database.closeConnection(con);
			return ResponseEntity.ok().headers(responseHeaders).body(type);
		}	
	}
	
	
	@RequestMapping(value = "/priority",method = RequestMethod.GET)
	public ResponseEntity<ListPriority> GetPriority() {
		
		ListPriority priority = new ListPriority();
		
		HttpHeaders responseHeaders = new HttpHeaders();
	    responseHeaders.set("Access-Control-Allow-Origin", "*");
	    
		ResultSet result = null;
		PreparedStatement preparedStmt = null;
		Connection con = Database.connectDatabase();
		
		String sql = "SELECT * FROM devices_priority";
		
		try {
			
			preparedStmt = con.prepareStatement(sql);
			result = preparedStmt.executeQuery();	
			
			List<DevicesPriority> devices = new ArrayList<DevicesPriority>();
			
			while(result.next()) {
				
				DevicesPriority objDevice = new DevicesPriority();
				objDevice.setPriorityID(result.getInt(1));
				objDevice.setPriorityName(result.getString(2));
				
				devices.add(objDevice);
			}
			
			priority.setCodeStatus(200);
			priority.setNameStatus("Success");
			priority.setPriority(devices);
			
			Database.closeConnection(con);
			return ResponseEntity.ok().headers(responseHeaders).body(priority);
			
		} catch (SQLException ex) {
			priority.setCodeStatus(400);
			priority.setNameStatus(ex.getMessage());
			Database.closeConnection(con);
			return ResponseEntity.ok().headers(responseHeaders).body(priority);
		}
		
	}
	
	@RequestMapping(value = "/devicestatus",method = RequestMethod.GET)
	public ResponseEntity<ListStatus> getStatus(){
		ListStatus listStatus = new ListStatus();
		
		HttpHeaders responseHeaders = new HttpHeaders();
	    responseHeaders.set("Access-Control-Allow-Origin", "*");
	    
		ResultSet result = null;
		PreparedStatement preparedStmt = null;
		Connection con = Database.connectDatabase();
		
		String sql = "SELECT * FROM devices_status";
		
		try {
			preparedStmt = con.prepareStatement(sql);
			result = preparedStmt.executeQuery();	
			
			List<DevicesStatus> devicesStatus = new ArrayList<DevicesStatus>();
			
			while(result.next()) {
				
				DevicesStatus objDevice = new DevicesStatus();
				
				objDevice.setStatusID(result.getInt(1));
				objDevice.setStatusName(result.getString(2));
				
				devicesStatus.add(objDevice);
			}
			
			listStatus.setCodeStatus(200);
			listStatus.setNameStatus("Success");
			listStatus.setStatus(devicesStatus);
			
			Database.closeConnection(con);
			return ResponseEntity.ok().headers(responseHeaders).body(listStatus);
			
		} catch (SQLException ex) {
			
			listStatus.setCodeStatus(400);
			listStatus.setNameStatus(ex.getMessage());
			
			Database.closeConnection(con);
			return ResponseEntity.ok().headers(responseHeaders).body(listStatus);
		}
		
	}
	
}
