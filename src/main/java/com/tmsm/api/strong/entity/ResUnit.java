package com.tmsm.api.strong.entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;

import com.tmsm.api.model.Status;
import com.tmsm.api.utility.Database;
import com.tmsm.model.strong.ListUnits;
import com.tmsm.model.strong.Unit;

@RequestMapping("/unit")
public class ResUnit {

		@CrossOrigin
		@RequestMapping(method = RequestMethod.GET)
		public ResponseEntity<ListUnits> getunit(){
			
			ListUnits unit = new ListUnits();
			
			ResultSet result = null;
			Connection con = Database.connectDatabase();
			PreparedStatement preparedStmt = null;
			
			try {
				String sql = "select * from unit";
				
				preparedStmt = con.prepareStatement(sql);
				result = preparedStmt.executeQuery();
				
				List<Unit> listUnit = new ArrayList<Unit>();
				
				while(result.next()) {
					Unit resUnit = new Unit();
					
					resUnit.setUnitId(result.getInt(1));
					resUnit.setUnitName(result.getString(2));
					
					listUnit.add(resUnit);
				}
				
				unit.setCodeStatus(200);
				unit.setNameStatus("Success");
				unit.setUnit(listUnit);
				
				
			} catch (SQLException e) {
				unit.setCodeStatus(400);
				unit.setNameStatus(e.getMessage());
			}finally {
				Database.closeConnection(con);
				return ResponseEntity.ok().body(unit);
			}			
		}
		
		
		@CrossOrigin
		@RequestMapping(method = RequestMethod.POST)
		public ResponseEntity<Status> creatUnit(@RequestBody Unit unit){
			
			Status status = new Status();
			PreparedStatement preparedStmt = null;
			Connection con = Database.connectDatabase();
			
			try {
				
				String sql = "insert into unit (unit_name) values (?)";
				
				preparedStmt = con.prepareStatement(sql);
				preparedStmt.setString(1, unit.getUnitName());
				
				boolean statusInsert = preparedStmt.execute();
				
				if(!statusInsert) {
					status.setCodeStatus(200);
					status.setNameStatus("Create Success");
				}else {
					status.setCodeStatus(400);
					status.setNameStatus("Create unsuccess");
				}
				
			} catch (SQLException e) {
				status.setCodeStatus(500);
				status.setNameStatus(e.getMessage());
			} finally {
				Database.closeConnection(con);
				return ResponseEntity.ok().body(status);
			}			
		}
		
		
		@CrossOrigin
		@RequestMapping(method = RequestMethod.PUT)
		public ResponseEntity<Status> updateUnit(@RequestBody Unit unit){
			
			Status status = new Status();
			PreparedStatement preparedStmt = null;
			Connection con = Database.connectDatabase();
			
			try {
				
				String sql = "update unit set unit_name = ? where unit_id = ?";
				
				preparedStmt = con.prepareStatement(sql);
				preparedStmt.setString(1, unit.getUnitName());
				preparedStmt.setInt(2, unit.getUnitId());
				
				boolean statusUpdate = preparedStmt.execute();
				
				if(!statusUpdate) {
					status.setCodeStatus(200);
					status.setNameStatus("Success");
				}
				
			} catch (SQLException e) {
				status.setCodeStatus(400);
				status.setNameStatus(e.getMessage());
			}finally {
				Database.closeConnection(con);
				return ResponseEntity.ok().body(status);
			}
			
		}
		
		
		@CrossOrigin
		@RequestMapping(value = "/{id}",method = RequestMethod.DELETE)		
		public ResponseEntity<Status> deleteUnit(@RequestPart(value="id") Integer id){
			
			Status status = new Status();
			PreparedStatement preparedStmt = null;
			Connection con = Database.connectDatabase();
			
			try {
				String sql = "delete from unit where unit_id = ?";
				
				preparedStmt = con.prepareStatement(sql);
				preparedStmt.setInt(1, id);
				
			boolean statusDelete = preparedStmt.execute();
			
			if(!statusDelete) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");
			}
				
			} catch (SQLException e) {
				status.setCodeStatus(500);
				status.setNameStatus(e.getMessage());
			}finally {
				Database.closeConnection(con);
				return ResponseEntity.ok().body(status);
			}
 		}
}
