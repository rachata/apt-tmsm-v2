package com.tmsm.api;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin()
public class HelloWorldController {

	@RequestMapping({ "/hello" })
	public String hello() {
		return "Hello World";
	}

	
	@RequestMapping({ "/hellos" })
	public String hellos() {
		return "Hello Worlds";
	}

	
	@RequestMapping({ "/permit" })
	public String permit() {
		return "Hello World";
	}
	
}
